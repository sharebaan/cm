<?php

use App\Vechi as V;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
 
class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function da($prod){
				if($prod == 'Apple'){
					return 1;
				}
				else if($prod == 'Samsung'){
					return 2;
				}
				else if($prod == 'Sony'){
					return 3;
				}
				else if($prod == 'LG'){
					return 4;
				}
				else if($prod == 'HTC'){
					return 5;
				}
				else if($prod == 'Nokia'){
					return 6;
				}
				else if($prod == 'BlackBerry' || $prod == 'Blackberry'){
					return 7;
				}
				else if($prod == 'Motorola'){
					return 8;
				}
				else if($prod == 'Huawei'){
					return 9;
				}
				else if($prod == 'ZTE'){
					return 10;
				}
				else if($prod == 'Sony Ericsson'){
					return 11;
				}
				else if($prod == 'Alcatel'){
					return 13;
				}
				else if($prod == 'Lenovo'){
					return 15;
				}
				else if($prod == 'Acer'){
					return 17;
				}
				else if($prod == 'Asus'){
					return 18;
				}
				else if($prod == 'Vodafone'){
					return 21;
				}
				else if($prod == 'Orange'){
					return 22;
				}
				else if($prod == 'Allview'){
					return 23;
				}
				else if($prod == 'Cosmote'){
					return 24;
				}
				else if($prod == 'Xiaomi'){
					return 25;
				}
				else{
					return null;
				}
			}

	public function poza($folder,$cod){
		foreach($folder as $f){
			
				if(substr($f,0,9) == $cod){
					return $f;
				}else{
					return '';
				}
				
		}
	}




	public function run()
	{
		$faker = Faker\Factory::create();
		Model::unguard();

		// $this->call('UserTableSeeder');

		App\CatBaza::create([
				'nume'=>'Telefoane mobile GSM'
			]);

		App\CatBaza::create([
				'nume'=>'Tablete'
			]);

		App\CatBaza::create([
				'nume'=>'PC / Laptop'
			]);

		App\CatBaza::create([
				'nume'=>'Service'
			]);

		App\CatBaza::create([
				'nume'=>'Diverse'
			]);

		App\SubcatBaza1::create([
				'nume' => 'Componente GSM',
				'cat_baza_id' => 1
			]);
		
		App\SubcatBaza1::create([
				'nume' => 'Accesorii GSM',
				'cat_baza_id' => 1
			]);

		App\SubcatBaza1::create([
				'nume' => 'Accesorii GSM originale',
				'cat_baza_id' => 1
			]);

		App\SubcatBaza1::create([
				'nume' => 'Telefoane / Terminale GSM',
				'cat_baza_id' => 1
			]);

		App\SubcatBaza1::create([
				'nume' => 'Componente tableta',
				'cat_baza_id' => 2
			]);
		
		App\SubcatBaza1::create([
				'nume' => 'Accesorii tableta',
				'cat_baza_id' => 2
			]);

		App\SubcatBaza1::create([
				'nume' => 'Tablete',
				'cat_baza_id' => 2
			]);

		App\SubcatBaza1::create([
				'nume' => 'Componente PC / Laptop',
				'cat_baza_id' => 3
			]);

		App\SubcatBaza1::create([
				'nume' => 'Accesorii PC / Laptop',
				'cat_baza_id' => 3
			]);

		App\SubcatBaza1::create([
				'nume' => 'Periferice PC / Laptop',
				'cat_baza_id' => 3
			]);

		App\SubcatBaza1::create([
				'nume' => 'Desktop / Laptop',
				'cat_baza_id' => 3
			]);

		App\SubcatBaza1::create([
				'nume' => 'Aparatura service',
				'cat_baza_id' => 4
			]);

		App\SubcatBaza1::create([
				'nume' => 'Accesorii si piese de schimb aparatura',
				'cat_baza_id' => 4
			]);

		App\SubcatBaza1::create([
				'nume' => 'Interfete decodari si rescriere software',
				'cat_baza_id' => 4
			]);

		App\SubcatBaza1::create([
				'nume' => 'Loguri si activari software',
				'cat_baza_id' => 4
			]);

		App\SubcatBaza1::create([
				'nume' => 'Diverse service',
				'cat_baza_id' => 4
			]);

		App\SubcatBaza1::create([
				'nume' => 'Decodari online',
				'cat_baza_id' => 4
			]);

		App\SubcatBaza1::create([
				'nume' => 'Diverse',
				'cat_baza_id' => 5
			]);

		App\Subcat2Copil1::create([
				'nume' => 'Componente',
				'subcat_baza_1_id' => 1 
			]);

		App\Subcat2Copil1::create([
				'nume' => 'Componente carcase',
				'subcat_baza_1_id' => 1 
			]);

		App\Subcat2Copil1::create([
				'nume' => 'Microcontacte diverse',
				'subcat_baza_1_id' => 1 
			]);

		App\Subcat2Copil1::create([
				'nume' => 'Butoane diverse',
				'subcat_baza_1_id' => 1 
			]);

		App\Subcat2Copil1::create([
				'nume' => 'Conectori',
				'subcat_baza_1_id' => 1 
			]);

		App\Subcat2Copil1::create([
				'nume' => 'Circuite Integrate',
				'subcat_baza_1_id' => 1 
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Display / LCD',
				'subcat2_copil_1_id' => 1
			]);
	
		App\Subcat3Copil2::create([
				'nume' => 'Touchscreen',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Benzi / Cabluri flexibile',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Cititoare SIM',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Geam',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Buzere/Sonerii',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Camere',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Microfoane',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Mufe Incarcare',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Joystick',
				'subcat2_copil_1_id' => 1
			]);
		App\Subcat3Copil2::create([
				'nume' => 'Diverse',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Casti',
				'subcat2_copil_1_id' => 1
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Nesortate',
				'subcat2_copil_1_id' => 1
			]);
  
		App\Subcat3Copil2::create([
				'nume' => 'Rame',
				'subcat2_copil_1_id' => 2
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Mijloace',
				'subcat2_copil_1_id' => 2
			]);


		App\Subcat3Copil2::create([
				'nume' => 'Capace camera',
				'subcat2_copil_1_id' => 2
			]);
		App\Subcat3Copil2::create([
				'nume' => 'Capace baterie',
				'subcat2_copil_1_id' => 2
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Microcontacte on/off',
				'subcat2_copil_1_id' => 3
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Microcontacte volum',
				'subcat2_copil_1_id' => 3
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Microcontacte meniu',
				'subcat2_copil_1_id' => 3
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Microcontacte diverse',
				'subcat2_copil_1_id' => 3
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Butoane on/off',
				'subcat2_copil_1_id' => 4
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Butoane volum',
				'subcat2_copil_1_id' => 4
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Butoane meniu',
				'subcat2_copil_1_id' => 4
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Butoane diverse',
				'subcat2_copil_1_id' => 4
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Conectori',
				'subcat2_copil_1_id' => 5
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Flash',
				'subcat2_copil_1_id' => 6
			]);

		App\Subcat3Copil2::create([
				'nume' => 'CPU',
				'subcat2_copil_1_id' => 6
			]);

		App\Subcat3Copil2::create([
				'nume' => 'Power IC',
				'subcat2_copil_1_id' => 6
			]);

		App\Subcat4Copil3::create([
				'nume' => 'Benzi Alimentare',
				'subcat3_copil_2_id' => 3
			]);

		App\Subcat4Copil3::create([
				'nume' => 'Benzi Flexibile',
				'subcat3_copil_2_id' => 3 
			]);

		App\Subcat4Copil3::create([
				'nume' => 'Benzi SIM / Card',
				'subcat3_copil_2_id' => 3 
			]);

		App\Subcat4Copil3::create([
				'nume' => 'Benzi Audio',
				'subcat3_copil_2_id' => 3
			]);

		App\Subcat4Copil3::create([
				'nume' => 'Camera principala / spate',
				'subcat3_copil_2_id' => 7 
			]);

		App\Subcat4Copil3::create([
				'nume' => 'camera frontala / fata',
				'subcat3_copil_2_id' => 7
			]);

		App\Branduri::create([
				'nume' => 'Apple'
			]);

		App\Branduri::create([
				'nume' => 'Samsung'
			]);

		App\Branduri::create([
				'nume' => 'Sony'
			]);

		App\Branduri::create([
				'nume' => 'LG'
			]);

		App\Branduri::create([
				'nume' => 'HTC'
			]);

		App\Branduri::create([
				'nume' => 'Nokia'
			]);

		App\Branduri::create([
				'nume' => 'Blackberry'
			]);

		App\Branduri::create([
				'nume' => 'Motorola'
			]);

		App\Branduri::create([
				'nume' => 'Huawei'
			]);

		App\Branduri::create([
				'nume' => 'ZTE'
			]);

		App\Branduri::create([
				'nume' => 'Sony Ericsson'
			]);

		App\Branduri::create([
				'nume' => 'Dell-Freebrand'
			]);

		App\Branduri::create([
				'nume' => 'Alcatel'
			]);

		App\Branduri::create([
				'nume' => 'Amazon-Freebrand'
			]);

		App\Branduri::create([
				'nume' => 'Lenovo'
			]);

		App\Branduri::create([
				'nume' => 'Google-Freebrand'
			]);

		App\Branduri::create([
				'nume' => 'Acer'
			]);

		App\Branduri::create([
				'nume' => 'Asus'
			]);

		App\Branduri::create([
				'nume' => 'Philips-Freebrand'
			]);

		App\Branduri::create([
				'nume' => 'Siemens-Freebrand'
			]);

		App\Branduri::create([
				'nume' => 'Vodafone'
			]);

		App\Branduri::create([
				'nume' => 'Orange'
			]);

		App\Branduri::create([
				'nume' => 'Allview'
			]);

		App\Branduri::create([
				'nume' => 'Cosmote'
			]);

		App\Branduri::create([
				'nume' => 'Xiaomi'
			]);




		App\Producator::create([
				'nume' => 'OEM'
			]);

		App\Producator::create([
				'nume' => 'Apple'
			]);

		App\Producator::create([
				'nume' => 'Samsung'
			]);

		App\Producator::create([
				'nume' => 'Sony'
			]);

		App\Producator::create([
				'nume' => 'LG'
			]);

		App\Producator::create([
				'nume' => 'HTC'
			]);

		App\Producator::create([
				'nume' => 'Nokia'
			]);

		App\Producator::create([
				'nume' => 'Blackberry'
			]);

		App\Producator::create([
				'nume' => 'Motorola'
			]);

		App\Producator::create([
				'nume' => 'Huawei'
			]);

		App\Producator::create([
				'nume' => 'ZTE'
			]);

		App\Producator::create([
				'nume' => 'Sony Ericsson'
			]);

		App\Producator::create([
				'nume' => 'Dell-Freebrand'
			]);

		App\Producator::create([
				'nume' => 'Alcatel'
			]);

		App\Producator::create([
				'nume' => 'Amazon-Freebrand'
			]);

		App\Producator::create([
				'nume' => 'Lenovo'
			]);

		App\Producator::create([
				'nume' => 'Google-Freebrand'
			]);

		App\Producator::create([
				'nume' => 'Acer'
			]);

		App\Producator::create([
				'nume' => 'Asus'
			]);

		App\Producator::create([
				'nume' => 'Philips-Freebrand'
			]);

		App\Producator::create([
				'nume' => 'Siemens-Freebrand'
			]);

		App\Producator::create([
				'nume' => 'Vodafone'
			]);

		App\Producator::create([
				'nume' => 'Orange'
			]);

		App\Producator::create([
				'nume' => 'Allview'
			]);

		App\Producator::create([
				'nume' => 'Cosmote'
			]);

		App\Producator::create([
				'nume' => 'Xiaomi'
			]);

/*		App\Membrii::create([
					'status' => 1,
					'tipCont'=> 'PJ',
					'email' => 'sefulataste@gmail.com',
					'password'=> Hash::make('sefdesef'),
					'nume' => 'membru',
					'prenume' => 'prenume',
					'nume_companie' => 'firma',
					'adresa_fac' => 'adresa factura',
					'localitate_fac' => 'localitate factura',
					'judet_sector_fac' => 'judet/sector factura',
					'adresa_liv' => 'adresa livrare',
					'localitate_liv' => 'localitate livrare',
					'judet_sector_liv' => 'sector/judet livrare',
					'CUI'  => '6413161531',
					'nr_reg'  => 'ro654351651',
					'adresa_sediu' => 'adresa sediu',
					'localitate_sediu' => 'localitate sediu',
					'judet_sector_sediu' => 'judet/sector sediu',
					'banca' => 'banca638244341',
					'cont_IBAN' => 'a6s4654faad54f',
					'mentiuni' => 'Pellentesque viverra felis dui, quis malesuada velit laoreet sed.
					 Proin vitae ultrices arcu. Fusce sit amet augue diam.
					  Nulla laoreet interdum ante non consectetur. Vestibulum in pellentesque nunc.
					   Nunc imperdiet diam eget erat tempor, ut consectetur nunc viverra.
					 Sed et dapibus sem. Mauris lobortis vestibulum dolor ac egestas.',
					'KAM_com' => 'kam',
					'admin' => 1,
					'KAM_liv' => 'kam liv',
					'detalii_plata' => 'cash',
					'detalii_livrare' => 'predare',
					'detalii_transport' => 'curier',
					'recomandari' => 'de la gigi'
				]);
*/
		$vechi = V::all();
		
		for($x=1;$x<count($vechi);$x++){

			App\Produse::create([
					'denumire_produs' => $vechi[$x]->title,
					'cat_baza_id' => 1,
					'brand_id' => $this->da($vechi[$x]->producator),
					'producator_id'=> 1,
					'cod_produs'=> $vechi[$x]->cod,
					'poza'=> '',				
					'discount'=> 1,
					'stoc'=> $vechi[$x]->stoc,
					'culoare'=>'',
					'dimensiune'=>'',
					'tip_ambalaj'=>'',
					'atentie'=>'',
					'detalii'=>$vechi[$x]->text,
					'alerta_stoc' => 2,
					'pret'=>number_format($vechi[$x]->price,2,'.',','),
					'total_stoc'=>$vechi[$x]->price * $vechi[$x]->stoc,
					'noutati'=>0,
					'promotii'=>0,
					'lichidari_stoc'=>0,
					'pozitie_raft'=>''
				]);

					

		/*	for($e=1;$e<6;$e++){
				App\Poze::create([
						'poza' => $faker->imageUrl($width = 1000, $height = 1000, 'technics'),
						'produs_id' => $x
					]);	
			}*/
			

			App\PivSB1::create([
				'subcat_baza_1_id' => 1,
				'prod_id' => $x
			]);

			



			if($vechi[$x]->cat_id == 110){
				App\PivS2C1::create([
					'subcat2_copil_1_id' => 1,
					'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 4,
					'prod_id' => $x 
					]);
			}
			else if($vechi[$x]->cat_id == 111){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 6,
					'prod_id' => $x 
					]);
			}
			else if($vechi[$x]->cat_id == 115){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 7,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 116  ){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 11,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 194){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 13,
					'prod_id' => $x 
					]);	
			}
			else if($vechi[$x]->cat_id == 125 || $vechi[$x]->cat_id == 127){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 2,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 128){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 1,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 132){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 5,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 136){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 3,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 138){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 3,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 18,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 139){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 3,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 19,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 140){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 3,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 20,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 151){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 5,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 26,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 193){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 12,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 59){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 2,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 15,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 60){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 2,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 17,
					'prod_id' => $x 
					]); 
			}
			else if($vechi[$x]->cat_id == 62){
				App\PivS2C1::create([
				'subcat2_copil_1_id' => 2,
				'prod_id' => $x
				]);

				App\PivS3C2::create([
					'subcat3_copil_2_id' => 14,
					'prod_id' => $x 
					]); 
			}

		
			if($vechi[$x]->stoc == 0){
				App\Disponibil::create([
					'produs_id'=> $x,
					'in_stoc'=>0,
					'stoc_redus'=>0,
					'produs_indisponibil'=>0,
					'in_curand'=>1
				]);				
			}
			else if($vechi[$x]->stoc <= 2){
				App\Disponibil::create([
					'produs_id'=> $x,
					'in_stoc'=>0,
					'stoc_redus'=>1,
					'produs_indisponibil'=>0,
					'in_curand'=>0
				]);	
			}
			else if($vechi[$x]->stoc > 2){
				App\Disponibil::create([
					'produs_id'=> $x,
					'in_stoc'=>1,
					'stoc_redus'=>0,
					'produs_indisponibil'=>0,
					'in_curand'=>0
				]);	
			}
		}



/*			function da($prod){
				if($prod == 'Apple'){
					return 1;
				}
				else if($prod == 'Samsung'){
					return 2;
				}
				else if($prod == 'Sony'){
					return 3;
				}
				else if($prod == 'LG'){
					return 4;
				}
				else if($prod == 'HTC'){
					return 5;
				}
				else if($prod == 'Nokia'){
					return 6;
				}
				else if($prod == 'BlackBerry' || $prod == 'Blackberry'){
					return 7;
				}
				else if($prod == 'Motorola'){
					return 8;
				}
				else if($prod == 'Huawei'){
					return 9;
				}
				else if($prod == 'ZTE'){
					return 10;
				}
				else if($prod == 'Sony Ericsson'){
					return 11;
				}
				else if($prod == 'Alcatel'){
					return 13;
				}
				else if($prod == 'Lenovo'){
					return 15;
				}
				else if($prod == 'Acer'){
					return 17;
				}
				else if($prod == 'Asus'){
					return 18;
				}
				else if($prod == 'Vodafone'){
					return 21;
				}
				else if($prod == 'Orange'){
					return 22;
				}
				else if($prod == 'Allview'){
					return 23;
				}
				else if($prod == 'Cosmote'){
					return 24;
				}
				else if($prod == 'Xiaomi'){
					return 25;
				}
				else{
					return null;
				}
			}

foreach($products1 as $p){


	$prod = new App\Produse();
	$prod->denumire_produs = $p['title'];
	$prod->cod_produs = $p['cod'];
	$prod->stoc = $p['stoc'];
	$prod->pret = $p['price'];
	$prod->brand_id = da($p['producator']);
	$prod->descriere = $p['text'];
	$prod->cat_baza_id = 1;
	$prod->total_stoc = $prod->pret * $prod->stoc;

	$prod->save();

	App\PivSB1::create([
		'subcat_baza_1_id' => 1,
		'prod_id' => $prod->id
		]);

	App\PivS2C1::create([
		'subcat2_copil_1_id' => 1,
		'prod_id' => $prod->id
		]);



	if($p['cat_id'] == 110){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 4,
			'prod_id' => $prod->id 
			]);
	}
	else if($p['cat_id'] == 111){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 6,
			'prod_id' => $prod->id 
			]);
	}
	else if($p['cat_id'] == 115){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 7,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 116 || $p['cat_id'] == 194){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 11,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 125 || $p['cat_id'] == 127){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 02,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 128){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 1,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 132){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 5,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 136){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 3,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 138){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 16,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 139){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 17,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 140){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 18,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 151){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 24,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 193){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 28,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 59){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 13,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 60){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 15,
			'prod_id' => $prod->id 
			]); 
	}
	else if($p['cat_id'] == 62){
		App\PivS3C2::create([
			'subcat3_copil_2_id' => 12,
			'prod_id' => $prod->id 
			]); 
	}
}


});	

/*		
*/

//===========================================================

/*		for($x=1;$x<=2;$x++){
			App\Produse::create([
					'denumire_produs' => $faker->name,
					'cat_baza_id' => $faker->numberBetween($min = 1, $max = 5),
					'brand_id' => $faker->numberBetween($min = 1, $max = 25),
					'producator_id'=> $faker->numberBetween($min = 1, $max = 26),
					'cod_produs'=> rand(100000000,999999999),
					'poza'=> $faker->imageUrl($width = 500, $height = 500, 'technics'),				
					'discount'=> $faker->numberBetween($min = 5, $max = 30),
					'stoc'=> 100,
					'culoare'=>'culoare',
					'dimensiune'=>'dimensiune',
					'tip_ambalaj'=>'bulk',
					'atentie'=>'text atentie',
					'detalii'=>'text detalii',
					'alerta_stoc' => 10,
					'pret'=>number_format($faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 15000),2,'.',','),
					'total_stoc'=>$faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 15000) * 100,
					'intrari'=>30,
					'iesiri'=>50,
					'retur'=>2,
					'noutati'=>1,
					'promotii'=>1,
					'vanzari'=>130,
					'pozitie_raft'=>'A/3/25'
				]);

			

			for($e=1;$e<6;$e++){
				App\Poze::create([
						'poza' => $faker->imageUrl($width = 1000, $height = 1000, 'technics'),
						'produs_id' => $x
					]);	
			}
			

			App\PivSB1::create([
					'subcat_baza_1_id' => $faker->numberBetween($min = 1, $max = 18),
					'prod_id' => $x
				]);

			App\PivS2C1::create([
					'subcat2_copil_1_id' => $faker->numberBetween($min = 1, $max = 6),
					'prod_id' => $x
				]);

			App\PivS3C2::create([
					'subcat3_copil_2_id' => $faker->numberBetween($min = 1, $max = 25),
					'prod_id' => $x
				]);

			App\PivS4C3::create([
					'subcat4_copil_3_id' => $faker->numberBetween($min = 1, $max = 6),
					'prod_id' => $x
				]);

			App\Disponibil::create([
					'produs_id'=> $x,
					'in_stoc'=>1,
					'stoc_redus'=>0,
					'produs_indisponibil'=>0,
					'in_curand'=>0
				]);
			
		}


/*
		for($x=0;$x<=7;$x++){
			App\Membrii::create([
					'status' => 1,
					'tipCont'=> 'PJ',
					'email' => 'membru'.$x.'@test.com',
					'nume' => 'membru'.$x,
					'prenume' => 'prenume'.$x,
					'nume_companie' => 'firma'.$x.'srl',
					'adresa_fac' => 'adresa factura'.$x,
					'localitate_fac' => 'localitate factura'.$x,
					'judet_sector_fac' => 'judet/sector factura'.$x,
					'adresa_liv' => 'adresa livrare'.$x,
					'localitate_liv' => 'localitate livrare'.$x,
					'judet_sector_liv' => 'sector/judet livrare'.$x,
					'CUI'  => '6413161531',
					'nr_reg'  => 'ro654351651',
					'adresa_sediu' => 'adresa sediu'.$x,
					'localitate_sediu' => 'localitate sediu'.$x,
					'judet_sector_sediu' => 'judet/sector sediu'.$x,
					'banca' => 'banca638244341',
					'cont_IBAN' => 'a6s4654faad54f',
					'mentiuni' => 'Pellentesque viverra felis dui, quis malesuada velit laoreet sed.
					 Proin vitae ultrices arcu. Fusce sit amet augue diam.
					  Nulla laoreet interdum ante non consectetur. Vestibulum in pellentesque nunc.
					   Nunc imperdiet diam eget erat tempor, ut consectetur nunc viverra.
					 Sed et dapibus sem. Mauris lobortis vestibulum dolor ac egestas.',
					'KAM_com' => 'kam'.$x,
					'KAM_liv' => 'kam liv'.$x,
					'detalii_plata' => 'cash',
					'detalii_livrare' => 'predare',
					'detalii_transport' => 'curier',
					'recomandari' => 'de la gigi'
				]);
		}
/*
		for($x=1;$x<=7;$x++){
			App\Comenzi::create([
				'comanda_id' => $x,
				'membru_id' => $x,
				'produs_id' => $x,
				'nume_produs' => 'asdf',
				'cantitate' => 20,
				'total' => 1000,
				'completa' => 1,
				'metoda_plata' => 'ramburs',
				'metoda_livrare' => 'curier',
				'mod_cumparare' => 'PJ',
				'tip_document' => 'factura',
				'KAM_livrare' => 'gigi'  
			]);	
		}*/		
	}

}
