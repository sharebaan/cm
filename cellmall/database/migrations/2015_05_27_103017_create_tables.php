<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	/*	Schema::create('test',function($table){
			$table->increments('id');
			$table->decimal('da',10,2);
		});*/		

/*		Schema::create('cat_baza',function($table){
			$table->increments('id');
			$table->string('nume');

			$table->timestamps();
		});

		Schema::create("subcat_baza_1",function($table){
			$table->increments('id');
			$table->string('nume');

			$table->integer('cat_baza_id')->unsigned();
			$table->foreign('cat_baza_id')->references('id')->on('cat_baza');

			$table->timestamps();
		});

		Schema::create('subcat2_copil_1',function($table){
			$table->increments('id');
			$table->string('nume');

			$table->integer('subcat_baza_1_id')->unsigned();
			$table->foreign('subcat_baza_1_id')->references('id')->on('subcat_baza_1');

			$table->timestamps();
		});

		Schema::create('subcat3_copil_2',function($table){
			$table->increments('id');
			$table->string('nume');

			$table->integer('subcat2_copil_1_id')->unsigned();
			$table->foreign('subcat2_copil_1_id')->references('id')->on('subcat2_copil_1');

			$table->timestamps();
		});

		Schema::create('subcat4_copil_3',function($table){
			$table->increments('id');
			$table->string('nume');

			$table->integer('subcat3_copil_2_id')->unsigned();
			$table->foreign('subcat3_copil_2_id')->references('id')->on('subcat3_copil_2');

			$table->timestamps();
		});

		Schema::create('branduri',function($table){
			$table->increments('id');
			$table->string('nume');
			$table->string('poza');

			$table->timestamps();
		});
		

		Schema::create('producatori',function($table){
			$table->increments('id');

			$table->string('nume');
			$table->string('poza');

			$table->timestamps();
		});

		Schema::create('produse',function($table){
			$table->increments('id');

			$table->integer('cat_baza_id')->unsigned();
			$table->foreign('cat_baza_id')->references('id')->on('cat_baza');

			$table->integer('brand_id')->unsigned();
			$table->foreign('brand_id')->references('id')->on('branduri');
		
			$table->integer('producator_id')->unsigned();
			$table->foreign('producator_id')->references('id')->on('producatori');
			$table->string('culoare');
			$table->string('dimensiune');
			$table->string('tip_ambalaj');

			$table->text('atentie');

			$table->longText('detalii');
			$table->string('cod_produs');
			$table->string('denumire_produs');
			$table->string('poza');
			$table->decimal('pret',10,2);
			$table->string('discount',5,2);
			$table->integer('stoc');
			$table->decimal('total_stoc',10,2);
			
			$table->boolean('promotii')->default(0);
			$table->boolean('noutati')->default(0);
			$table->integer('alerta_stoc');
			$table->boolean('lichidari_stoc')->default(0);
			$table->integer('vizitari');
			
			$table->string('pozitie_raft');

			$table->timestamps();			
		});
		
		Schema::create('status_stoc',function($table){
			$table->increments('id');

			$table->integer('prod_id')->unsigned();
			$table->foreign('prod_id')->references('id')->on('produse');

			$table->integer('intrari');
			$table->integer('iesiri');
			$table->integer('retur');
			$table->integer('vanzari');

			$table->timestamps();
		});
		
	/*	Schema::create('descrieri',function($table){
			$table->increments('id');

			$table->integer('produs_id')->unsigned();
			$table->foreign('produs_id')->references('id')->on('produse');

			

			$table->timestamps();
		});*/

		

/*		Schema::create('poze',function($table){
			$table->increments('id');
			$table->string('poza');

			$table->integer('produs_id')->unsigned();
			$table->foreign('produs_id')->references('id')->on('produse');

			$table->timestamps();
		});

	/*	Schema::create('membrii',function($table){
			$table->increments('id');
			$table->boolean('status')->default(0);
			$table->string('tipCont');
			$table->string('nume');
			$table->string('prenume');
			$table->bigInteger('CNP');
			$table->string('email');
			$table->string('password');
			$table->bigInteger('telefon');
			$table->string('adresa_fac');
			$table->string('localitate_fac');
			$table->string('judet_sector_fac');
			$table->string('adresa_liv');
			$table->string('localitate_liv');
			$table->string('judet_sector_liv');
			$table->string('nume_companie');
			$table->string('CUI');
			$table->string('nr_reg');
			$table->string('adresa_sediu');
			$table->string('localitate_sediu');
			$table->string('judet_sector_sediu');
			$table->string('banca');
			$table->string('cont_IBAN');
			$table->string('recomandari');
			$table->text('mentiuni');

			$table->string('KAM_com');
			$table->string('KAM_liv');
			$table->boolean('admin')->default(0);

			$table->string('detalii_plata');
			$table->string('detalii_livrare');
			$table->string('detalii_transport');

			$table->rememberToken();
			$table->timestamps();
		});*/

	/*	Schema::create('users',function($table){
			$table->increments('id');
			$table->string('username');
			$table->string('password');
			$table->boolean('prioritate')->default(0);

			$table->timestamps();
		});*/

/*		Schema::create('mesaje',function($table){
			$table->increments('id');
			$table->integer('membru_id')->unsigned();
			$table->foreign('membru_id')->references('id')->on('membrii');
			$table->string('subiect');
			$table->text('text');

			$table->timestamps();
		});
		Schema::create('carucior',function($table){
			$table->increments('id');

			$table->integer('produs_id')->unsigned();
			$table->foreign('produs_id')->references('id')->on('produse');

			$table->integer('membru_id')->unsigned();
			$table->foreign('membru_id')->references('id')->on('membrii');

			$table->integer('qty');
			$table->decimal('total',10,2);

			$table->timestamps();
		});

		Schema::create('piv_sb1',function($table){
			$table->increments('id');

			$table->integer('subcat_baza_1_id')->unsigned();
			$table->foreign('subcat_baza_1_id')->references('id')->on('subcat_baza_1');
			
			$table->integer('prod_id')->unsigned();
			$table->foreign('prod_id')->references('id')->on('produse');

			$table->timestamps();
		});

		Schema::create('piv_s2c1',function($table){
			$table->increments('id');

			$table->integer('subcat2_copil_1_id')->unsigned();
			$table->foreign('subcat2_copil_1_id')->references('id')->on('subcat2_copil_1');
			
			$table->integer('prod_id')->unsigned();
			$table->foreign('prod_id')->references('id')->on('produse');

			$table->timestamps();
		});

		Schema::create('piv_s3c2',function($table){
			$table->increments('id');

			$table->integer('subcat3_copil_2_id')->unsigned();
			$table->foreign('subcat3_copil_2_id')->references('id')->on('subcat3_copil_2');
			
			$table->integer('prod_id')->unsigned();
			$table->foreign('prod_id')->references('id')->on('produse');

			$table->timestamps();
		});

		Schema::create('piv_s4c3',function($table){
			$table->increments('id');

			$table->integer('subcat4_copil_3_id')->unsigned();
			$table->foreign('subcat4_copil_3_id')->references('id')->on('subcat4_copil_3');
			
			$table->integer('prod_id')->unsigned();
			$table->foreign('prod_id')->references('id')->on('produse');

			$table->timestamps();
		});
		
		Schema::create('comenzi',function($table){
			$table->increments('id');

			$table->integer('comandafac_key');//->unsigned();

			$table->integer('membru_id')->unsigned();
			$table->foreign('membru_id')->references('id')->on('membrii');
			$table->integer('produs_id')->unsigned();
			$table->foreign('produs_id')->references('id')->on('produse');

			$table->string('nume_produs');
			$table->string('cantitate');
			$table->decimal('total',10,2);

			$table->string('metoda_plata');
			$table->string('contbanca');
			$table->string('metoda_livrare');
			$table->string('mod_cumparare');
			$table->string('tip_document');

			$table->text('ment_livrare');

			$table->string('KAM_livrare');

			$table->timestamps();
		});

		Schema::create('status_factura',function($table){
			$table->increments('id');

			$table->integer('comanda_id');
			//$table->foreign('comanda_id')->references('id')->on('comanda_fac');

			$table->integer('membru_id')->unsigned();
			$table->foreign('membru_id')->references('id')->on('membrii');

			$table->integer('nr_factura');
			$table->decimal('total',10,2);
			$table->string('status_comanda');
			$table->string('stare_plata');
			$table->string('stare_transport');
			$table->string('metoda_plata');
			$table->string('metoda_livrare');

			$table->timestamps();
		});

		Schema::create('disponibilitate',function($table){
			$table->increments('id');

			$table->integer('produs_id')->unsigned();
			$table->foreign('produs_id')->references('id')->on('produse');	

			$table->boolean('in_stoc')->default(0);
			$table->boolean('stoc_redus')->default(0);
			$table->boolean('produs_indisponibil')->default(0);
			$table->boolean('in_curand')->default(0);

			$table->timestamps();
		});	

		Schema::create('alerta_stoc',function($table){
			$table->increments('id');

			$table->integer('produs_id')->unsigned();
			$table->foreign('produs_id')->references('id')->on('produse');

			$table->integer('disponibilitate_id')->unsigned();
			$table->foreign('disponibilitate_id')->references('id')->on('disponibilitate');

			$table->integer('stoc_ramas');

			$table->timestamps();							
		});

	/*	Schema::create('comanda_fac',function($table){
			$table->increments('id');
			$table->integer('comanda_id')->unsigned();		

			$table->integer('membru_id')->unsigned();
			$table->foreign('membru_id')->references('id')->on('membrii');
			
			$table->boolean('anulata')->default(0);
			
			$table->boolean('neprocesata')->default(0);
			$table->boolean('curs_procesare')->default(0);
			$table->boolean('procesata')->default(0);
			$table->boolean('curs_livrare')->default(0);
			$table->boolean('livrata')->default(0);

			$table->string('total');

			$table->timestamps();
		});

	/*	Schema::create('prod_temp',function($table){
			$table->increments('id');

			$table->integer('cat_baza_id')->unsigned();
			$table->foreign('cat_baza_id')->references('id')->on('cat_baza');

			$table->integer('brand_id')->unsigned();
			$table->foreign('brand_id')->references('id')->on('branduri');
		
			$table->integer('producator_id')->unsigned();
			$table->foreign('producator_id')->references('id')->on('producatori');
			$table->string('culoare');
			$table->string('dimensiune');
			$table->string('tip_ambalaj');

			$table->text('atentie');

			$table->longText('detalii');
			$table->string('cod_produs');
			$table->string('denumire_produs');
			$table->string('poza');
			$table->decimal('pret',10,2);
			$table->string('discount',5,2);
			$table->integer('stoc');
			$table->decimal('total_stoc',10,2);
			$table->integer('intrari');
			$table->integer('iesiri');
			$table->integer('retur');
			$table->boolean('promotii')->default(0);
			$table->boolean('noutati')->default(0);
			$table->integer('alerta_stoc');
			$table->boolean('lichidari_stoc')->default(0);
			$table->integer('vizitari');
			$table->integer('vanzari');
			$table->string('pozitie_raft');


			$table->timestamps();			
		});
		*/			
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		/*Schema::drop('piv_sb1');
		Schema::drop('piv_s2c1');
		Schema::drop('piv_s3c2');
		Schema::drop('piv_s4c3');
		Schema::drop('cat_baza');
		Schema::drop('subcat_baza_1');
		Schema::drop('subcat2_copil_1');
		Schema::drop('subcat3_copil_2');
		Schema::drop('subcat4_copil_3');
		Schema::drop('produse');
		Schema::drop('branduri');
		Schema::drop('disponibilitate');
		Schema::drop('producatori');
		Schema::drop('poze');
		Schema::drop('membrii');
		Schema::drop('users');
		Schema::drop('mesaje');
		Schema::drop('carucior');
		Schema::drop('comenzi');
		Schema::drop('comanda_fac');
		Schema::drop('status_factura');*/
	}

}
