<div class="sidebar hidden-xs hidden-sm col-md-2 col-lg-2">

	<form method="get" action="/filtre" id="filtru">

				<div class="filtre" id="basicf">

					<div class="checkbox col-sm-offset-2">

						@if(isset($input))
							@if($eQuery == null)
							<h5 class='stergFtit'><strong >Sterge filtrele aplicate</strong></h5>
						<label  class="stergF">
							<input type="checkbox" class="allprodrem" value=''  name="totprod"> Sterge Toate Filtrele
						</label>
							@endif

							@if(isset($input['noutati']))															
									<label class="remove">
								<input type="checkbox" checked class="Rinput" name="noutati"value='1'> Noutati
									</label><br>
							@endif
							@if(isset($input['promotii']))							
									<label class="remove">
								<input type="checkbox" checked class="Rinput" name="promotii"value='1'> Promotii
									</label><br>
							@endif
							@if(isset($input['lichidari_stoc']))															
									<label class="remove">
								<input type="checkbox" checked class="Rinput" name="lichidari_stoc"value='1'> Lichidari Stoc
									</label><br>
							@endif
							@if(isset($input['discount']))							
									<label class="remove">
								<input type="checkbox" checked class="Rinput" name="discount" value='1'> Discounturi
									</label><br>
							@endif
							@if(isset($input['swap']))							
									<label class="remove">
								<input type="checkbox" checked class="Rinput" name="swap" value='1'> SWAP
									</label><br>
							@endif
							@if(isset($input['poza']))									
									<label class="remove">
								<input type="checkbox" checked class="Rinput" name="poza" value='1'> Doar cu Poza
									</label><br>
							@endif

							@if(isset($compare))
									@if(isset($compare['stoc']))
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value="{{$compare['stoc']}}">In Stoc
									</label><br>
									@endif
									@if(isset($compare['rangemin']) || isset($compare['rangemax']))
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value="{{$compare['rangemax']}}">Raza Preturi (RON)
									</label><br>
									@endif
							@endif

							@if(isset($select))
									@if(isset($select['pret']))
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value="{{$select['pret']}}">Deselect ord pret
									</label><br>
									@endif								
									@if(isset($select['denumire_produs']))
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value="{{$select['denumire_produs']}}">Deselect ord alfabetica
									</label><br>
									@endif
							@endif

							@foreach($brands as $i)
																	
								@if($i == 1)
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Apple
									</label><br>
								@elseif($i == 2)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"  value='{{$i}}'>Samsung
									</label><br>
								@elseif($i == 3)
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Sony
									</label><br>
								@elseif($i == 4)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"  value='{{$i}}'>LG
									</label><br>
								@elseif($i == 5)
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>HTC
									</label><br>
								@elseif($i == 6)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Nokia
									</label><br>
								@elseif($i == 7)
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Blackberry
									</label><br>
								@elseif($i == 8)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Motorola
									</label><br>
								@elseif($i == 9)
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Huawei
									</label><br>
								@elseif($i == 10)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>ZTE
									</label><br>
								@elseif($i == 11)
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Sony Ericsson
									</label><br>
								@elseif($i == 12)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Dell
									</label><br>
								@elseif($i == 13)
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Alcatel
									</label><br>
								@elseif($i == 14)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Amazon
									</label><br>
								@elseif($i == 15)
									<label class="remove">
										<input type="checkbox" checked class="Rinput"  value='{{$i}}'>Lenovo
									</label><br>
								@elseif($i == 16)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"  value='{{$i}}'>Google
									</label><br>
								@elseif($i == 17)
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Acer
									</label><br>
								@elseif($i == 18)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Asus
									</label><br>
								@elseif($i == 19)
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Philips
									</label><br>
								@elseif($i == 20)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Siemens
									</label><br>
								@elseif($i == 21)
									<label class="remove">
										<input type="checkbox" checked class="Rinput" value='{{$i}}'>Vodafone
									</label><br>
								@elseif($i == 22)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Orange
									</label><br>
								@elseif($i == 23)
									<label class="remove">
										<input type="checkbox" checked class="Rinput"  value='{{$i}}'>Allview
									</label><br>
								@elseif($i == 24)
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Cosmote
									</label><br>
								@elseif($i == 25)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Xiaomi
									</label><br>	
								@elseif($i == 26)				
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>Meizu
									</label><br>
								@elseif($i == 27)
									<label class="remove">
										<input type="checkbox" checked class="Rinput"   value='{{$i}}'>T-Mobile
									</label><br>		
								@endif
								
							@endforeach	
							
							@if($eQuery == null)
							<hr class='stergFlinie'>
							@endif

						@endif

					</div>

					

					<div class="checkbox col-sm-offset-2">

						<label>
							@if(isset($compare))
								@if(isset($compare['stoc']))
									<input type="checkbox" class="Finput inputs" name="stoc" checked value='1'> In Stoc
								@else
									<input type="checkbox" class="Finput inputs" name="stoc" value='1'> In Stoc
								@endif	
							@else
								<input type="checkbox" class="Finput inputs" name="stoc" value='1'> In Stoc
							@endif

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>
							@if(isset($input))
								@if(isset($input['noutati']))	
									<input type="checkbox" checked class="Finput inputs" name="noutati"value='1'> Noutati
								@else
									<input type="checkbox" class="Finput inputs" name="noutati"value='1'> Noutati
								@endif
							@else
								<input type="checkbox" class="Finput inputs" name="noutati"value='1'> Noutati
							@endif

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>
							@if(isset($input))
								@if(isset($input['promotii']))
									<input type="checkbox" checked class="Finput inputs" name="promotii"value='1'> Promotii
								@else
									<input type="checkbox" class="Finput inputs" name="promotii"value='1'> Promotii
								@endif
							@else
								<input type="checkbox" class="Finput inputs" name="promotii"value='1'> Promotii
							@endif

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>
							@if(isset($input))
								@if(isset($input['lichidari_stoc']))
									<input type="checkbox" checked class="Finput inputs" name="lichidari_stoc"value='1'> Lichidari Stoc
								@else
									<input type="checkbox" class="Finput inputs" name="lichidari_stoc"value='1'> Lichidari Stoc
								@endif
							@else
								<input type="checkbox" class="Finput inputs" name="lichidari_stoc"value='1'> Lichidari Stoc
							@endif

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>
							@if(isset($input))
								@if(isset($input['discount']))	
									<input type="checkbox" checked class="Finput inputs" name="discount"value='1'> Discounturi
								@else
									<input type="checkbox" class="Finput inputs" name="discount"value='1'> Discounturi
								@endif
							@else
								<input type="checkbox" class="Finput inputs" name="discount"value='1'> Discounturi
							@endif

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>
							@if(isset($input))
								@if(isset($input['swap']))	
									<input type="checkbox" checked class="Finput inputs" name="swap"value='1'> SWAP
								@else
									<input type="checkbox" class="Finput inputs" name="swap"value='1'> SWAP
								@endif
							@else
								<input type="checkbox" class="Finput inputs" name="swap" value='1'> SWAP
							@endif

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label  >
						@if(isset($input))
							@if(isset($input['poza']))

							<input type="checkbox" checked class="Finput inputs" name="poza"value='1'> Doar cu Poza
							@else

							<input type="checkbox" class="Finput inputs" name="poza"value='1'> Doar cu Poza
							@endif
						@else
							<input type="checkbox" class="Finput inputs" name="poza"value='1'> Doar cu Poza

						@endif


						</label>	

					</div>

				

				</div>



				<div class="filtre"  id="fPret">

					<h5><strong class="col-sm-offset-1" > Pret</strong></h5>

					<div class="">
						@if(isset($select))
							@if(isset($select['pret']))
								@if($select['pret'] == 'ASC')
								<select name="pret" class="FinputO inputs col-sm-offset-2" id="ordpret">

					    			<option value="ASC">Crescator</option>

					    			<option value="DESC">Descrescator</option>
									<option value="">-- Deselecteaza --</option>

					    		</select>
								@else
								<select name="pret" class="FinputO inputs col-sm-offset-2" id="ordpret">

					    			<option value="DESC">Descrescator</option>
					    			<option value="ASC">Crescator</option>

									<option value="">-- Deselecteaza --</option>

					    		</select>
								@endif
								
							@else
								<select name="pret" class="FinputO inputs col-sm-offset-2" id="ordpret">

					    			<option value="">-- Selecteaza --</option>

					    			<option value="ASC">Crescator</option>

					    			<option value="DESC">Descrescator</option>

					    		</select>
							@endif
						@else
						<select name="pret" class="FinputO inputs col-sm-offset-2" id="ordpret">

					    	<option value="">-- Selecteaza --</option>

					    	<option value="ASC">Crescator</option>

					    	<option value="DESC">Descrescator</option>

					    </select>
						@endif
					    

					</div>

					<h5><strong class="col-sm-offset-1" > RON</strong></h5>

					@if(isset($compare))
						@if(isset($compare['rangemin'])&& isset($compare['rangemax']))
						<b style="margin-right:20px;"></b><input id="ex2" type="range" class="FinputR inputs col-sm-offset-2" value="" data-slider-min="0" data-slider-max="1500" data-slider-step="1" data-slider-value="[{{$compare['rangemin']}},{{$compare['rangemax']}}]" style="width:125px;" />
						<input type="hidden" value="{{$compare['rangemin']}}" name='rangemin' class="inputs" id="rangemin">
						<input type="hidden" value="{{$compare['rangemax']}}" name='rangemax' class="inputs" id="rangemax">
						@elseif(isset($compare['rangemin']))
						<b style="margin-right:20px;"></b><input id="ex2" type="range" class="FinputR inputs col-sm-offset-2" value="" data-slider-min="0" data-slider-max="1500" data-slider-step="1" data-slider-value="[{{$compare['rangemin']}},1500]" style="width:125px;" />
						<input type="hidden" value="{{$compare['rangemin']}}" name='rangemin' class="inputs" id="rangemin">
						<input type="hidden"  name='rangemax' class="inputs" id="rangemax">
						@elseif(isset($compare['rangemax']))
						<b style="margin-right:20px;"></b><input id="ex2" type="range" class="FinputR inputs col-sm-offset-2" value="" data-slider-min="0" data-slider-max="1500" data-slider-step="1" data-slider-value="[0,{{$compare['rangemax']}}]" style="width:125px;" />
						<input type="hidden"  name='rangemin' class="inputs" id="rangemin">
						<input type="hidden" value="{{$compare['rangemax']}}" name='rangemax' class="inputs" id="rangemax">
						@else
						<b style="margin-right:20px;"></b><input id="ex2" type="range" class="FinputR inputs col-sm-offset-2" value="" data-slider-min="0" data-slider-max="1500" data-slider-step="1" data-slider-value="[0,1500]" style="width:125px;" />
						<input type="hidden" name='rangemin' class="inputs" id="rangemin">
						<input type="hidden" name='rangemax' class="inputs" id="rangemax">
						@endif
					@else
					<b style="margin-right:20px;"></b><input id="ex2" type="range" class="FinputR inputs col-sm-offset-2" value="" data-slider-min="0" data-slider-max="1500" data-slider-step="1" data-slider-value="[0,1500]" style="width:125px;" />
					<input type="hidden" name='rangemin' class="inputs" id="rangemin">
					<input type="hidden" name='rangemax' class="inputs" id="rangemax">
					@endif
					

					

				</div>	

			

			<div class="filtre"  id="faz">

					<h5><strong class="col-sm-offset-1">Alfabetic</strong></h5>

					<div class="">
						@if(isset($select))
							@if(isset($select['denumire_produs']))
								@if($select['denumire_produs'] == 'ASC')
								<select name="denumire_produs" class="FinputO inputs col-sm-offset-2" id="denprod">
					    			<option value="ASC">A-Z</option>
					    			<option value="DESC">Z-A</option>
					    			<option value="">-- Deselecteaza --</option>
					    		</select>
								@else
								<select name="denumire_produs" class="FinputO inputs col-sm-offset-2" id="denprod">
					    			<option value="DESC">Z-A</option>
					    			<option value="ASC">A-Z</option>
					    			<option value="">-- Deselecteaza --</option>

					    		</select>
								@endif
							@else
								<select name="denumire_produs" class="FinputO inputs col-sm-offset-2" id="denprod">
					    			<option value="">-- Selecteaza --</option>
					    			<option value="ASC">A-Z</option>
					    			<option value="DESC">Z-A</option>
					    		</select>
							@endif	
						@else
						<select name="denumire_produs" class="FinputO inputs col-sm-offset-2" id="denprod">

					    	<option value="">-- Selecteaza --</option>

					    	<option value="ASC">A-Z</option>

					    	<option value="DESC">Z-A</option>

					    </select>
						@endif
					    

					</div>

					

			</div>	



			<div class="filtre" id="fBrand">

				<h5><strong class="col-sm-offset-1">Branduri</strong></h5>

				@if(isset($branduri))

					@foreach($branduri as $index=>$brand)
						
							@if(isset($brands))
								
									<div class="checkbox col-sm-offset-2">
										<label class="brand">
											<input type="checkbox" class="Finput inputs" {{in_array($brand->id,$brands) ? 'checked' : ''}}  name="brand{{$brand->id}}" value="{{$brand->id}}"> {{$brand->nume}}
										</label>
									</div>
								
							@else
							<div class="checkbox col-sm-offset-2">
								<label class="brand">
									<input type="checkbox" class="Finput inputs"  name="brand{{$brand->id}}" value="{{$brand->id}}"> {{$brand->nume}}
								</label>
							</div>
							@endif
							


					@endforeach

				@endif

				
			<a href="" class="totb">Mai multe</a>
				
			</div>

			
			</form>


			<div class="modal fade" id="addfilter" role="dialog">

			<div class="modal-dialog">

				<div class="modal-content">

					<div class="modal-header">

						<h4 style="text-align:center;">Te rog asteapta</h4>

					</div>

					<div class="modal-body">

						<div class="progress"  style="background-color:#098ead">

							<div class="progress-bar filterload" data-transitiongoal="100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background-color:#f5811e;">

							</div>		

						</div>			

					</div>

				</div>

			</div>	

		</div>	






</div>
