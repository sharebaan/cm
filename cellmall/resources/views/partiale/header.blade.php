  <header class="col-xs-12 col-sm-12 col-md-12  col-lg-12">



    <nav class="navbar navbar-default hidden-xs hidden-sm top-bar-desk">

      <div class="container-fluid">

        <ul class="nav nav-justified top-bar-list">

          <li>

            <a href="/home">

              <span class="glyphicon glyphicon-home"></span> Acasa</a>

          </li>

          <li>

            <a href="/informatii">

              <span class="glyphicon glyphicon-info-sign"></span> Informatii</a>

          </li>

          <li>

            <a href="/informatii/termenisiconditii">

              <span class="glyphicon glyphicon-book"></span> Termeni/Conditii</a>

          </li>

          <li>

            <a href="/informatii/contact">

              <span class="glyphicon glyphicon-envelope"></span> Contact</a>

          </li>

          <li>

            <a href="/produsnoutati">

              <span class="glyphicon glyphicon-star"></span> Noutati</a>

          </li>

		  <li>

            <a href="/produspromotii">

              <span class="glyphicon glyphicon-gift"></span> Promotii</a>

          </li>
          <li>

            <a href="/lichidaristoc">

              <span class="glyphicon glyphicon-piggy-bank"></span> Lichidari Stoc</a>

          </li>

          <li>

            <a href="/ofertespeciale">

              <span class="glyphicon glyphicon-scissors"></span> Discount</a>

          </li>
		@if(Auth::user()->admin == 1 || Auth::user()->angajat_produse == 1)
          <li>

            <a href="/dashboard"target="_blank">

              <span class="glyphicon glyphicon-wrench"></span> Dashboard</a>

          </li>
		@endif
          <li>

            <a href="/logout">

              <span class="glyphicon glyphicon-log-out"></span> Iesire</a>

          </li>

        </ul>

      </div>

    </nav>



    <div class="row col-xs-12 col-sm-12 hidden-md hidden-lg center-block top-bar-mob">

      <a class="btn btn-default center-block col-xs-4 col-sm-4 contmeu-mob" id="" href="/contulmeu" role="button">Contul Meu <span class="glyphicon glyphicon-user"></span></a>

      <a class="btn btn-default center-block col-xs-4 col-sm-4" href="/fincomanda" role="button">Cosul meu <span class="glyphicon glyphicon-shopping-cart"></span></a>

      <a class="btn btn-default center-block col-xs-4 col-sm-4" href="/logout" role="button">Iesire <span class="glyphicon glyphicon-log-out"></span></a>

    </div>





    <a href="/home"><img src="/thumbs/logo.png" class=" col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4" alt="CellMall"></a>


	<h4 class="bunvenit col-xs-12 col-sm-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 ">Salut, {{Auth::user()->prenume}} {{Auth::user()->nume}}</h4>	


    <form action="/cauta" method="get" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

      <div id="custom-search-input">

        <div class="input-group col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">

          <input type="search" name="search" class="search-query form-control" placeholder="Cauta ..." />

          <span class="input-group-btn">

            <button class="btn btn-danger" type="submit">

              <span class=" glyphicon glyphicon-search"></span>

            </button>

          </span>

        </div>

      </div>

    </form>



    <ul class="col-xs-12 col-sm-12 hidden-md hidden-lg mob-pills" style="display:none;">

      <li><a href="" class="col-xs-9 col-sm-9 next-level" ><span class="glyphicon glyphicon-chevron-down"></span> Telefoane Mobile / GSM</a><a class="col-xs-3 col-sm-3" href="/home/telefoanegsm"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

        <ul class="col-xs-12 col-sm-12 mob-pills-lvl-1" style="display:none;">

          <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Componente GSM</a><a class="col-xs-3 col-sm-3" href="/home/componenteaccesorii/{{$telgsm[0]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

            <ul class="col-xs-12 col-sm-12 mob-pills-lvl-2" style="display:none;">

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Componente</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[0]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[0]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Display LCD</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[1]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Touchscreen</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[2]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Benzi / Cabluri Flexibile</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[3]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Cititoare SIM</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[4]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Geam</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[5]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Buzzere / Sonerii</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[6]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Camere</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[7]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microfoane</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[8]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Mufe Incarcare</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[9]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Joystick</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[10]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Diverse</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[11]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Casti</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[12]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Service</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Componente Carcase</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[1]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[13]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Rame</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[14]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Mijloace</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[15]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Capace Camera</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[16]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Capace Baterie</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Microcontacte Diverse</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[2]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[17]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microcontacte On / Off</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[18]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microcontacte Volum</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[19]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microcontacte Meniu</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[20]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microcontacte Diverse</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Butoane Diverse</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[3]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[21]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Butoane On / Off</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[22]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Butoane Volum</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[23]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Butoane Meniu</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[24]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Butoane Diverse</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Conectori</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[4]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[25]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Conectori</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Circuite Integrate</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[5]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[26]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Flash</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[27]->id}}"><span class="glyphicon glyphicon-eye-open"></span> CPU</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[28]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Power IC / Surse</a></li>
                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[29]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Placi de baza / Mainboard</a></li>

                  </ul>

                </li>

            </ul>

          </li>

          <li><a class="col-xs-9 col-sm-9" href=""><span class="glyphicon glyphicon-chevron-down"></span> Accesorii GSM</a><a class="col-xs-3 col-sm-3" href="#"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a></li>

          <li><a class="col-xs-9 col-sm-9" href=""><span class="glyphicon glyphicon-chevron-down"></span> Accesorii GSM Originale</a><a class="col-xs-3 col-sm-3" href="#"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a></li>

          <li><a class="col-xs-9 col-sm-9" href=""><span class="glyphicon glyphicon-chevron-down"></span> Telefoane / Terminale GSM</a><a class="col-xs-3 col-sm-3" href="#"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a></li>

        </ul>

      </li>

      <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Tablete</a><a class="col-xs-3 col-sm-3" href="#"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

        <ul class="col-xs-12 col-sm-12 mob-pills-lvl-1" style="display:none;">

		  <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Componente Tableta</a><a class="col-xs-3 col-sm-3" href="#"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

			<ul class="col-xs-12 col-sm-12 mob-pills-lvl-2" style="display:none;">

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Componente</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[6]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[30]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Display LCD</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[31]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Touchscreen</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[32]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Benzi / Cabluri Flexibile</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[33]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Cititoare SIM</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[34]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Geam</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[35]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Buzzere / Sonerii</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[36]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Camere</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[37]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microfoane</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[38]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Mufe Incarcare</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[39]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Joystick</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[40]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Diverse</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[41]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Casti</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[42]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Service</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Componente Carcase</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[7]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[43]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Rame</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[44]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Mijloace</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[45]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Capace Camera</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[46]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Capace Baterie</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Microcontacte Diverse</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[8]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[47]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microcontacte On / Off</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[48]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microcontacte Volum</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[49]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microcontacte Meniu</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[50]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Microcontacte Diverse</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Butoane Diverse</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[9]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[51]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Butoane On / Off</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[52]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Butoane Volum</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[53]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Butoane Meniu</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[54]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Butoane Diverse</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Conectori</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[10]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[55]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Conectori</a></li>

                  </ul>

                </li>

                <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> Circuite Integrate</a><a class="col-xs-3 col-sm-3" href="/home/titlu/{{$componenteT[11]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

                  <ul class="col-xs-12 col-sm-12 mob-pills-lvl-3" style="display:none;">

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[56]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Flash</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[57]->id}}"><span class="glyphicon glyphicon-eye-open"></span> CPU</a></li>

                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[58]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Power IC / Surse</a></li>
                    <li><a class="col-xs-12 col-sm-12" href="/home/categorie/{{$componente[59]->id}}"><span class="glyphicon glyphicon-eye-open"></span> Placi de baza / Mainboard</a></li>

                  </ul>

                </li>

            </ul>

		  </li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Accesorii Tableta</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Tablete</a></li>

        </ul>

      </li>

      <li><a class="col-xs-9 col-sm-9 next-level" href=""><span class="glyphicon glyphicon-chevron-down"></span> PC / Laptop</a><a class="col-xs-3 col-sm-3" href="#"><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

        <ul class="col-xs-12 col-sm-12 mob-pills-lvl-1" style="display:none;">

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Componente PC / Laptop</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Accesorii PC / Laptop</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Periferice PC / Laptop</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Desktop PC / Laptop</a></li>

        </ul>

      </li>

      <li><a class="col-xs-9 col-sm-9 next-level" href="/home/categorie/{{$componente[12]->id}}"><span class="glyphicon glyphicon-chevron-down"></span> Service</a><a class="col-xs-3 col-sm-3" href=""><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

        <ul class="col-xs-12 col-sm-12 mob-pills-lvl-1" style="display:none;">

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Aparatura Service</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Accesorii si piese de schimb aparatura</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Interfete decodari si rescriere software</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Loguri si activari software</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Diverse Service</a></li>

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Decodari Online</a></li>

        </ul>

      </li>

      <li><a class="col-xs-9 col-sm-9 next-level" href="#"><span class="glyphicon glyphicon-chevron-down"></span> Diverse</a><a class="col-xs-3 col-sm-3" href=""><span class="glyphicon glyphicon-eye-open"></span> Vezi</a>

        <ul class="col-xs-12 col-sm-12 mob-pills-lvl-1" style="display:none;">

          <li><a class="col-xs-12 col-sm-12" href="#"><span class="glyphicon glyphicon-eye-open"></span> Diverse</a></li>

        </ul>

      </li>

    </ul>



    <div class="col-xs-12 col-sm-12 hidden-md hidden-lg mob-nav">

      <span class="ddsym glyphicon glyphicon-list col-xs-1 col-sm-5"></span>
	  <span class="meniutext col-xs-10 col-sm-6 ">Categorii Produse</span>
    </div>





  </header>

		

    <nav class="desk-nav col-md-12 col-lg-12 hidden-sm hidden-xs">

      <ul class="nav-user">

        <li><a href="/contulmeu"class="cosmeu" id="{{Auth::user()->prenume}} {{Auth::user()->nume}}">Contul Meu <span class="glyphicon glyphicon-user"></span></a></li>

        <li><a href="/fincomanda"class="contmeu">Cosul Meu <span class="glyphicon glyphicon-shopping-cart"></span></a>

          <ul class="cos">

			
			@for($i=count($cart['nume'])-1;$i>=0;$i--)
				
		   <div class="item-cos col-md-12 col-lg-12">
				<a href="/produs/{{$cart['id'][$i]}}" class="col-md-11 col-lg-11" >
				
			  
			  <div class="cosbuc statcos"><p>{{$cart['qty'][$i]}}x</p></div>

              <div class="cosnume statcos">
			  @if(strlen($cart['nume'][$i]) > 35)
                {{substr($cart['nume'][$i],0,30)}}... 
			  @else
				{{$cart['nume'][$i]}}	
			  @endif
              </div>
			<div style="display:none;" class="codcosmic">{{$cart['cod'][$i]}}</div>	
              <div class="coscant statcos"><p>{{$cart['total'][$i]}} </p><p>RON</p></div>
				
			</a>
             <a class="cossterge statcos col-md-1 col-lg-1" name="{{$cart['id'][$i]}}" href=""><img src="/../thumbs/stergemic.png"></a> 
			  
            </div>
			
					
			@endfor

            <a href="/fincomanda" class="fin-comanda col-md-12 col-lg-12" >Finalizeaza Comanda</a>

          </ul>

        </li>

      </ul>

      <ul class="main-navigation">



      <li><a href="/home">Acasa</a></li>

      <li><a href="/home/telefoanegsm">Telefoane Mobile / GSM</a>

        <ul>

          <li><a href="/home/componenteaccesorii/{{$telgsm[0]->id}}">Componente GSM</a>

            <ul>

              <li><a href="/home/titlu/{{$componenteT[0]->id}}" class="lvl-3-title">Componente</a></li>

              <li><a href="/home/categorie/{{$componente[0]->id}}">Display / LCD</a></li>

              <li><a href="/home/categorie/{{$componente[1]->id}}">Touchscreen</a></li>

              <li><a href="/home/categorie/{{$componente[2]->id}}">Benzi / Cabluri Flexibile</a></li>

              <li><a href="/home/categorie/{{$componente[3]->id}}">Cititoare SIM</a></li>

              <li><a href="/home/categorie/{{$componente[4]->id}}">Geam</a></li>

              <li><a href="/home/categorie/{{$componente[5]->id}}">Buzzere / Sonerii</a></li>

              <li><a href="/home/categorie/{{$componente[6]->id}}">Camere</a></li>

              <li><a href="/home/categorie/{{$componente[7]->id}}">Microfoane</a></li>

              <li><a href="/home/categorie/{{$componente[8]->id}}">Mufe Incarcare</a></li>

              <li><a href="/home/categorie/{{$componente[9]->id}}">Joystick</a></li>

              <li><a href="/home/categorie/{{$componente[10]->id}}">Diverse</a></li>

              <li><a href="/home/categorie/{{$componente[11]->id}}">Casti</a></li>

              <li><a href="/home/categorie/{{$componente[12]->id}}">Service</a></li>

              <li><a href="/home/titlu/{{$componenteT[1]->id}}" class="lvl-3-title">Componente Carcase</a></li>

              <li><a href="/home/categorie/{{$componente[13]->id}}">Rame</a></li>

              <li><a href="/home/categorie/{{$componente[14]->id}}">Mijloace</a></li>

              <li><a href="/home/categorie/{{$componente[15]->id}}">Capace Camera</a></li>

              <li><a href="/home/categorie/{{$componente[16]->id}}">Capace Baterie</a></li>

              <li><a href="/home/titlu/{{$componenteT[2]->id}}" class="lvl-3-title">Microcontacte Diverse</a></li>

              <li><a href="/home/categorie/{{$componente[17]->id}}">Microcontacte On / Off</a></li>

              <li><a href="/home/categorie/{{$componente[18]->id}}">Microcontacte Volum</a></li>

			  <li><a href="/home/categorie/{{$componente[19]->id}}">Microcontacte meniu</a></li>

              <li><a href="/home/categorie/{{$componente[20]->id}}">Microcontacte Diverse</a></li>

              <li><a href="/home/titlu/{{$componenteT[3]->id}}" class="lvl-3-title">Butoane Diverse</a></li>

              <li><a href="/home/categorie/{{$componente[21]->id}}">Butoane On / Off</a></li>

              <li><a href="/home/categorie/{{$componente[22]->id}}">Butoane Volum</a></li>

              <li><a href="/home/categorie/{{$componente[23]->id}}">Butoane Meniu</a></li>

              <li><a href="/home/categorie/{{$componente[24]->id}}">Butoane Diverse</a></li>

              <li><a href="/home/titlu/{{$componenteT[4]->id}}" class="lvl-3-title">Conectori</a></li>

              <li><a href="/home/categorie/{{$componente[25]->id}}">Conectori</a></li>

              <li><a href="/home/titlu/{{$componenteT[5]->id}}" class="lvl-3-title">Circuite Integrate</a></li>

              <li><a href="/home/categorie/{{$componente[26]->id}}">Flash</a></li>

              <li><a href="/home/categorie/{{$componente[27]->id}}">CPU</a></li>

              <li><a href="/home/categorie/{{$componente[28]->id}}">Power IC</a></li>
              <li><a href="/home/categorie/{{$componente[29]->id}}">Placi de baza / Mainboard</a></li>

            </ul>

          </li>

          <li><a href="#">Accesorii GSM</a></li>

          <li><a href="#">Accesorii GSM Originale</a></li>

          <li><a href="#">Telefoane / Terminale GSM</a></li>

        </ul>

      </li>

      <li><a href="#">Tablete</a>

        <ul>

		  <li><a href="/home/componenteaccesorii/{{$telgsm[4]->id}}">Componente Tableta</a>

		 	<ul>

              <li><a href="/home/titlu/{{$componenteT[6]->id}}" class="lvl-3-title">Componente</a></li>

              <li><a href="/home/categorie/{{$componente[30]->id}}">Display / LCD</a></li>

              <li><a href="/home/categorie/{{$componente[31]->id}}">Touchscreen</a></li>

              <li><a href="/home/categorie/{{$componente[32]->id}}">Benzi / Cabluri Flexibile</a></li>

              <li><a href="/home/categorie/{{$componente[33]->id}}">Cititoare SIM</a></li>

              <li><a href="/home/categorie/{{$componente[34]->id}}">Geam</a></li>

              <li><a href="/home/categorie/{{$componente[35]->id}}">Buzzere / Sonerii</a></li>

              <li><a href="/home/categorie/{{$componente[36]->id}}">Camere</a></li>

              <li><a href="/home/categorie/{{$componente[37]->id}}">Microfoane</a></li>

              <li><a href="/home/categorie/{{$componente[38]->id}}">Mufe Incarcare</a></li>

              <li><a href="/home/categorie/{{$componente[39]->id}}">Joystick</a></li>

              <li><a href="/home/categorie/{{$componente[40]->id}}">Diverse</a></li>

              <li><a href="/home/categorie/{{$componente[41]->id}}">Casti</a></li>

              <li><a href="/home/categorie/{{$componente[42]->id}}">Service</a></li>

              <li><a href="/home/titlu/{{$componenteT[7]->id}}" class="lvl-3-title">Componente Carcase</a></li>

              <li><a href="/home/categorie/{{$componente[43]->id}}">Rame</a></li>

              <li><a href="/home/categorie/{{$componente[44]->id}}">Mijloace</a></li>

              <li><a href="/home/categorie/{{$componente[45]->id}}">Capace Camera</a></li>

              <li><a href="/home/categorie/{{$componente[46]->id}}">Capace Baterie</a></li>

              <li><a href="/home/titlu/{{$componenteT[8]->id}}" class="lvl-3-title">Microcontacte Diverse</a></li>

              <li><a href="/home/categorie/{{$componente[47]->id}}">Microcontacte On / Off</a></li>

              <li><a href="/home/categorie/{{$componente[48]->id}}">Microcontacte Volum</a></li>

			  <li><a href="/home/categorie/{{$componente[49]->id}}">Microcontacte meniu</a></li>

              <li><a href="/home/categorie/{{$componente[50]->id}}">Microcontacte Diverse</a></li>

              <li><a href="/home/titlu/{{$componenteT[9]->id}}" class="lvl-3-title">Butoane Diverse</a></li>

              <li><a href="/home/categorie/{{$componente[51]->id}}">Butoane On / Off</a></li>

              <li><a href="/home/categorie/{{$componente[52]->id}}">Butoane Volum</a></li>

              <li><a href="/home/categorie/{{$componente[53]->id}}">Butoane Meniu</a></li>

              <li><a href="/home/categorie/{{$componente[54]->id}}">Butoane Diverse</a></li>

              <li><a href="/home/titlu/{{$componenteT[10]->id}}" class="lvl-3-title">Conectori</a></li>

              <li><a href="/home/categorie/{{$componente[55]->id}}">Conectori</a></li>

              <li><a href="/home/titlu/{{$componenteT[11]->id}}" class="lvl-3-title">Circuite Integrate</a></li>

              <li><a href="/home/categorie/{{$componente[56]->id}}">Flash</a></li>

              <li><a href="/home/categorie/{{$componente[57]->id}}">CPU</a></li>

              <li><a href="/home/categorie/{{$componente[58]->id}}">Power IC</a></li>
              <li><a href="/home/categorie/{{$componente[59]->id}}">Placi de baza / Mainboard</a></li>

            </ul> 

		  </li>

          <li><a href="#">Accesorii Tableta</a></li>

          <li><a href="#">Tablete</a></li>

        </ul>

      </li>

      <li><a href="#">PC / Laptop</a>

        <ul>

          <li><a href="#">Componente PC / Laptop</a></li>

          <li><a href="#">Accesorii PC / Laptop</a></li>

          <li><a href="#">Periferice PC / Laptop</a></li>

          <li><a href="#">Desktop / Laptop</a></li>

        </ul>

      </li>

      <li><a href="/home/categorie/{{$componente[12]->id}}">Service</a>

        <ul>

          <li><a href="#">Aparatura service</a></li>

          <li><a href="#">Accesorii si piese de schimb aparatura</a></li>

          <li><a href="#">Interfete decodari si rescriere software</a></li>

          <li><a href="#">Loguri si activari software</a></li>

          <li><a href="#">Diverse service</a></li>

          <li><a href="#">Decodari online</a></li>

        </ul>

      </li>

      <li><a href="#">Diverse</a>

        <ul>

          <li><a href="#">Diverse</a></li>

        </ul>

      </li>



    </ul>

  </nav>


