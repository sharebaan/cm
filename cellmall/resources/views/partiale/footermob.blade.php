<div class="footer-mob col-xs-12 col-sm-12 hidden-md hidden-lg">

          <ul class="fm-container col-xs-12 col-sm-6">
            <li class="col-xs-6 col-xs-offset-3"><a href="/home"><span class="glyphicon glyphicon-home"></span> Acasa</a></li>
            <li class="col-xs-6 col-xs-offset-3"><a href="/informatii"><span class="glyphicon glyphicon-info-sign"></span> Informatii</a></li>
            <li class="col-xs-6 col-xs-offset-3"><a href="/informatii/termenisiconditii"><span class="glyphicon glyphicon-book"></span> Termeni si Conditii</a></li>
            <li class="col-xs-6 col-xs-offset-3"><a href="informatii/contact"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>
          </ul>


          <ul class="fm-container col-xs-12 col-sm-6">
            <li class="col-xs-6 col-xs-offset-3"><a href="/produsnoutati"><span class="glyphicon glyphicon-star"></span> Noutati</a></li>
            <li class="col-xs-6 col-xs-offset-3"><a href="/produspromotii"><span class="glyphicon glyphicon-gift"></span> Promotii</a></li>
            <li class="col-xs-6 col-xs-offset-3"><a href="/lichidaristoc"><span class="glyphicon glyphicon-piggy-bank"></span> Lichidari Stoc</a></li>
            <li class="col-xs-6 col-xs-offset-3"><a href="/ofertespeciale"><span class="glyphicon glyphicon-scissors"></span> Discount</a></li>
          </ul>

    </div>
