<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="/css/home.css">

	<link rel="stylesheet" href="/bootstrap/bootstrap-slider/dist/css/bootstrap-slider.min.css">



	<title>CellMall</title>

</head>

<body>

	








<header >

<div class="nav-center col-md-12 " >

			<ul class="nav nav-pills">  

				<li><a href="/home"><span class="glyphicon glyphicon-home"></span> Acasa</a></li>

				<li><a href="/informatii" ><span class="glyphicon glyphicon-info-sign"></span> Informatii</a></li>

				<li><a href="/informatii/termenisiconditii" ><span class="glyphicon glyphicon-book"></span> Termeni si Conditii</a></li>

				<li><a href="/informatii/contact"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>

				<li><a href="/noutati" ><span class="glyphicon glyphicon-star"></span> Noutati</a></li>

				<li><a href="/promotii"><span class="glyphicon glyphicon-gift"></span> Promotii</a></li>

				<li><a href="/lichidaristoc" ><span class="glyphicon glyphicon-piggy-bank"></span> Lichidari Stoc</a></li>

				<li><a href="/ofertespeciale" ><span class="glyphicon glyphicon-scissors"></span> Discount</a></li>
				@if(Auth::user()->admin == 1)
					<li><a href="/dashboard" target="_blank" ><span class="glyphicon glyphicon-wrench"></span> Dashboard</a></li>
				@endif	
				<li><a href="/logout" ><span class="glyphicon glyphicon-log-out"></span> Iesire</a></li>

			</ul>

		</div>



		<div class="head col-md-12 ">

			<div class="row">

				<img src="/thumbs/logo.png" class="col-md-3" id="logo" alt="">

				<div class="col-md-5" id="search">

					<form action="/cauta" method="get">

						<div class="input-group">

							<input type="text" class="form-control" name="search" placeholder="Cauta produs ...">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>

							</span>

						</div>

					</form>	

				</div><!-- /input-group -->

				<div class="salut col-sm-4 ">Salut, <b class="bsalut">{{Auth::user()->nume}} {{Auth::user()->prenume}}</b></div>

				<div class="col-md-4" id="user">

					<div class="col-sm-6">

						<a href="/contulmeu" ><b>Contul Meu</b></a>

						<a href="/contulmeu" ><img src="/thumbs/user (2).png" id="upic" alt=""></a>

					</div>



					<div class="col-sm-6 cosb butcos">

						<a href="/fincomanda" class="cosb" ><b>Cosul Meu</b> <img src="/thumbs/cos (5).png" id="upic" alt=""></a>



						<div class="cart">

							@if(isset($cart))

							@if(empty($cart['nume']))

							<b class="cgol">Caruciorul este gol.</b>

							@else    

							@for($i=count($cart['nume'])-1;$i>=0;$i--)
							<a class="itemca" href="/produs/{{$cart['id'][$i]}}">
							<div class="itemcnoajax">

								<div class="cqty ic"><b>{{$cart['qty'][$i]}}x</b></div>

								@if(strlen($cart['nume'][$i]) > 60)

								<div class="cnume"><b>{{substr($cart['nume'][$i],0,60)}} ...</b></div>	

								@else

								<div class="cnume"><b>{{$cart['nume'][$i]}}</b></div>

								@endif

								<div class="cpret ic">{{$cart['total'][$i]}} RON</div>

								<div class="cdelete ic"><a href="" name="{{$cart['id'][$i]}}" class="cdel"><img src="/thumbs/stergemic.png" alt="Sterge Produs"></a></div>

							</div>
							</a>
							<br class="brnoajax">                                   

							@endfor

							<div class="fincomanda">

								<a href="/fincomanda">Finalizeaza Comanda</a>

							</div> 

							@endif    



							@endif



						</div>



					</div>			





				</div>

			</div>







			<div class="navb">			

				@if(isset($telgsm))

				<ul class="main-navigation">

					<li><a href="/home"><strong>Acasa</strong></a></li>

					<li><a href="/home/telefoanegsm"><strong>Telefoane Mobile / GSM</strong></a>

						<ul>



							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[0]->id}}">{{$telgsm[0]->nume}}</a>

								<div class="menu1">

									@if(isset($componente))

									<div class="colm">

										<a href="/home/titlu/{{$componenteT[0]->id}}" class="colmp titlu">{{$componenteT[0]->nume}}</a>

										@for($x=0;$x<count($componente);$x++)

										@if($componente[$x]->id == 13)

										<a href="/home/categorie/{{$componente[12]->id}}" class="colmp"> {{$componente[12]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[1]->id}}" class="colmp titlu">{{$componenteT[1]->nume}}</a>

										@elseif($componente[$x]->id == 17)

										<a href="/home/categorie/{{$componente[16]->id}}" class="colmp"> {{$componente[16]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[2]->id}}" class="colmp titlu">{{$componenteT[2]->nume}}</a>

										@elseif($componente[$x]->id == 21)

										<a href="/home/categorie/{{$componente[20]->id}}" class="colmp"> {{$componente[20]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[3]->id}}" class="colmp titlu">{{$componenteT[3]->nume}}</a>

										@elseif($componente[$x]->id == 25)

										<a href="/home/categorie/{{$componente[24]->id}}" class="colmp"> {{$componente[24]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[4]->id}}" class="colmp titlu">{{$componenteT[4]->nume}}</a>

										@elseif($componente[$x]->id == 26)

										<a href="/home/categorie/{{$componente[25]->id}}" class="colmp"> {{$componente[25]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[5]->id}}" class="colmp titlu">{{$componenteT[5]->nume}}</a>		

										@else	

										<a href="/home/categorie/{{$componente[$x]->id}}" class="colmp"> {{$componente[$x]->nume}}</a>

										@endif	

										@endfor

									</div><br>

									@endif

								</div>

							</li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[1]->id}}">{{$telgsm[1]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[2]->id}}">{{$telgsm[2]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[3]->id}}">{{$telgsm[3]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Tablete</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[4]->id}}">{{$telgsm[4]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[5]->id}}">{{$telgsm[5]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[6]->id}}">{{$telgsm[6]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>PC / Laptop</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[7]->id}}">{{$telgsm[7]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[8]->id}}">{{$telgsm[8]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[9]->id}}">{{$telgsm[9]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[10]->id}}">{{$telgsm[10]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Service</strong></a>

						<ul>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[11]->id}}">{{$telgsm[11]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[12]->id}}">{{$telgsm[12]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[13]->id}}">{{$telgsm[13]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[14]->id}}">{{$telgsm[14]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[15]->id}}">{{$telgsm[15]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[16]->id}}">{{$telgsm[16]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Diverse</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[17]->id}}">{{$telgsm[17]->nume}}</a></li>

						</ul>

					</li>

				</ul>					

				@endif

			</div>



		</div>



	</header>




		

		<div class="col-sm-12" id="content">

		
		</div>

	

		

	<div class="foter col-sm-12">

			<div class="foot">

				<div class="foot" >
				<div id="myCarousel4" style="border-radius:0px;background-color:#fff;" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox" >
						
						<div class="item active ">
						<?php     
						for($x=0;$x<14;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
							if($x == 0 || $x == 1 ){continue;}
							else{
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
							}
						}          
						?>
						</div>
						<div class="item">
						<?php     
						for($x=14;$x<26;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
						}          
						?>
						</div>
						<div class="item">
						<?php     
						for($x=26;$x<39;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
						}          
						?>
						</div>
						<div class="item">
						<?php     
						for($x=39;$x<52;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
						}          
						?>
						</div>
						<div class="item">
						<?php     
						for($x=52;$x<65;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
						}          
						?>
						</div>
						<div class="item">
						<?php     
						for($x=65;$x<78;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
						}          
						?>
						</div>
						<div class="item">
						<?php     
						for($x=78;$x<81;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
						}          
						?>
						</div>
						<div class="item">
						<?php     
						for($x=81;$x<94;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
						}          
						?>
						</div>
						<div class="item">
						<?php     
						for($x=94;$x<107;$x++){
							$poze = scandir("/home/cellmall/public_html/pozebranduricellmall/");
								?>          
									<img src="/pozebranduricellmall/{{$poze[$x]}}" class="brit" alt="{{$x}}">                                   
								<?php
						}          
						?>
						</div>
						

				
			</div>

			</div>

	</div>

				

		

		







<script src="/js/jquery.js"></script>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>

<script src="/js/cart.js"></script>

<script src="/js/prodfin.js"></script>

</body>

</html>