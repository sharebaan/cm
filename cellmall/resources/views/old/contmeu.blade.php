<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/css/home.css">
	<link rel="stylesheet" href="/slick/slick.css">
	<link rel="stylesheet" href="/slick/slick-theme.css">
	<title>CellMall</title>
</head>

<body>


<header >

<div class="nav-center col-md-12 " >

			<ul class="nav nav-pills">  

				<li><a href="/home"><span class="glyphicon glyphicon-home"></span> Acasa</a></li>

				<li><a href="/informatii" ><span class="glyphicon glyphicon-info-sign"></span> Informatii</a></li>

				<li><a href="/informatii/termenisiconditii" ><span class="glyphicon glyphicon-book"></span> Termeni si Conditii</a></li>

				<li><a href="/informatii/contact"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>

				<li><a href="/noutati" ><span class="glyphicon glyphicon-star"></span> Noutati</a></li>

				<li><a href="/promotii"><span class="glyphicon glyphicon-gift"></span> Promotii</a></li>

				<li><a href="/lichidaristoc" ><span class="glyphicon glyphicon-piggy-bank"></span> Lichidari Stoc</a></li>

				<li><a href="/ofertespeciale" ><span class="glyphicon glyphicon-scissors"></span> Discount</a></li>
				@if(Auth::user()->admin == 1)
					<li><a href="/dashboard" target="_blank" ><span class="glyphicon glyphicon-wrench"></span> Dashboard</a></li>
				@endif
				<li><a href="/logout" ><span class="glyphicon glyphicon-log-out"></span> Iesire</a></li>

			</ul>

		</div>



		<div class="head col-md-12 ">

			<div class="row">

				<img src="/thumbs/logo.png" class="col-md-3" id="logo" alt="">

				<div class="col-md-5" id="search">

					<form action="/cauta" method="get">

						<div class="input-group">

							<input type="text" class="form-control" name="search" placeholder="Cauta produs ...">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>

							</span>

						</div>

					</form>	

				</div><!-- /input-group -->

				<div class="salut col-sm-4 ">Salut, <b class="bsalut">{{Auth::user()->nume}} {{Auth::user()->prenume}}</b></div>

				<div class="col-md-4" id="user">

					<div class="col-sm-6">

						<a href="/contulmeu" ><b>Contul Meu</b></a>

						<a href="/contulmeu" ><img src="/thumbs/user (2).png" id="upic" alt=""></a>

					</div>



					<div class="col-sm-6 cosb butcos">

						<a href="/fincomanda" class="cosb" ><b>Cosul Meu</b> <img src="/thumbs/cos (5).png" id="upic" alt=""></a>



						<div class="cart">

							@if(isset($cart))

							@if(empty($cart['nume']))

							<b class="cgol">Caruciorul este gol.</b>

							@else    

							@for($i=count($cart['nume'])-1;$i>=0;$i--)
							<a class="itemca" href="/produs/{{$cart['id'][$i]}}">
							<div class="itemcnoajax">

								<div class="cqty ic"><b>{{$cart['qty'][$i]}}x</b></div>

								@if(strlen($cart['nume'][$i]) > 60)

								<div class="cnume"><b>{{substr($cart['nume'][$i],0,60)}} ...</b></div>	

								@else

								<div class="cnume"><b>{{$cart['nume'][$i]}}</b></div>

								@endif

								<div class="cpret ic">{{$cart['total'][$i]}} RON</div>

								<div class="cdelete ic"><a href="" name="{{$cart['id'][$i]}}" class="cdel"><img src="/thumbs/stergemic.png" alt="Sterge Produs"></a></div>

							</div>
							</a>
							<br class="brnoajax">                                   

							@endfor

							<div class="fincomanda">

								<a href="/fincomanda">Finalizeaza Comanda</a>

							</div> 

							@endif    



							@endif



						</div>



					</div>			





				</div>

			</div>







			<div class="navb">			

				@if(isset($telgsm))

				<ul class="main-navigation">

					<li><a href="/home"><strong>Acasa</strong></a></li>

					<li><a href="/home/telefoanegsm"><strong>Telefoane Mobile / GSM</strong></a>

						<ul>



							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[0]->id}}">{{$telgsm[0]->nume}}</a>

								<div class="menu1">

									@if(isset($componente))

									<div class="colm">

										<a href="/home/titlu/{{$componenteT[0]->id}}" class="colmp titlu">{{$componenteT[0]->nume}}</a>

										@for($x=0;$x<count($componente);$x++)

										@if($componente[$x]->id == 13)

										<a href="/home/categorie/{{$componente[12]->id}}" class="colmp"> {{$componente[12]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[1]->id}}" class="colmp titlu">{{$componenteT[1]->nume}}</a>

										@elseif($componente[$x]->id == 17)

										<a href="/home/categorie/{{$componente[16]->id}}" class="colmp"> {{$componente[16]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[2]->id}}" class="colmp titlu">{{$componenteT[2]->nume}}</a>

										@elseif($componente[$x]->id == 21)

										<a href="/home/categorie/{{$componente[20]->id}}" class="colmp"> {{$componente[20]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[3]->id}}" class="colmp titlu">{{$componenteT[3]->nume}}</a>

										@elseif($componente[$x]->id == 25)

										<a href="/home/categorie/{{$componente[24]->id}}" class="colmp"> {{$componente[24]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[4]->id}}" class="colmp titlu">{{$componenteT[4]->nume}}</a>

										@elseif($componente[$x]->id == 26)

										<a href="/home/categorie/{{$componente[25]->id}}" class="colmp"> {{$componente[25]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[5]->id}}" class="colmp titlu">{{$componenteT[5]->nume}}</a>		

										@else	

										<a href="/home/categorie/{{$componente[$x]->id}}" class="colmp"> {{$componente[$x]->nume}}</a>

										@endif	

										@endfor

									</div><br>

									@endif

								</div>

							</li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[1]->id}}">{{$telgsm[1]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[2]->id}}">{{$telgsm[2]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[3]->id}}">{{$telgsm[3]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Tablete</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[4]->id}}">{{$telgsm[4]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[5]->id}}">{{$telgsm[5]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[6]->id}}">{{$telgsm[6]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>PC / Laptop</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[7]->id}}">{{$telgsm[7]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[8]->id}}">{{$telgsm[8]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[9]->id}}">{{$telgsm[9]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[10]->id}}">{{$telgsm[10]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Service</strong></a>

						<ul>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[11]->id}}">{{$telgsm[11]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[12]->id}}">{{$telgsm[12]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[13]->id}}">{{$telgsm[13]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[14]->id}}">{{$telgsm[14]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[15]->id}}">{{$telgsm[15]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[16]->id}}">{{$telgsm[16]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Diverse</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[17]->id}}">{{$telgsm[17]->nume}}</a></li>

						</ul>

					</li>

				</ul>					

				@endif

			</div>



		</div>



	</header>	



	<div class="col-sm-12" id="content">

	<div class="mininav col-sm-12 ">

			<a role="button" href="" class="btn btn-danger editcont">Editeaza Contul</a>
			<a role="button" href="/istoriccom" class="btn btn-primary">Istoric Comenzi</a>

			


		</div>
		<div class="alert alert-danger col-sm-8 col-sm-offset-2 atentie"style="margin-top:10px;display:none;margin-bottom:0;"  role="alert">
			<h3 style="text-align:center;margin-top:0px;"><span class="glyphicon glyphicon-exclamation-sign" style="margin-top:0" aria-hidden="true"></span>  Atentie</h3>
			<div class="glyphicon glyphicon-arrow-up oblique"></div>
			<h4 style="text-align:center">Aceasta pagina permite editarea contului tau.</h4>
			<h4 style="text-align:center">Daca nu doresti editarea contului apasa "Anuleaza Editarea Contului".</h4>
		</div>
		<div class="col-sm-12 datetotal" style="border:none">
		<form id="editc" method="post">
			<div class="fincom col-sm-12 ">		

				<div class="datepers col-sm-12">

					<div class="datep col-sm-6">
					@if(Auth::user()->demo != 1)
						<h4>Date Personale</h4>

						<b class="bolddate datec">Nume: </b><b class="datec dnume">{{Auth::user()['nume']}}</b><br>
						<label for="ednume" class="edcont" style="display:none"><b class="bolddate">Nume</b></label>	
						<input type="text" class="form-control edcont ednume" id="ednume" name="ednume" style="display:none" placeholder="Nume">
						<b class="bolddate datec">Prenume: </b><b class="datec dprenume">{{Auth::user()['prenume']}}</b><br>
						<label for="edprenume" class="edcont" style="display:none"><b class="bolddate">Prenume</b></label>
						<input type="text" class="form-control edcont edprenume"`id="edprenume" name="edprenume" style="display:none" placeholder="Prenume">	
						<b class="bolddate datec">CNP: </b><b class="datec dcnp">{{Auth::user()['CNP']}}</b><br>
						<label for="edcnp" class="edcont" style="display:none"><b class="bolddate">CNP</b></label>
						<input type="text" class="form-control edcont edcnp" id="edcnp" name="edcnp" style="display:none" placeholder="CNP">	
						<b class="bolddate datec">E-mail: </b><b class="datec demail">{{Auth::user()['email']}}</b><br>
						<label for="edemail" class="edcont" style="display:none"><b class="bolddate">E-mail</b></label>
						<input type="text" disabled="disabled" class="form-control edcont edemail" id="edemail" name="edemail" style="display:none" placeholder="E-mail">
						<b class="bolddate datec">Telefon: </b><b class="datec dtelefon">{{Auth::user()['telefon']}}</b><br>
						<label for="edtelefon" class="edcont" style="display:none"><b class="bolddate">Telefon</b></label>
						<input type="text" class="form-control edcont edtelefon" id="edtelefon" name="edtelefon" style="display:none" placeholder="Telefon">	
					</div>

					<div class="datef col-sm-6 " >

						<h4>Detalii Companie</h4>

						<b class="bolddate datec">Nume Companie: </b><b class="datec dnumecomp">{{Auth::user()['nume_companie']}}</b><br>
						<label for="edtelefon" class="edcont" style="display:none"><b class="bolddate">Nume Companie</b></label>
						<input type="text" class="form-control edcont ednumecomp" id="ednumecomp" name="ednumecomp" style="display:none" placeholder="Nume Companie">	
						<b class="bolddate datec">C.U.I.: </b><b class="datec dcui">{{Auth::user()['CUI']}}</b>	<br>
						<label for="edcui" class="edcont" style="display:none"><b class="bolddate">C.U.I.</b></label>
						<input type="text" class="form-control edcont edcui" id="edcui" name="edcui" style="display:none" placeholder="C.U.I.">	
						<b class="bolddate datec">Nr.ord.reg.com/an: </b><b class="datec dnrreg">{{Auth::user()['nr_reg']}}</b>	<br>
						<label for="ednrreg" class="edcont" style="display:none"><b class="bolddate">Nr.ord.reg.com/an</b></label>
						<input type="text" class="form-control edcont ednrreg" id="ednrreg" name="ednrreg" style="display:none" placeholder="Nr.ord.reg.com/an">	
						<b class="bolddate datec">Banca: </b><b class="datec dbanca">{{Auth::user()['banca']}}</b>	<br>
						<label for="edbanca" class="edcont" style="display:none"><b class="bolddate">Banca</b></label>
						<input type="text" class="form-control edcont edbanca" id="edbanca" name="edbanca" style="display:none" placeholder="Banca">
						<b class="bolddate datec">Cont IBAN: </b><b class="datec dcontib">{{Auth::user()['cont_IBAN']}}</b>	<br>	
						<label for="edcontib" class="edcont" style="display:none"><b class="bolddate">Cont IBAN</b></label>
						<input type="text" class="form-control edcont edcontib" id="edcontib" name="edcontib" style="display:none" placeholder="Cont IBAN">
					</div>

				</div>

				<div class="adrese col-sm-12">

					<div class="sediua col-sm-4">

						<h4>Adresa Sediu Social</h4>

						<b class="bolddate datec">Adresa Sediu Social: </b><b class="datec dass">{{Auth::user()['adresa_sediu']}}</b>	<br>
						<label for="edass" class="edcont" style="display:none"><b class="bolddate">Adresa Sediu Social</b></label>
						<input type="text" class="form-control edcont edass" id="edass" name="edass" style="display:none" placeholder="Adresa Sediu Social">
						<b class="bolddate datec">Localitate Sediu Social: </b><b class="datec dlss">{{Auth::user()['localitate_sediu']}}</b><br>
						<label for="edlss" class="edcont" style="display:none"><b class="bolddate">Localitate Sediu Social</b></label>
						<input type="text" class="form-control edcont edlss" id="edlss" name="edlss" style="display:none" placeholder="Localitate Sediu Social">
						<b class="bolddate datec">Judet/Sector Sediu Social: </b><b class="datec djsss">{{Auth::user()['judet_sector_sediu']}}</b><br>
						<label for="edjsss" class="edcont" style="display:none"><b class="bolddate">Judet/Sector Sediu Social</b></label>
						<input type="text" class="form-control edcont edjsss" id="edjsss" name="edjsss" style="display:none" placeholder="Judet/Sector Sediu Social">
					</div>

					<div class="facturarea col-sm-4">

						<h4>Adresa Facturare</h4>

						<b class="bolddate datec">Adresa Facturare: </b><b class="datec daf">{{Auth::user()['adresa_fac']}}</b><br>
						<label for="eddaf" class="edcont" style="display:none"><b class="bolddate">Adresa Facturare</b></label>
						<input type="text" class="form-control edcont edaf" id="edaf" name="edaf" style="display:none" placeholder="Adresa Facturare">
						<b class="bolddate datec">Localitate Facturare: </b><b class="datec dlf">{{Auth::user()['localitate_fac']}}</b><br>
						<label for="eddlf" class="edcont" style="display:none"><b class="bolddate">Localitate Facturare</b></label>
						<input type="text" class="form-control edcont edlf" id="edlf" name="edlf" style="display:none" placeholder="Localitate Facturare">
						<b class="bolddate datec">Judet/Sector Facturare: </b><b class="datec djsf">{{Auth::user()['judet_sector_fac']}}</b>	<br>
						<label for="eddjsf" class="edcont" style="display:none"><b class="bolddate">Judet/Sector Facturare</b></label>
						<input type="text" class="form-control edcont edjsf" id="edjsf" name="edjsf" style="display:none" placeholder="Judet/Sector Facturare">
					</div>

					<div class="livrarea col-sm-4">

						<h4>Adresa Livrare</h4>

						<b class="bolddate datec">Adresa Livrare: </b><b class="datec dal">{{Auth::user()['adresa_liv']}}</b>	<br>
						<label for="edal" class="edcont" style="display:none"><b class="bolddate">Adresa Livrare</b></label>
						<input type="text" class="form-control edcont edal" id="edal" name="edal" style="display:none" placeholder="Adresa Livrare">
						<b class="bolddate datec">Localitate Livrare: </b><b class="datec dll">{{Auth::user()['localitate_liv']}}</b>	<br>
						<label for="edll" class="edcont" style="display:none"><b class="bolddate">Localitate Livrare</b></label>
						<input type="text" class="form-control edcont edll" id="edll" name="edll" style="display:none" placeholder="Localitate Livrare">
						<b class="bolddate datec">Judet/Sector Livrare: </b><b class="datec djsl">{{Auth::user()['judet_sector_liv']}}</b><br>
						<label for="edjsl" class="edcont" style="display:none"><b class="bolddate">Judet/Sector Livrare</b></label>
						<input type="text" class="form-control edcont edjsl" id="edjsl" name="edjsl" style="display:none" placeholder="Judet/Sector Livrare">
					</div>

				</div>
				@else
											<h4>Date Personale</h4>

						<b class="bolddate datec">Nume: </b><b class="datec dnume">{{Auth::user()['nume']}}</b><br>
						<label for="ednume" class="edcont" style="display:none"><b class="bolddate">Nume</b></label>	
						<input type="text" class="form-control edcont ednume"disabled id="ednume" name="ednume" style="display:none" placeholder="Nume">
						<b class="bolddate datec">Prenume: </b><b class="datec dprenume">{{Auth::user()['prenume']}}</b><br>
						<label for="edprenume" class="edcont" style="display:none"><b class="bolddate">Prenume</b></label>
						<input type="text" class="form-control edcont edprenume"disabled id="edprenume" name="edprenume" style="display:none" placeholder="Prenume">	
						<b class="bolddate datec">CNP: </b><b class="datec dcnp">{{Auth::user()['CNP']}}</b><br>
						<label for="edcnp" class="edcont" style="display:none"><b class="bolddate">CNP</b></label>
						<input type="text" class="form-control edcont edcnp"disabled id="edcnp" name="edcnp" style="display:none" placeholder="CNP">	
						<b class="bolddate datec">E-mail: </b><b class="datec demail">{{Auth::user()['email']}}</b><br>
						<label for="edemail" class="edcont" style="display:none"><b class="bolddate">E-mail</b></label>
						<input type="text" disabled="disabled" class="form-control edcont edemail" id="edemail" name="edemail" style="display:none" placeholder="E-mail">
						<b class="bolddate datec">Telefon: </b><b class="datec dtelefon">{{Auth::user()['telefon']}}</b><br>
						<label for="edtelefon" class="edcont" style="display:none"><b class="bolddate">Telefon</b></label>
						<input type="text" disabled class="form-control edcont edtelefon" id="edtelefon" name="edtelefon" style="display:none" placeholder="Telefon">	
					</div>

					<div class="datef col-sm-6 " >

						<h4>Detalii Companie</h4>

						<b class="bolddate datec">Nume Companie: </b><b class="datec dnumecomp">{{Auth::user()['nume_companie']}}</b><br>
						<label for="edtelefon" class="edcont" style="display:none"><b class="bolddate">Nume Companie</b></label>
						<input type="text" disabled class="form-control edcont ednumecomp" id="ednumecomp" name="ednumecomp" style="display:none" placeholder="Nume Companie">	
						<b class="bolddate datec">C.U.I.: </b><b class="datec dcui">{{Auth::user()['CUI']}}</b>	<br>
						<label for="edcui" class="edcont" style="display:none"><b class="bolddate">C.U.I.</b></label>
						<input type="text"disabled class="form-control edcont edcui" id="edcui" name="edcui" style="display:none" placeholder="C.U.I.">	
						<b class="bolddate datec">Nr.ord.reg.com/an: </b><b class="datec dnrreg">{{Auth::user()['nr_reg']}}</b>	<br>
						<label for="ednrreg" class="edcont" style="display:none"><b class="bolddate">Nr.ord.reg.com/an</b></label>
						<input type="text"disabled class="form-control edcont ednrreg" id="ednrreg" name="ednrreg" style="display:none" placeholder="Nr.ord.reg.com/an">	
						<b class="bolddate datec">Banca: </b><b class="datec dbanca">{{Auth::user()['banca']}}</b>	<br>
						<label for="edbanca" class="edcont" style="display:none"><b class="bolddate">Banca</b></label>
						<input type="text"disabled class="form-control edcont edbanca" id="edbanca" name="edbanca" style="display:none" placeholder="Banca">
						<b class="bolddate datec">Cont IBAN: </b><b class="datec dcontib">{{Auth::user()['cont_IBAN']}}</b>	<br>	
						<label for="edcontib" class="edcont" style="display:none"><b class="bolddate">Cont IBAN</b></label>
						<input type="text"disabled class="form-control edcont edcontib" id="edcontib" name="edcontib" style="display:none" placeholder="Cont IBAN">
					</div>

				</div>

				<div class="adrese col-sm-12">

					<div class="sediua col-sm-4">

						<h4>Adresa Sediu Social</h4>

						<b class="bolddate datec">Adresa Sediu Social: </b><b class="datec dass">{{Auth::user()['adresa_sediu']}}</b>	<br>
						<label for="edass" class="edcont" style="display:none"><b class="bolddate">Adresa Sediu Social</b></label>
						<input type="text"disabled class="form-control edcont edass" id="edass" name="edass" style="display:none" placeholder="Adresa Sediu Social">
						<b class="bolddate datec">Localitate Sediu Social: </b><b class="datec dlss">{{Auth::user()['localitate_sediu']}}</b><br>
						<label for="edlss" class="edcont" style="display:none"><b class="bolddate">Localitate Sediu Social</b></label>
						<input type="text"disabled class="form-control edcont edlss" id="edlss" name="edlss" style="display:none" placeholder="Localitate Sediu Social">
						<b class="bolddate datec">Judet/Sector Sediu Social: </b><b class="datec djsss">{{Auth::user()['judet_sector_sediu']}}</b><br>
						<label for="edjsss" class="edcont" style="display:none"><b class="bolddate">Judet/Sector Sediu Social</b></label>
						<input type="text"disabled class="form-control edcont edjsss" id="edjsss" name="edjsss" style="display:none" placeholder="Judet/Sector Sediu Social">
					</div>

					<div class="facturarea col-sm-4">

						<h4>Adresa Facturare</h4>

						<b class="bolddate datec">Adresa Facturare: </b><b class="datec daf">{{Auth::user()['adresa_fac']}}</b><br>
						<label for="eddaf" class="edcont" style="display:none"><b class="bolddate">Adresa Facturare</b></label>
						<input type="text"disabled class="form-control edcont edaf" id="edaf" name="edaf" style="display:none" placeholder="Adresa Facturare">
						<b class="bolddate datec">Localitate Facturare: </b><b class="datec dlf">{{Auth::user()['localitate_fac']}}</b><br>
						<label for="eddlf" class="edcont" style="display:none"><b class="bolddate">Localitate Facturare</b></label>
						<input type="text"disabled class="form-control edcont edlf" id="edlf" name="edlf" style="display:none" placeholder="Localitate Facturare">
						<b class="bolddate datec">Judet/Sector Facturare: </b><b class="datec djsf">{{Auth::user()['judet_sector_fac']}}</b>	<br>
						<label for="eddjsf" class="edcont" style="display:none"><b class="bolddate">Judet/Sector Facturare</b></label>
						<input type="text"disabled class="form-control edcont edjsf" id="edjsf" name="edjsf" style="display:none" placeholder="Judet/Sector Facturare">
					</div>

					<div class="livrarea col-sm-4">

						<h4>Adresa Livrare</h4>

						<b class="bolddate datec">Adresa Livrare: </b><b class="datec dal">{{Auth::user()['adresa_liv']}}</b>	<br>
						<label for="edal" class="edcont" style="display:none"><b class="bolddate">Adresa Livrare</b></label>
						<input type="text"disabled class="form-control edcont edal" id="edal" name="edal" style="display:none" placeholder="Adresa Livrare">
						<b class="bolddate datec">Localitate Livrare: </b><b class="datec dll">{{Auth::user()['localitate_liv']}}</b>	<br>
						<label for="edll" class="edcont" style="display:none"><b class="bolddate">Localitate Livrare</b></label>
						<input type="text"disabled class="form-control edcont edll" id="edll" name="edll" style="display:none" placeholder="Localitate Livrare">
						<b class="bolddate datec">Judet/Sector Livrare: </b><b class="datec djsl">{{Auth::user()['judet_sector_liv']}}</b><br>
						<label for="edjsl" class="edcont" style="display:none"><b class="bolddate">Judet/Sector Livrare</b></label>
						<input type="text"disabled class="form-control edcont edjsl" id="edjsl" name="edjsl" style="display:none" placeholder="Judet/Sector Livrare">
					</div>

				</div>
				@endif
			</div>
			<input type="hidden" name="edid" value="{{Auth::user()['id']}}">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<a  href="" style="font-size:20px;display:none;" class="btn btn-success col-sm-8 col-sm-offset-2 edsubmit"><span style="margin-right:20px;" class="glyphicon glyphicon-ok"></span> Salveaza Modificarile Efectuate <span class="glyphicon glyphicon-ok" style="margin-left:20px;"></span></a>	
		</form>
		</div>


		<div class="modal fade" id="editcontatentie" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h3 style="text-align:center;color:#098ead;" class="modtitlu">Atentie</h3>
					</div>
					<div class="modal-body">
						<h5 style="text-align:center;color:#f5811e;margin-bottom:10px;" class="modintreb"><b>Esti sigur ca doresti modificarea datelor contului tau ?</b></h5>
						<div class="form-group butvalid" style="margin-left:150px;">
							<button class="btn btn-success okedit" style="margin-left:30px;">Doresc</button>
							<button class="btn btn-danger noedit" style="margin-left:30px;">Nu Doresc</button>
						</div>

						<div class="progress"   style="background-color:#098ead;display:none;">
							<div class="progress-bar econt" data-transitiongoal="100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background-color:#f5811e;">
							</div>		
						</div>

						<h5 class="ecomplete" style="text-align:center;color:#f5811e;display:none;"><b>Editare completa.</b></h5>			
					</div>
				</div>
			</div>	
		</div>
		

		@include('partiale/footer')	











				<script src="/js/jquery.js"></script>

				<script src="/bootstrap/js/bootstrap.min.js"></script>

				<script type="text/javascript" src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>

				<script src="/js/cart.js"></script>

				<script src="/js/prodfin.js"></script>
				<script>
					$(document).ready(function(){


						$('.editcont').click(function(e){
							e.preventDefault();
							
							$('.modtitlu').text('Atentie');
							$('.modintreb').show();
							$('.butvalid').show();
							$('.progress').hide();
							$('.econt').css('width','0%');

							if(!$(this).hasClass('active')){
								$('.atentie').fadeIn(200);
								$(this).addClass('active');
								$(this).text('Anuleaza Editarea Contului');
								$('.datec').fadeOut(100);
								$('.edcont').fadeIn(100);
								$('.ednume').val($('.dnume').text());
								$('.edprenume').val($('.dprenume').text());
								$('.edcnp').val($('.dcnp').text());
								$('.edemail').val($('.demail').text());
								$('.edtelefon').val($('.dtelefon').text());
								$('.ednumecomp').val($('.dnumecomp').text());
								$('.edcui').val($('.dcui').text());
								$('.ednrreg').val($('.dnrreg').text());
								$('.edbanca').val($('.dbanca').text());
								$('.edcontib').val($('.dcontib').text());
								$('.edass').val($('.dass').text());
								$('.edlss').val($('.dlss').text());
								$('.edjsss').val($('.djsss').text());
								$('.edaf').val($('.daf').text());
								$('.edlf').val($('.dlf').text());
								$('.edjsf').val($('.djsf').text());
								$('.edal').val($('.dal').text());
								$('.edll').val($('.dll').text());
								$('.edjsl').val($('.djsl').text());
								$('.edsubmit').fadeIn(100);
							}else{
								$('.atentie').fadeOut(200);
								$(this).removeClass('active');
								$(this).text('Editeaza Contul');
								$('.datec').fadeIn(100);
								$('.edcont').fadeOut(100);
								$('.edsubmit').fadeOut(100);
							}
						});
						
						$('.edsubmit').click(function(e){
							e.preventDefault();
							$('#editcontatentie').modal({
								backdrop: 'static',
    							keyboard: false
							});
							$('#editcontatentie').modal('show');
							$('.noedit').click(function(){
								$('.ecomplete').hide();
								$('.atentie').fadeOut(200);
								$('.editcont').removeClass('active');
								$('.editcont').text('Editeaza Contul');
								$('.datec').fadeIn(100);
								$('.edcont').fadeOut(100);
								$('.edsubmit').fadeOut(100);
								$('#editcontatentie').modal('hide');
							});
							$('.okedit').click(function(){
								$('.modtitlu').text('Te rog asteapta');
								$('.modintreb').hide();
								$('.butvalid').hide();
								$('.progress').fadeIn(200);
								$('.progress .progress-bar').progressbar({
									transition_delay: 1000
								});
								setTimeout(function(){
									$('.ecomplete').show();
								},1500);
								$.post('/editcont',$('#editc').serialize()).done(function(data){
									$.each(data,function(i,v){
										//console.log(v);
										$('.dnume').text(v.nume);
										$('.dprenume').text(v.prenume);
										$('.dcnp').text(v.CNP);
										$('.demail').text(v.email);
										$('.dtelefon').text(v.telefon);
										$('.dnumecomp').text(v.nume_companie);
										$('.dcui').text(v.CUI);
										$('.dnrreg').text(v.nr_reg);
										$('.dbanca').text(v.banca);
										$('.dcontib').text(v.cont_IBAN);
										$('.dass').text(v.adresa_sediu);
										$('.dlss').text(v.localitate_sediu);
										$('.djsss').text(v.judet_sector_sediu);
										$('.daf').text(v.adresa_fac);
										$('.dlf').text(v.localitate_liv);
										$('.djsf').text(v.judet_sector_fac);
										$('.dal').text(v.adresa_liv);
										$('.dll').text(v.localitate_liv);
										$('.djsl').text(v.judet_sector_liv);
									});
								});	
									
									setTimeout(function(){
										$('.atentie').fadeOut(200);
										$('.editcont').removeClass('active');
										$('.editcont').text('Editeaza Contul');
										$('.datec').fadeIn(100);
										$('.edcont').fadeOut(100);
										$('.edsubmit').fadeOut(100);
										$('.ecomplete').hide();
										
									},2000);	
									setTimeout(function(){
										$('#editcontatentie').modal('hide');
									},2000);
								
							});
							
						});
					});
				</script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
			</body>

			</html>
