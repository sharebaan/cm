<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="/css/home.css">

	<link rel="stylesheet" href="/bootstrap/bootstrap-slider/dist/css/bootstrap-slider.min.css">

	<link rel="stylesheet" href="/slick/slick.css">
	<link rel="stylesheet" href="/slick/slick-theme.css">

	<title>CellMall</title>

</head>

<body>

	

<header >

<div class="nav-center col-md-12 " >

			<ul class="nav nav-pills">  

				<li><a href="/home"><span class="glyphicon glyphicon-home"></span> Acasa</a></li>

				<li><a href="/informatii" ><span class="glyphicon glyphicon-info-sign"></span> Informatii</a></li>

				<li><a href="/informatii/termenisiconditii" ><span class="glyphicon glyphicon-book"></span> Termeni si Conditii</a></li>

				<li><a href="/informatii/contact"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>

				<li><a href="/noutati" ><span class="glyphicon glyphicon-star"></span> Noutati</a></li>

				<li><a href="/promotii"><span class="glyphicon glyphicon-gift"></span> Promotii</a></li>

				<li><a href="/lichidaristoc" ><span class="glyphicon glyphicon-piggy-bank"></span> Lichidari Stoc</a></li>

				<li><a href="/ofertespeciale" ><span class="glyphicon glyphicon-scissors"></span> Discount</a></li>
				@if(Auth::user()->admin == 1)
					<li><a href="/dashboard" target="_blank" ><span class="glyphicon glyphicon-wrench"></span> Dashboard</a></li>
				@endif	
				<li><a href="/logout" ><span class="glyphicon glyphicon-log-out"></span> Iesire</a></li>

			</ul>

		</div>



		<div class="head col-md-12 ">

			<div class="row">

				<img src="/thumbs/logo.png" class="col-md-3" id="logo" alt="">

				<div class="col-md-5" id="search">

					<form action="/cauta" method="get">

						<div class="input-group">

							<input type="text" class="form-control" name="search" placeholder="Cauta produs ...">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>

							</span>

						</div>

					</form>	

				</div><!-- /input-group -->

				<div class="salut col-sm-4 ">Salut, <b class="bsalut">{{Auth::user()->nume}} {{Auth::user()->prenume}}</b></div>

				<div class="col-md-4" id="user">

					<div class="col-sm-6">

						<a href="/contulmeu" ><b>Contul Meu</b></a>

						<a href="/contulmeu" ><img src="/thumbs/user (2).png" id="upic" alt=""></a>

					</div>



					<div class="col-sm-6 cosb butcos">

						<a href="/fincomanda" class="cosb" ><b>Cosul Meu</b> <img src="/thumbs/cos (5).png" id="upic" alt=""></a>



						<div class="cart">

							@if(isset($cart))

							@if(empty($cart['nume']))

							<b class="cgol">Caruciorul este gol.</b>

							@else    

							@for($i=count($cart['nume'])-1;$i>=0;$i--)
							<a class="itemca" href="/produs/{{$cart['id'][$i]}}">
							<div class="itemcnoajax">

								<div class="cqty ic"><b>{{$cart['qty'][$i]}}x</b></div>

								@if(strlen($cart['nume'][$i]) > 60)

								<div class="cnume"><b>{{substr($cart['nume'][$i],0,60)}} ...</b></div>	

								@else

								<div class="cnume"><b>{{$cart['nume'][$i]}}</b></div>

								@endif

								<div class="cpret ic">{{$cart['total'][$i]}} RON</div>

								<div class="cdelete ic"><a href="" name="{{$cart['id'][$i]}}" class="cdel"><img src="/thumbs/stergemic.png" alt="Sterge Produs"></a></div>

							</div>
							</a>
							<br class="brnoajax">                                   

							@endfor

							<div class="fincomanda">

								<a href="/fincomanda">Finalizeaza Comanda</a>

							</div> 

							@endif    



							@endif



						</div>



					</div>			





				</div>

			</div>







			<div class="navb">			

				@if(isset($telgsm))

				<ul class="main-navigation">

					<li><a href="/home"><strong>Acasa</strong></a></li>

					<li><a href="/home/telefoanegsm"><strong>Telefoane Mobile / GSM</strong></a>

						<ul>



							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[0]->id}}">{{$telgsm[0]->nume}}</a>

								<div class="menu1">

									@if(isset($componente))

									<div class="colm">

										<a href="/home/titlu/{{$componenteT[0]->id}}" class="colmp titlu">{{$componenteT[0]->nume}}</a>

										@for($x=0;$x<count($componente);$x++)

										@if($componente[$x]->id == 13)

										<a href="/home/categorie/{{$componente[12]->id}}" class="colmp"> {{$componente[12]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[1]->id}}" class="colmp titlu">{{$componenteT[1]->nume}}</a>

										@elseif($componente[$x]->id == 17)

										<a href="/home/categorie/{{$componente[16]->id}}" class="colmp"> {{$componente[16]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[2]->id}}" class="colmp titlu">{{$componenteT[2]->nume}}</a>

										@elseif($componente[$x]->id == 21)

										<a href="/home/categorie/{{$componente[20]->id}}" class="colmp"> {{$componente[20]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[3]->id}}" class="colmp titlu">{{$componenteT[3]->nume}}</a>

										@elseif($componente[$x]->id == 25)

										<a href="/home/categorie/{{$componente[24]->id}}" class="colmp"> {{$componente[24]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[4]->id}}" class="colmp titlu">{{$componenteT[4]->nume}}</a>

										@elseif($componente[$x]->id == 26)

										<a href="/home/categorie/{{$componente[25]->id}}" class="colmp"> {{$componente[25]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[5]->id}}" class="colmp titlu">{{$componenteT[5]->nume}}</a>		

										@else	

										<a href="/home/categorie/{{$componente[$x]->id}}" class="colmp"> {{$componente[$x]->nume}}</a>

										@endif	

										@endfor

									</div><br>

									@endif

								</div>

							</li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[1]->id}}">{{$telgsm[1]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[2]->id}}">{{$telgsm[2]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[3]->id}}">{{$telgsm[3]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Tablete</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[4]->id}}">{{$telgsm[4]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[5]->id}}">{{$telgsm[5]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[6]->id}}">{{$telgsm[6]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>PC / Laptop</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[7]->id}}">{{$telgsm[7]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[8]->id}}">{{$telgsm[8]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[9]->id}}">{{$telgsm[9]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[10]->id}}">{{$telgsm[10]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Service</strong></a>

						<ul>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[11]->id}}">{{$telgsm[11]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[12]->id}}">{{$telgsm[12]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[13]->id}}">{{$telgsm[13]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[14]->id}}">{{$telgsm[14]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[15]->id}}">{{$telgsm[15]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[16]->id}}">{{$telgsm[16]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Diverse</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[17]->id}}">{{$telgsm[17]->nume}}</a></li>

						</ul>

					</li>

				</ul>					

				@endif

			</div>



		</div>



	</header>

					

			@if(Session::has('err'))

               <div class="alert alert-danger col-sm-8 col-sm-offset-2" role="alert">

                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>

                    <span class="sr-only">Eroare:</span>

                    	{!!Session::get('err')!!}

                    

               </div>

               @endif



               



		

		<div class="col-sm-12" id="content">

			<div class="fincom col-sm-12 ">

				<div class="comtitle">

					<div class="comimg tit"></div>

					<div class="comprod tit">Denumire Produs</div>
					
					<div class="comcod tit">Cod</div>

					<div class="comqty tit" >Cantitate</div>

					<div class="compretbuc tit">Pret/buc</div>

					<div class="comtotal tit">Subtotal</div>

				</div>

				

				<div class="cosmare">

				@if(isset($cart))

					<form action="/cumpara" method="post" id="cumpara">

						@for($i=count($cart['nume'])-1;$i>=0;$i--)					

							<div class="rowitem">

								@if($cart['poza'][$i] == '')

								<div class="comimg">

									<a href="/produs/{{$cart['id'][$i]}}"><img src="/products/no_image.png" alt="" class="img-rounded"></a>

								</div>

								@else

								<div class="comimg">

									<a href="/produs/{{$cart['id'][$i]}}"><img src="{{$cart['poza'][$i]}}" alt="" class="img-rounded"></a>

								</div>

								@endif

							<div class="comprod"><a href="/produs/{{$cart['id'][$i]}}"><b class="npcm">{{$cart['nume'][$i]}}</b></a>

								<input type="hidden" name="comprod{{$i}}" value="{{$cart['nume'][$i]}}">

							</div>
							<div class="comcod">
								{{$cart['cod'][$i]}}								
							</div>
							<div class="comqty">

								<input type="hidden" name="comqty{{$i}}" value="{{$cart['qty'][$i]}}">

								<div class="form-group qtygroupfin">

									<div class="btn-group" style="float:left">									

										<button class="btn btn-default plusminfin minusfin" value="-" type="button" >-</button>

									</div>

									<input type="text" class="form-control qtyfin" style="font-weight:bold;" disabled value="{{$cart['qty'][$i]}}">

									<div class="btn-group">

										<button class="btn btn-default plusminfin plusfin" value="+" type="button" >+</button>

									</div>

								</div>

							</div>

							<div class="compretbuc" ><p class="pretbuc">{{$cart['total'][$i] / $cart['qty'][$i]}}</p><p>RON</p>

								<input type="hidden" name="compretbuc{{$i}}" value="{{$cart['total'][$i] / $cart['qty'][$i]}}">

							</div>

							<div class="comtotal"><p class="ptotal">{{$cart['total'][$i]}}</p><p>RON</p></div>

							<div class="comsterge"><a href="" name="{{$cart['id'][$i]}}" class="stergemare"><img src="/thumbs/stergemare.png" alt="Sterge Produs"></a></div> 

							</div>

														

						@endfor	

						

						<div class="col-sm-12 datetotal">

							<div class="totcos col-sm-12">

								<div class="totalp col-sm-12">

									<h4 class="totptit tsus">Total Cos (TVA inclus): </h4> <h4 class="tpunit">RON</h4><h4 class="tp" id="totp">{{$cart['totalp']}}</h4>

								</div>

							</div>

							<div class="costt col-sm-12">

								<div class="totalp col-sm-12">

									<h4 class="totptit tsus">Cost Transport: </h4> <h4 class="tpunit">RON</h4><h4 class="tpc" id="ctranssus">0</h4>

								</div>

							</div>

							<div class="totcom col-sm-12">

								<div class="totalp col-sm-12">

									<h4 class="totptit tsus">Total de Plata (TVA inclus): </h4> <h4 class="tpunit">RON</h4><h4 class="tp" id="tottvasus"></h4>

								</div>

							</div>

							<div class="butcom col-sm-12 col-sm-offset-2">

								<a href="/home" role="button" class="btn btn-warning inapoicumpsus ">Inapoi la Cumparaturi</a>

								<div class="comandas" style="display:none;"><a type="button" href="#" class="btn btn-primary comandasus">Finalizeaza Comanda</a></div>

							</div>

						</div>



						<div class="datepers col-sm-12" >

							<div class="datep col-sm-6">

								<h4>Date Personale</h4>

								<b class="bolddate">Nume: </b><b >{{Auth::user()['nume']}}</b><br>

								<b class="bolddate">Prenume: </b><b >{{Auth::user()['prenume']}}</b><br>

								<b class="bolddate">CNP: </b><b>{{Auth::user()['CNP']}}</b><br>

								<b class="bolddate">Email: </b><b>{{Auth::user()['email']}}</b><br>

								<b class="bolddate">Telefon: </b><b>{{Auth::user()['telefon']}}</b><br>

							</div>

							<div class="datef col-sm-6 " >

								<h4>Detalii Companie</h4>

								<b class="bolddate">Nume Companie: </b><b>{{Auth::user()['nume_companie']}}</b><br>

								<b class="bolddate">C.U.I.: </b><b>{{Auth::user()['CUI']}}</b>	<br>

								<b class="bolddate">Nr.ord.reg.com/an: </b><b>{{Auth::user()['nr_reg']}}</b>	<br>

								<b class="bolddate">Banca: </b><b>{{Auth::user()['banca']}}</b>	<br>

								<b class="bolddate">Cont IBAN: </b><b>{{Auth::user()['cont_IBAN']}}</b>	<br>	

							</div>

						</div>

						<div class="adrese col-sm-12" >

							<div class="sediua col-sm-4">

								<h4>Adresa Sediu Social</h4>

								<b class="bolddate">Adresa Sediu Social: </b><b>{{Auth::user()['adresa_sediu']}}</b>	<br>

								<b class="bolddate">Localitate Sediu Social: </b><b>{{Auth::user()['localitate_sediu']}}</b><br>

								<b class="bolddate">Judet/Sector Sediu Social: </b><b>{{Auth::user()['judet_sector_sediu']}}</b><br>

							</div>

							<div class="facturarea col-sm-4">

								<h4>Adresa Facturare</h4>

								<b class="bolddate">Adresa Facturare: </b><b>{{Auth::user()['adresa_fac']}}</b><br>

								<b class="bolddate">Localitate Facturare: </b><b>{{Auth::user()['localitate_fac']}}</b><br>

								<b class="bolddate">Judet/Sector Facturare: </b><b>{{Auth::user()['judet_sector_fac']}}</b>	<br>

							</div>

							<div class="livrarea col-sm-4">

								<h4>Adresa Livrare</h4>

								<b class="bolddate">Adresa Livrare: </b><b>{{Auth::user()['adresa_liv']}}</b>	<br>

								<b class="bolddate">Localitate Livrare: </b><b>{{Auth::user()['localitate_liv']}}</b>	<br>

								<b class="bolddate">Judet/Sector Livrare: </b><b>{{Auth::user()['judet_sector_liv']}}</b><br>

							</div>

						</div>

						<div class="optiuniplata col-sm-12">

							<div class="metplata col-sm-4">
								<small class="pasi">Primul pas <span class="glyphicon glyphicon-circle-arrow-right"></span></small>
								<h4>Alege metoda de plata</h4>

								<div class="radio col-sm-12">

									<label for="numerar">

										<input type="radio" class="radio mp" name="metplata" id="numerar" value="Numerar">

										Numerar</label>

										<div class="small col-sm-12">

											<small>Achitare comanda la sediul nostru (doar persoane juridice)</small>

										</div>	

								</div>

									

								<div class="radio col-sm-12">

									<label for="numerarcn">

										<input type="radio" class="radio mp" name="metplata" id="numerarcn" value="Numerar (Contul Nostru)">

										Numerar in contul nostru</label>

										<div class="small col-sm-12">

											<small>Depunere numerar direct in contul:</small>

										</div>	

								</div>

									<div class="form-group col-sm-8 contbanca1" >

										<select name="contbanca1" id="contbanca1" class="form-control">

											<option value="" selected="selected">Libra Internet Bank SA</option>

											<option value="">Banca Transilvania SA</option>

										</select>

									</div>



									<div class="radio col-sm-12">

										<label for="op">

											<input type="radio" class="radio mp"  name="metplata" id="op" value="Ordin Plata">

											Ordin de plata</label>

											<div class="small col-sm-12">

												<small>Plata in avans in contul:</small>

											</div>

									</div>

										<div class="form-group col-sm-8 contbanca2" >

											<select name="contbanca2" id="contbanca2" class="form-control">

												<option value="" selected="selected">Libra Internet Bank SA</option>

												<option value="">Banca Transilvania SA</option>

											</select>

										</div>								

										<div class="radio col-sm-12">

											<label for="virb">

												<input type="radio" class="radio mp" name="metplata" id="virb" value="Virament Bancar">

												Virament Bancar</label>

												<div class="small col-sm-12">

													<small>Din contul dumneavoastra in contul:</small>

												</div>

										</div>

											<div class="form-group col-sm-8 contbanca3">

												<select name="contbanca3" id="contbanca3" class="form-control">

													<option value="" selected="selected">Libra Internet Bank SA</option>

													<option value="">Banca Transilvania SA</option>

												</select>

											</div>

											<div class="radio col-sm-12">

												<label for="ramburs">

													<input type="radio" class="radio mp" name="metplata" id="ramburs" value="Ramburs">

													Ramburs</label>

													<div class="small col-sm-12">

														<small>Plata la livrare</small>

													</div>

											</div>

											



							</div>

							<div class="metliv col-sm-4" style="display:none;">

								<h4>Alege metoda de livrare</h4>

								<div class="radio col-sm-12">

									<label for="lasediu">

									<input type="radio" class="radio" name="metliv" id="lasediu" value="La Sediu">

										Ridicare la sediul firmei</label>

										<div class="small col-sm-12">

											<small>0 RON</small>

										</div>	

									</div>

								<div class="radio col-sm-12">

									<label for="curbuc">

									<input type="radio" class="radio" name="metliv" id="curbuc" value="Curier Bucuresti">

										Curier Bucuresti</label>

									<div class="small col-sm-12">

											<small>Transport gratuit (Livrare in aceeasi zi pentru comenzile efectuate pana ora 14:00)</small>

										</div>		

									</div>

								<div class="radio col-sm-12">

									<label for="fancur">

									<input type="radio" class="radio" name="metliv" id="fancur" value="Fan Curier">

										Fan Curier</label>

										<div class="small col-sm-12">

											<small>17 RON pentru orice localitate din tara (Livrare 24h-48h)</small>

										</div>			

									</div>

								<div class="radio col-sm-12">

									<label for="cargus">

									<input type="radio" class="radio" name="metliv" id="cargus" value="Cargus">

										Cargus</label>

										<div class="small col-sm-12">

											<small>17 RON pentru orice localitate din tara (Livrare 24h-48h)</small>

										</div>			

									</div>											

							</div>

							<div class="tipfac col-sm-4" style="display:none;">

								<h4>Alege tipul de facturare</h4>

								<div class="radio col-sm-12">

									<label for="pf">

									<input type="radio" class="radio" name="tipfac" id="pf" value="Persoana Fizica">

										Persoana Fizica</label>	

								</div>

								<div class="radio col-sm-12">

									<label for="pj">

									<input type="radio" class="radio" name="tipfac" id="pj" value="Persoana Juridica">

										Persoana Juridica</label>	

								</div>
								<br><br><br>
								<div class="radio col-sm-12">
									<a href="" class="resetop"> <img src="/thumbs/refresh.png" class="resetop" style="width:32px;" alt="Reseteaza optiunile">	Reseteaza optiunile</a>
								</div>
							</div>



						</div>

						<div class="mentotcump col-sm-12" >

							<div class="mentiune col-sm-6">

								<h4>Adauga mentiuni</h4>

								<textarea name="mentliv" placeholder="Scrie aici alte mentiuni legate de comanda ta (puncte de reper, intervale orare distincte, etc) ..." class="form-control" id="mentiuniarea" cols="30" rows="10"></textarea>

							</div>

							<div class="cump col-sm-6" >

								<div class="totalp col-sm-12">

									<h4 class="totptit">Total Cos (TVA inclus): </h4> <h4 class="tpunit">RON</h4><h4 class="tp" id="totp">{{$cart['totalp']}}</h4>

								</div>

								<div class="totalp col-sm-12">

									<h4 class="totptit">Cost Transport: </h4> <h4 class="tpunit">RON</h4><h4 class="tpc" id="ctrans">0</h4>

								</div>

								<div class="totalp col-sm-12">

									<h4 class="totptit">Total de Plata (TVA inclus): </h4> <h4 class="tpunit">RON</h4><h4 class="tp" id="tottva"></h4>

								</div>

								
								<div class="tercon col-sm-12 form-group"><label for="checkterm" style="cursor:pointer;">Prin trimiterea comenzii se accepta <a href="/informatii/termenisiconditii">Termenii si Conditiile</a> CellMall.ro </label><input type="checkbox"value='1'style="margin-left:5px;" name="acceptterm" id="checkterm" class="form-controll"></div>
								<a href="/home" role="button" class="btn btn-warning inapoicump">Inapoi la Cumparaturi</a>

								<div class="comanda"><button type="submit" class="btn btn-primary comanda">Trimite Comanda</button></div>
								
							</div>

						</div>

						
						<input type="hidden" name="totalp" id="totalpinput" value="{{$cart['totalp']}}"> 

						<input type="hidden" name="_token" value="{{csrf_token()}}">

					</form>

					

				@endif	

				</div>

			</div>

		</div>

	

		

@include('partiale/footer')			

		

		







<script src="/js/jquery.js"></script>

<script src="/js/jnumber.min.js"></script>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>

<script src="/js/cart.js"></script>

<script src="/js/prodfin.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
</body>

</html>
