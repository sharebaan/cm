<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" href="/css/home.css">

	<title>CellMall</title>

</head>

<body>

	









	<header >

<div class="nav-center col-md-12 " >

			<ul class="nav nav-pills">  

				<li><a href="/home"><span class="glyphicon glyphicon-home"></span> Acasa</a></li>

				<li><a href="/informatii" ><span class="glyphicon glyphicon-info-sign"></span> Informatii</a></li>

				<li><a href="/informatii/termenisiconditii" ><span class="glyphicon glyphicon-book"></span> Termeni si Conditii</a></li>

				<li><a href="/informatii/contact"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>

				<li><a href="/produsnoutati" ><span class="glyphicon glyphicon-star"></span> Noutati</a></li>

				<li><a href="/produspromotii"><span class="glyphicon glyphicon-gift"></span> Promotii</a></li>

				<li><a href="/lichidaristoc" ><span class="glyphicon glyphicon-piggy-bank"></span> Lichidari Stoc</a></li>

				<li><a href="/ofertespeciale" ><span class="glyphicon glyphicon-scissors"></span> Discount</a></li>
				@if(Auth::user()->admin == 1)
					<li><a href="/dashboard" target="_blank" ><span class="glyphicon glyphicon-wrench"></span> Dashboard</a></li>
				@endif
				<li><a href="/logout" ><span class="glyphicon glyphicon-log-out"></span> Iesire</a></li>

			</ul>

		</div>



		<div class="head col-md-12 ">

			<div class="row">

				<img src="/thumbs/logo.png" class="col-md-3" id="logo" alt="">

				<div class="col-md-5" id="search">

					<form action="/cauta" method="get">

						<div class="input-group">

							<input type="text" class="form-control" name="search" placeholder="Cauta produs ...">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>

							</span>

						</div>

					</form>	

				</div><!-- /input-group -->

				<div class="salut col-sm-4 ">Salut, <b class="bsalut">{{Auth::user()->nume}} {{Auth::user()->prenume}}</b></div>

				<div class="col-md-4" id="user">

					<div class="col-sm-6">

						<a href="/contulmeu" ><b>Contul Meu</b></a>

						<a href="/contulmeu" ><img src="/thumbs/user (2).png" id="upic" alt=""></a>

					</div>



					<div class="col-sm-6 cosb butcos">

						<a href="/fincomanda" class="cosb" ><b>Cosul Meu</b> <img src="/thumbs/cos (5).png" id="upic" alt=""></a>



						<div class="cart">

							@if(isset($cart))

							@if(empty($cart['nume']))

							<b class="cgol">Caruciorul este gol.</b>

							@else    

							@for($i=count($cart['nume'])-1;$i>=0;$i--)
							<a class="itemca" href="/produs/{{$cart['id'][$i]}}">
							<div class="itemcnoajax">

								<div class="cqty ic"><b>{{$cart['qty'][$i]}}x</b></div>

								@if(strlen($cart['nume'][$i]) > 60)

								<div class="cnume"><b>{{substr($cart['nume'][$i],0,60)}} ...</b></div>	

								@else

								<div class="cnume"><b>{{$cart['nume'][$i]}}</b></div>

								@endif

								<div class="cpret ic">{{$cart['total'][$i]}} RON</div>

								<div class="cdelete ic"><a href="" name="{{$cart['id'][$i]}}" class="cdel"><img src="/thumbs/stergemic.png" alt="Sterge Produs"></a></div>

							</div>
							</a>
							<br class="brnoajax">                                   

							@endfor

							<div class="fincomanda">

								<a href="/fincomanda">Finalizeaza Comanda</a>

							</div> 

							@endif    



							@endif



						</div>



					</div>			





				</div>

			</div>







			<div class="navb">			

				@if(isset($telgsm))

				<ul class="main-navigation">

					<li><a href="/home"><strong>Acasa</strong></a></li>

					<li><a href="/home/telefoanegsm"><strong>Telefoane Mobile / GSM</strong></a>

						<ul>



							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[0]->id}}">{{$telgsm[0]->nume}}</a>

								<div class="menu1">

									@if(isset($componente))

									<div class="colm">

										<a href="/home/titlu/{{$componenteT[0]->id}}" class="colmp titlu">{{$componenteT[0]->nume}}</a>

										@for($x=0;$x<count($componente);$x++)

										@if($componente[$x]->id == 13)

										<a href="/home/categorie/{{$componente[12]->id}}" class="colmp"> {{$componente[12]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[1]->id}}" class="colmp titlu">{{$componenteT[1]->nume}}</a>

										@elseif($componente[$x]->id == 17)

										<a href="/home/categorie/{{$componente[16]->id}}" class="colmp"> {{$componente[16]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[2]->id}}" class="colmp titlu">{{$componenteT[2]->nume}}</a>

										@elseif($componente[$x]->id == 21)

										<a href="/home/categorie/{{$componente[20]->id}}" class="colmp"> {{$componente[20]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[3]->id}}" class="colmp titlu">{{$componenteT[3]->nume}}</a>

										@elseif($componente[$x]->id == 25)

										<a href="/home/categorie/{{$componente[24]->id}}" class="colmp"> {{$componente[24]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[4]->id}}" class="colmp titlu">{{$componenteT[4]->nume}}</a>

										@elseif($componente[$x]->id == 26)

										<a href="/home/categorie/{{$componente[25]->id}}" class="colmp"> {{$componente[25]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[5]->id}}" class="colmp titlu">{{$componenteT[5]->nume}}</a>		

										@else	

										<a href="/home/categorie/{{$componente[$x]->id}}" class="colmp"> {{$componente[$x]->nume}}</a>

										@endif	

										@endfor

									</div><br>

									@endif

								</div>

							</li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[1]->id}}">{{$telgsm[1]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[2]->id}}">{{$telgsm[2]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[3]->id}}">{{$telgsm[3]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Tablete</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[4]->id}}">{{$telgsm[4]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[5]->id}}">{{$telgsm[5]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[6]->id}}">{{$telgsm[6]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>PC / Laptop</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[7]->id}}">{{$telgsm[7]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[8]->id}}">{{$telgsm[8]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[9]->id}}">{{$telgsm[9]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[10]->id}}">{{$telgsm[10]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Service</strong></a>

						<ul>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[11]->id}}">{{$telgsm[11]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[12]->id}}">{{$telgsm[12]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[13]->id}}">{{$telgsm[13]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[14]->id}}">{{$telgsm[14]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[15]->id}}">{{$telgsm[15]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[16]->id}}">{{$telgsm[16]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Diverse</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[17]->id}}">{{$telgsm[17]->nume}}</a></li>

						</ul>

					</li>

				</ul>					

				@endif

			</div>



		</div>



	</header>

	



<!--		

			<div class="col-sm-2" id="sidebar">

			<form action="/filtru" method="get" id="filtru">

				<div class="filtre" id="basicf">

					<div class="checkbox col-sm-offset-2">

					<label style="display:none" class="stergF">

							<input type="checkbox"  name="totprod"> Sterge Toate Filtrele

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label >

							<input type="checkbox" class="Finput" name="totprod"> Toate Produsele

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="instoc"> In Stoc

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="noutati"> Noutati

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="promotii"> Promotii

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="oferte"> Oferte Speciale

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="discount"> Discounturi

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="eol"> End of Life

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="swap"> SWAP / SH

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="resigilate"> Resigilate

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label  >

							<input type="checkbox" class="Finput" name="doarpoza"> Doar cu Poza

						</label>	

					</div>

				

				</div>



				<div class="filtre"  id="fPret">

					<h5><strong class="col-sm-offset-1" > Pret</strong></h5>

					<div class=" ">

					    <select name="pret"class="FinputO col-sm-offset-2" id="">

					    	<option value="">-- Selecteaza --</option>

					    	<option value="cresc">Crescator</option>

					    	<option value="desc">Descrescator</option>

					    </select>

					</div>

					<br>

					<h5><strong class="col-sm-offset-1" > RON</strong></h5>

					<b style="margin-right:20px;">    </b><input id="ex2" type="range" class=" FinputR col-sm-offset-2" value="" data-slider-min="0" data-slider-max="1500" data-slider-step="1" data-slider-value="[0,1500]" style="width:125px;" />

				</div>	

			

			<div class="filtre"  id="faz">

					<h5><strong class="col-sm-offset-1">Alfabetic</strong></h5>

					<div class="">

					    <select name="" class="FinputO col-sm-offset-2" id="">

					    	<option value="">-- Selecteaza --</option>

					    	<option value="az">A-Z</option>

					    	<option value="za">Z-A</option>

					    </select>

					</div>

					

			</div>

			

			<div class="filtre"  id="fcelemai">

					<h5><strong class="col-sm-offset-1">Cele Mai ...</strong></h5>

					<div class="">

					    <select name="" class="FinputO col-sm-offset-2" id="">

					    	<option value="">-- Selecteaza --</option>

					    	<option value="az">Nou Adaugate</option>

					    	<option value="za">Vechi Adaugate</option>

					    	<option value="za">Vizitate</option>

					    	<option value="za">Cautate</option>

					    	<option value="za">Accesate</option>

					    	<option value="za">Cumparate</option>

					    	<option value="za">Recomandate</option>

					    </select>

					</div>

					

			</div>



			<div class="filtre" id="fBrand">

				<h5><strong class="col-sm-offset-1">Branduri</strong></h5>

				@if(isset($branduri))

					@foreach($branduri as $brand)

						@if($brand->id > 10)

							<div class="checkbox col-sm-offset-2">

								<label style="display:none" class="brand hid">

									<input type="checkbox" class="Finput" name="brand{{$brand->id}}" > {{$brand->nume}}

								</label>

							</div>

						@else

							<div class="checkbox col-sm-offset-2">

								<label class="brand">

									<input type="checkbox" class="Finput"  name="brand{{$brand->id}}" > {{$brand->nume}}

								</label>

							</div>

						@endif			

					@endforeach

				@endif

				<div class="checkbox col-sm-offset-2">

					<label>

						<input type="checkbox" name="brand" class="totb"><mesaj> Vezi toate</mesaj> 

					</label>

				</div>

			</div>

					

			</form>	

		</div> -->



		<div class="col-sm-12" id="content">
			@if($produs->poza == '')
				<div class="row col-sm-4 "><img src="/products/no_image.png" class="img-rounded pprod" alt=""></div>
			@else
				<div class="row col-sm-4 imgyes"><span class="vp">Vezi Poza <span class="glyphicon glyphicon-zoom-in"></span></span><img src="{{$produs->poza}}"  class="img-rounded pprod" alt=""></div>
			@endif
			
			
			<div class="col-sm-8 ">
				<h3 style="display:block;" class="col-sm-12"><b>{{$produs->denumire_produs}}</b></h3><br>
				<div class="row">
					<div class="col-sm-3">	
					<small class="col-sm-12"><b class="bprod">Marca: </b>{{$br->nume}}</small>
					<small class="col-sm-12"><b class="bprod">Producator: </b>{{$prod->nume}}</small>
					<small class="col-sm-12"><b class="bprod">Cod Produs: </b>{{$produs->cod_produs}}</small>
					</div>
					
					<div class="col-sm-5">
						@if(Auth::user()->demo == 1)
							<small class="dispprod col-sm-12"><b>Disponibilitate:  </b><b  class="dispd"style="color:#f5811e;">Client Demo <span style="color:#f5811e;" class="glyphicon glyphicon-ok-circle symbol"></span></b></small>
						@else
						@if($disp[0]->in_stoc == 1)
							<small class="dispprod col-sm-12"><b>Disponibilitate:  </b><b  class="dispd">In Stoc <span style="color:#6fd646;" class="glyphicon glyphicon-ok-circle symbol"></span></b></small>
						@elseif($disp[0]->stoc_redus == 1)
							<small class="dispprod col-sm-12 "><b>Disponibilitate: </b><b  class="dispr">Stoc Redus <span style="color:#fce916;" class="glyphicon glyphicon-exclamation-sign"></span></b></small>
						@elseif($disp[0]->produs_indisponibil == 1)
							<small class="dispprod col-sm-12 "><b>Disponibilitate: </b><b  class="dispn">Produs Indisponibil <span style="color:#f73d3d;" class="glyphicon glyphicon-remove-sign"></span></b></small>
						@else
							<small class="dispprod col-sm-12 "><b>Disponibilitate: </b><b  class="dispc">In Curand <span style="color:#9866c5;" class="glyphicon glyphicon-time"></span></b></small>
						@endif
						<br>
						@endif
						<small class="dispprod col-sm-12"><b>Garantie:  </b><b  class="dispd">6 Luni<span style="color:#6fd646;" class="glyphicon glyphicon-ok-circle symbol"></span></b></small>
						<br>
					
						<small class="sprod col-sm-12"><b>Discount: <b class="sprodd"> {{$produs->discount}}%</b></b></small>
					</div>
					
					<div class="col-sm-4">
						@if($produs->noutati != 0)
							<p class="col-sm-12"><b class="disps "><span class="glyphicon glyphicon-star"></span> Produs Nou Adaugat</b></p>	
						@endif
						@if($produs->promotii != 0)
							<p class="col-sm-12"><b class="disps"><span class="glyphicon glyphicon-gift"></span> Produs aflat la Promotie.</b></p>	
						@endif
						@if($produs->lichidari_stoc != 0)
							<p class="col-sm-12"><b class="disps"><span class="glyphicon glyphicon-piggy-bank"></span> Produs in Lichidare de Stoc.</b></p>	
						@endif
					</div>	
				
				
				<div class="row col-sm-4 col-sm-offset-4">
					<h4 class="">Pret: {{$produs->pret}} RON</h4>
					
					<div>
					@if(Auth::user()->demo == 1)
							<div class="btn-group but" style="cursor:not-allowed;margin-left:20px;" >

									<a href="javascript: void(0)" ><button type="button"  disabled name="{{$produs->cod_produs}}" class="caradd btn btn-primary">Adauga in cos</button></a>

								</div>	
								<div class="codprod col-sm-12" style="margin-left:30px;color:#098ead;"><p>Cantitate</p></div>

								<div class="form-group ">

									<div class="btn-group qtygroupmare" style="cursor:not-allowed;">									

										<button  class="btn btn-default plusmin minus" value="-" type="button" disabled>-</button>

									</div>
									
									<div class="col-sm-4 qtygroupmare">
										<input  type="text" class="form-control qty " style="text-align:center;" disabled>
									</div>

									<div class="btn-group qtygroupmare" style="cursor:not-allowed;">

										<button  class="btn btn-default plusmin plus" value="+" type="button" disabled>+</button>

									</div>

								</div>
					@else
						@if($disp[0]->in_stoc == 1 || $disp[0]->stoc_redus == 1)
						
							<div class="btn-group but" style="margin-left:20px;" >

									<a href="" ><button type="button"  name="{{$produs->cod_produs}}" class="caradd btn btn-primary">Adauga in cos</button></a>

								</div>	
								<div class="codprod col-sm-12" style="margin-left:30px;color:#098ead;"><p>Cantitate</p></div>

								<div class="form-group ">

									<div class="btn-group qtygroupmare" >									

										<button  class="btn btn-default plusmin minus" value="-" type="button" >-</button>

									</div>
									
									<div class="col-sm-4 qtygroupmare">
										<input  type="text" class="form-control qty " style="text-align:center;" >
									</div>

									<div class="btn-group qtygroupmare" >

										<button  class="btn btn-default plusmin plus" value="+" type="button" >+</button>

									</div>

								</div>
							
						@elseif($disp[0]->produs_indisponibil == 1 || $disp[0]->in_curand == 1)
								
							<div class="btn-group but" style="cursor:not-allowed;margin-left:20px;" >

									<a href="javascript: void(0)" ><button type="button"  disabled name="{{$produs->cod_produs}}" class="caradd btn btn-primary">Adauga in cos</button></a>

								</div>	
								<div class="codprod col-sm-12" style="margin-left:30px;color:#098ead;"><p>Cantitate</p></div>

								<div class="form-group ">

									<div class="btn-group qtygroupmare" style="cursor:not-allowed;">									

										<button  class="btn btn-default plusmin minus" value="-" type="button" disabled>-</button>

									</div>
									
									<div class="col-sm-4 qtygroupmare">
										<input  type="text" class="form-control qty " style="text-align:center;" disabled>
									</div>

									<div class="btn-group qtygroupmare" style="cursor:not-allowed;">

										<button  class="btn btn-default plusmin plus" value="+" type="button" disabled>+</button>

									</div>

								</div>								
						@endif	
					@endif
					</div>
							
				</div>
				
							
			</div>
			</div>
				<div class="row" style="display:none">
					<div class="piccar col-sm-4 ">
						@foreach($poze as $p)
							<img src="{{$p->poza}}" class="img-rounded col-sm-2 pselect" alt="">
						@endforeach
					</div>
				</div>

				
					<h3 class="row col-sm-8 col-sm-offset-4 desc"> Descriere Produs</h3>
				<div class="col-sm-10 col-sm-offset-1 box-det" >
					@if(!$produs->detalii == '')
					<div class="col-sm-10 col-sm-offset-1 detalii"><p class="dettx">Detalii</p> <p>{!!$produs->detalii!!}</p></div>
					@endif
				</div>	
				<div class="col-sm-10 col-sm-offset-1 box-cul" >
					@if(!$produs->culoare == '')
					<div class=" col-sm-10 col-sm-offset-1 culoare"><p class="ctext">Culoare: </p> <p> {{$produs->culoare}}</p></div>
					@endif
				</div>	
				<div class="col-sm-10 col-sm-offset-1 box-dim" >
					@if(!$produs->dimensiune == '')
					<div class=" col-sm-10 col-sm-offset-1 dimensiune"><p class="dtext">Dimensiune: </p><p>{{$produs->dimensiune}}</p></div>
					@endif
				</div>	
				<div class="col-sm-10 col-sm-offset-1 box-tip" >
					@if(!$produs->tip_ambalaj == '')
					<div class=" col-sm-10 col-sm-offset-1 tipamb"><p class="ttext">Tip Ambalaj: </p><p>{{$produs->tip_ambalaj}}</p></div>
					@endif
				</div>	
				<div class="col-sm-10 col-sm-offset-1 box-at" >
					@if(!$produs->atentie == '')
					<div class=" col-sm-10 col-sm-offset-1 atentie"><p class="atext">Atentie: </p><p style="font-size:16px;">{{$produs->atentie}}</p></div>			
					@endif
				</div>

			<div class="modal fade" id="addcart" role="dialog">

			<div class="modal-dialog">

				<div class="modal-content">

					<div class="modal-header">

						<h4 style="text-align:center;">Te rog asteapta</h4>

					</div>

					<div class="modal-body">

						<div class="progress"  style="background-color:#098ead">

							<div class="progress-bar cartload" data-transitiongoal="100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background-color:#f5811e;">

							</div>		

						</div>			

					</div>

				</div>

			</div>	

		</div>			
	</div>

<div class="modal fade" id="pozamaremod" role="dialog">
		<!--	<div class="modal-dialog">-->
				
					<div class="modal-body mod-img ">
						<img src="{{$produs->poza}}" class="mod-img"  alt="">								
					</div>
			<!--	</div> -->
				
		</div>
		

@include('partiale/footer')



<script src="/js/jquery.js"></script>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>

<script src="/js/sidebar.js"></script>

<script src="/js/prodcart.js"></script>

<script src="/js/prod.js"></script>

<script src="/js/poze.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
<script src="/js/pozamare.js"></script>
</body>

</html>
