<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">

	<link rel="stylesheet" href="/css/home.css">

	<link rel="stylesheet" href="/bootstrap/bootstrap-slider/dist/css/bootstrap-slider.min.css">



	<title>CellMall</title>

</head>

<body>

	









<header >

<div class="nav-center col-md-12 " >

			<ul class="nav nav-pills">  

				<li><a href="/home"><span class="glyphicon glyphicon-home"></span> Acasa</a></li>

				<li><a href="/informatii" ><span class="glyphicon glyphicon-info-sign"></span> Informatii</a></li>

				<li><a href="/informatii/termenisiconditii" ><span class="glyphicon glyphicon-book"></span> Termeni si Conditii</a></li>

				<li><a href="/informatii/contact"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>

				<li><a href="/noutati" ><span class="glyphicon glyphicon-star"></span> Noutati</a></li>

				<li><a href="/promotii"><span class="glyphicon glyphicon-gift"></span> Promotii</a></li>

				<li><a href="/lichidaristoc" ><span class="glyphicon glyphicon-piggy-bank"></span> Lichidari Stoc</a></li>

				<li><a href="/ofertespeciale" ><span class="glyphicon glyphicon-scissors"></span> Discount</a></li>
				@if(Auth::user()->admin == 1)
					<li><a href="/dashboard" target="_blank" ><span class="glyphicon glyphicon-wrench"></span> Dashboard</a></li>
				@endif
				<li><a href="/logout" ><span class="glyphicon glyphicon-log-out"></span> Iesire</a></li>

			</ul>

		</div>



		<div class="head col-md-12 ">

			<div class="row">

				<img src="/thumbs/logo.png" class="col-md-3" id="logo" alt="">

				<div class="col-md-5" id="search">

					<form action="/cauta" method="get">

						<div class="input-group">

							<input type="text" class="form-control" name="search" placeholder="Cauta produs ...">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>

							</span>

						</div>

					</form>	

				</div><!-- /input-group -->

				<div class="salut col-sm-4 ">Salut, <b class="bsalut">{{Auth::user()->nume}} {{Auth::user()->prenume}}</b></div>

				<div class="col-md-4" id="user">

					<div class="col-sm-6">

						<a href="/contulmeu" ><b>Contul Meu</b></a>

						<a href="/contulmeu" ><img src="/thumbs/user (2).png" id="upic" alt=""></a>

					</div>



					<div class="col-sm-6 cosb butcos">

						<a href="/fincomanda" class="cosb" ><b>Cosul Meu</b> <img src="/thumbs/cos (5).png" id="upic" alt=""></a>



						<div class="cart">

							@if(isset($cart))

							@if(empty($cart['nume']))

							<b class="cgol">Caruciorul este gol.</b>

							@else    

							@for($i=count($cart['nume'])-1;$i>=0;$i--)
							<a class="itemca" href="/produs/{{$cart['id'][$i]}}">
							<div class="itemcnoajax">

								<div class="cqty ic"><b>{{$cart['qty'][$i]}}x</b></div>

								@if(strlen($cart['nume'][$i]) > 60)

								<div class="cnume"><b>{{substr($cart['nume'][$i],0,60)}} ...</b></div>	

								@else

								<div class="cnume"><b>{{$cart['nume'][$i]}}</b></div>

								@endif

								<div class="cpret ic">{{$cart['total'][$i]}} RON</div>

								<div class="cdelete ic"><a href="" name="{{$cart['id'][$i]}}" class="cdel"><img src="/thumbs/stergemic.png" alt="Sterge Produs"></a></div>

							</div>
							</a>
							<br class="brnoajax">                                   

							@endfor

							<div class="fincomanda">

								<a href="/fincomanda">Finalizeaza Comanda</a>

							</div> 

							@endif    



							@endif



						</div>



					</div>			





				</div>

			</div>







			<div class="navb">			

				@if(isset($telgsm))

				<ul class="main-navigation">

					<li><a href="/home"><strong>Acasa</strong></a></li>

					<li><a href="/home/telefoanegsm"><strong>Telefoane Mobile / GSM</strong></a>

						<ul>



							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[0]->id}}">{{$telgsm[0]->nume}}</a>

								<div class="menu1">

									@if(isset($componente))

									<div class="colm">

										<a href="/home/titlu/{{$componenteT[0]->id}}" class="colmp titlu">{{$componenteT[0]->nume}}</a>

										@for($x=0;$x<count($componente);$x++)

										@if($componente[$x]->id == 13)

										<a href="/home/categorie/{{$componente[12]->id}}" class="colmp"> {{$componente[12]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[1]->id}}" class="colmp titlu">{{$componenteT[1]->nume}}</a>

										@elseif($componente[$x]->id == 17)

										<a href="/home/categorie/{{$componente[16]->id}}" class="colmp"> {{$componente[16]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[2]->id}}" class="colmp titlu">{{$componenteT[2]->nume}}</a>

										@elseif($componente[$x]->id == 21)

										<a href="/home/categorie/{{$componente[20]->id}}" class="colmp"> {{$componente[20]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[3]->id}}" class="colmp titlu">{{$componenteT[3]->nume}}</a>

										@elseif($componente[$x]->id == 25)

										<a href="/home/categorie/{{$componente[24]->id}}" class="colmp"> {{$componente[24]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[4]->id}}" class="colmp titlu">{{$componenteT[4]->nume}}</a>

										@elseif($componente[$x]->id == 26)

										<a href="/home/categorie/{{$componente[25]->id}}" class="colmp"> {{$componente[25]->nume}}</a>

										<a href="/home/titlu/{{$componenteT[5]->id}}" class="colmp titlu">{{$componenteT[5]->nume}}</a>		

										@else	

										<a href="/home/categorie/{{$componente[$x]->id}}" class="colmp"> {{$componente[$x]->nume}}</a>

										@endif	

										@endfor

									</div><br>

									@endif

								</div>

							</li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[1]->id}}">{{$telgsm[1]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[2]->id}}">{{$telgsm[2]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[3]->id}}">{{$telgsm[3]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Tablete</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[4]->id}}">{{$telgsm[4]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[5]->id}}">{{$telgsm[5]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[6]->id}}">{{$telgsm[6]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>PC / Laptop</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[7]->id}}">{{$telgsm[7]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[8]->id}}">{{$telgsm[8]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[9]->id}}">{{$telgsm[9]->nume}}</a></li>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[10]->id}}">{{$telgsm[10]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Service</strong></a>

						<ul>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[11]->id}}">{{$telgsm[11]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[12]->id}}">{{$telgsm[12]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[13]->id}}">{{$telgsm[13]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[14]->id}}">{{$telgsm[14]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[15]->id}}">{{$telgsm[15]->nume}}</a></li>

							<li class="lvl1 service"><a href="/home/componenteaccesorii/{{$telgsm[16]->id}}">{{$telgsm[16]->nume}}</a></li>

						</ul>

					</li>

					<li><a href="#"><strong>Diverse</strong></a>

						<ul>

							<li class="lvl1"><a href="/home/componenteaccesorii/{{$telgsm[17]->id}}">{{$telgsm[17]->nume}}</a></li>

						</ul>

					</li>

				</ul>					

				@endif

			</div>



		</div>



	</header>
	



		

<!--		<div class="col-sm-2" id="sidebar">

			<form action="/filtru" method="get" id="filtru">

				<div class="filtre" id="basicf">

					<div class="checkbox col-sm-offset-2">

					<label style="display:none" class="stergF">

							<input type="checkbox"  name="totprod"> Sterge Toate Filtrele

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label >

							<input type="checkbox" class="Finput" name="totprod"> Toate Produsele

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="instoc"> In Stoc

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="noutati"> Noutati

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="promotii"> Promotii

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="oferte"> Oferte Speciale

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="discount"> Discounturi

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="eol"> End of Life

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="swap"> SWAP / SH

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label>

							<input type="checkbox" class="Finput" name="resigilate"> Resigilate

						</label>	

					</div>

					<div class="checkbox col-sm-offset-2">

						<label  >

							<input type="checkbox" class="Finput" name="doarpoza"> Doar cu Poza

						</label>	

					</div>

				

				</div>



				<div class="filtre"  id="fPret">

					<h5><strong class="col-sm-offset-1" > Pret</strong></h5>

					<div class=" ">

					    <select name="pret"class="FinputO col-sm-offset-2" id="">

					    	<option value="">-- Selecteaza --</option>

					    	<option value="cresc">Crescator</option>

					    	<option value="desc">Descrescator</option>

					    </select>

					</div>

					<br>

					<h5><strong class="col-sm-offset-1" > RON</strong></h5>

					<b style="margin-right:20px;">    </b><input id="ex2" type="range" class=" FinputR col-sm-offset-2" value="" data-slider-min="0" data-slider-max="1500" data-slider-step="1" data-slider-value="[0,1500]" style="width:125px;" />

				</div>	

			

			<div class="filtre"  id="faz">

					<h5><strong class="col-sm-offset-1">Alfabetic</strong></h5>

					<div class="">

					    <select name="" class="FinputO col-sm-offset-2" id="">

					    	<option value="">-- Selecteaza --</option>

					    	<option value="az">A-Z</option>

					    	<option value="za">Z-A</option>

					    </select>

					</div>

					

			</div>

			

			<div class="filtre"  id="fcelemai">

					<h5><strong class="col-sm-offset-1">Cele Mai ...</strong></h5>

					<div class="">

					    <select name="" class="FinputO col-sm-offset-2" id="">

					    	<option value="">-- Selecteaza --</option>

					    	<option value="az">Nou Adaugate</option>

					    	<option value="za">Vechi Adaugate</option>

					    	<option value="za">Vizitate</option>

					    	<option value="za">Cautate</option>

					    	<option value="za">Accesate</option>

					    	<option value="za">Cumparate</option>

					    	<option value="za">Recomandate</option>

					    </select>

					</div>

					

			</div> 



			<div class="filtre" id="fBrand">

				<h5><strong class="col-sm-offset-1">Branduri</strong></h5>

				@if(isset($branduri))

					@foreach($branduri as $brand)

						@if($brand->id > 10)

							<div class="checkbox col-sm-offset-2">

								<label style="display:none" class="brand hid">

									<input type="checkbox" class="Finput" name="brand{{$brand->id}}" > {{$brand->nume}}

								</label>

							</div>

						@else

							<div class="checkbox col-sm-offset-2">

								<label class="brand">

									<input type="checkbox" class="Finput"  name="brand{{$brand->id}}" > {{$brand->nume}}

								</label>

							</div>

						@endif			

					@endforeach

				@endif

				<div class="checkbox col-sm-offset-2">

					<label>

						<input type="checkbox" name="brand" class="totb"><mesaj> Vezi toate</mesaj> 

					</label>

				</div>

			</div>

					

			</form>	

		</div> -->



		<div class="col-sm-10 col-sm-offset-1" id="content">

			@if(isset($prod))

				@if(count($prod) != 0)

					@foreach($prod as $index=> $p)
						@if($p->ascuns != 1)
						<div class="prod col-sm-3">

							@if($p->poza == '')

							<a href="/produs/{{$p->id}}"><img src="/products/no_image.png" class="col-sm-10 thumbnail noimg"  alt=""></a>

							@else

								<a href="/produs/{{$p->id}}"><img src="{{$p->poza}}" class="col-sm-10 thumbnail" alt=""></a>

							@endif
							
							<div class="titlu col-sm-12"><a href="/produs/{{$p->id}}"> <b> {{substr($p->denumire_produs,0,130)}}</b></a></div>

							<img src="/thumbs/rsz_logo.png" class="thumbnail br" alt="">

							<div class="codprod col-sm-12"><p>Cod Produs: {{$p->cod_produs}}</p></div>

							<div class="pret col-sm-12"><p>{{$p->pret}} RON</p></div>
						@if(Auth::user()->demo == 1)
									<div class="btn-group but">

									<a href="javascript: void(0)" style="cursor:not-allowed;" disabled><button type="button" name="{{$p->cod_produs}}" class="adauga btn btn-primary" disabled>Adauga in cos</button></a>

								</div>	
								<div class="demop col-sm-12"><p>Client Demo <span class="glyphicon glyphicon-tags"></span></p></div>
								<div class="codprod col-sm-12"><p>Cantitate</p></div>

								<div class="form-group qtygroup">

									<div class="btn-group" style="float:left">									

										<button style="cursor:not-allowed;" class="btn btn-default plusmin minus" value="-" type="button" disabled>-</button>

									</div>

									<input type="text" class="form-control qty" disabled>

									<div class="btn-group">

										<button style="cursor:not-allowed;" class="btn btn-default plusmin plus" value="+" type="button" disabled>+</button>

									</div>

								</div>	
								@else
							@if($disp[$index]->in_stoc == 1)
								<div class="btn-group but">

									<a href="" ><button type="button" name="{{$p->cod_produs}}" class="adauga btn btn-primary">Adauga in cos</button></a>

								</div>	
								<div class="statokprod col-sm-12"><p>In Stoc <span class="glyphicon glyphicon-ok-sign"></span></p></div>
								<div class="codprod col-sm-12"><p>Cantitate</p></div>

								<div class="form-group qtygroup">

									<div class="btn-group" style="float:left">									

										<button class="btn btn-default plusmin minus" value="-" type="button" >-</button>

									</div>

									<input type="text" class="form-control qty">

									<div class="btn-group">

										<button class="btn btn-default plusmin plus" value="+" type="button" >+</button>

									</div>

								</div>
							@elseif($disp[$index]->stoc_redus == 1)
								<div class="btn-group but">

									<a href="" ><button type="button" name="{{$p->cod_produs}}" class="adauga btn btn-primary">Adauga in cos</button></a>

								</div>	
								<div class="statredprod col-sm-12"><p>Stoc Redus <span class="glyphicon glyphicon-exclamation-sign"></span></p></div>
								<div class="codprod col-sm-12"><p>Cantitate</p></div>

								<div class="form-group qtygroup">

									<div class="btn-group" style="float:left">									

										<button class="btn btn-default plusmin minus" value="-" type="button" >-</button>

									</div>

									<input type="text" class="form-control qty">

									<div class="btn-group">

										<button class="btn btn-default plusmin plus" value="+" type="button" >+</button>

									</div>

								</div>
							@elseif($disp[$index]->produs_indisponibil == 1)
								<div class="btn-group but">

									<a href="javascript: void(0)" style="cursor:not-allowed;" disabled><button type="button" name="{{$p->cod_produs}}" class="adauga btn btn-primary" disabled>Adauga in cos</button></a>

								</div>	
								<div class="statindiprod col-sm-12"><p>Produs Indisponibil <span class="glyphicon glyphicon-remove-sign"></span></p></div>
								<div class="codprod col-sm-12"><p>Cantitate</p></div>

								<div class="form-group qtygroup">

									<div class="btn-group" style="float:left">									

										<button style="cursor:not-allowed;" class="btn btn-default plusmin minus" value="-" type="button" disabled>-</button>

									</div>

									<input type="text" class="form-control qty" disabled>

									<div class="btn-group">

										<button style="cursor:not-allowed;" class="btn btn-default plusmin plus" value="+" type="button" disabled>+</button>

									</div>

								</div>
							@else
								<div class="btn-group but">

									<a href="javascript: void(0)" style="cursor:not-allowed;" disabled><button type="button" name="{{$p->cod_produs}}" class="adauga btn btn-primary" disabled>Adauga in cos</button></a>

								</div>	
								<div class="statsoonprod col-sm-12"><p>In Curand <span class="glyphicon glyphicon-time"></span></p></div>
								<div class="codprod col-sm-12"><p>Cantitate</p></div>

								<div class="form-group qtygroup">

									<div class="btn-group" style="float:left">									

										<button style="cursor:not-allowed;" class="btn btn-default plusmin minus" value="-" type="button" disabled>-</button>

									</div>

									<input type="text" class="form-control qty" disabled>

									<div class="btn-group">

										<button style="cursor:not-allowed;" class="btn btn-default plusmin plus" value="+" type="button" disabled>+</button>

									</div>

								</div>
							@endif
							@endif
								

						</div>
						@endif
					@endforeach

				@else

					<h3>In curand ...</h3>	

				@endif					

				

			<div class="pages">	

			{!! $prod->render() !!}

			</div>

			@endif

		</div>



		<div class="modal fade" id="addcart" role="dialog">

			<div class="modal-dialog">

				<div class="modal-content">

					<div class="modal-header">

						<h4 style="text-align:center;">Va rugam asteptati</h4>

					</div>

					<div class="modal-body">

						<div class="progress"  style="background-color:#098ead">

							<div class="progress-bar cartload" data-transitiongoal="100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background-color:#f5811e;">

							</div>		

						</div>			

					</div>

				</div>

			</div>	

		</div>

	

		

@include('partiale/footer')	

	</div>

				

		

		







<script src="/js/jquery.js"></script>

<script src="/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>

<script src="/js/cart.js"></script>

<script src="/js/prod.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
</body>

</html>
