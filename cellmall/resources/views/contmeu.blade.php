   <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/layout.css">
  <link rel="stylesheet" href="/slick/slick.css">
  <link rel="stylesheet" href="/slick/slick-theme.css">
  <title>Contul Meu</title>
</head>
<body>

@include('partiale/header')

<div class="mininav col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="pull-right">
    <a href="" role="button" class="btn btn-danger editcont ">Editeaza Contul</a>
    <a href="/istoriccom" role="button" class="btn btn-primary istoric ">Istoric Comenzi</a>
  </div>
</div>

<div role="alert" style="display:none;" class="alert alert-danger atentie col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
  <h3 class="atentie-title"><span class="glyphicon glyphicon-exclamation-sign" style="margin-top:0" aria-hidden="true"></span> Atentie</h3>
  <h4 class="atentie-content">Aceasta pagina permite editarea contului tau.</h4>
  <h4 class="atentie-content">Daca nu doresti editarea contului apasa "Anuleaza Editarea Contului".</h4>
</div>


  <div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="datetotal col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <form id="editc" method="post" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="fincom col-xs-12 col-sm-12 col-md-12 col-lg-12">
		@if(Auth::user()->demo != 1)
          <div class="date-pers  col-lg-6">
            <h4>Date Personale</h4>
            <b class="title-data datec">Nume: </b><b class="datec dnume">{{Auth::user()['nume']}}</b><br>
            <label for="ednume" class="edcont"><b>Nume</b></label>
            <input type="text" class="form-control edcont ednume" id="ednume" name="ednume" placeholder="Nume">
            <b class="title-data datec">Prenume: </b><b class="datec dprenume">{{Auth::user()['prenume']}}</b><br>
            <label for="edprenume" class="edcont"><b>Prenume</b></label>
            <input type="text" class="form-control edcont edprenume" id="edprenume" name="edprenume" placeholder="Prenume">
            <b class="title-data datec">CNP: </b><b class="datec dcnp">{{Auth::user()['CNP']}}</b><br>
            <label for="edcnp" class="edcont"><b>CNP</b></label>
            <input type="text" class="form-control edcont edcnp" id="edcnp" name="edcnp" placeholder="C.N.P">
            <b class="title-data datec">E-mail: </b><b class="datec dcnp">{{Auth::user()['email']}}</b><br>
            <label for="edemail" class="edcont"><b>E-mail</b></label>
            <input type="text" disabled="disabled" class="form-control edcont edemail" id="edemail" name="edemail" placeholder="E-mail">
            <b class="title-data datec">Telefon: </b><b class="datec dtelefon">{{Auth::user()['telefon']}}</b><br>
            <label for="edtelefon" class="edcont"><b>Telefon</b></label>
            <input type="text" class="form-control edcont edtelefon" id="edtelefon" name="edtelefon" placeholder="Telefon">
          </div>
          <div class="detalii-comp col-lg-6">
            <h4>Detalii Companie</h4>
            <b class="title-data datec">Nume Companie: </b><b class="datec dnumecomp">{{Auth::user()['nume_companie']}}</b><br>
            <label for="ednumecomp" class="edcont"><b>Nume Companie</b></label>
            <input type="text" class="form-control edcont ednumecomp" id="ednumecomp" name="ednumecomp" placeholder="Nume Companie">
            <b class="title-data datec">C.U.I: </b><b class="datec dcui">{{Auth::user()['CUI']}}</b><br>
            <label for="edcui" class="edcont"><b>C.U.I</b></label>
            <input type="text" class="form-control edcont edcui" id="edcui" name="edcui" placeholder="C.U.I">
            <b class="title-data datec">Nr.ord.reg.com/an: </b><b class="datec dnrreg">{{Auth::user()['nr_reg']}}</b><br>
            <label for="ednrreg" class="edcont"><b>Nr.ord.reg.com/an:</b></label>
            <input type="text" class="form-control edcont ednrreg" id="ednrreg" name="ednrreg" placeholder="Nr.ord.reg.com/an:">
            <b class="title-data datec">Banca: </b><b class="datec dbanca">{{Auth::user()['banca']}}</b><br>
            <label for="edbanca" class="edcont"><b>Banca</b></label>
            <input type="text" class="form-control edcont edbanca" id="edbanca" name="edbanca" placeholder="Banca">
            <b class="title-data datec">Cont IBAN: </b><b class="datec dcontib">{{Auth::user()['cont_IBAN']}}</b><br>
            <label for="dcontib" class="edcont"><b>Cont IBAN</b></label>
            <input type="text" class="form-control edcont dcontib" id="dcontib" name="dcontib" placeholder="Cont IBAN">
          </div>
          <div class="adresa-sed-soc col-lg-4">
            <h4>Adresa Sediu Social</h4>
            <b class="title-data datec">Adresa Sediu Social: </b><b class="datec dass">{{Auth::user()['adresa_sediu']}}</b><br>
            <label for="edass" class="edcont"><b>Adresa Sediu Social</b></label>
            <input type="text" class="form-control edcont edass" id="edass" name="edass" placeholder="Adresa Sediu Social">
            <b class="title-data datec">Localitate Sediu Social: </b><b class="datec dlss">{{Auth::user()['localitate_sediu']}}</b><br>
            <label for="edlss" class="edcont"><b>Localitate Sediu Social</b></label>
            <input type="text" class="form-control edcont edlss" id="edlss" name="edlss" placeholder="Localitate Sediu Social">
            <b class="title-data datec">Judet/Sector Sediu Social: </b><b class="datec djsss">{{Auth::user()['judet_sector_sediu']}}</b><br>
            <label for="edjsss" class="edcont"><b>Judet/Sector Sediu Social</b></label>
            <input type="text" class="form-control edcont edjsss" id="edjsss" name="edjsss" placeholder="Judet/Sector Sediu Social">
          </div>
          <div class="adresa-fac col-lg-4">
            <h4>Adresa Facturare</h4>
            <b class="title-data datec">Adresa Facturare: </b><b class="datec daf">{{Auth::user()['adresa_fac']}}</b><br>
            <label for="edaf" class="edcont"><b>Adresa Facturare</b></label>
            <input type="text" class="form-control edcont edaf" id="edaf" name="edaf" placeholder="Adresa Facturare">
            <b class="title-data datec">Localitate Facturare: </b><b class="datec dlf">{{Auth::user()['localitate_fac']}}</b><br>
            <label for="edlf" class="edcont"><b>Localitate Facturare</b></label>
            <input type="text" class="form-control edcont edlf" id="edlf" name="edlf" placeholder="Localitate Facturare">
            <b class="title-data datec">Judet/Sector Facturare: </b><b class="datec djsf">{{Auth::user()['judet_sector_fac']}}</b><br>
            <label for="edjsf" class="edcont"><b>Judet/Sector Facturare</b></label>
            <input type="text" class="form-control edcont edjsf" id="edjsf" name="edjsf" placeholder="Judet/Sector Facturare">
          </div>
          <div class="adresa-liv col-lg-4">
            <h4>Adresa Livrare</h4>
            <b class="title-data datec">Adresa Livrare: </b><b class="datec dal">{{Auth::user()['adresa_liv']}}</b><br>
            <label for="edal" class="edcont"><b>Adresa Livrare</b></label>
            <input type="text" class="form-control edcont edal" id="edal" name="edal" placeholder="Adresa Livrare">
            <b class="title-data datec">Localitate Livrare: </b><b class="datec dll">{{Auth::user()['localitate_liv']}}</b><br>
            <label for="edll" class="edcont"><b>Localitate Livrare</b></label>
            <input type="text" class="form-control edcont edll" id="edll" name="edll" placeholder="Localitate Livrare">
            <b class="title-data datec">Judet/Sector Livrare: </b><b class="datec djsl">{{Auth::user()['judet_sector_liv']}}</b><br>
            <label for="edjsl" class="edcont"><b>Judet/Sector Livrare</b></label>
            <input type="text" class="form-control edcont edjsl" id="edjsl" name="edjsl" placeholder="Judet/Sector Livrare">
          </div>
		@else
			<div class="date-pers col-lg-6">
            <h4>Date Personale</h4>
            <b class="title-data datec">Nume: </b><b class="datec dnume">{{Auth::user()['nume']}}</b><br>
            <label for="ednume" class="edcont"><b>Nume</b></label>
            <input type="text"disabled="disabled" class="form-control edcont ednume" id="ednume" name="ednume" placeholder="Nume">
            <b class="title-data datec">Prenume: </b><b class="datec dprenume">{{Auth::user()['prenume']}}</b><br>
            <label for="edprenume" class="edcont"><b>Prenume</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edprenume" id="edprenume" name="edprenume" placeholder="Prenume">
            <b class="title-data datec">CNP: </b><b class="datec dcnp">{{Auth::user()['CNP']}}</b><br>
            <label for="edcnp" class="edcont"><b>CNP</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edcnp" id="edcnp" name="edcnp" placeholder="C.N.P">
            <b class="title-data datec">E-mail: </b><b class="datec dcnp">{{Auth::user()['email']}}</b><br>
            <label for="edemail" class="edcont"><b>E-mail</b></label>
            <input type="text" disabled="disabled" class="form-control edcont edemail" id="edemail" name="edemail" placeholder="E-mail">
            <b class="title-data datec">Telefon: </b><b class="datec dtelefon">{{Auth::user()['telefon']}}</b><br>
            <label for="edtelefon" class="edcont"><b>Telefon</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edtelefon" id="edtelefon" name="edtelefon" placeholder="Telefon">
          </div>
          <div class="detalii-comp col-lg-6">
            <h4>Detalii Companie</h4>
            <b class="title-data datec">Nume Companie: </b><b class="datec dnumecomp">{{Auth::user()['nume_companie']}}</b><br>
            <label for="ednumecomp" class="edcont"><b>Nume Companie</b></label>
            <input type="text"disabled="disabled" class="form-control edcont ednumecomp" id="ednumecomp" name="ednumecomp" placeholder="Nume Companie">
            <b class="title-data datec">C.U.I: </b><b class="datec dcui">{{Auth::user()['CUI']}}</b><br>
            <label for="edcui" class="edcont"><b>C.U.I</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edcui" id="edcui" name="edcui" placeholder="C.U.I">
            <b class="title-data datec">Nr.ord.reg.com/an: </b><b class="datec dnrreg">{{Auth::user()['nr_reg']}}</b><br>
            <label for="ednrreg" class="edcont"><b>Nr.ord.reg.com/an:</b></label>
            <input type="text"disabled="disabled" class="form-control edcont ednrreg" id="ednrreg" name="ednrreg" placeholder="Nr.ord.reg.com/an:">
            <b class="title-data datec">Banca: </b><b class="datec dbanca">{{Auth::user()['banca']}}</b><br>
            <label for="edbanca" class="edcont"><b>Banca</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edbanca" id="edbanca" name="edbanca" placeholder="Banca">
            <b class="title-data datec">Cont IBAN: </b><b class="datec dcontib">{{Auth::user()['cont_IBAN']}}</b><br>
            <label for="dcontib" class="edcont"><b>Cont IBAN</b></label>
            <input type="text"disabled="disabled" class="form-control edcont dcontib" id="dcontib" name="dcontib" placeholder="Cont IBAN">
          </div>
          <div class="adresa-sed-soc col-lg-4">
            <h4>Adresa Sediu Social</h4>
            <b class="title-data datec">Adresa Sediu Social: </b><b class="datec dass">{{Auth::user()['adresa_sediu']}}</b><br>
            <label for="edass" class="edcont"><b>Adresa Sediu Social</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edass" id="edass" name="edass" placeholder="Adresa Sediu Social">
            <b class="title-data datec">Localitate Sediu Social: </b><b class="datec dlss">{{Auth::user()['localitate_sediu']}}</b><br>
            <label for="edlss" class="edcont"><b>Localitate Sediu Social</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edlss" id="edlss" name="edlss" placeholder="Localitate Sediu Social">
            <b class="title-data datec">Judet/Sector Sediu Social: </b><b class="datec djsss">{{Auth::user()['judet_sector_sediu']}}</b><br>
            <label for="edjsss" class="edcont"><b>Judet/Sector Sediu Social</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edjsss" id="edjsss" name="edjsss" placeholder="Judet/Sector Sediu Social">
          </div>
          <div class="adresa-fac col-lg-4">
            <h4>Adresa Facturare</h4>
            <b class="title-data datec">Adresa Facturare: </b><b class="datec daf">{{Auth::user()['adresa_fac']}}</b><br>
            <label for="edaf" class="edcont"><b>Adresa Facturare</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edaf" id="edaf" name="edaf" placeholder="Adresa Facturare">
            <b class="title-data datec">Localitate Facturare: </b><b class="datec dlf">{{Auth::user()['localitate_fac']}}</b><br>
            <label for="edlf" class="edcont"><b>Localitate Facturare</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edlf" id="edlf" name="edlf" placeholder="Localitate Facturare">
            <b class="title-data datec">Judet/Sector Facturare: </b><b class="datec djsf">{{Auth::user()['judet_sector_fac']}}</b><br>
            <label for="edjsf" class="edcont"><b>Judet/Sector Facturare</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edjsf" id="edjsf" name="edjsf" placeholder="Judet/Sector Facturare">
          </div>
          <div class="adresa-liv col-lg-4">
            <h4>Adresa Livrare</h4>
            <b class="title-data datec">Adresa Livrare: </b><b class="datec dal">{{Auth::user()['adresa_liv']}}</b><br>
            <label for="edal" class="edcont"><b>Adresa Livrare</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edal" id="edal" name="edal" placeholder="Adresa Livrare">
            <b class="title-data datec">Localitate Livrare: </b><b class="datec dll">{{Auth::user()['localitate_liv']}}</b><br>
            <label for="edll" class="edcont"><b>Localitate Livrare</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edll" id="edll" name="edll" placeholder="Localitate Livrare">
            <b class="title-data datec">Judet/Sector Livrare: </b><b class="datec djsl">{{Auth::user()['judet_sector_liv']}}</b><br>
            <label for="edjsl" class="edcont"><b>Judet/Sector Livrare</b></label>
            <input type="text"disabled="disabled" class="form-control edcont edjsl" id="edjsl" name="edjsl" placeholder="Judet/Sector Livrare">
          </div>
		@endif
        </div>
		<input type="hidden" name="edid" value="{{Auth::user()['id']}}">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<a href="" style="display:none;" class="btn btn-success edsubmit col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2"><span style="" class="glyphicon glyphicon-ok"></span> Salveaza Modificarile Efectuate <span style="" class="glyphicon glyphicon-ok"></span></a>
      </form>
    </div>
  </div>

<div class="modal fade" id="editcontatentie" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h3 style="text-align:center;color:#098ead;" class="modtitlu">Atentie</h3>
					</div>
					<div class="modal-body">
						<h5 style="text-align:center;color:#f5811e;margin-bottom:10px;" class="modintreb"><b>Esti sigur ca doresti modificarea datelor contului tau ?</b></h5>
						<div class="form-group butvalid " style="">
							<button class="btn btn-success okedit  " style="">Doresc</button>
							<button class="btn btn-danger noedit " style="">Nu Doresc</button>
						</div>

						<div class="progress"   style="background-color:#098ead;display:none;">
							<div class="progress-bar econt" data-transitiongoal="100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background-color:#f5811e;">
							</div>		
						</div>

						<h5 class="ecomplete" style="text-align:center;color:#f5811e;display:none;"><b>Editare completa.</b></h5>			
					</div>
				</div>
			</div>	
		</div>
@include('partiale/footer')
@include('partiale/footermob')

  <script src="/js/jquery-2.1.4.min.js"></script>
 <script src="/bootstrap/js/bootstrap.min.js"></script>

  <script src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>

  <script src="/js/mobdd.js"></script>
  <script src="/js/editcont.js"></script>
<script src="/js/addcartprod.js"></script>
<script src="/js/misc.js"></script>
</body>
</html>                                                                                                                                                                                                                                                                                                                                                        
