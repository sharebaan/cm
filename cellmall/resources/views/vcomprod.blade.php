   <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/layout.css">
  <link rel="stylesheet" href="/slick/slick.css">
  <link rel="stylesheet" href="/slick/slick-theme.css">
  <title>Produse din Comanda</title>
</head>
<body>

@include('partiale/header')

 <div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="mininav col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="pull-right">
    <a href="/istoriccom" role="button" class="btn btn-primary istoric ">Istoric Comenzi</a>
  </div>
</div>
@if(isset($cprod))
	
    <table class="fincom-prod">
      <thead>
        <tr>
          <th class="c1">&nbsp;</th>
          <th class="c2">Denumire Produs</th>
          <th class="c3">Cod</th>
          <th class="c4">Cantitate</th>
          <th class="c5">Pret/buc</th>
          <th class="c6">Subtotal</th>
          <th class="c7">&nbsp;</th>
        </tr>
      </thead>
      <tbody>

	@for($i=count($cprod)-1;$i>=0;$i--)
        <tr>
		@if($cprod[$i]['poza'] == '')
          <td data-label="Imagine produs" class="c1"><a href="/produs/{{$cprod[$i]['poza']}}"><img src="/products/no_image.png" alt=""></a></td>
		@else
			<td data-label="Imagine produs" class="c1"><a href="/produs/{{$cprod[$i]['id']}}"><img src="{{$cprod[$i]['poza']}}" alt=""></a></td>
		@endif
          <td data-label="Denumire Produs" class="c2"><a href="/produs/{{$cprod[$i]['id']}}">{{$cprod[$i]['denumire_produs']}}</a></td>
          <td data-label="Cod" class="c3">{{$cprod[$i]['cod_produs']}}</td>
		  
          <td data-label="Cantitate" class="c4">
              <div class="comqty" >{{$pqty[$i]->cantitate}}</div>
          </td>
			@if($dprod[$i]->isEmpty() == false)
          		<td data-label="Pret/buc" class="c5"><p>{{number_format($dprod[$i][0]->pret_disc,2,'.',',')}}</p><p>&nbsp RON</p></td>
          		<td data-label="Subtotal" class="c6"><p>{{number_format($pqty[$i]->cantitate * $dprod[$i][0]->pret_disc,2,'.',',')}}</p><p>&nbsp RON</p></td>
			@else
          		<td data-label="Pret/buc" class="c5"><p>{{number_format($cprod[$i]['pret'],2,'.',',')}}</p><p>&nbsp RON</p></td>
          		<td data-label="Subtotal" class="c6"><p>{{number_format($pqty[$i]->cantitate * $cprod[$i]['pret'],2,'.',',')}}</p><p>&nbsp RON</p></td>
			@endif
          </td>
        </tr>
	@endfor
        
      </tbody>
    </table>
    
@endif
  </div>

@include('partiale/footer')
@include('partiale/footermob')


<script src="/js/jquery-2.1.4.min.js"></script>
<script src="/js/mobdd.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
<script src="/js/addcartprod.js"></script>
<script src="/js/misc.js"></script>
</body>
</html>                                                                                                                                                                                                                                                                                                                                                        
