<!DOCTYPE html>

<html lang="en">



<head>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <link rel="stylesheet" href="/css/layout.css">
	<link rel="stylesheet" href="/slick/slick.css">
	<link rel="stylesheet" href="/slick/slick-theme.css">
	<link rel="stylesheet" href="/js/fotorama-4.6.4/fotorama.css">  
  <title>CellMall</title>

</head>

<body>
@include("partiale/header")
@if(!isset($produs))
	<h4 style="color:#f5811e;margin-left:20px;" >Imi pare rau, dar nu exista acest produs.</h4>	
		@if(Auth::user()->admin == 1 || Auth::user()->angajat_produse == 1)
			<div class="mininav col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<a href="/produsid/anterior/{{$null->id - 1}}" role="button" class="btn btn-danger">Anterior Id</a>
    			<a href="/produsid/urmator/{{$null->id + 1}}" role="button" style="float:right;" class="btn btn-primary">Urmatorul Id</a>
			</div>
			<div class="mininav col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<a href="/codprodus/anterior/{{$null->cod_produs - 1}}" role="button" class="btn btn-danger">Anterior Cod Produs</a>
    			<a href="/codprodus/urmator/{{$null->cod_produs + 1}}" role="button" style="float:right;" class="btn btn-primary">Urmatorul Cod Produs</a>
			</div>
		@endif		

@else
@if(Auth::user()->admin == 1 || Auth::user()->angajat_produse == 1)
<div class="mininav col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<a href="/produsid/anterior/{{$produs->id - 1}}" role="button" class="btn btn-danger">Anterior Id</a>
    <a href="/produsid/urmator/{{$produs->id + 1}}" role="button" style="float:right;" class="btn btn-primary">Urmatorul Id</a>
</div>
<div class="mininav col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<a href="/codprodus/anterior/{{$produs->cod_produs - 1}}" role="button" class="btn btn-danger">Anterior Cod Produs</a>
    <a href="/codprodus/urmator/{{$produs->cod_produs + 1}}" role="button" style="float:right;" class="btn btn-primary">Urmatorul Cod Produs</a>
</div>
@endif
<div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<div class="pozaveziprodus col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-3 col-md-offset-0 col-lg-3 col-lg-offset-0">
		@if($produs->poza == '/products/no_image.png')
			<img src="/products/no_image.png" class="img-rounded img-responsive pprod col-xs-12 col-sm-12 col-md-12 col-lg-12" alt="">
		@elseif($produs->poza == '')
			<img src="/products/no_image.png" class="img-rounded img-responsive pprod col-xs-12 col-sm-12 col-md-12 col-lg-12" alt="">
		@else
			<div class="fotorama" data-allowfullscreen="true" data-loop="true" data-nav="thumbs" data-width="100%" data-ratio="800/600">
				@foreach($poze as $p)
					<img src="{{$p->poza}}" class="imgyes col-xs-12 col-sm-12 col-md-12 col-lg-12">	
				@endforeach		
			</div>				
		@endif
	</div>	
	
	<div class="titluveziprodus col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-9 col-md-offset-0 col-lg-9 col-lg-offset-0">
		<div class="titlu-vp col-xs-12 col-sm-12 col-md-12 col-lg-12"><h3>{{$produs->denumire_produs}}</h3></div>
		<div class="detalii1 col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 col-md-4 col-md-offset-0 col-lg-4 col-lg-offset-0">
			<small><b class="bprod">Marca: </b>{{$br->nume}}</small><br>
			<small><b class="bprod">Producator: </b>{{$prod->nume}}</small><br>
			<small><b class="bprod">Cod Produs: </b>{{$produs->cod_produs}}</small><br>
			<small><b class="bprod">Id Produs: </b>{{$produs->id}}</small><br>
		</div>
		<div class="detalii2 col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 col-md-4 col-md-offset-0 col-lg-4 col-lg-offset-0">
			@if(Auth::user()->demo == 1)
				<small class="dispprod col-sm-12"><b>Disponibilitate:  </b><b  class="dispd"style="color:#f5811e;">Client Demo <span style="color:#f5811e;" class="glyphicon glyphicon-ok-circle symbol"></span></b></small>
			@else
			@if($disp[0]->in_stoc == 1)
				<small class="dispprod col-sm-12"><b>Disponibilitate:  </b><b  style="color:#6fd646;">In Stoc <span style="color:#6fd646;" class="glyphicon glyphicon-ok-circle symbol"></span></b></small>
			@elseif($disp[0]->stoc_redus == 1)
				<small class="dispprod col-sm-12 "><b>Disponibilitate: </b><b  style="color:#fce916;">Stoc Redus <span style="color:#fce916;" class="glyphicon glyphicon-exclamation-sign"></span></b></small>
			@elseif($disp[0]->produs_indisponibil == 1)
				<small class="dispprod col-sm-12 "><b>Disponibilitate: </b><b  style="color:#f73d3d;">Produs Indisponibil <span style="color:#f73d3d;" class="glyphicon glyphicon-remove-sign"></span></b></small>
			@else
				<small class="dispprod col-sm-12 "><b>Disponibilitate: </b><b  style="color:#9866c5;">In Curand <span style="color:#9866c5;" class="glyphicon glyphicon-time"></span></b></small>
			@endif
				<br>
			@endif
				<small class="dispprod col-sm-12"><b>Garantie:  </b><b  style="color:#6fd646;">6 Luni <span style="color:#6fd646;" class="glyphicon glyphicon-ok-circle symbol"></span></b></small>
				<br>	
				@if($produs->discount == null)
				<small class="dispprod col-sm-12"><b>Discount: <b class="sprodd" style="color:#333;"> 0%</b></b></small>
				@else
				<small class="dispprod col-sm-12"><b>Discount: <b class="sprodd" style="color:#333;"> {{$produs->discount}}%</b></b></small>
				@endif
		</div>
		<div class="detalii3 col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 col-md-4 col-md-offset-0 col-lg-4 col-lg-offset-0">
			@if($produs->noutati != 0)
				<p class="col-sm-12"><b class="disps "><span class="glyphicon glyphicon-star"></span> Produs Nou Adaugat</b></p>	
			@endif
			@if($produs->promotii != 0)
				<p class="col-sm-12"><b class="disps"><span class="glyphicon glyphicon-gift"></span> Produs aflat la Promotie.</b></p>	
			@endif
			@if($produs->lichidari_stoc != 0)
				<p class="col-sm-12"><b class="disps"><span class="glyphicon glyphicon-piggy-bank"></span> Produs in Lichidare de Stoc.</b></p>	
			@endif
		</div>
		<div class="adauga-vp col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3">
			@if($produs->dpc->isEmpty() == false)
				<h5 style="text-decoration:line-through;">Pret Vechi: {{$produs->pret}} RON</h5>
				<h4>Pret Personal: {{$produs->dpc[0]->pret_disc}} RON</h4>
			@else
				@if($produs->discount == 0)
					<h4>Pret: {{$produs->pret}} RON</h4>
				@else	
					<h5 style="text-decoration:line-through;">Pret Vechi: {{$produs->pret_original}} RON</h5>
					<h4>Pret Nou: {{$produs->pret}} RON</h4>
				@endif
			@endif

			@if(Auth::user()->demo == 1)
			<div class="bucket-adauga   col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><button name="{{$produs->id}}" disabled style="cursor:not-allowed" class="caradd btn btn-primary">Adauga in cos</button></div>



        	<div class="bucket-qty-title-vp col-lg-12"><small >Cantitate</small></div>


			
			<div class="bucket-vp col-xs-12 col-sm-12  col-md-7 col-md-offset-3 col-lg-5 col-lg-offset-4">
        		<div class="form-group qtygroup ">
				
          		<div class="btn-group bgfirst" >

            	<button disabled style="cursor:not-allowed" class="btn btn-default plusmin minus" value="-" type="button">-</button>

          		</div>

          		<input type="text" class="form-control qty">

          		<div class="btn-group bgsecond">

              		<button disabled style="cursor:not-allowed" class="btn btn-default plusmin plus" value="+" type="button">+</button>

          		</div>

				</div>
				</div>
		@else
			@if($disp[0]->in_stoc == 1)
				@if(Auth::user()->admin || Auth::user()->angajat_produse)
					<div class="bucket-adauga  col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><a href="/dashboard/veziindashboard/{{$produs->cod_produs}}" target="_blank" name="{{$produs->cod_produs}}" class=" btn btn-primary">Vezi in dashboard</a></div>
				@endif
				<div class="bucket-adauga   col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><button name="{{$produs->id}}" class="caradd btn btn-primary">Adauga in cos</button></div>
        		<div class="bucket-qty-title-vp col-lg-12"><small >Cantitate</small></div>


				
				<div class="bucket-vp col-xs-12 col-sm-12  col-md-7 col-md-offset-3 col-lg-5 col-lg-offset-4">
        		<div class="form-group qtygroup ">
				
          		<div class="btn-group bgfirst" >

            		<button class="btn btn-default plusmin minus" value="-" type="button">-</button>

          		</div>

          		<input type="text" class="form-control qty">

          		<div class="btn-group bgsecond">

              		<button class="btn btn-default plusmin plus" value="+" type="button">+</button>

          		</div>

				</div>
				</div>	
			@elseif($disp[0]->stoc_redus == 1)
				@if(Auth::user()->admin || Auth::user()->angajat_produse)
					<div class="bucket-adauga  col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><a href="/dashboard/veziindashboard/{{$produs->cod_produs}}" role="button" name="{{$produs->cod_produs}}" class=" btn btn-primary">Vezi in dashboard</a></div>
				@endif
				<div class="bucket-adauga   col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><button name="{{$produs->id}}" class="caradd btn btn-primary">Adauga in cos</button></div>
        		<div class="bucket-qty-title-vp col-lg-12"><small >Cantitate</small></div>


				
				<div class="bucket-vp col-xs-12 col-sm-12  col-md-7 col-md-offset-3 col-lg-5 col-lg-offset-4">
        		<div class="form-group qtygroup ">
				
          		<div class="btn-group bgfirst" >

            	<button class="btn btn-default plusmin minus" value="-" type="button">-</button>

          		</div>

          		<input type="text" class="form-control qty">

          		<div class="btn-group bgsecond">

              		<button class="btn btn-default plusmin plus" value="+" type="button">+</button>

          		</div>

				</div>
				</div>
			@elseif($disp[0]->produs_indisponibil == 1)
				@if(Auth::user()->admin || Auth::user()->angajat_produse)
					<div class="bucket-adauga  col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><a href="/dashboard/veziindashboard/{{$produs->cod_produs}}" target="_blank"role="button" name="{{$produs->cod_produs}}" class=" btn btn-primary">Vezi in dashboard</a></div>
				@endif
				<div class="bucket-adauga   col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><button disabled style="cursor:not-allowed" name="{{$produs->cod_produs}}" class="caradd btn btn-primary">Adauga in cos</button></div>
        		<div class="bucket-qty-title-vp col-lg-12"><small >Cantitate</small></div>


				
				<div class="bucket-vp col-xs-12 col-sm-12  col-md-7 col-md-offset-3 col-lg-5 col-lg-offset-4">
        		<div class="form-group qtygroup ">
				
          		<div class="btn-group bgfirst" >

            	<button disabled style="cursor:not-allowed" class="btn btn-default plusmin minus" value="-" type="button">-</button>

          		</div>

          		<input disabled style="cursor:not-allowed" type="text" class="form-control qty">

          		<div class="btn-group bgsecond">

              		<button disabled style="cursor:not-allowed" class="btn btn-default plusmin plus" value="+" type="button">+</button>

          		</div>

				</div>
				</div>	
			@else
				@if(Auth::user()->admin || Auth::user()->angajat_produse)
					<div class="bucket-adauga  col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><a href="/dashboard/veziindashboard/{{$produs->cod_produs}}" role="button" name="{{$produs->cod_produs}}" class=" btn btn-primary">Vezi in dashboard</a></div>
				@endif
				<div class="bucket-adauga   col-xs-12 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 "><button disabled style="cursor:not-allowed" name="{{$produs->cod_produs}}" class="caradd btn btn-primary">Adauga in cos</button></div>
        		<div class="bucket-status incurand-vp col-xs-12 col-sm-12 col-md-12 col-lg-12"><small >In Curand <span class="glyphicon glyphicon-time"></span></small></div>
        		<div class="bucket-qty-title-vp col-lg-12"><small >Cantitate</small></div>


				<div class="bucket-vp col-xs-12 col-sm-12  col-md-7 col-md-offset-3 col-lg-5 col-lg-offset-4">
        		<div class="form-group qtygroup ">
				
          		<div class="btn-group bgfirst" >

            	<button disabled style="cursor:not-allowed" class="btn btn-default plusmin minus" value="-" type="button">-</button>

          		</div>

          		<input disabled style="cursor:not-allowed" type="text" class="form-control qty">

          		<div class="btn-group bgsecond">

              		<button disabled style="cursor:not-allowed" class="btn btn-default plusmin plus" value="+" type="button">+</button>

          		</div>

				</div>
				</div>
			@endif
		@endif
		</div>
		
	</div>
		
	<div class="descriere col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
		<h4>Descriere Produs</h4>
		@if(!$produs->detalii == '')
					<div class="col-sm-10 col-sm-offset-1 detalii"><p class="dettx">Detalii</p> <p>{!!$produs->detalii!!}</p></div>
					@endif
		@if(!$produs->culoare == '')
					<div class=" col-sm-10 col-sm-offset-1 culoare"><p class="ctext">Culoare: </p> <p> {{$produs->culoare}}</p></div>
					@endif
		@if(!$produs->dimensiune == '')
					<div class=" col-sm-10 col-sm-offset-1 dimensiune"><p class="dtext">Dimensiune: </p><p>{{$produs->dimensiune}}</p></div>
					@endif
		@if(!$produs->tip_ambalaj == '')
					<div class=" col-sm-10 col-sm-offset-1 tipamb"><p class="ttext">Tip Ambalaj: </p><p>{{$produs->tip_ambalaj}}</p></div>
					@endif
		@if(!$produs->atentie == null)
					<div class=" col-sm-10 col-sm-offset-1 atentie"><p class="atext">Atentie: </p><p style="font-size:16px;">{{$produs->atentie}}</p></div>			
					@endif
		
	</div>
</div>
<div class="modal fade" id="addcart" role="dialog">

			<div class="modal-dialog">

				<div class="modal-content">

					<div class="modal-header">

						<h4 style="text-align:center;">Te rog asteapta</h4>

					</div>

					<div class="modal-body">

						<div class="progress"  style="background-color:#098ead">

							<div class="progress-bar cartload" data-transitiongoal="100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background-color:#f5811e;">

							</div>		

						</div>			

					</div>

				</div>

			</div>	

		</div>			
	</div>
@endif


@include('partiale/footer')
@include('partiale/footermob')

  <script src="/js/jquery-2.1.4.min.js"></script>

  <script src="/bootstrap/js/bootstrap.min.js"></script>

  <script src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>

<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
  <script src="/js/mobdd.js"></script>

  <script src="/js/cart.js"></script>
<script src="/js/fotorama-4.6.4/fotorama.js">
		$('.fotorama').fotorama();	
</script>
  <script src="/js/addcartvp.js"></script>
<script src="/js/misc.js"></script>
<script src="/js/picswap.js"></script>
</body>
</html>
