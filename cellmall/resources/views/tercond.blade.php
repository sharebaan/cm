
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/layout.css">
  <link rel="stylesheet" href="/slick/slick.css">
  <link rel="stylesheet" href="/slick/slick-theme.css">
  <title>Contact</title>
</head>
<body>

@include('partiale/header')

  <div class="content-term col-xs-12 col-sm-12 col-md-12 col-lg-12 ">

    <div class="termcond col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
      <h4>TERMENI SI CONDITII:</h4>

          <br>

          <h4>1. DEFINITII</h4>

          <h5>Termenii folositi in prezentul act vor avea urmatorul inteles:</h5>

          <b>1.1. Pagina CELLMALL.RO</b>

          <br>

          <p>&#8226 Site-ul CELLMALL.RO, inclusiv orice sectiune sau subpagina a acestuia. Site-urile si/sau paginile de internet ori alte componente ale acestora ce apartin unor terti si care sunt accesate de catre Utilizatori ca urmare a legaturilor sau trimiterilor disponibile pe site-ul CELLMALL.RO nu fac obiectul si nici nu intra in sfera acestei definitii.

          </p>

          <b>1.2. Utilizator / User / Membru / Client / Cumparator</b>

          <br>

          <p>&#8226 Orice persoana juridica si/sau fizica, in varsta de cel putin 18 ani, cu capacitate de exercitiu deplina, care acceseaza Pagina CELLMALL.RO fiind interesata de Datele, Informatiile, Produsele, Anunturile si/sau Serviciile furnizate prin intermediul Paginii CELLMALL.RO;</p>

          <p>&#8226 Orice persoana fizica (inclusiv persoana fizica autorizata) si/sau persoana juridica care achizitioneaza Produse si/sau Servicii furnizate de S.C. VERTEK TECHNOLOGY S.R.L. prin intermediul Paginii CELLMALL.RO;</p>

          <p>&#8226 Orice persoana care acceseaza SITE-ul, in scopuri private sau profesionale si care a acceptat TERMENII SI CONDITIILE prezentului SITE, indeplinind in acest sens toate cerintele procesului de inregistrare;</p>

          <b>1.3. Furnizor / Vanzator</b>

          <br>

          <p>&#8226 S.C. VERTEK TECHNOLOGY S.R.L. - societatea comerciala S.C. VERTEK TECHNOLOGY S.R.L., persoana juridica romana, cu sediul in Bucuresti, Strada Elev Stefanescu, nr. 1, Bloc 443, Apartament 22, Sector 2, Bucuresti, Cod Postal: 021683, inregistrata la Oficiul Registrului Comertului de pe langa Tribunalul Bucuresti sub nr J40/1820/2014, CUI 32799853, atribut fiscal RO, capital social: 200 lei, Cont: RO94 BREL 0002 0008 0939 0100 deschis la LIBRA INTERNET BANK, Sucursala FUNDENI, inscrisa in Registrul de evidenta a prelucrarii datelor cu caracter personal sub nr. @@@@@@@@@@@@@@, telefon: 0766.85.88.88, fax: @@@@@@@@@@@@@, e-mail: contact@cellmall.ro;

          </p>

          <b>1.4. Comanda</b>

          <br>

          <p>&#8226 Oferta publica lansata de un Furnizor / Vanzator sau/si de catre un Utilizator / User / Membru / Client / Cumparator cu scopul de a fi publicata de catre S.C. VERTEK TECHNOLOGY S.R.L. prin intermediul Paginii CELLMALL.RO cu privire la vanzarea/cumpararea/promovarea/inchirierea  unui Produs si/sau Serviciu;

          </p>

          <b>1.5. Produs</b>

          <br>

          <p>&#8226 Orice bun sau serviciu care poate fi comercializat conform legii si care nu incalca limitele impuse prin prezentul;

          </p>

          <b>1.6. Termeni si Conditii</b>

          <br>

          <p>&#8226 Prevederile din acest document, create de S.C. VERTEK TECHNOLOGY S.R.L. care definesc conditiile si regulile pe care Utilizatorul / User-ul / Membrul / Clientul / Cumparatorul trebuie sa le respecte in vederea utilizarii Paginii CELLMALL.RO, aplicabile in egala masura la achizitionarea de catre Utilizatori a Serviciilor, Bunurilor si Produselor furnizate de S.C. VERTEK TECHNOLOGY S.R.L. prin intermediul Paginii CELLMALL.RO;

          </p>

          <b>1.7. Operatori</b>

          <br>

          <p>&#8226 Persoane special instruite de catre Furnizor / Vanzator pentru verificarea/moderarea/administrarea continutului Anunturilor, Comenzilor si Produselor de catre Utilizatori pe pagina CELLMALL.RO.

          </p>

          <br>

          <h4>2. SCOPUL URMARIT:</h4>

          <p>2.1. Scopul acestor Termeni si Conditii este de a defini si reglementa conditiile de utilizare a acestui site, precum si termenii si conditiile aplicabile Serviciilor si Produselor oferite de S.C. VERTEK TECHNOLOGY S.R.L. prin intermediul SITE-ului CELLMALL.RO persoanelor fizice si/sau juridice in vederea  vanzarii, cumpararii sau inchirierii de Produse, Bunuri sau Servicii.

          </p>

          <p>2.2 Pagina CELLMALL.RO contine legaturi sau trimiteri catre alte site-uri/pagini de internet pentru utilizarea carora se vor aplica termeni si conditii de utilizare specifici, astfel cum sunt acestia specificati pe site-urile/paginile respective, S.C. VERTEK TECHNOLOGY S.R.L.(CELLMALL.RO) nefiind raspunzatoare si neasumandu-si nicio obligatie pentru continutul respectivelor site-uri si/sau cu privire la orice alte legaturi sau trimiteri din acestea. Includerea legaturilor sau trimiterilor in Pagina CELLMALL.RO este facuta de regula pentru ajutorul sau in interesul Utilizatorului, iar in alte cazuri in scop publicitar. Intrucat S.C. VERTEK TECHNOLOGY S.R.L.(CELLMALL.RO) nu poate garanta/controla actualitatea/exactitatea informatiilor prezente pe siturile unor terti, la care se face trimitere pe Pagina CELLMALL.RO, Utilizatorul acceseaza acele site-uri si/sau le foloseste Produsele/Serviciile exclusiv pe propriul risc si pe propria raspundere.

          </p>

          <br>

          <h4>3. CONDITII DE UTILIZARE A SITE-ULUI:</h4>

          <b>3.1. Acceptarea Termenilor si Conditiilor privind utilizarea site-ului:</b>

          <p>&#8226 Folosirea site-ului CELLMALL.RO (inclusiv accesarea si vizitarea acestuia, inregistrarea, folosirea si/sau cumpararea produselor si serviciilor oferite de S.C. VERTEK TECHNOLOGY S.R.L. prin intermediul site-ului CELLMALL.RO) implica acceptarea in totalitate si neconditionata a Termenilor si Conditiilor expusi, cu toate consecintele care decurg din acceptarea acestora.

          </p>

          <p>&#8226 Utilizatorul va avea acces permanent la Termenii si Conditiile pentru utilizarea Site-ului pentru a le putea consulta in orice moment.

          </p>

          <p>&#8226 Inregistrarea ca Utilizator / User / Membru / Client / Cumparator al acestui site reprezinta acordul dumneavoastra implicit privind primirea mesajelor electronice legate de Produsele si/sau Serviciile comercializate.

          </p>

          <p>&#8226 Folosirea site-ului CELLMALL.RO si serviciile furnizate de acesta sunt rezervate doar persoanelor juridice si/sau persoanelor fizice care au implinit 18 ani.

          </p>

          <b>3.2. Schimbarea Termenilor si Conditiilor:</b>



          <p>&#8226 S.C. VERTEK TECHNOLOGY S.R.L. (CELLMALL.RO) isi rezerva dreptul de a modifica sau de a actualiza unilateral Termenii si Conditiile oricand, fara notificarea prealabila a persoanelor care utilizeaza acest site si totodata fara precizarea motivelor. Continuarea folosirii site-ului implica acceptarea modificarilor efectuate.

          </p>

          <p>&#8226 S.C. VERTEK TECHNOLOGY S.R.L. (CELLMALL.RO) isi rezerva dreptul de a-si selecta clientii, de a suspenda / sterge / bloca sau inchide accesul catre conturi, fara notificari prealabile precum si in cazul in care exista suspiciunea ca prin intermediul comenzilor publicate sau a conturilor create s-au adus sau se pot aduce prejudicii referitoare la securitatea activitatilor altor utilizatori ai serviciului. Deasemenea, in cazul in care se constata ca afecteaza in mod negativ reputatia Furnizorului sau sunt in orice alt fel daunatoare pentru Furnizor.

          </p>

          <p>&#8226 In situatia in care nu sunteti de acord cu aceste  conditii, va solicitam sa nu utilizati site-ul nostru.

          </p>

          <br>

          <h4>4. DREPTURILE DE AUTOR:</h4>

          <p>4.1.  Intregul continut al Paginii CELLMALL.RO incluzand, dar fara a se limita la texte, imagini, fotografii digitale, simboluri grafice, date tehnice, descrieri, programe, precum si orice alte date, informatii si aplicatii, sunt proprietatea S.C. VERTEK TECHNOLOGY S.R.L. si/sau a partenerilor sai, fiind protejate conform Legii drepturilor de autor si legilor privind dreptul de proprietate intelectuala si industriala.</p>



          <p>4.2. Anunturile si comenzile publicate de Utilizatori sunt obiect al drepturilor de proprietate intelectuala apartinand Utilizatorilor Paginii CELLMALL.RO, fiind protejate ca atare conform legii. Preluarea oricaror informatii si/sau date de pe acest site in scopul republicarii, distribuirii totale sau partiale sau in orice alte scopuri, sub orice forma, fara acordul scris prealabil al Furnizorului este strict interzisa si se pedepseste conform legilor in vigoare.</p>



          <br>

          <h4>5. CORECTAREA INFORMATIILOR ERONATE:</h4>

          <p>5.1. Ne rezervam dreptul de a corecta eventuale omisiuni, erori in afisare care pot surveni in urma unor greseli de dactilografiere, lipsa de acuratete sau erori ale produselor software, fara a anunta in prealabil.</p>

          <p>5.2. Ne declinam raspunderea pentru orice situatie care poate aparea din cauza unor erori de software sau defectiuni tehnice aparute la server.

          </p>

          <br>

          <h4>6. CONFIDENTIALITATE:</h4>

          <p>6.1. S.C. VERTEK TECHNOLOGY S.R.L. este inregistrata in registrul de evidenta a prelucrarii datelor cu caracter personal al Autoritatii Nationale de Supraveghere a Prelucrarii Datelor cu Caracter Personal la nr. @@@@@@@@@@@@@@. Conform cerintelor Legii nr. 677/2001 pentru protectia persoanelor cu privire la prelucrarea datelor cu caracter personal si libera circulatie a acestor date, modificata si completata si ale Legii nr. 506/2004 privind prelucrarea datelor cu caracter personal si protectia vietii private in sectorul comunicatiilor electronice S.C. VERTEK TECHNOLOGY S.R.L. are obligatia de a administra numai pentru scopurile specificate datele personale pe care ni le furnizati despre dumneavoastra sau o alta persoana si in conditii de maxima securitate.

          </p>

          <p>6.2. Scopul solicitarii si colectarii datelor cu caracter personal este de a opera si livra comenzile efectuate de dumneavoastra online.

          </p>

          <p>6.3.Conform Legii nr. 677/2001, beneficiati de dreptul de acces, de interventie si editare a datelor, dreptul de a nu fi supus unei decizii individuale precum si dreptul de a va adresa justitiei sau sa va opuneti prelucrarii datelor personale care va privesc si sa solicitati stergerea datelor. Pentru a beneficia de aceste drepturi, va rugam sa ne trimiteti o cerere scrisa prin posta sau curier rapid pe adresa societatii noastre.

          </p>

          <br>

          <h4>7. FRAUDA:</h4>

          <p>7.1. Orice incercare de a accesa datele personale ale altui utilizator, de a modifica continutul site-ului CELLMALL.RO sau de a afecta performantele serverului pe care ruleaza site-ul CELLMALL.RO, va fi considerata tentativa de fraudare a sistemelor CELLMALL.RO si va pune in miscare cercetarea penala impotriva aceluia sau acelora care a(u) incercat acest fapt.

          </p>

          <br>

          <h4>8. DREPTURILE SI OBLIGATIILE UTILIZATORULUI:</h4>

          <p>8.1. Utilizatorul are dreptul de a beneficia in intregime de serviciile site-ului, atat timp cat nu se constata o incalcare a prevederilor acestui document.

          </p>

          <p>8.2. Utilizatorul isi asuma raspunderea pentru continutul Comenzilor publicate si declara ca a verificat compatibilitatea Produselor si/sau a Serviciilor solicitate iar acestea corespund cu starea reala si juridica a produsului ce face obiectul Comenzii si ca nu incalca drepturile Furnizorului si/sau ale unor terte persoane.

          </p>

          <p>8.3. In afara informatiilor legate de propria comanda, Utilizatorului ii este strict interzis sa preia orice informatii de pe Pagina CELLMALL.RO in vederea republicarii totale sau partiale sub orice forma, fara a avea acordul in scris al Furnizorului in acest sens.

          </p>

          <p>8.4. Utilizatorul este de acord cu verificarea/moderarea/up-datarea/actualizarea/ in functie de stoc a Comenzilor publicate pe Pagina CELLMALL.RO de catre Operatorii Furnizorului ce are ca scop inlaturarea/actualizarea/administrarea comenzilor incomplete, evident incorecte si/sau care nu respecta prevederile prezentului document sau care au un continut considerat jignitor sau calomnios, sunt contrare bunelor practici etc., incercand astfel sa se ofere celorlalti Utilizatori o baza de date care sa contina un numar cat mai mic de Comenzi eronate.

          </p>

          <br>

          <h4>9. SOLUTIONAREA SESIZARILOR:</h4>

          <p>9.1. Procedura de depunere si solutionare a plangerilor si sesizarilor primite din partea Utilizatorilor:

          </p>



          <p>&#8226 Orice Utilizator poate depune o plangere sau sesizare care priveste sau are legatura cu serviciile oferite de Furnizor, respectiv Comenzile transmise catre S.C. VERTEK TEHNOLOGY S.R.L. pe/prin Pagina CELLMALL.RO, in termen de maxim 7 zile de la data producerii evenimentului si/sau aparitiei cazului sesizat.

          </p>

          <p>&#8226 De asemenea, Utilizatorii au posibilitatea sa transmita Furnizorului oricand plangeri si/sau sesizari cu privire la activitatile cu aparenta nelegala desfasurate de un Utilizator al Paginii CELLMALL.RO care privesc si/sau au legatura cu serviciile furnizate de S.C. VERTEK TEHNOLOGY S.R.L. ori cu privire la informatiile cu aparenta nelegala furnizate de un Utilizator al Paginii CELLMALL.RO, despre care au cunostinta.

          </p>

          <p>&#8226 Plangerile vor fi depuse prin intermediul formularului de contact aflat pe Pagina CELLMALL.RO.

          </p>

          <p>&#8226 Plangerile si/sau sesizarile transmise Furnizorului vor include datele de identificare a Utilizatorului, numarul de identificare a Comenzii ce face obiectul plangerii (daca reclamatia sau sesizarea priveste o anumita Comanda), precum si orice alte date necesare Furnizorului in vederea identificarii evenimentului sesizat si/sau a Comenzii (dupa caz) si o descriere cat mai detaliata a motivului pentru care a fost inaintata plangerea/sesizarea, astfel incat Furnizorul sa o poata solutiona in cel mai scurt timp posibil.

          </p>

          <p>&#8226 Plangerile/sesizarile vor fi analizate si vor primi raspuns in termen de maxim 5 zile lucratoare de la primirea acestora de catre Furnizor, in cazul in care au fost insotite de un set complet de date necesare identificarii Utilizatorului, Comenzii si/sau a problemei. In situatia in care petentul nu a transmis Furnizorului toate datele necesare identificarii problemelor semnalate si solutionarii plangerii/sesizarii respective, acesta va fi instiintat de Furnizor cu privire la informatiile lipsa, urmand ca termenul de solutionare de mai sus sa fie prelungit corespunzator.

          </p>

          <p>9.2. Pentru problemele legate de comenzi, care nu par a putea fi rezolvate prin e-mail sau telefonic cu persoana cu care discutati, puteti lua legatura pentru o conciliere interna gratuita cu consultantul nostru pe astfel de probleme comercial-administrative: George-Cristian Minca Telefon:0766.85.88.88 sau la urmatoarea adresa de e-mail: admin@cellmall.ro.

          </p>

          <p>9.3. Pentru a se adresa Autoritatii Nationale pentru Protectia Consumatorilor, Utilizatorul poate accesa link-ul:

          </p>

          <br>

          <b><a href="http://www.anpc.gov.ro/">http://www.anpc.gov.ro/

          </a></b>

          <br>

          <br>

          <h4>10. NOTA DE INFORMARE PRIVIND PROTECTIA DATELOR CU CARACTER PERSONAL:</h4>

          <p>10.1. Conform prevederilor Legii nr. 677/2001 pentru protectia persoanelor cu privire la prelucrarea datelor cu caracter personal si libera circulatie a acestor date, modificata si completata prin Legea 102/2005, S.C. VERTEK TEHNOLOGY S.R.L. are obligatia de a administra in conditii de siguranta si numai pentru scopurile specificate, datele personale pe care Utilizatorii i le furnizeaza cu ocazia accesarii Paginii CELLMALL.RO si/sau completarii formularelor disponibile pe Pagina CELLMALL.RO.

          </p>

          <p>10.2. S.C. VERTEK TEHNOLOGY S.R.L. este inscrisa in Registrul de evidenta a prelucrarii datelor cu caracter personal sub nr. @@@@@@@@@@@@@.

          </p>

          <p>10.3. Orice persoana care acceseaza Pagina CELLMALL.RO si/sau care ofera date si/sau informatii cu caracter personal prin intermediul formularelor disponibile pe Pagina CELLMALL.RO, isi manifesta consimtamantul in mod expres si neechivoc pentru ca S.C. VERTEK TEHNOLOGY S.R.L. sa prelucreze datele cu caracter personal furnizate de Utilizatori cu aceasta ocazie. Datele cu caracter personal colectate de catre S.C. VERTEK TEHNOLOGY S.R.L. prin intermediul Paginii CELLMALL.RO vor fi utilizate pentru scopurile asociate achizitionarii de catre Utilizatori a produselor si serviciilor oferite de Furnizor prin intermediul acestui site, respectiv prestarii in conditii optime a serviciilor solicitate de catre Utilizatori Furnizorului, precum si, in general, a administrarii relatiei Furnizorului cu Utilizatorii, in scopuri de marketing (inclusiv marketing direct), in vederea efectuarii de studii de piata si inclusiv intreprinderea de alte activitati de catre Furnizor, permise de lege, ce nu fac obiectul altor aprobari separate exprese din partea Utilizatorilor. Prin urmare, toate datele oferite de Utilizatori prin accesarea si/sau completarea formularelor disponibile pe Pagina cellmall.ro vor putea fi colectate si prelucrate de catre Furnizor, putand fi folosite de catre acesta. In situatii exceptionale, conform prevederilor legale in vigoare, autoritatile pot solicita ca Furnizorul sa dezvaluie anumite informatii care pot conduce la identificarea unui Utilizator, conform prevederilor legale aplicabile.



          </p>

          <p>10.4. Conform Legii nr. 677/2001, fiecare Utilizator beneficiaza de dreptul de acces, de interventie asupra datelor, dreptul de a nu fi supus unei decizii individuale si dreptul de a se adresa justitiei. Totodata, Utilizatorul are dreptul sa se opuna prelucrarii datelor personale care il privesc si sa solicite stergerea datelor.

          </p>

          <p>10.5. Pentru exercitarea oricaruia dintre aceste drepturi, Utilizatorul se poate adresa cu o cerere scrisa in acest sens, datata si semnata, la urmatoarea adresa: S.C. VERTEK TEHNOLOGY S.R.L., Bucuresti, Strada Elev Stefanescu, nr.1, Bloc 443, Apartament 22, Sector 2, Cod Postal 021683 sau pe adresa de e-mail: admin@cellmall.ro specificand totodata dreptul pe care doreste sa il exercite asupra datelor sale.

          </p>



          <br>

          <h4>11. GARANTIE:</h4>

          <p>11.1. Toate Produsele comercializate de catre S.C. VERTEK TEHNOLOGY S.R.L. (CELLMALL.RO) sunt Produse compatibile cu exceptia Produselor unde se specifica explicit originalitatea (ORIGINAL/ORIGINALA).

          </p>

          <p>11.2. Pentru a beneficia de garantie in cazul Produselor din categoria Display-uri, Touchscreen-uri, Geam-uri, Benzi si Componente GSM, mentionam ca acestea trebuie sa fie testate/montate intr-un Service GSM care va elibera un bon de service ce va atesta operatiunea efectuata. In caz contrar Produsele nu beneficiaza de garantie deoarece montarea lor necesita cunostinte de service.

          </p>

          <p>11.3. Produsele nu beneficiza de garantie in cazul in care se constata ca au fost deteriorate in urma montarii sau utilizarii incorecte.

          </p>

          <p>11.4. Produsele cumparate de pe site-urile noastre beneficiaza de garantia obisnuita oferita de catre fiecare producator in parte si anume:

          </p>

          <ul>

          <li>Acumulatorii si Incarcatoarele au garantie 12 luni, exceptand unde este specificat in mod expres alta perioada de garantie (produse second-hand, SWAP)

          </li>

          <li>Celelalte produse, (Display, Touchscreen-uri, Benzi, Box-uri, Componentele GSM, Scule Service GSM, Carcase, Cabluri de date, Xsim, Rsim-uri, etc.), fiind considerate consumabile, au garantie 3 luni, exceptand unde este specificat in mod expres alta perioada de garantie (produse second-hand, SWAP)

          </li>

          <li>Terminalele GSM (Telefonele Mobile), Tabletele si Laptop-urile au garantie 12/24 luni, exceptand unde este specificat in mod expres alta perioada de garantie (produse second-hand, SWAP)

          </li>

          </ul>

          <p>11.5. In cazul LCD-urilor (Display-uri) Produsele sunt considerate defecte si se inlocuiesc doar daca au mai mult de 3 pixeli morti.

          </p>

          <p>11.6. Garantia se ofera numai in cazul in care Produsele au sigiliile de garantie intregi, folia protectoare intacta (neindepartata) (in cazul display-urilor si touchscreen-urilor), daca produsele nu sunt deteriorate mecanic sau fizic, nu prezinta urme de lovituri sau urme de lichid, pete de orice culoare (in principal pe LCD-uri) survenite ca urmare a nefolosirii corespunzatoare a Produselor.

          </p>

          <p><b>&#8226 Atentie! Va rugam ca Produsele din cateoriile Display si Touchscreen sa le testati fara sa indepartati sigiliile si/sau foliile de protectie. Indepartarea acestora duce automat la pierderea GARANTIEI.</b>

          </p>

          <p>11.7. In cazul in care se constata ca Produsele sunt defecte, acestea beneficiaza de transport tur-retur gratuit, inlocuirea facandu-se fie in ziua urmatoare a constatarii, fie la urmatoarea comanda (in functie de cum se stabileste de comun acord cu Utilizatorul).

          </p>

          <p>11.8. In cazul in care se constata ca Produsele sunt functionale sau deteriorate in urma montarii sau utilizarii incorecte a Utilizatorului returnarea Produselor se va face pe cheltuiala Utilizatorului si achitand ramburs suma de 38 RON@@@@@@@@@@@@@ care reprezinta contravaloarea celor 2 transporturi (tur-retur).

          </p>

          <p>11.9. Eventualele reclamatii privind deficientele estetice si/sau de functionalitate a Produselor vor fi luate in considerare si vor fi analizate doar daca acestea se reclama in primele 48 ore de la momentul receptiei Produsului.

          </p>

          <br>

          <h4>12. ANULARE COMANDA</h4>

          <p>12.1. S.C. VERTEK TEHNOLOGY S.R.L. (CELLMALL.RO) si furnizorii sai nu isi asuma responsabilitatea pentru greselile de tiparire sau afisare referitor la preturile, caracteristicile sau imaginile Produselor. In cazul in care o comanda nu poate fi onorata din cauza unor astfel de erori, CELLMALL.RO isi rezerva dreptul de a anula comanda respectiva fara o notificare prealabila.

          </p>

          <p>12.2. S.C. VERTEK TEHNOLOGY S.R.L. (CELLMALL.RO) face toate eforturile in a furniza informatii corecte despre pretul si caracteristicile Produselor. Este posibil ca unele detalii, denumiri, descrieri, compatibilitati, imagini sau preturi sa fie eronate. Pentru aceasta este absolut necesara verificarea tuturor detaliilor referitoare la Produs. In cazul inregistrarii unei comenzi cu un pret derizoriu sau cu informatii incorecte S.C. VERTEK TEHNOLOGY S.R.L. are dreptul sa anuleze comanda Cumparatorului.

          </p>

          <p>12.3. S.C. VERTEK TEHNOLOGY S.R.L. (CELLMALL.RO) isi rezerva dreptul de a anula comanda, fara nicio obligatie ulterioara a vreunei parti una fata de cealalta sau fara ca vreo parte sa poata sa pretinda celeilalte daune-interese, in urmatoarele situatii, si nu numai:

          </p>

          <ul>

          <li>preturile si detaliile afisate pe site-ul CELLMALL.RO sunt rezultatul unei erori materiale sau sunt derizorii;

          </li>

          <li>produsul/produsele comandate nu se mai afla pe stoc;

          </li>

          <li>tranzactia nu a fost acceptata de catre banca emitenta a cardului Cumparatorului;

          </li>

          <li>datele furnizate de catre Client/Cumparator/User/Utilizator/Membru pe Site sunt incomplete si/sau incorecte;

          </li>

          </ul>

          <br>

          <h4>13. DISPOZILTII FINALE:</h4>

          <p>13.1. Orice problema cauzata de produsele si / serviciile prezentate pe SITE se va solutiona pe cale amiabila in termen de 15 zile lucratoare de la data sesizarii in scris a problemelor, de catre UTILIZATOR.

          </p>

          <p>13.2. S.C. VERTEK TEHNOLOGY S.R.L. (CELLMALL.RO) nu raspunde de nici o pierdere, costuri, procese, pretentii, cheltuieli sau alte raspunderi, in cazul in care acestea sunt cauzate direct de nerespectarea Termenilor si conditiilor.

          </p>

          <p>13.3. S.C. VERTEK TEHNOLOGY S.R.L. (CELLMALL.RO) nu raspunde pentru prejudiciile create ca urmare a nefunctionarii Site-ului precum si pentru cele rezultand din imposibilitatea accesare anumitor link-uri publicate pe Site.

          </p>

          <p>13.4. S.C. VERTEK TEHNOLOGY S.R.L. (CELLMALL.RO) isi rezerva dreptul de a putea efectua orice modificari ale acestor prevederi, precum si orice modificari asupra site-ului CELLMALL.RO, structurii acestuia sau orice alte modificari ce ar putea afecta site-ul, fara a fi necesara vreo notificare prealabila catre utilizatori in acest sens. Daca oricare dintre clauzele de mai sus va fi gasita nula sau nevalida, indiferent de cauza, aceasta clauza nu va afecta valabilitatea celorlalte clauze. In momentul efectuarii unei comenzi on-line, Clientul/Utilizatorul/Userul/Membrul/Cumparatorul declara ca a luat cunostinta si este intru totul de acord cu cele mentionate mai sus.

          </p>

          <p>13.5. Toate produsele comercializate de catre S.C. VERTEK TEHNOLOGY S.R.L. sunt produse compatibile cu exceptia produselor unde se specifica explicit originalitatea (ORIGINAL/ORIGINALA).

          </p>

    </div>

	@include('partiale/footer')
    @include('partiale/footermob')

  
  <script src="/js/jquery-2.1.4.min.js"></script>

  <script src="/bootstrap/js/bootstrap.min.js"></script>

  <script src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
  <script src="/js/mobdd.js"></script>

  <script src="/js/cart.js"></script>
	
  <script src="/js/addcartprod.js"></script>
<script src="/js/misc.js"></script>
</body>
</html>
