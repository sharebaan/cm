<!DOCTYPE html>

<html lang="en">



<head>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <link rel="stylesheet" href="/css/layout.css">
  <link rel="stylesheet" href="/bootstrap/bootstrap-slider/dist/css/bootstrap-slider.min.css">
  <link rel="stylesheet" href="/slick/slick.css">
  <link rel="stylesheet" href="/slick/slick-theme.css">
  <title>CellMall</title>

</head>



<body>

@include("partiale/header")

  <div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">



   @include("partiale/sidebar") 



    <div id="content-wrapper" class="content-wrapper col-xs-12 col-sm-12 col-md-10 col-lg-10">

    @if(isset($prod))
      @if(count($prod) != 0)
	@foreach($prod as $index=>$p)
      <div class="bucket col-xs-12 col-sm-6 col-md-4 col-lg-3">
		
		@if($p->promotii == 1)
			<div class="simbol">
				<img src="/simboluri/promo_badge_green.gif" class="">
			</div>
		@elseif($p->noutati == 1)
			<div class="simbol">
				<img src="/simboluri/promo_badge_red.gif" class="">
			</div>	
		@elseif($p->lichidari_stoc == 1)
			<div class="simbol">
				<img src="/simboluri/promo_badge_pink.gif" class="">
			</div>
		@elseif($p->discount != 0)
			<div class="simbol">
				<img src="/simboluri/promo_badge_darkblue.gif" class="">
				<small class="simbol2">{{$p->discount}}%</small>
			</div>
		@elseif($p->swap == 1)
			<div class="simbol">
				<img src="/simboluri/promo_badge_grey.jpg" class="">
			</div>
		@else
			<div class="simbol"></div>
		@endif
	@if($p->poza == '')
		<a href="/produs/{{$p->id}}"><img src="/products/no_image.png" class="img-responsive img-thumbnail bucket-img" alt=""></a>	
	@else
		<a href="/produs/{{$p->id}}" ><img src="{{$p->poza}}" class="img-responsive  img-thumbnail  bucket-img" alt=""></a>
	@endif
        

        <div class="bucket-title-div">

          <a href="/produs/{{$p->id}}" class="bucket-title"><b>{{substr($p->denumire_produs,0,130)}}</b></a>

        </div>

        <small class="bucket-codprod">Cod Produs: {{$p->cod_produs}}</small>
        <small class="bucket-codprod">Id Produs: {{$p->id}}</small>

		@for($x=0;$x<count($dpc);$x++)
			@if($dpc[$x]->prod_id == $p->id)
				<p class="bucket-price">{{$dpc[$x]->pret_disc}} RON</p>	
			@else
				<p class="bucket-price">{{$p->pret}} RON</p>
			@endif	
		@endfor
 

		@if(Auth::user()->demo == 1)
			<button style="cursor:not-allowed;" class="bucket-adauga btn btn-primary  col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 " disabled>Adauga in cos</button>

        	<small class="bucket-status">Client Demo<span class="glyphicon glyphicon-ok-sign"></span></small>

        	<small class="bucket-qty-title">Cantitate</small>



        	<div class="form-group qtygroup">

          	<div class="btn-group bgfirst" >

           	 <button style="cursor:not-allowed;" class="btn btn-default plusmin minus" value="-" type="button">-</button>

          	</div>

          	<input type="text" class="form-control qty"disabled>

          	<div class="btn-group bgsecond">

              	<button style="cursor:not-allowed;" class="btn btn-default plusmin plus" value="+" type="button">+</button>

          	</div>

        	</div>

		@else
			@if($disp[$index]->in_stoc == 1)
				<button name="{{$p->id}}" class="bucket-adauga btn btn-primary  col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 ">Adauga in cos</button>

        		<small class="bucket-status instoc">In Stoc <span class="glyphicon glyphicon-ok-sign"></span></small>

        		<small class="bucket-qty-title">Cantitate</small>



        		<div class="form-group qtygroup">

          		<div class="btn-group bgfirst">

            		<button class="btn btn-default plusmin minus" value="-" type="button">-</button>

          		</div>

          		<input type="text" class="form-control qty">

          		<div class="btn-group bgsecond">

              		<button class="btn btn-default plusmin plus" value="+" type="button">+</button>

          		</div>

        		</div>
			@elseif($disp[$index]->stoc_redus == 1)
				<button name="{{$p->id}}" class="bucket-adauga btn btn-primary  col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 ">Adauga in cos</button>

        		<small class="bucket-status stocredus">Stoc Redus <span class="glyphicon glyphicon-exclamation-sign"></span></small>

        		<small class="bucket-qty-title">Cantitate</small>



        		<div class="form-group qtygroup">

          		<div class="btn-group bgfirst" >

            	<button class="btn btn-default plusmin minus" value="-" type="button">-</button>

          		</div>

          		<input type="text" class="form-control qty">

          		<div class="btn-group bgsecond">

              		<button class="btn btn-default plusmin plus" value="+" type="button">+</button>

          		</div>

        		</div>
			@elseif($disp[$index]->produs_indisponibil == 1)
				<button disabled style="cursor:not-allowed"  class="bucket-adauga btn btn-primary  col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 ">Adauga in cos</button>

        		<small class="bucket-status stocind">Produs Indisponibil <span class="glyphicon glyphicon-remove-sign"></span></small>

        		<small class="bucket-qty-title">Cantitate</small>



        		<div class="form-group qtygroup">

          		<div class="btn-group bgfirst" >

            	<button class="btn btn-default plusmin minus" style="cursor:not-allowed" disabled  value="-" type="button">-</button>

          		</div>

          		<input type="text" class="form-control qty" style="cursor:not-allowed" disabled>

          		<div class="btn-group bgsecond">

              		<button class="btn btn-default plusmin plus" style="cursor:not-allowed" disabled value="+" type="button">+</button>

          		</div>

        		</div>
			@else
				<button disabled style="cursor:not-allowed" class="bucket-adauga btn btn-primary  col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 ">Adauga in cos</button>

        		<small class="bucket-status incurand">In Curand <span class="glyphicon glyphicon-time"></span></small>

        		<small class="bucket-qty-title">Cantitate</small>



        		<div class="form-group qtygroup">

          		<div class="btn-group bgfirst" >

            	<button class="btn btn-default plusmin minus" style="cursor:not-allowed" disabled  value="-" type="button">-</button>

          		</div>

          		<input type="text" class="form-control qty" style="cursor:not-allowed" disabled>

          		<div class="btn-group bgsecond">

              		<button class="btn btn-default plusmin plus" style="cursor:not-allowed" disabled value="+" type="button">+</button>

          		</div>

        		</div>
			@endif
		@endif

       
      
    </div>
	@endforeach
	 @else
		<h4>In curand ...</h4>
     @endif
    @endif

	@if(isset($prod))
		<div class="pages col-xs-12 col-sm-12 col-md-12 col-lg-12">
			{!! $prod->render() !!}
		</div>
	@endif

  </div>

  <div class="modal fade" id="addcart" role="dialog">

			<div class="modal-dialog">

				<div class="modal-content">

					<div class="modal-header">

						<h4 style="text-align:center;">Te rog asteapta</h4>

					</div>

					<div class="modal-body">

						<div class="progress"  style="background-color:#098ead">

							<div class="progress-bar cartload" data-transitiongoal="100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background-color:#f5811e;">

							</div>		

						</div>			

					</div>

				</div>

			</div>	

		</div>
@include('partiale/footer')
@include('partiale/footermob')


<script src="/js/jquery-2.1.4.min.js"></script>

<script src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/bootstrap/bootstrap-slider/dist/bootstrap-slider.min.js"></script>

<script src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
<script src="/js/mobdd.js"></script>

<script src="/js/cart.js"></script>
	
<script src="/js/addcartprod.js"></script>

<script src="/js/misc.js"></script>
<script src="/js/filtre.js"></script>
<script>var slider = $('#ex2').slider({});</script>
</body>



</html>

