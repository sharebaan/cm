
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/layout.css">
  <link rel="stylesheet" href="/slick/slick.css">
  <link rel="stylesheet" href="/slick/slick-theme.css">
  <title>Contact</title>
</head>
<body>
@include('partiale/header')
  <div class="content-term col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
    <div class="termcond col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
      <h4>INFORMATII:</h4>

          <br>

          <h4>DATELE FIRMEI:</h4>

		<p>Numele: SC VERTEK TECHNOLOGY SRL </p>

		<p>Adresa: STR ELEV STEFANESCU, NR 1, BLOC 443, APARTAMENT 22, SECTOR 2, BUCURESTI, COD POSTAL  021683

		</p>

		<p>Cod Unic de Inregistrare: RO 32799853 </p>

		<p>Nr. de ordine in registrul comertului: J40/1820/2014 </p>

		<p>Capital Social: 200 Lei</p>

		<p>Cont: RO94 BREL 0002 0008 0939 0100 deschis la LIBRA INTERNET BANK, Sucursala FUNDENI</p>

		<p>Program: </p>

		<p>Luni - Vineri: 10:00 - 18:00 </p>

		<p>Sambata: 8:00 - 12:00</p>

		<p>Duminica: Inchis</p>

		<p>Telefon: 0766.85.88.88</p>

		<p>E-mail:</p>

		<ul>

			<li><p> contact@cellmall.ro</p></li>

			<li><p>admin@cellmall.ro</p></li>

		</ul>

		<h4>SCURT ISTORIC</h4>

		<p>@@@@@@@@@@@@@@</p>



    </div>
</div>

    
@include('partiale/footer')
@include('partiale/footermob')

  <script src="/js/jquery-2.1.4.min.js"></script>
  <script src="/bootstrap/js/bootstrap.min.js"></script>

  <script src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
  <script src="/js/mobdd.js"></script>

  <script src="/js/addcartprod.js"></script>
<script src="/js/misc.js"></script>
</body>
</html>
