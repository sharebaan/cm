<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Resetare Parola</title>
</head>
<body>
	<h2>{{$subiect}}</h2>

	@if($status == 0)
	<p>Salut, {{$membru[0]->prenume}} {{$membru[0]->nume}} da click pe link-ul de mai jos pentru a-ti schimba parola.</p>
	<br>
	<a href="http://www.cellmall.ro/parolareset/{{$membru[0]->nume}}/{{$link}}">Da un click aici.</a>
	@else
	<p>User-ul {{$membru[0]->prenume}} {{$membru[0]->nume}} si-a schimbat parola</p>
	@endif
</body>
</html>
