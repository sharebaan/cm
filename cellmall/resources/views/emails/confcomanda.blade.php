<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Confirmare Comanda</title>
</head>
<body>
	<h2>{{$subiect}} {{$com[0]->comanda_id}} de pe <a href="http://cellmall.ro">cellmall.ro</a></h2>

	<p>Buna ziua ,</p>
	<p>Comanda 	{{$com[0]->comanda_id}} a fost preluata in curs de procesare.
	Pentru urmarirea coletului puteti apela la numarul 07xxxxxxxx sau pe pagina contului
tau la sectiune istoric comenzi.</p>
	<p>Costul transportului pentru acesta comanda este de {{$com[0]->total}} RON</p>
	
	<ul>
		<li>In momentul receptiei, va rugam sa verificati integritatea exterioara a coletului si apoi sa semnati de primire</li>
		<li>Factura produselor este atasata acestui email, iar chitanta va fi eliberata de catre reprezentantul firmei de curierat.</li>
	</ul>

	<p>Coletul dvs. a fost pregatit de @@@@@.Daca coletul tau nu ajunge in maxim 48 de ore, te rog sa ne contactatezi imediat.</p>
	<p>In cazul in care refuzi un colet, ne rezervam dreptul de a nu mai onora comenzile.</p>
	<p>Pentru a relua colaborarea, se va achita in avans contravaloarea transportului pentru coletul 
	refuzat(tur - retur) si cea a comenzii in curs in avans.</p>
	<br>
	<p>Iti multumim !</p>
	<br>
	<p>Deoarece este foarte important pentru noi sa avem o colaborare cat mai buna,
te rog sa semnalezi orice problema intampinati in procesul de comanda printr-o simpla sesizare prin formularul de <a href="http://cellmall.ro/informatii/contact">contact</a></p>
	<br>
	<p>Cu stima,</p>
	<br>
	<p>Echipa CellMall.ro</p>
	<br>
	<br>
	<p>Acesta este un email automat din partea sistemului.Va rugam nu dati reply acestui email!Pentru detalii legate de comanda ta ne poti contacta la contact@cellmall.rosau la numarul 0766 858 888</p> 
	
</body>
</html>
