<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Aprobare Cont</title>
</head>
<body>
	<h2>{{$subiect}}</h2>

	<h4>Bine ai venit la CellMall.ro!</h4>
	<p>Felicitari <b>{{$prenume}} {{$nume}}</b>, esti la doar un <a href="http://cellmall.ro">click</a>
 distanta de a ne vizita pe <a href="http://cellmall.ro" style="font-size:18px;"><b>CellMall.ro</b></a>.</p>
<p>Te poti loga cu adresa ta de E-mail: <b>{{$email}}</b></p>
<br>
	<p>Pentru mai multe informatii despre sistemul de vanzare online si detalii despre
 functionarea magazinului nostru va
 recomandam sa cititi sectiunea <a href="http://cellmall.ro/informatii/termenisiconditii">Termeni si Conditii</a></p>
</body>
</html>
