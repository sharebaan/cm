<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Contact</title>
</head>
<body>
	<h2>{{$subiect}}</h2>
	
	@if($dep == "DT")
		<h4>Departamentul Tehnic</h4>
	@elseif($dep == "DC")
		<h4>Departamentul Comercial</h4>
	@else
		<h4>Departamentul Administrativ</h4>	
	@endif
	<p>{{$mesaj}}</p>
</body>
</html>