<!DOCTYPE html>

<html lang="en">

	<head>

		<meta charset="UTF-8">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Latest compiled and minified CSS -->

		<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/welcome.css">


		<title>Inregistrare</title>

	</head>
	
	

	<body>





					<div class="inner cover col-xs-12 col-sm-12 col-md-12 col-lg-12">

						<div class="row center-block">

							<img src="/thumbs/logo.png" alt="..." class="center-block img-responsive">	

						</div>

						<div class="alert alert-success success-verde col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<p>Pentru a deveni partener CellMall trebuie sa completezi acest "Formular de Inregistrare".Toate campurile sunt necesare si trebuie sa contina date valide si verificabile.</p>
						</div>					

						<form class="form-horizontal" action="/crearecontpj" method="post" id="cont" >

						

						<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" style="display:none;">

							<select class="form-control" name="tipcont" id="tpcont">

								<option value="PJ" selected="selected">Persoana Juridica</option>

								<option value="PF">Persoana Fizica</option>

							</select>

						</div>
							
						@if($errors->any())
							<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										A aparut o eroare in Crearea Contului. Verifica cu atentie datele introduse de tine.
									
									</div>
						@endif	



						<div class="form-group titpj">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<h4 class="well">Companie</h4>

								</div>

						</div>							

								

								@if(!empty($errors->first('Nume_Companie')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Nume_Companie')}}
									</div>

								@endif

							<div class="form-group ncomp">
								

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" value="{{old('Nume_Companie')}}" class="form-control" name="Nume_Companie" placeholder="Nume Companie">

								</div>

							</div>

								@if(!empty($errors->first('CUI')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('CUI')}}
									</div>

								@endif

							<div class="form-group cui">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" value="{{old('CUI')}}" class="form-control" name="CUI" placeholder="C.U.I">

								</div>

							</div>

								@if(!empty($errors->first('Nr_ord_reg_com/an')))

								
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Nr_ord_reg_com/an')}}
									</div>

								@endif

							<div class="form-group nrreg">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" value="{{old('Nr_ord_reg_com/an')}}" name="Nr_ord_reg_com/an" placeholder="Nr.ord.reg.com/an">

								</div>

							</div>

								@if(!empty($errors->first('Banca')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Banca')}}
									</div>

								@endif

							<div class="form-group banc">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" value="{{old('Banca')}}" name="Banca" placeholder="Banca">

								</div>

							</div>

								@if(!empty($errors->first('Cont_IBAN')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Cont_IBAN')}}
									</div>

								@endif

							<div class="form-group ciban">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" value="{{old('Cont_IBAN')}}" name="Cont_IBAN" placeholder="Cont IBAN">

								</div>

							</div>



								

							<div class="form-group titpj">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<h4 class="well">Adresa Sediu Social</h4>

								</div>

							</div>



								@if(!empty($errors->first('Adresa_Sediu_Social')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Adresa_Sediu_Social')}}
									</div>

								@endif

							<div class="form-group ass">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" value="{{old('Adresa_Sediu_Social')}}" name="Adresa_Sediu_Social" placeholder="Adresa Sediu Social">

								</div>

							</div>

								@if(!empty($errors->first('Localitate_Sediu_Social')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Localitate_Sediu_Social')}}
									</div>

								@endif

							<div class="form-group lss">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" value="{{old('Localitate_Sediu_Social')}}" name="Localitate_Sediu_Social" placeholder="Localitate Sediu Social">

								</div>

							</div>

							@if(!empty($errors->first('Judet/Sector_Sediu_Social')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Judet/Sector_Sediu_Social')}}
									</div>

								@endif

							<div class="form-group jsss">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" value="{{old('Judet/Sector_Sediu_Social')}}" name="Judet/Sector_Sediu_Social" placeholder="Judet/Sector Sediu Social">

								</div>

							</div>



						

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<h4 class="well">Persoana de Contact</h4>

								</div>

							</div>					



							@if(!empty($errors->first('Nume')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Nume')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="Nume" value="{{old('Nume')}}" placeholder="Nume">

								</div>

							</div>

							@if(!empty($errors->first('Prenume')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Prenume')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="Prenume" value="{{old('Prenume')}}" placeholder="Prenume">

								</div>

							</div>

							@if(!empty($errors->first('CNP')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('CNP')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="CNP" value="{{old('CNP')}}" placeholder="CNP">

								</div>

							</div>

							@if(!empty($errors->first('E-mail')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('E-mail')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="email" class="form-control" name="E-mail" value="{{old('E-mail')}}" placeholder="E-mail">

								</div>

							</div>

							@if(!empty($errors->first('Parola')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Parola')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="password" class="form-control" name="Parola" value="{{old('Parola')}}" placeholder="Parola">

								</div>

							</div>

							@if(!empty($errors->first('Verificare_Parola')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Verificare_Parola')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="password" class="form-control" name="Verificare_Parola" value="{{old('Verificare_Parola')}}" placeholder="Verificare Parola">

								</div>

							</div>

							@if(!empty($errors->first('Telefon')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Telefon')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="Telefon" value="{{old('Telefon')}}" placeholder="Telefon">

								</div>

							</div>



							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<h4 class="well">Adresa Facturare</h4>

								</div>

							</div>

							

							

							<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

								<div class="checkbox" id="pj">
									<input type="checkbox" class="facSameSoc" name="copysoc">
									<label class="facSameSoc">

										 <p style="color: #098ead;font-weight:bold;">Click aici daca adresa de facturare este aceeasi cu adresa sediului social.</p>	

									</label>

								</div>

							</div>

							





							@if(!empty($errors->first('Adresa_Facturare')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Adresa_Facturare')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="Adresa_Facturare" value="{{old('Adresa_Facturare')}}" placeholder="Adresa Facturare">

								</div>

							</div>

							@if(!empty($errors->first('Localitate_Facturare')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Localitate_Facturare')}}
									</div>	

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">


									<input type="text" class="form-control" name="Localitate_Facturare" value="{{old('Localitate_Facturare')}}" placeholder="Localitate Facturare">

								</div>

							</div>

							@if(!empty($errors->first('Judet/Sector_Facturare')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Judet/Sector_Facturare')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="Judet/Sector_Facturare" value="{{old('Judet/Sector_Facturare')}}" placeholder="Judet/Sector Facturare">

								</div>

							</div>





							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<h4 class="well">Adresa Livrare</h4>

								</div>

							</div>





							<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

								<div class="checkbox" id="pj">
									<input type="checkbox"  class="livSameFac col-sm-offset-2" name="copyfac">
									<label class="livSameFac" >
										  <p style="color:#098ead;font-weight:bold;">Click aici daca adresa de livrare este aceeasi cu adresa de facturare.</p>	
									</label>

								</div>

							</div>

							@if(!empty($errors->first('Adresa_Livrare')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Adresa_Livrare')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="Adresa_Livrare" value="{{old('Adresa_Livrare')}}" placeholder="Adresa Livrare">

								</div>

							</div>

							@if(!empty($errors->first('Localitate_Livrare')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Localitate_Livrare')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="Localitate_Livrare" value="{{old('Localitate_Livrare')}}" placeholder="Localitate Livrare">

								</div>

							</div>

							@if(!empty($errors->first('Judet/Sector_Livrare')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Judet/Sector_Livrare')}}
									</div>

								@endif

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<input type="text" class="form-control" name="Judet/Sector_Livrare" value="{{old('Judet/Sector_Livrare')}}" placeholder="Judet/Sector Livrare">

								</div>

							</div>	

												

							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<textarea class="form-control" rows="3" name="mentiuni" value="{{old('mentiuni')}}" placeholder="Mentiuni: Noteaza aici alte mentiuni legate de viitorul tau cont (puncte de reper, intervale orare distincte, etc.)"></textarea>

								</div>

							</div>
								@if(!empty($errors->first('Recomandari')))

									
									<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
										<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
										<span class="sr-only">Eroare:</span>
										{{$errors->first('Recomandari')}}
									</div>

								@endif							<div class="form-group">

								<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

									<textarea class="form-control"  rows="3" name="Recomandari" placeholder="Recomandari: Noteaza aici persoana/persoanele care va recomanda si datele de contact ale acesteia/acestora (Ex: Nume,Prenume,Magazin,Telefon,E-mail,User etc.)"></textarea>

								</div>

							</div>

							<input type="hidden" name="_token" value="{{ csrf_token() }}">		

							<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">

								<button type="submit" id="register" class="btn btn-success col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">Inregistreaza-te</button>

							</div>
						

						</form>

						<br>

						<div class="pozetech col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4" >
						<div class="col-xs-4" >
								<img src="/thumbs/android.png" class="img-responsive" style="display:block;margin:0 auto;" alt="...">
						</div>
						<div class="col-xs-4" >
								<img src="/thumbs/apple.png" class="img-responsive" style="display:block;margin:0 auto;" alt="...">
						</div>
						<div class="col-xs-4" >
								<img src="/thumbs/windows.png" class="img-responsive" style="display:block;margin:0 auto;" alt="...">
						</div>
					</div>

					</div>















		



	<script src="/js/jquery.js"></script>
	<script src="/bootstrap/js/bootstrap.min.js"></script>
	<script src="/js/register.js"></script>
	

	</body>

</html>	
