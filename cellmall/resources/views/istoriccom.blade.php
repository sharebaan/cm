
   <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/layout.css">
  <link rel="stylesheet" href="/slick/slick.css">
  <link rel="stylesheet" href="/slick/slick-theme.css">
  <title>Istoric</title>
</head>
<body>
  

@include('partiale/header')

<div class="mininav col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="pull-right">
    <a href="/contulmeu" role="button" class="btn btn-primary editcont ">Detalii Cont</a>
  </div>
</div>


@if(isset($comenzi))
  <div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="table-responsive">
			
            <table class="table table-striped istoric-table">
              <thead>
                <tr>
                  <th>Nr.crt</th>
                  <th>Data</th>
                  <th>Nr.Comanda</th>
                  <th>Factura</th>
                  <th>Total</th>
                  <th>Status</th>
                  <th>Stare Plata</th>
                  <th>Stare Transport</th>
                </tr>
              </thead>
              <tbody>
				
				@for($i=0;$i<count($comenzi);$i++)
                <tr>
                  <td>{{$i+1}}</td>
                  <td>{{$comenzi[$i]->created_at}}</td>
                  <td><a href="/veziprod/{{$comenzi[$i]->comanda_id}}">{{$comenzi[$i]->comanda_id}} <span class="glyphicon glyphicon-eye-open"></span></a></td>
                  <td><a href="#">{{$comenzi[$i]->nr_factura}} <span class="glyphicon glyphicon-eye-open"></span></a></td>
                  <td>{{$comenzi[$i]->total}} RON</td>
                  <td>{{$comenzi[$i]->status_comanda}}</td>
                  <td>{{$comenzi[$i]->stare_plata}}</td>
                  <td>{{$comenzi[$i]->stare_transport}}</td>
                </tr>
				@endfor
				
				
              </tbody>
            </table>
          </div>
			
  </div>
		<div class="pages">
			{!! $comenzi->render() !!}
		</div>
@endif
@include('partiale/footer')
@include('partiale/footermob')
  <script src="/js/jquery-2.1.4.min.js"></script>
  <script src="/bootstrap/js/bootstrap.min.js"></script>

  <script src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
  <script src="/js/mobdd.js"></script>

  <script src="/js/addcartprod.js"></script>
<script src="/js/misc.js"></script>
</body>
</html>
