<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/layout.css">
  <link rel="stylesheet" href="/slick/slick.css">
  <link rel="stylesheet" href="/slick/slick-theme.css">
  <title>CellMall</title>
</head>

<body>
@include('partiale/header')
    <div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">
			@if(Session::has('comsucc'))					
					<div class="alert alert-success  col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Succes:</span>
						{{Session::get('comsucc')}}
					</div>
				@endif
			<img src="/thumbs/First-Samsung-Galaxy-S6-Promo.jpg" class="img-responsive hidden-lg hidden-md col-sm-12 col-xs-12">
			<h3 class="cartitprez hidden-xs hidden-sm col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">Noutati</h3>
    		<div class="noucar hidden-xs hidden-sm col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
				
				@if(isset($carouselnou))
					
					@foreach($carouselnou as $index => $item)
						
						<div>
							@if($item->poza == '')
							<a href="/produs/{{$item->id}}"><img src="/products/no_image.png" class="img-rounded img-responsive noucarimg" alt=""></a>
							@else
								<a href="/produs/{{$item->id}}"><img src="{{$item->poza}}" class="img-rounded img-responsive noucarimg" alt=""></a>
							@endif

							<div class="cartit">
								<a href="/produs/{{$item->id}}">@if(strlen($item->denumire_produs)>170)
																	<b>{{substr($item->denumire_produs,0,170)}}...</b>
																	@else
																	<b>{{$item->denumire_produs}}</b>
																	@endif</a>
							</div>

							<div class="carcod">
								<small><b>Cod Produs: {{$item->cod_produs}}</b></small>
							</div>
							<div class="carpret">
								<b>{{$item->pret}} RON</b>
							</div>
							<input type="hidden" value="{{$item->cod_produs}}">
							@if($disp[$index]->produs_indisponibil == 1 || $disp[$index]->in_curand == 1 || Auth::user()->demo == 1)
								<div class="carcos" style="cursor:not-allowed;">
									<a href="javascript: void(0)"  disabled><button  type="button"  name="{{$item->cod_produs}}" class="caradd btn btn-primary" disabled>Adauga in cos</button></a>
								</div>
							@else
								<div class="carcos">
									<a href="" type="button" name="{{$item->cod_produs}}" class="caradd btn btn-primary">Adauga in cos</a>
								</div>
								
							@endif
							
						</div>						
					@endforeach
				@endif
			</div>
     		<div class="modal fade hidden-xs hidden-sm" id="addcart" role="dialog">

			<div class="modal-dialog">

				<div class="modal-content">

					<div class="modal-header">

						<h4 style="text-align:center;">Te rog asteapta</h4>

					</div>

					<div class="modal-body">

						<div class="progress"  style="background-color:#098ead">

							<div class="progress-bar cartload" data-transitiongoal="100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;background-color:#f5811e;">

							</div>		

						</div>			

					</div>

				</div>

			</div>	

		</div> 
  </div>

@include('partiale/footer')
@include('partiale/footermob')

  <script src="/js/jquery-2.1.4.min.js"></script>

  <script src="/bootstrap/js/bootstrap.min.js"></script>

  <script src="/bootstrap/bootstrap-progressbar-master/bootstrap-progressbar.min.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
  <script src="/js/mobdd.js"></script>

  <script src="/js/cart.js"></script>
	
<script src="/js/carcart.js"></script>
<script src="/js/misc.js"></script>

</body>

</html>
