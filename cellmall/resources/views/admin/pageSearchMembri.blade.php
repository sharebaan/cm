@extends('admin.layout.dashboard')

@section('content')

@include('admin.layout.tree')

	@if(isset($membrii))
		<div class="tabel tabmem">
			
			@if($sp[1] != 0)
			<div class="row mr">
				<div class="cidp"><h4><a href="{{$sp[0]}}/1/id/search?{{$sp[2]}}">Id</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/email/search?{{$sp[2]}}">Email</a></h4></div>
				<div class="colmicp memr"><h4><a href="{{$sp[0]}}/1/telefon/search?{{$sp[2]}}">Telefon</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/nume/search?{{$sp[2]}}">Nume</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/prenume/search?{{$sp[2]}}">Prenume</a></h4></div>
				<div class="colmicp memr"><h4><a href="{{$sp[0]}}/1/cnp/search?{{$sp[2]}}">CNP</a></h4></div>
				
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/adresafac/search?{{$sp[2]}}">Adresa Facturare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/localfac/search?{{$sp[2]}}">Local. Facturare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/judetfac/search?{{$sp[2]}}">Judet Facturare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/adresaliv/search?{{$sp[2]}}">Adresa Livrare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/localliv/search?{{$sp[2]}}">Local. Livrare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/judetliv/search?{{$sp[2]}}">Judet Livrare</a></h4></div>		
					
				<div class="colmicp memr"><h4><a href="{{$sp[0]}}/1/status/search?{{$sp[2]}}">Status</a></h4></div>
				<div class="colmicp memr"><h4><a href="{{$sp[0]}}/1/tipcont/search?{{$sp[2]}}">Tip Cont</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/recomandari/search?{{$sp[2]}}">Recomandari</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/created/search?{{$sp[2]}}">Creat</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/1/updated/search?{{$sp[2]}}">Editat</a></h4></div>
			</div>
			@else
			<div class="row mr">
				<div class="cidp"><h4><a href="{{$sp[0]}}/0/id/search?{{$sp[2]}}">Id</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/email/search?{{$sp[2]}}">Email</a></h4></div>
				<div class="colmicp memr"><h4><a href="{{$sp[0]}}/0/telefon/search?{{$sp[2]}}">Telefon</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/nume/search?{{$sp[2]}}">Nume</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/prenume/search?{{$sp[2]}}">Prenume</a></h4></div>
				<div class="colmicp memr"><h4><a href="{{$sp[0]}}/0/cnp/search?{{$sp[2]}}">CNP</a></h4></div>
				
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/adresafac/search?{{$sp[2]}}">Adresa Facturare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/localfac/search?{{$sp[2]}}">Local. Facturare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/judetfac/search?{{$sp[2]}}">Judet Facturare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/adresaliv/search?{{$sp[2]}}">Adresa Livrare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/localliv/search?{{$sp[2]}}">Local. Livrare</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/judetliv/search?{{$sp[2]}}">Judet Livrare</a></h4></div>		
					
				<div class="colmicp memr"><h4><a href="{{$sp[0]}}/0/status/search?{{$sp[2]}}">Status</a></h4></div>
				<div class="colmicp memr"><h4><a href="{{$sp[0]}}/0/tipcont/search?{{$sp[2]}}">Tip Cont</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/recomandari/search?{{$sp[2]}}">Recomandari</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/created/search?{{$sp[2]}}">Creat</a></h4></div>
				<div class="colmarep memr"><h4><a href="{{$sp[0]}}/0/updated/search?{{$sp[2]}}">Editat</a></h4></div>
			</div>
			@endif

			@foreach($membrii as $membru)
				<div>
					<div class="row ">
						<div><a href="" id="{{$membru->id}}" class="detalii">detalii</a></div>
						<div class="cidp">{{$membru->id}}</div>
						<div class="colmarep memr">{{$membru->email}}</div>
						<div class="colmicp memr">{{$membru->telefon}}</div>
						<div class="colmarep memr">{{$membru->nume}}</div>
						<div class="colmarep memr">{{$membru->prenume}}</div>
						<div class="colmicp memr">{{$membru->CNP}}</div>
						<div class="colmarep memr">{{$membru->adresa_sediu}}</div>
						<div class="colmarep memr">{{$membru->localitate_sediu}}</div>
						<div class="colmarep memr">{{$membru->judet_sector_sediu}}</div>
						<div class="colmarep memr">{{$membru->adresa_liv}}</div>
						<div class="colmarep memr">{{$membru->localitate_liv}}</div>
						<div class="colmarep memr">{{$membru->judet_sector_liv}}</div>
						@if($membru->status == 1)
							<div class="colmicp memr">Aprobat</div>
						@else
							<div class="colmicp memr">Neaprobat</div>
						@endif
						<div class="colmicp memr">{{$membru->tipCont}}</div>
						@if($membru->recomandari == null)
							<div class="colmarep memr">Nu Exista</div>
						@else
							<div class="colmarep memr">{{$membru->recomandari}}</div>
						@endif
						<div class="colmarep memr">{{$membru->created_at}}</div>
						<div class="colmarep memr">{{$membru->updated_at}}</div>
					</div>
				<div class="dropd" id="{{$membru->id}}">
						<form action="/dashboard/editmem/{{$membru->id}}" method="post">
							<a href="" class="memedit">Edit</a>

							<div>
									<h4>Status</h4>
									<p class="input status">{{$membru->status}}</p>
									@if($membru->status==1)
									<select class="eIn" name="status" style="display:none">
										<option value="{{$membru->status}}">Aprobat</option>
										
										<option value="0">Neaprobat</option>
									</select><br>
									@else
									<select class="eIn" name="status" style="display:none">
										<option value="{{$membru->status}}">Neaprobat</option>
										<option value="1">Aprobat</option>
									
									</select><br>
									@endif
										
									
									<h4>Demo</h4>	
										<p class="input demo">{{$membru->demo}}</p>
									@if($membru->demo==1)
										<select class="eIn" name="demo"  style="display:none">
										<option value="{{$membru->demo}}">Demo</option>
										<option value="0">Real</option>
										
									</select><br>
									@else
										<select class="eIn" name="demo"  style="display:none">
										<option value="{{$membru->demo}}">Real</option>
										<option value="1">Demo</option>
										
									</select><br>
									@endif
									<h4>Email</h4>
						 			<p class="input email">{{$membru->email}}</p>
						 			<input type="text" class="eIn" name="email" placeholder="Email" value="{{$membru->email}}" style="display:none"><br>
						 			<h4>Telefon</h4>
										<p class="input telefon">{{$membru->telefon}}</p> 
										<input type="text" class="eIn" name="telefon" placeholder="Telefon" value="{{$membru->telefon}}" style="display:none"><br>
						 			<h4>Nume</h4>
						 			<p class="input nume">{{$membru->nume}}</p>
						 			<input type="text" class="eIn" name="nume" placeholder="Nume" value="{{$membru->nume}}" style="display:none"><br>
						 			<h4>Prenume</h4>
						 			<p class="input prenume">{{$membru->prenume}}</p>
						 			<input type="text" class="eIn" name="prenume" placeholder="Prenume" value="{{$membru->prenume}}" style="display:none"><br>
						 			<h4>CNP</h4>
						 			<p class="input cnp">{{$membru->CNP}}</p>
						 			<input type="text" class="eIn" name="cnp" placeholder="CNP" value="{{$membru->CNP}}" style="display:none"><br>
						 			
									<h4>Nume companie</h4>
										<p class="input numecomp">{{$membru->nume_companie}}</p> 
										<input type="text" class="eIn" name="numecomp" placeholder="Nume companie" value="{{$membru->nume_companie}}" style="display:none"><br>
									<h4>CUI</h4>
										<p class="input cui">{{$membru->CUI}}</p> 
										<input type="text" class="eIn" name="cui" placeholder="CUI" style="display:none" value="{{$membru->CUI}}"><br>
									<h4>Nr.Reg. Comertului</h4>
										<p class="input nrreg">{{$membru->nr_reg}}</p> 
										<input type="text" class="eIn" name="nrreg" placeholder="Nr.Reg. Comertului" value="{{$membru->nr_reg}}" style="display:none"><br>			
									<h4>Cont IBAN</h4>
										<p class='input iban'>{{$membru->cont_IBAN}}</p>
										<input type="text" class="eIn" name="ContIBAN" placeholder="Cont IBAN" value="{{$membru->cont_IBAN}}" style="display:none"> <br>
									<h4>Banca</h4>	
										<p class='input banca'>{{$membru->banca}}</p>
										<input type="text" class="eIn" name="Banca" placeholder="Banca" value="{{$membru->banca}}" style="display:none"> <br>
								<h4>Detalii Sediu</h4>
								<hr>
								<h4 class="eIn" style="display:none;">Adresa Sediu</h4>
								<input type="text" class="eIn" name="AdresaSediu" placeholder="Adresa sediu" value="{{$membru->adresa_sediu}}" style="display:none">
								<h4 class="eIn"style="display:none;">Localitate Sediu</h4>	
								<input type="text" class="eIn" name="LocalitateSediu" placeholder="Localitate sediu" value="{{$membru->localitate_sediu}}" style="display:none">
								<h4 class="eIn"style="display:none;">Judet Sector Sediu</h4>
								<input type="text" class="eIn" name="JudetSectorSediu" placeholder="Judet/Sector sediu" value="{{$membru->judet_sector_sediu}}" style="display:none">
									<ul>
										<li class="input ased">{{$membru->adresa_sediu}}
										</li>
										<li class="input lsed">{{$membru->localitate_sediu}}
										</li>
										<li class="input jssed">{{$membru->judet_sector_sediu}}</li>
									</ul><br>

								<h4>Detalii Facturare</h4>
								<hr>
								<h4 class="eIn" style="display:none;">Adresa Factura</h4>
								<input type="text" class="eIn" name="Adresafactura" placeholder="Adresa factura" value="{{$membru->adresa_fac}}" style="display:none">
								<h4 class="eIn" style="display:none;">Localitate Factura</h4>
								<input type="text" class="eIn" name="Localitatefactura" placeholder="Localitate factura" value="{{$membru->localitate_fac}}" style="display:none">
								<h4 class="eIn" style="display:none;">Judet/Sector Factura</h4>
								<input type="text" class="eIn" name="JudetSectorfactura" placeholder="Judet/Sector factura" value="{{$membru->judet_sector_fac}}" style="display:none">
									<ul>
										<li class="input afac">{{$membru->adresa_fac}}
										</li>
										<li class="input lfac">{{$membru->localitate_fac}}
										</li>
										<li class="input jsfac">{{$membru->judet_sector_fac}}
										</li>
									</ul><br>	
								
								<h4>Detalii Livrare</h4>
								<hr>
								<h4 class="eIn" style="display:none;">Adresa Livrare</h4>
								<input type="text" class="eIn" name="Adresalivrare" placeholder="Adresa livrare" value="{{$membru->adresa_liv}}" style="display:none">
								<h4 class="eIn" style="display:none;">Localitate Livrare</h4>
								<input type="text" class="eIn" name="Localitatelivrare" placeholder="Localitate livrare" value="{{$membru->localitate_liv}}" style="display:none">
								<h4 class="eIn" style="display:none;">Judet/Sector Livrare</h4>
								<input type="text" class="eIn" name="JudetSectorlivrare" placeholder="Judet/Sector livrare" value="{{$membru->judet_sector_liv}}" style="display:none">
									<ul>
										<li class="input aliv">{{$membru->adresa_liv}}
										</li>
										<li class="input lliv">{{$membru->localitate_liv}}
										</li>
										<li class="input jsliv">{{$membru->judet_sector_liv}}
										</li>
									</ul><br>
								
								
								
								
								<h4>KAM</h4>
								<input type="text" class="eIn" name="KAMcomercial" placeholder="KAM comercial" value="{{$membru->KAM_com}}" style="display:none">
								<input type="text" class="eIn" name="KAMlivrare" placeholder="KAM livrare" value="{{$membru->KAM_liv}}" style="display:none">
									<ul>
										<li class="input kcom">{{$membru->KAM_com}}
										</li>
										<li class="input kliv">{{$membru->KAM_liv}}
										</li>
									</ul><br>
								
								<h4>Detalii</h4>
								<input type="text" class="eIn" name="Detaliiplata" placeholder="Detalii plata" value="{{$membru->detalii_plata}}" style="display:none">
								<input type="text" class="eIn" name="Detaliilivrare" placeholder="Detalii livrare" value="{{$membru->detalii_livrare}}" style="display:none">
								<input type="text" class="eIn" name="Detaliitransport" placeholder="Detalii transport" value="{{$membru->detalii_transport}}" style="display:none">
									<ul>
										<li class="input detplata">{{$membru->detalii_plata}}
										</li>
										<li class="input detliv">{{$membru->detalii_livrare}}
										</li>
										<li class="input dettrans">{{$membru->detalii_transport}}
										</li>
									</ul><br>
								
								<h4>Mentiuni</h4>
								<textarea name="mentiuni" class="eIn" cols="30" rows="10" style="display:none">{{$membru->mentiuni}}</textarea><br>
								<p class="input mentiuni">{{$membru->mentiuni}}</p><br>

								<h4>Recomandari</h4>
								<input type="text" class="eIn" name="recomand" value="{{$membru->recomandari}}" style="display:none">
								<p class="input recomand">{{$membru->recomandari}}</p>
								
									<h4>Tip Cont</h4>
									<p class="input tipcont">{{$membru->tipCont}}</p>
								@if($membru->tipCont=='Pj')
										<select class="eIn" name="tipcont"  style="display:none">
										<option value="{{$membru->tipCont}}">Persoana Juridica</option>
										
										<option value="PF">Persoana Fizica</option>
									</select><br>
									@else
										<select class="eIn" name="tipcont"  style="display:none">
										<option value="{{$membru->tipCont}}">Persoana Fizica</option>
										<option value="PJ">Persoana Juridica</option>
										
									</select><br>
									@endif
							</div>	
							
								<input type="submit" class="editare"  value="Salvare" style="display:none">
								<a href="" class="memcancel" style="display:none">Cancel</a>
								<input type="hidden" name="_token" value="{{csrf_token()}}">
						</form>

					</div>
				</div>	
			@endforeach

		</div>

		{!!$membrii->render()!!}	
	@endif

@stop
