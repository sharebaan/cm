<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>AdminLogin</title>
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/landpage.css">

</head>
<body>
	<div class="site-wrapper">

		<div class="site-wrapper-inner">

			<div class="cover-container">


				<form action="/logadmin" method="post" class="form-controll col-sm-6 col-sm-offset-3" autocomplete="off">
					<input type="text" class="form-control" name="nadmin" placeholder="Nume Admin"><br>
					<input type="password" class="form-control" name="padmin" placeholder="Parola"><br>
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="submit" value="Logare" class="btn btn-primary col-sm-6 col-sm-offset-3">
				</form>
			
			</div>
		</div>			
	</div>
	<script src="/js/jquery.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>