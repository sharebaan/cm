<!DOCTYPE>
<html>
	<head>
		<title >Dashboard</title>
		<style>
			@media print{
				title{
					display:none !important;
				}
				.no-print{
					display:none !important;
				}
				@page{
					size:auto;
					margin: 0mm;	
				}
			}
			.first-copy ,.second-copy{
				width:154px;	
				display:block;
				margin:0 auto;
			}
			.second-copy{
				margin-left:30px;
				margin-top:4px;
			}
			p{
				font-size:0.6em;
				text-align:center;
				//font-weight:bold;
				margin:2px , 0;
				
			}
			.np p{
			//	letter-spacing:-1px;
			}
			.np{
				height:37px;
				
			}
			.cp{
				margin-top:0px;				
				margin-bottom:2px;
			}
			.row{
				margin-bottom:14px;
				margin-top:14px;
			}
			#row1{
			
			}
			#row2{
			
			}
			#row3{
				margin-top:15px;	
			}
			#row4{
			
				margin-top:20px;	
			}
		</style>
		<script src="/js/jquery.js"></script>
	</head>

	<body>
		@if(isset($prod))
			<div class="row" id="row1">
				<div class="first-copy">	
					<div class='np'><p >{{substr($prod[0]->denumire_produs,0,110)}}</p></div>	
					<img id="barcode" class="{{$prod[0]->cod_produs}}"/>
					<script>
						$(document).ready(function(){
							$('#barcode').JsBarcode($('#barcode').attr('class'),{width:1,height:25});	
						});
					</script>
					<p class='cp'>{{$prod[0]->cod_produs}}</p>
				</div>
				
			</div>
			<div class="row" id="row2">
				<div class="first-copy">	
					<div class='np'><p >{{substr($prod[0]->denumire_produs,0,110)}}</p></div>	
					<img id="barcode3" class="{{$prod[0]->cod_produs}}"/>
					<script>
						$(document).ready(function(){
							$('#barcode3').JsBarcode($('#barcode3').attr('class'),{width:1,height:25});	
						});
					</script>
					<p class='cp'>{{$prod[0]->cod_produs}}</p>
				</div>
				
			</div>
			<div class="row" id="row3">
				<div class="first-copy">	
					<div class='np'><p >{{substr($prod[0]->denumire_produs,0,110)}}</p></div>	
					<img id="barcode5" class="{{$prod[0]->cod_produs}}"/>
					<script>
						$(document).ready(function(){
							$('#barcode5').JsBarcode($('#barcode5').attr('class'),{width:1,height:25});	
						});
					</script>
					<p class='cp'>{{$prod[0]->cod_produs}}</p>
				</div>
				
			</div>
			<div class="row" id="row4">
				<div class="first-copy">	
					<div class='np'><p >{{substr($prod[0]->denumire_produs,0,110)}}</p></div>	
					<img id="barcode7"class="{{$prod[0]->cod_produs}}"/>
					<script>
						$(document).ready(function(){
							$('#barcode7').JsBarcode($('#barcode7').attr('class'),{width:1,height:25});	
						});
					</script>
					<p class='cp'>{{$prod[0]->cod_produs}}</p>
				</div>
				
			</div>
			
			
			
	@endif	
		
		 <a href="#"  class="no-print" onclick="window.print();return false;">print</a> 

		<script src="/js/JsBarcode-master/JsBarcode.js"></script>
		<script src="/js/JsBarcode-master/CODE128.js"></script>
		
	</body>
</html>
