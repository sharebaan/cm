@extends('admin.layout.dashboard')

@section('content')

<form action="/dashboard/adaugaProd" id="adaugaForm" autocomplete="off" method="post">
	@if(isset($baze))
	<select name="baza" class="bazaa" id="">
		<option value="">Alegeti</option>
		@foreach($baze as $baza)
		<option value="{{$baza->id}}" name="bazaopt">{{$baza->nume}}</option>
		@endforeach
	</select>
	@endif 

	<select name="subcatbaza" id="subcatbaza" class="subcatbaza" style="display:none">
		<option value="">Alegeti</option>
	</select>
	<select name="subcat1" id="subcat1" class="subcat1" style="display:none">
		<option value="">Alegeti</option>
	</select>
	<select name="subcat2" id="subcat2" class="subcat2" style="display:none">
		<option value="">Alegeti</option>
	</select>
	<select name="subcat3" id="subcat3" class="subcat3" style="display:none">
		<option value="">Alegeti</option>
	</select><br>

	@if(isset($branduri))
	<select name="brand" class="brand" id="">
		<option value="">Alegeti Brand</option>
		@foreach($branduri as $b)
		<option value="{{$b->id}}" name="brand">{{$b->nume}}</option>
		@endforeach
	</select><br>
	@endif
	
	@if(isset($producator))
	<select name="producator" class="brand" id="">
		<option value="">Alegeti Producator</option>
		@foreach($producator as $prodr)
		<option value="{{$prodr->id}}" name="brand">{{$prodr->nume}}</option>
		@endforeach
	</select><br>
	@endif

	<textarea name="denumire_produs" id="" cols="30" placeholder="Denumire Produs" rows="10"></textarea><br>
	<input type="text" name="poza" placeholder="poza"><br>
	<input type="text" name="cod" placeholder="Cod Produs"><br>
	<textarea name="detalii" id="" cols="60" rows="10" placeholder="Detalii Descriere"></textarea><br>
	<select name="atentie" id="">
		<option value="">Alege atentie</option>
		<option value="Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.">Cunostinte deosebite</option>
		<option value="Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.">Cunostinte banale</option>
	</select><br>
	<!--<input type="text" name="culoare" placeholder="Culoare"><br>-->
	<textarea name="culoare" cols='60' rows='3' placeholder="Culoare"></textarea><br>
	<!--<input type="text" name="dimensiune" placeholder="Dimensiune"><br>-->
	<textarea name="dimensiune" cols='60' rows='3' placeholder="Dimensiune"></textarea><br>
	<select name="tip_ambalaj" id="">
		<option value="">Alege tip amabalaj</option>
		<option value="Bulk / Folie Protectie">Bulk / Folie Protectie</option>
		<option value="Blister / Cutie">Blister / Cutie</option>
	</select><br>
	<input type="text" name="pret" placeholder="Pret"><br>
	<input type="text" name="discount" placeholder="Discount"><br>
	<input type="text" name="stoc" placeholder="Stoc"><br>
	SWAP <br>
	<select name="swap" id="">
		<option value="non">SWAP</option>
		<option value="1">Da</option>
		<option value="0">Nu</option>
	</select><br>
	Promotii <br>
	<select name="promotii" id="">
		<option value="non">Promotii</option>
		<option value="1">Da</option>
		<option value="0">Nu</option>
	</select><br>
	Noutati <br>
	<select name="noutati" id="">
		<option value="1">Noutati</option>
		<option value="1">Da</option>
		<option value="0">Nu</option>
	</select><br>
	<input type="text" name="alerta_stoc" placeholder="Alerta stoc"><br>
	<select name="lichidari_stoc" id="">
		<option value="non">lichidari stoc</option>
		<option value="1">Da</option>
		<option value="0">Nu</option>
	</select><br>
	
	

	<input type="text" name="pozitie_raft" placeholder="Pozitie Raft"><br>
	<input type="hidden" name="_token" value="{{csrf_token()}}">
	<input type="submit" value="Adauga">
</form>	
@stop
