<html>
	<head>
		<title>Factura</title>
		<style>
			table{
				width:1000px;				
				display:block;
				margin:0 auto;
				background-color:#000;
				text-align:center;
			}
			th{
				color:#000;
			}
			tr ,td ,th{
				background-color:white;
			}
			table , tr ,td ,th {
				border: 1px solid #000;
			}	
			.c1{
				width:30px;
			}
			.c2{
				width:130px;
			}
			.c3{
				width:600px;	
			}
			.c4{
				width:100px;
			}
			.c5{
				width:110px;
			}
			.c6{
				width:110px;
			}
			.c7{

			}
			.data-table{
				text-align:left;
			}
			.td-tit{
				font-weight:bold;
				font-size:18px;
			}
			.comanda{
				width:50%;
				margin-left:4%;
				display:inline-block;
			}
			.comanda h4{margin:0;margin-top:10px;}	
			.comanda h4 p{margin:0;margin-top:10px;}	
			.kam{
				width:45%;
				display:inline-block;
			}
			.header-comanda p,.header-kam p{font-size:24px;}
			.header-comanda,.header-kam,.data-head{
				color:#000;
			}
			.header-comanda,.header-kam{
				margin:0;
			}
			.header-comanda p,.header-kam p{
				margin:5px;;
			}
			.mentiune{
				width:1000px;
				display:block;
				margin:0 auto;
				text-align:center;		
			}
			.mentiune h4{
				color:#000;
			}
			.mentiune p{
				color:#000;
			}
			.data-head , .data-head p{
				margin-top:0;
				margin-bottom:0;
			}
			.data-head p{
				font-size:24px;
				display:inline-block;
				color:#000;	
			}
			.header-comanda p,.header-kam p{
				display:inline-block;
				color:#000;
			}
						
			
		
		</style>
	</head>
	<body>
		<div class='tit-data'>	
			<div class='comanda'>
				<h3 class="header-comanda">Comanda Id: <p>{{$statcom[0]->comandafac_key}}</p></h3>
				<h3 class="header-comanda">Factura Id: <p>{{$comanda[0]->nr_factura}}</p></h3>
				<h3 class="header-comanda">Data: <p>{{$statcom[0]->created_at}}</p></h3>
			</div>
			<div class='kam'>
				@if($statcom[0]->KAM_livrare == '')
				<h3 class="header-kam">KAM Livrare: <p>Nu Exista</p></h3>
				@else
				<h3 class="header-kam">KAM Livrare: <p>{{$statcom[0]->KAM_livrare}}</p></h3>
				@endif
			</div>
		</div>
		<table>
			<tr>
				<th class='c1'>Id</th>
				<th class='c2'>Cod Produs</th>
				<th class='c3'>Denumire Produs</th>
				<th class='c4'>Cantitate</th>
				<th class='c5'>Pret/Buc</th>
				<th class='c6'>Subtotal</th>
			</tr>
			@foreach($statcom as $index=>$com)

				<tr>
					<td class='c1'>{{$com->id}}</td>
					<td class='c2'>{{$com['produs']->cod_produs}}</td>
					<td class='c3'>{{$com['produs']->denumire_produs}}</td>
					<td class='c4'>{{$com->cantitate}}</td>
					@if($discP[$index]->isEmpty() == false)
					<td class='c5'>{{$discP[$index][0]->pret_disc}}</td>
					<td class='c6'>{{number_format($com->cantitate * $discP[$index][0]->pret_disc,2,'.',',')}}</td>
					@else
					<td class='c5'>{{$com['produs']->pret}}</td>
					<td class='c6'>{{number_format($com->cantitate * $com['produs']->pret,2,'.',',')}}</td>
					@endif
				</tr>	
			@endforeach
					
			</table>
			<div class='wrapper'>
			<div class='totaluri'>
				<h3 class='data-head'>Total Cos: <p>{{number_format($com->total - $com->transport,2,'.',',')}}</p></h3>
				<h3 class='data-head'>Total Transport: <p>{{$com->transport}}</p></h3>	
				<h3 class='data-head'>Total de Plata: <p>{{$com->total}}</p></h3>	
			</div>
	
			<table class="data-table">
				<tr>
					<td class="td-tit">Metoda Plata</td>
					<td>{{$statcom[0]->metoda_plata}}</td>
				</tr>	
				<tr>
					<td class="td-tit">Cont Banca</td>
					<td>{{$statcom[0]->contbanca}}</td>
				</tr>
				<tr>
					<td class="td-tit">Metoda Livrare</td>
					<td>{{$statcom[0]->metoda_livrare}}</td>
				</tr>
				<tr>
					<td class="td-tit">Tip Facturare</td>
					<td>{{$statcom[0]->mod_cumparare}}</td>
				</tr>
				<tr>
					<td class="td-tit">Tip Document</td>
					<td>{{$statcom[0]->tip_document}}</td>
				</tr>
				<tr><td></td><td></td></tr>
				<tr>
					<td class="td-tit">Nume</td>
					<td>{{$membru->nume}}</td>
				</tr>
				<tr>
					<td class="td-tit">Prenume</td>
					<td>{{$membru->prenume}}</td>
				</tr>
				<tr>
					<td class="td-tit">CNP</td>
					<td>{{$membru->CNP}}</td>
				</tr>
				<tr>
					<td class="td-tit">E-mail</td>
					<td>{{$membru->email}}</td>
				</tr>
				<tr>
					<td class="td-tit">Telefon</td>
					<td>{{$membru->telefon}}</td>
				</tr>
				<tr>
					<td class="td-tit">Recomandari</td>
					<td>{{$membru->recomandari}}</td>
				</tr>
				<tr><td></td><td></td></tr>
				<tr>
					<td class="td-tit">Nume Companie</td>
					<td>{{$membru->nume_companie}}</td>
				</tr>
				<tr>
					<td class="td-tit">C.U.I</td>
					<td>{{$membru->CUI}}</td>
				</tr>
				<tr>
					<td class="td-tit">Nr.ord.reg.com/an</td>
					<td>{{$membru->nr_reg}}</td>
				</tr>
				<tr>
					<td class="td-tit">Banca</td>
					<td>{{$membru->banca}}</td>
				</tr>
				<tr>
					<td class="td-tit">Cont IBAN</td>
					<td>{{$membru->cont_IBAN}}</td>
				</tr>
				<tr><td></td><td></td></tr>
				<tr>
					<td class="td-tit">Adresa Sediu Social</td>
					<td>{{$membru->adresa_sediu}}</td>
				</tr>
				<tr>
					<td class="td-tit">Localitate Sediu Social</td>
					<td>{{$membru->localitate_sediu}}</td>
				</tr>
				<tr>
					<td class="td-tit">Judet/Sector Sediu Social</td>
					<td>{{$membru->judet_sector_sediu}}</td>
				</tr>
				<tr><td></td><td></td></tr>
				<tr>
					<td class="td-tit">Adresa Facturare</td>
					<td>{{$membru->adresa_fac}}</td>
				</tr>
				<tr>
					<td class="td-tit">Localitate Facturare</td>
					<td>{{$membru->localitate_fac}}</td>
				</tr>
				<tr>
					<td class="td-tit">Judet/Sector Facturare</td>
					<td>{{$membru->judet_sector_fac}}</td>
				</tr>
				<tr><td></td><td></td></tr>
				<tr>
					<td class="td-tit">Adresa Livrare</td>
					<td>{{$membru->adresa_liv}}</td>
				</tr>
				<tr>
					<td class="td-tit">Localitate Livrare</td>
					<td>{{$membru->localitate_liv}}</td>
				</tr>
				<tr>
					<td class="td-tit">Judet/Sector Livrare</td>
					<td>{{$membru->judet_sector_liv}}</td>
				</tr>
				<tr><td></td><td></td></tr>
				<tr>
					<td class="td-tit">Mentiuni Cont</td>
					<td>{{$membru->mentiuni}}</td>
				</tr>
				<tr>
					<td class="td-tit">Mentiune Comanda Curenta</td>
					<td>{{$statcom[0]->ment_livrare}}</td>
				</tr>
			</table>				


	
				
			
			
			</div>
			
	</body>
</html>

		
