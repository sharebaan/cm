@extends('admin.layout.dashboard')

@section('content')

@include('admin.layout.tree')

	@if(isset($membru))
		<div class="tabel">	
				<div>
					<div class="row" style="background-color:#48aaad;">
						<div ><h4><p class="mp">Id:</p> {{$membru->id}}</h4></div><br>
						<div ><h4><p class="mp">Status:</p> {{$membru->status}}</h4></div><br>
						<div ><h4><p class="mp">Tip Cont:</p> {{$membru->tipCont}}</h4></div><br>
						<h3 class="tm">Date Personale</h3>
						<div ><h4><p class="mp">Nume:</p> {{$membru->nume}}</h4></div><br>
						<div ><h4><p class="mp">Prenume:</p> {{$membru->prenume}}</h4></div><br>
						<div ><h4><p class="mp">CNP:</p> {{$membru->CNP}}</h4></div><br>
						<div ><h4><p class="mp">Email:</p> {{$membru->email}}</h4></div><br>
						<div ><h4><p class="mp">Telefon:</p> {{$membru->telefon}}</h4></div><br>
						<h3 class="tm">Detalii Companie</h3>
						<div ><h4><p class="mp">Nume Companie:</p> {{$membru->nume_companie}}</h4></div><br>
						<div ><h4><p class="mp">CUI:</p> {{$membru->CUI}}</h4></div><br>
						<div ><h4><p class="mp">Banca:</p> {{$membru->banca}}</h4></div><br>
						<div ><h4><p class="mp">Cont IBAN:</p> {{$membru->cont_IBAN}}</h4></div><br>
						<h3 class="tm">Adresa Sediu Social</h3>
						<div ><h4><p class="mp">Adresa Sediu:</p> {{$membru->adresa_sediu}}</h4></div><br>
						<div ><h4><p class="mp">Localitate Sediu:</p> {{$membru->localitate_sediu}}</h4></div><br>
						<div ><h4><p class="mp">Judet/Sector Sediu:</p> {{$membru->judet_sector_sediu}}</h4></div><br>
						<h3 class="tm">Adresa Facturare</h3>
						<div ><h4><p class="mp">Adresa Facturare:</p> {{$membru->adresa_fac}}</h4></div><br>
						<div ><h4><p class="mp">Localitate Facturare:</p> {{$membru->localitate_fac}}</h4></div><br>
						<div ><h4><p class="mp">Judet/Sector Facturare:</p> {{$membru->judet_sector_fac}}</h4></div><br>
						<h3 class="tm">Adresa Livrare</h3>
						<div ><h4><p class="mp">Adresa Livrare:</p> {{$membru->adresa_liv}}</h4></div><br>
						<div ><h4><p class="mp">Localitate Livrare:</p> {{$membru->localitate_liv}}</h4></div><br>
						<div ><h4><p class="mp">Judet/Sector Livrare:</p> {{$membru->judet_sector_liv}}</h4></div><br>
						
						
						<div ><h4><p class="mp">KAM Comercial:</p> {{$membru->KAM_com}}</h4></div><br>
						<div ><h4><p class="mp">KAM Livrare:</p> {{$membru->KAM_liv}}</h4></div><br>
						
					</div>
				</div>		
		</div>
	@endif

@stop