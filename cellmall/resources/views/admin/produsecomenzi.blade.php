@extends('admin.layout.dashboard')

@section('content')

@include('admin.layout.tree')

	@if(isset($prod))
		<div class="tabelComenzi">

			<div class="row" style="margin-left:40px;">
				<div class="comenzi cid"><h4>Id</h4></div>
				<div class="comenzi colmic"><h4>Membru Id</h4></div>
				<div class="comenzi colmic"><h4>Produs Id</h4></div>
				<div class="comenzi colmare"><h4>Nume Produs</h4></div>
				<div class="comenzi colmic"><h4>Cantitate</h4></div>
				<div class="comenzi colmic"><h4>Total</h4></div>
				<div class="comenzi colmare"><h4>Metoda Plata</h4></div>
				<div class="comenzi colmic"><h4>Contbanca</h4></div>
				<div class="comenzi colmare"><h4>Metoda Livrare</h4></div>
				<div class="comenzi colmare"><h4>Mod Cumparare</h4></div>
				<div class="comenzi colmare"><h4>Tip Document</h4></div>
				<div class="comenzi colmare"><h4>Mentiune Livrare</h4></div>
				<div class="comenzi colmic"><h4>KAM Livrare</h4></div>
			</div>
			@foreach($prod as $comanda)

			<div>

				<div class="row">
					<div><a href="" id="" class="detalii">detalii</a></div>
					<div class="comenzi cid"><h4>{{$comanda->id}}</h4></div>
					<div class="comenzi colmic"><h4>{{$comanda->membru_id}}</h4></div>
					<div class="comenzi colmic"><h4>{{$comanda->produs_id}}</h4></div>
					<div class="comenzi colmare"><h4>{{$comanda->nume_produs}}</h4></div>
					<div class="comenzi colmic"><h4>{{$comanda->cantitate}}</h4></div>
					<div class="comenzi colmic"><h4>{{$comanda->total}}</h4></div>
					<div class="comenzi colmare"><h4>{{$comanda->metoda_plata}}</h4></div>
					<div class="comenzi colmic"><h4>{{$comanda->contbanca}}</h4></div>
					<div class="comenzi colmare"><h4>{{$comanda->metoda_livrare}}</h4></div>
					<div class="comenzi colmare"><h4>{{$comanda->mod_cumparare}}</h4></div>
					@if($comanda->tip_document == null)
						<div class="comenzi colmare"><h4>Nu exista</h4></div>
					@else
						<div class="comenzi colmare"><h4>{{$comanda->tip_document}}</h4></div>	
					@endif
					@if($comanda->ment_livrare == null)
						<div class="comenzi colmare"><h4>Nu exista</h4></div>
					@else
						<div class="comenzi colmare"><h4>{{$comanda->ment_livrare}}</h4></div>
					@endif
					@if($comanda->KAM_livrare == null)
						<div class="comenzi colmic"><h4>Nu exista</h4></div>
					@else
						<div class="comenzi colmic"><h4>{{$comanda->KAM_livrare}}</h4></div>
					@endif
						
				</div>	
			
			</div>
			@endforeach
		</div>
		
	@endif

@stop
