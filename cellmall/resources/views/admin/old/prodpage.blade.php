@extends('admin.layout.dashboard')
 
@section('content')

@include('admin.layout.tree')

	@if(isset($pageProdus))
	
		<div class="tabel">

			<div class="row">
				<div id="id"><h4>Id</h4></div>
				<div id="codprod"><h4>Cod Produs</h4></div>
				<div id="numeprod"><h4>Denumire Produs</h4></div>
				<div id="pret"><h4>Pret</h4></div>
				<div class="li4"><h4>Stoc</h4></div>
				<div class="li4"><h4>Total stoc</h4></div>
				<div class="li4"><h4>Intrari</h4></div>
				<div class="li4"><h4>Iesiri</h4></div>
				<div class="li4"><h4>Retur</h4></div>
				<div class="li4"><h4>Vanzari</h4></div>
				<div class="li5"><h4>Pozitie Raft</h4></div>
				<div class="li5"><h4>Brand</h4></div>
				<div class="li5"><h4>Disponibil</h4></div>
			</div>
			
			@for($e=1;$e<count($pageProdus);$e++)
			
				<div>
					<div class="row">
						<div id="id"><h4>{{$pageProdus[$e]->id}}</h4></div>
						<div id="codprod"><h4>{{$pageProdus[$e]->cod_produs}}</h4></div>
						<div id="numeprod"><h4>{{$pageProdus[$e]->denumire_produs}}</h4></div>
						<div id="pret"><h4>{{$pageProdus[$e]->pret}}</h4></div>
						<div class="li4"><h4>{{$pageProdus[$e]->stoc}}</h4></div>
						<div class="li4"><h4>{{$pageProdus[$e]->total_stoc}}</h4></div>
						<div class="li4"><h4>{{$pageProdus[$e]->intrari}}</h4></div>
						<div class="li4"><h4>{{$pageProdus[$e]->iesiri}}</h4></div>
						<div class="li4"><h4>{{$pageProdus[$e]->retur}}</h4></div>
						<div class="li4"><h4>{{$pageProdus[$e]->vanzari}}</h4></div>
						<div class="li5"><h4>{{$pageProdus[$e]->pozitie_raft}}</h4></div>
						@if(isset($pageProdusBrand))
							<div class="li5">{{$pageProdusBrand[$pageProdus[$e]->brand_id - 1]->nume}}</div>
						@endif

						@if(isset($subcatBazeDisp))
							
							@if($subcatBazeDisp[$e - 1]->in_stoc == 1)
								<div class="li5">In Stoc</div>
							@elseif($subcatBazeDisp[$e - 1]->stoc_redus == 1)
								<div class="li5">Stoc Redus</div>
							@elseif($subcatBazeDisp[$e - 1]->produs_indisponibil == 1)
								<div class="li5">Prod Indisponibil</div>
							@elseif($subcatBazeDisp[$e - 1]->in_curand == 1)
								<div class="li5">In curand</div>
							@endif
						@endif		

						<div><a href="" id="" class="detalii">detalii</a></div>
						<div><a href="" id="" class="print">print</a></div>
					</div>
					<div class="dropd" id="">
						<form action="/dashboard/prodedit/{{$pageProdus[$e]->id}}" method="get">
							<a href="" class="sbedit">edit</a>
							
							<div>

								<h4>Disponibilitate</h4>
								@if($subcatBazeDisp[$e - 1]->in_stoc == 1)
								<div class="li5">In Stoc</div>
								@elseif($subcatBazeDisp[$e - 1]->stoc_redus == 1)
								<div class="li5">Stoc Redus</div>
								@elseif($subcatBazeDisp[$e - 1]->produs_indisponibil == 1)
								<div class="li5">Prod Indisponibil</div>
								@elseif($subcatBazeDisp[$e - 1]->in_curand == 1)
								<div class="li5">In curand</div>
								@endif
								<select name="disp" class="sbIn" style="display:none" id="">
									
									<option value="non">Alege Dsiponibilitatea</option>
									<option value="stoc">In Stoc</option>
									<option value="redus">Stoc Redus</option>
									<option value="indis">Prod Indisponibil</option>
									<option value="curand">In Curand</option>
								</select>
								<input type="hidden" name="dispid" class="sbIn" value="{{$subcatBazeDisp[$e - 1]->id}}">
								<h4>Cod Produs</h4>
								<p class="sbinput">{{$pageProdus[$e]->cod_produs}}</p>
								<input type="text" name="codprod" class="sbIn" placeholder="Cod Produs" style="display:none" value="{{$pageProdus[$e]->cod_produs}}">
								<h4>Denumire Produs</h4>
								<p class="sbinput">{{$pageProdus[$e]->denumire_produs}}</p>
							<!--	<input type="text" name="denprod" class="sbIn" placeholder="Denumire Produs" style="display:none" value="{{$pageProdus[$e]->denumire_produs}}"> -->
								<textarea name="denprod" class="sbIn" id="" placeholder="Denumire Produs" style="display:none"  cols="30" rows="10">{{$pageProdus[$e]->denumire_produs}}</textarea>
								<h4>Poza</h4>
								<p class="sbinput">{{$pageProdus[$e]->poza}}</p>
								<input type="text" name="poza" class="sbIn" placeholder="Poza" style="display:none" value="{{$pageProdus[$e]->poza}}">
								<h4>Pret</h4>
								<p class="sbinput">{{$pageProdus[$e]->pret}}</p>
								<input type="text" name="pret" class="sbIn" placeholder="Pret" style="display:none" value="{{$pageProdus[$e]->pret}}">
								<h4>Stoc</h4>
								<p class="sbinput">{{$pageProdus[$e]->stoc}}</p>
								<input type="text" name="stoc" class="sbIn" placeholder="Stoc" style="display:none" value="{{$pageProdus[$e]->stoc}}">
								<h4>Total stoc</h4>
								<p class="sbinput">{{$pageProdus[$e]->total_stoc}}</p>
								<input type="text" name="totalstoc" class="sbIn" placeholder="Total stoc" style="display:none" value="{{$pageProdus[$e]->total_stoc}}">
								<h4>Alerta stoc</h4>
								<p class="sbinput">{{$pageProdus[$e]->alerta_stoc}}</p>
								<input type="text" name="alertastoc" class="sbIn" placeholder="Total stoc" style="display:none" value="{{$pageProdus[$e]->alerta_stoc}}">
								<h4>Intrari</h4>
								<p class="sbinput">{{$pageProdus[$e]->intrari}}</p>
								<input type="text" name="intrari" class="sbIn" placeholder="Intrari" style="display:none" value="{{$pageProdus[$e]->intrari}}">
								<h4>Iesiri</h4>
								<p class="sbinput">{{$pageProdus[$e]->iesiri}}</p>
								<input type="text" name="iesiri" class="sbIn" placeholder="Iesiri" style="display:none" value="{{$pageProdus[$e]->iesiri}}">
								<h4>Retur</h4>
								<p class="sbinput">{{$pageProdus[$e]->retur}}</p>
								<input type="text" name="retur" class="sbIn" placeholder="Retur" style="display:none" value="{{$pageProdus[$e]->retur}}">
								<h4>Vanzari</h4>
								<p class="sbinput">{{$pageProdus[$e]->vanzari}}</p>
								<input type="text" name="vanzari" class="sbIn" placeholder="Vanzari" style="display:none" value="{{$pageProdus[$e]->vanzari}}">
								<h4>Discount</h4>
								<p class="sbinput discount">{{$pageProdus[$e]->discount}}</p>
								<input type="text" name="discount" class="sbIn" placeholder="Discount" style="display:none" value="{{$pageProdus[$e]->discount}}">
								<h4>Promotii</h4>
								<p class="sbinput promotii">{{$pageProdus[$e]->promotii}}</p>
							<!--	<input type="text" name="promotii" class="sbIn" placeholder="Promotii" style="display:none" value="{{$pageProdus[$e]->promotii}}"> -->
								<select name="noutati" class="sbIn" style="display:none" id="" style="display:none">
									<option value="1">Afirmativ</option>
									<option value="0">Negativ</option>
								</select>
								<h4>Noutati</h4>
								<p class="sbinput noutati">{{$pageProdus[$e]->noutati}}</p>
							<!--	<input type="text" name="noutati" class="sbIn" placeholder="Noutati" style="display:none" value="{{$pageProdus[$e]->noutati}}"> -->
								<select name="noutati" class="sbIn" style="display:none" id="" style="display:none">
									<option value="1">Afirmativ</option>
									<option value="0">Negativ</option>
								</select>
								<h4>Lichidari stoc</h4>
								<p class="sbinput lichidstoc">{{$pageProdus[$e]->lichidari_stoc}}</p>
							<!--	<input type="text" name="lichidstoc" class="sbIn" placeholder="Lichidari Stoc" style="display:none" value="{{$pageProdus[$e]->lichidari_stoc}}"> -->
								<select name="lichidstoc" class="sbIn" style="display:none" id="" style="display:none">
									<option value="1">Afirmativ</option>
									<option value="0">Negativ</option>
								</select>
								<h4>Descriere Detalii</h4>
								<p class="sbinput detalii">{{$pageProdus[$e]->detalii}}</p>
								<textarea name="detalii" class="sbIn" id="" placeholder="Descriere Detalii" style="display:none" cols="30" rows="10">{{$pageProdus[$e]->detalii}}</textarea>
								<h4>Culoare</h4>
								<p class="sbinput culoare">{{$pageProdus[$e]->culoare}}</p>
								<input type="text" name="culoare" class="sbIn" placeholder="Culoare" style="display:none" value="{{$pageProdus[$e]->culoare}}">
								<h4>Dimensiune</h4>
								<p class="sbinput dimensiune">{{$pageProdus[$e]->dimensiune}}</p>
								<input type="text" name="dimensiune" class="sbIn" placeholder="Dimensiune" style="display:none" value="{{$pageProdus[$e]->dimensiune}}">
								<h4>Tip Ambalaj</h4>
								<p class="sbinput tip_ambalaj">{{$pageProdus[$e]->tip_ambalaj}}</p>
								<select name="tip_ambalaj" class="sbIn" style="display:none" id="">
									
									<option value="non">Alege Tipul de Ambalaj</option>
									<option value="Bulk / Folie Protectie">Bulk / Folie Protectie</option>
									<option value="Blister / Cutie">Blister / Cutie</option>
									
								</select>
								<h4>Atentie</h4>
								<p class="sbinput atentie">{{$pageProdus[$e]->atentie}}</p>
								<select name="atentie" class="sbIn" id="" style="display:none">
									<option value="non">Alege atentie</option>
									<option value="Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.">Cunostinte deosebite</option>
									<option value="Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.">Cunostinte banale</option>
								</select><br>
								
								<h4>Pozitie Raft</h4>
								<p class="sbinput pozraft">{{$pageProdus[$e]->pozitie_raft}}</p>
								<input type="text" name="pozraft" class="sbIn" placeholder="Pozitie Raft" style="display:none" value="{{$pageProdus[$e]->pozitie_raft}}">
								
								
							</div>
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="submit" class="sbeditare" style="display:none" value="Editare">
						</form>											
					</div>
				</div>	
			@endfor

		</div>

		{!!$pageProdus->render()!!}

	@endif

@stop