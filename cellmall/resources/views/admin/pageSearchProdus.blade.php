@extends('admin.layout.dashboard')

@section('content')

@include('admin.layout.tree')

	
	@if(isset($produs))

		<div class="tabel">
			
			<div class="row" style="margin-left:115px;">
				@if(isset($sp))							
					@if($sp[1] != 0)
						<div class="cidp"><h4><a href="{{$sp[0]}}/1/id/search?{{$sp[2]}}">Id</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/codprod/search?{{$sp[2]}}">Cod Produs</a></h4></div>
						<div class="colmarep"><h4><a href="{{$sp[0]}}/1/numeprod/search?{{$sp[2]}}">Denumire Produs</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/poza/search?{{$sp[2]}}">Poza</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/pret_original/search?{{$sp[2]}}">Pret fara TVA</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/tva/search?{{$sp[2]}}">TVA</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/pretTva/search?{{$sp[2]}}">Pret cu TVA</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/pret/search?{{$sp[2]}}">Pret afisat cu TVA</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/stoc/search?{{$sp[2]}}">Stoc</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/totalstoc/search?{{$sp[2]}}">Total stoc</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/alertastoc/search?{{$sp[2]}}">Alerta Stoc</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/pozitieraft/search?{{$sp[2]}}">Pozitie Raft</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/brand/search?{{$sp[2]}}">Brand</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/producator/search?{{$sp[2]}}">Producator</a></h4></div>
						<div class="colmicp"><h4>
							
							<a href="{{$sp[0]}}/1/disponibil/search?{{$sp[2]}}">Disponibilitate</a>	
						
						</h4></div>
						<div class="colmarep"><h4><a href="{{$sp[0]}}/1/detalii/search?{{$sp[2]}}">Detalii</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/culoare/search?{{$sp[2]}}">Culoare</a></h4></div>
						<div class="colmarep"><h4><a href="{{$sp[0]}}/1/dimensiune/search?{{$sp[2]}}">Dimensiune</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/atentie/search?{{$sp[2]}}">Atentie</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/tipambalaj/search?{{$sp[2]}}">Tip Ambalaj</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/noutati/search?{{$sp[2]}}">Noutati</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/promotii/search?{{$sp[2]}}">Promotii</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/lichidaristoc/search?{{$sp[2]}}">Lichidari Stoc</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/discount/search?{{$sp[2]}}">Discount</a></h4></div>	
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/swap/{{$sp[2]}}/prod">Swap</a></h4></div>	
						<div class="colmicp"><h4><a href="{{$sp[0]}}/1/ascuns/{{$sp[2]}}/prod">Ascuns</a></h4></div>	
						<div class="coldata"><h4><a href="{{$sp[0]}}/1/updated/search?{{$sp[2]}}">Data Modificare</a></h4></div>
						<div class="coldata"><h4><a href="{{$sp[0]}}/1/created/search?{{$sp[2]}}">Data Creere</a></h4></div>
						
					@else	
						
						<div class="cidp"><h4><a href="{{$sp[0]}}/0/id/search?{{$sp[2]}}">Id</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/codprod/search?{{$sp[2]}}">Cod Produs</a></h4></div>
						<div class="colmarep"><h4><a href="{{$sp[0]}}/0/numeprod/search?{{$sp[2]}}">Denumire Produs</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/poza/search?{{$sp[2]}}">Poza</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/pret_original/search?{{$sp[2]}}">Pret fara TVA</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/tva/search?{{$sp[2]}}">TVA</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/pretTva/search?{{$sp[2]}}">Pret cu TVA</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/pret/search?{{$sp[2]}}">Pret afisat cu TVA</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/stoc/search?{{$sp[2]}}">Stoc</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/totalstoc/search?{{$sp[2]}}">Total stoc</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/alertastoc/search?{{$sp[2]}}">Alerta Stoc</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/pozitieraft/search?{{$sp[2]}}">Pozitie Raft</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/brand/search?{{$sp[2]}}">Brand</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/producator/search?{{$sp[2]}}">Producator</a></h4></div>
						<div class="colmicp"><h4>
							
							<a href="{{$sp[0]}}/0/disponibil/search?{{$sp[2]}}">Disponibilitate</a>	
						
						</h4></div>
						<div class="colmarep"><h4><a href="{{$sp[0]}}/0/detalii/search?{{$sp[2]}}">Detalii</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/culoare/search?{{$sp[2]}}">Culoare</a></h4></div>
						<div class="colmarep"><h4><a href="{{$sp[0]}}/0/dimensiune/search?{{$sp[2]}}">Dimensiune</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/atentie/search?{{$sp[2]}}">Atentie</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/tipambalaj/search?{{$sp[2]}}">Tip Ambalaj</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/noutati/search?{{$sp[2]}}">Noutati</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/promotii/search?{{$sp[2]}}">Promotii</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/lichidaristoc/search?{{$sp[2]}}">Lichidari Stoc</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/discount/search?{{$sp[2]}}">Discount</a></h4></div>
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/swap/{{$sp[2]}}/prod">Swap</a></h4></div>	
						<div class="colmicp"><h4><a href="{{$sp[0]}}/0/ascuns/{{$sp[2]}}/prod">Ascuns</a></h4></div>	
						<div class="coldata"><h4><a href="{{$sp[0]}}/0/updated/search?{{$sp[2]}}">Data Modificare</a></h4></div>
						<div class="coldata"><h4><a href="{{$sp[0]}}/0/created/search?{{$sp[2]}}">Data Creere</a></h4></div>
					@endif
				@else
						<div class="cidp"><h4>Id</h4></div>
						<div class="colmicp"><h4>Cod Produs</h4></div>
						<div class="colmarep"><h4>Denumire Produs</h4></div>
						<div class="colmicp"><h4>Poza</h4></div>
						<div class="colmicp"><h4>Pret fara TVA</h4></div>
						<div class="colmicp"><h4>TVA</h4></div>
						<div class="colmicp"><h4>Pret cu TVA</h4></div>
						<div class="colmicp"><h4>Pret afisat cu Tva</h4></div>
						<div class="colmicp"><h4>Stoc</h4></div>
						<div class="colmicp"><h4>Total stoc</h4></div>
						<div class="colmicp"><h4>Alerta Stoc</h4></div>
						<div class="colmicp"><h4>Pozitie Raft</h4></div>
						<div class="colmicp"><h4>Brand</h4></div>
						<div class="colmicp"><h4>Producator</h4></div>
						<div class="colmicp"><h4>
							
							Disponibilitate	
						
						</h4></div>
						<div class="colmarep"><h4>Detalii</h4></div>
						<div class="colmicp"><h4>Culoare</h4></div>
						<div class="colmarep"><h4>Dimensiune</h4></div>
						<div class="colmicp"><h4>Atentie</h4></div>
						<div class="colmicp"><h4>Tip Ambalaj</h4></div>
						<div class="colmicp"><h4>Noutati</h4></div>
						<div class="colmicp"><h4>Promotii</h4></div>
						<div class="colmicp"><h4>Lichidari Stoc</h4></div>
						<div class="colmicp"><h4>Discount</h4></div>	
						<div class="colmicp"><h4>Ascuns</h4></div>	
						<div class="coldata"><h4>Data Modificare</h4></div>
						<div class="coldata"><h4>Data Creere</h4></div>
				@endif	
				
				

			</div>
			
		@foreach($produs as $e=>$prod)
				<div >
						
					<div class="row">
						<div><a href="" id="" class="detalii">detalii</a></div>
						<div><a href="/produs/{{$prod->id}}" target="_blank" id="" class="vezi"style="margin-left:5px;">vezi</a></div>
						<div><a href="/dashboard/print/{{$prod->cod_produs}}" id=""  class="print" style="margin-right:5px;">print</a></div>
						@if($prod->nelistat != 1)
							<div class="listareopt">
								<a href="" class="nelistprod" name="{{$prod->id}}">nu lista</a>
								<a href="" class="listprod" style="display:none;color:red;" name="{{$prod->id}}">listeaza</a>
							</div>
						@else
							<div class="listareopt">
								<a href="" class="nelistprod" style="display:none;" name="{{$prod->id}}">nu lista</a>
								<a href="" class="listprod" style="color:red;" name="{{$prod->id}}">listeaza</a>
							</div>
						@endif
						@if($prod->ascuns != 1)
							@if(Auth::user()->angajat_produse == 0)
						<div class="ascundeopt">
							<a href="" class="ascundeprod" name="{{$prod->id}}">ascunde</a>
							<a href="" class="arataprod" style="display:none;color:red;" name="{{$prod->id}}">ARATA</a>
						</div>	
							@endif
						@else
							<div class="ascundeopt">
								<a href="" class="ascundeprod" style="display:none;" name="{{$prod->id}}">ascunde</a>
								<a href="" class="arataprod" style="color:red;" name="{{$prod->id}}">ARATA</a>
							</div>							
						@endif
						<div class='cidp'><h4>{{$prod->id}}</h4></div>
						<div class='colmicp'><h4>{{$prod->cod_produs}}</h4></div>
						<div class='colmarep'><h4>{{$prod->denumire_produs}}</h4></div>
						@if($prod->poza == null)
							<div class="colmicp"><p class="tited">Nu Are</p></div>
						@else
							<div class="colmicp"><img class="pozatab" src="{{$prod->poza}}"></div>	
						@endif
						<div class='colmicp'><h4>{{$prod->pret_original}}</h4></div>
						<div class='colmicp'><h4>{{round($prod->pret_original * $prod->tva / 100,2)}}</h4></div>
						<div class='colmicp'><h4>{{$prod->pretTva}}</h4></div>
						<div class='colmicp'><h4>{{$prod->pret}}</h4></div>
						<div class="colmicp"><h4>{{$prod->stoc}}</h4></div>
						<div class="colmicp"><h4>{{$prod->total_stoc}}</h4></div>
						<div class="colmicp"><p class="tited">{{$prod->alerta_stoc}}</p></div>
						@if($prod->pozitie_raft == null)
							<div class="colmicp"><p class="tited">Nu Exista</p></div>
						@else
							<div class="colmicp"><p class="tited">{{$prod->pozitie_raft}}</p></div>	
						@endif
						@if(isset($brand))
							<div class="colmicp">{{$brand[$prod->brand_id -1]->nume}}</div>
						@endif
						
						@if(isset($producator))
							<div class="colmicp"><p class="tited">{{$producator[$prod->producator_id -1]->nume}}</p></div>
						@endif	
						@if(isset($disp))
							
							@if($disp[$e ]->in_stoc == 1)
								<div class="colmicp">In Stoc</div>
							@elseif($disp[$e ]->stoc_redus == 1)
								<div class="colmicp">Stoc Redus</div>
							@elseif($disp[$e ]->produs_indisponibil == 1)
								<div class="colmicp">Prod Indisponibil</div>
							@elseif($disp[$e ]->in_curand == 1)
								<div class="colmicp">In curand</div>
							@endif
						@endif		
						
						
						<div class="colmarep"><p class="tited">{{substr($prod->detalii,0,10)}} ...</p></div>
						@if($prod->culoare == null)
							<div class="colmicp"><p class="tited">Nu are culoare</p></div>
						@else
							<div class="colmicp"><p class="tited">{{$prod->culoare}}</p></div>	
						@endif
						
						@if($prod->dimensiune == null)
							<div class="colmarep"><p class="tited">Nu are dimensiune</p></div>
						@else
							<div class="colmarep"><p class="tited">{{$prod->dimensiune}}</p></div>
						@endif

						@if($prod->atentie == null)
							<div class="colmicp"><p class="tited">Nu Exista</p></div>
						@else
							<div class="colmicp"><p class="tited">{{substr($prod->atentie,0,20)}} ...</p></div>
						@endif
						
						@if($prod->tip_ambalaj == null)
							<div class="colmicp"><p class="tited">Nu Are</p></div>
						@else
							<div class="colmicp"><p class="tited">{{$prod->tip_ambalaj}}</p></div>	
						@endif
						<div class="colmicp"><p class="tited">{{$prod->noutati}}</p></div>
						<div class="colmicp"><p class="tited">{{$prod->promotii}}</p></div>
						
						<div class="colmicp"><p class="tited">{{$prod->lichidari_stoc}}</p></div>
						@if($prod->discount == null)
							<div class="colmicp"><p class="tited">Nu Are</p></div>
						@else
							<div class="colmicp"><p class="tited">{{$prod->discount}}</p></div>	
						@endif
						@if($prod->swap == null)
							<div class="colmicp"><p class="tited">Nu este</p></div>
						@else
							<div class="colmicp"><p class="tited">{{$prod->swap}}</p></div>	
						@endif
						@if($prod->ascuns == 1)
						<div class="colmicp"><p class="tited">Ascuns</p></div>	
						@else
						<div class="colmicp"><p class="tited">Vizibil</p></div>	
						@endif
						<div class="coldata"><p class="tited">{{$prod->updated_at}}</p></div>	
						<div class="coldata"><p class="tited">{{$prod->created_at}}</p></div>
					</div>


					<div class="dropd" id="">
						@if(Auth::user()->admin == 1)
						<a href="" class="clonare">Clonare Produs</a>	
						@endif
						<form action="/dashboard/prodedit" enctype="multipart/form-data" class="editp"method="post">
							
						@if(Auth::user()->admin == 1)
							<a href="" class="sbedit">Edit</a>
						@endif		
							<div>
								
								<p class="tited">Disponibilitate</p>
								@if($disp[$e ]->in_stoc == 1)
								<div class="li5 sbinput">In Stoc</div>
								@elseif($disp[$e ]->stoc_redus == 1)
								<div class="li5 sbinput">Stoc Redus</div>
								@elseif($disp[$e ]->produs_indisponibil == 1)
								<div class="li5 sbinput">Produs Indisponibil</div>
								@elseif($disp[$e ]->in_curand == 1)
								<div class="li5 sbinput">In curand</div>
								@endif
								<input type="hidden" name='idp' value='{{$prod->id}}'>
								<select name="disp" class="sbIn" style="display:none" id="">
									<option value="non">
										@if($disp[$e ]->in_stoc == 1)
											In Stoc
										@elseif($disp[$e ]->stoc_redus == 1)
											Stoc Redus
										@elseif($disp[$e ]->produs_indisponibil == 1)
											Produs Indisponibil
										@elseif($disp[$e ]->in_curand == 1)
											In curand
										@endif
									</option>
									<option value="stoc">In Stoc</option>
									<option value="redus">Stoc Redus</option>
									<option value="indis">Prod Indisponibil</option>
									<option value="curand">In Curand</option>
								</select>
								<input type="hidden" name="dispid" class="sbIn" value="{{$disp[$e ]->id}}">
								
								<p class="tited">Producator</p>
									<div class="li5 sbinput" >{{$producator[$prod->producator_id -1]->nume}}</div>
									<select name="prod" class="sbIn" style="display:none" id=""> 
										<option value="{{$prod->producator_id}}">{{$producator[$prod->producator_id -1]->nume}}</option>
										<option value="1">OEM</option>
										<option value="2">Apple</option>
										<option value="3">Samsung</option>
										<option value="4">Sony</option>
										<option value="5">LG</option>
										<option value="6">HTC</option>
										<option value="7">Nokia</option>
										<option value="8">Blackberry</option>
										<option value="9">Motorola</option>
										<option value="10">Huawei</option>
										<option value="11">ZTE</option>
										<option value="12">Sony Ericsson</option>
										<option value="13">Dell</option>
										<option value="14">Alcatel</option>
										<option value="15">Amazon</option>
										<option value="16">Lenovo</option>
										<option value="17">Google</option>
										<option value="18">Acer</option>
										<option value="19">Asus</option>
										<option value="20">Phillips</option>
										<option value="21">Siemens</option>
										<option value="22">Vodafone</option>
										<option value="23">Orange</option>
										<option value="24">Allview</option>
										<option value="25">Cosmote</option>
										<option value="26">Xiaomi</option>
							
										<option value="27">Meizu</option>
										<option value="28">T-Mobile</option>
										<option value="29">UV-MATECH</option>
										<option value="30">BaoJiesi</option>
									</select>

								<p class="tited">Brand</p>
									<div class="li5 sbinput" >{{$brand[$prod->brand_id -1]->nume}}</div>
									<select name="brand" class="sbIn" style="display:none" id="">		
										<option value="{{$prod->brand_id}}">{{$brand[$prod->brand_id -1]->nume}}</option>
										<option value="1">Apple</option>
										<option value="2">Samsung</option>
										<option value="3">Sony</option>
										<option value="4">LG</option>
										<option value="5">HTC</option>
										<option value="6">Nokia</option>
										<option value="7">Blackberry</option>
										<option value="8">Motorola</option>
										<option value="9">Huawei</option>
										<option value="10">ZTE</option>
										<option value="11">Sony Ericsson</option>
										<option value="12">Dell</option>
										<option value="13">Alcatel</option>
										<option value="14">Amazon</option>
										<option value="15">Lenovo</option>
										<option value="16">Google</option>
										<option value="17">Acer</option>
										<option value="18">Asus</option>
										<option value="19">Phillips</option>
										<option value="20">Siemens</option>
										<option value="21">Vodafone</option>
										<option value="22">Orange</option>
										<option value="23">Allview</option>
										<option value="24">Cosmote</option>
										<option value="25">Xiaomi</option>
									</select>

								<p class="tited">Cod Produs</p>
								<p class="sbinput">{{$prod->cod_produs}}</p>
								<input type="text" name="codprod" class="sbIn" placeholder="Cod Produs" style="display:none" value="{{$prod->cod_produs}}">
								<p class="tited">Denumire Produs</p>
								<p class="sbinput" style="width:300px;">{{$prod->denumire_produs}}</p>
							<!--	<input type="text" name="denprod" class="sbIn" placeholder="Denumire Produs" style="display:none" value=""> -->
								<textarea name="denprod" class="sbIn" id="" placeholder="Denumire Produs" style="display:none" cols="30" rows="10">{{$prod->denumire_produs}}</textarea>
								<p class="tited">Pret Afisat cu Tva</p>
								<p class="sbinput">{{$prod->pret}}</p>
								<input type="text" name="pret" class="sbIn" placeholder="Pret afista cu TVA" style="display:none" value="{{$prod->pret}}">
								<p class="tited">Pret fara TVA</p>
								<p class="sbinput">{{$prod->pret_original}}</p>
								<input type="text" name="pret_original" class="sbIn" placeholder="Pret fara TVA" style="display:none" value="{{$prod->pret_original}}">
								<p class="tited">Pret cu Tva</p>
								<p class="sbinput">{{$prod->pretTva}}</p>
								<input type="text" name="pretTva" class="sbIn" placeholder="Pret cu TVA" style="display:none" value="{{$prod->pretTva}}">
								<p class="tited">Alerta stoc</p>
								<p class="sbinput">{{$prod->alerta_stoc}}</p>
								<input type="text" name="alertastoc" class="sbIn" placeholder="Total stoc" style="display:none" value="{{$prod->alerta_stoc}}"><br>
								<p class="tited">Stoc</p>
								<p class="sbinput">{{$prod->stoc}}</p>
								<input type="text" name="stoc" class="sbIn" placeholder="Stoc" style="display:none" value="{{$prod->stoc}}">

								<input type="text" name="intrari" class="sbIn" placeholder="Intrari" style="display:none;" value="">
								<p>Thumbnail</p>
								@if(!empty($prod->poza))	
									<p class="sbinput">{{$prod->poza}}</p>
									<a href="" class="thumbnail" id="{{$prod->id}}">Schimba thumbnail</a>
								@else
									Nu exista thumbnail.<a href="" class="thumbnail" id="{{$prod->id}}">Alege un thumbnail.</a>	
								@endif
								<br>
								<div class="pozethumb">
										
								</div>
								<hr>
								<p class="tited">Poze</p>
								@if(empty($prod['poze'][0]))
									Nu exista {{count($prod['poze'])}}	
								@else
										
									@foreach($prod['poze'] as $poza)
										{{$poza->poza}}	
									@endforeach	
								@endif
								<input type="file" multiple name="poze[]"  class="sbIn" style="display:none">
								
								
								<p class="tited">Total stoc</p>
								<p class="sbinput">{{$prod->total_stoc}}</p>
								<input type="text" name="totalstoc" class="sbIn" placeholder="Total stoc" style="display:none" value="{{$prod->total_stoc}}">
								
								
								@if(Auth::user()->admin == 1)
								<a href="/dashboard/vezistatusstoc/{{$prod->id}}">Vezi status stoc</a><br>
								@endif
								
								<input type="text" name="iesiri" class="sbIn" placeholder="Iesiri" style="display:none" value="">
								
								<input type="text" name="retur" class="sbIn" placeholder="Retur" style="display:none" value=""><br>
								<p class="tited">Discount</p>
								<p class="sbinput discount">{{$prod->discount}}</p>
								<input type="text" name="discount" class="sbIn" placeholder="Discount" style="display:none" value="{{$prod->discount}}">
								<p class="tited">SWAP</p>
								<p class="sbinput swap">{{$prod->swap}}</p>
							<!--	<input type="text" name="promotii" class="sbIn" placeholder="Promotii" style="display:none" value=""> -->
								<select name="swap" class="sbIn" style="display:none" id="" style="display:none">
									@if($prod->swap == 1)
										<option value="1">Afirmativ</option>
										<option value="0">Negativ</option>
									@else
										<option value="0">Negativ</option>
										<option value="1">Afirmativ</option>
									@endif
								</select>
								<p class="tited">Promotii</p>
								<p class="sbinput promotii">{{$prod->promotii}}</p>
							<!--	<input type="text" name="promotii" class="sbIn" placeholder="Promotii" style="display:none" value=""> -->
								<select name="promotii" class="sbIn" style="display:none" id="" style="display:none">
									@if($prod->promotii == 1)
										<option value="1">Afirmativ</option>
										<option value="0">Negativ</option>
									@else
										<option value="0">Negativ</option>
										<option value="1">Afirmativ</option>
									@endif
								</select>
								<p class="tited">Noutati</p>
								<p class="sbinput noutati">{{$prod->noutati}}</p>
							<!--	<input type="text" name="noutati" class="sbIn" placeholder="Noutati" style="display:none" value=""> -->
								<select name="noutati" class="sbIn" style="display:none" id="" style="display:none">
									@if($prod->noutati == 1)
										<option value="1">Afirmativ</option>
										<option value="0">Negativ</option>
									@else
										<option value="0">Negativ</option>
										<option value="1">Afirmativ</option>
									@endif
								</select>
								<p class="tited">Lichidari stoc</p>
								<p class="sbinput lichidstoc">{{$prod->lichidari_stoc}}</p>
								<select name="lichidstoc" class="sbIn" style="display:none" id="" style="display:none">
									@if($prod->lichidari_stoc == 1)
										<option value="1">Afirmativ</option>
										<option value="0">Negativ</option>
									@else
										<option value="0">Negativ</option>
										<option value="1">Afirmativ</option>
									@endif
								</select>
								
								<p class="tited">Descriere Detalii</p>
								<p class="sbinput detalii">{{$prod->detalii}}...</p>
								<textarea name="detalii" class="sbIn" id="" placeholder="Descriere Detalii" style="display:none" cols="60" rows="10">{{$prod->detalii}}</textarea>	
								
								<p class="tited">Culoare</p>
								<p class="sbinput culoare">{{$prod->culoare}}</p>
								<textarea name="culoare" class="sbIn" id="" cols="60"  placeholder="Culoare" style="display:none" rows="3">{{$prod->culoare}}</textarea>
														<p class="tited">Dimensiune</p>
								<p class="sbinput dimensiune">{{$prod->dimensiune}}</p>
								<textarea name="dimensiune" class="sbIn" id="" cols="60"  placeholder="Dimensiune" style="display:none" rows="3">{{$prod->dimensiune}}</textarea>
								<p class="tited">Tip Ambalaj</p>
								<p class="sbinput tip_ambalaj">{{$prod->tip_ambalaj}}</p>
								<select name="tip_ambalaj" class="sbIn" style="display:none" id="">
									@if($prod->tip_ambalaj == 'Bulk / Folie Protectie')
										<option value="Bulk / Folie Protectie">Bulk / Folie Protectie</option>
										<option value="Blister / Cutie">Blister / Cutie</option>
									@elseif($prod->tip_ambalaj == 'Blister / Cutie')
										<option value="Blister / Cutie">Blister / Cutie</option>
										<option value="Bulk / Folie Protectie">Bulk / Folie Protectie</option>
									@else
										<option value="non">Alege Tipul de Ambalaj</option>
										<option value="Bulk / Folie Protectie">Bulk / Folie Protectie</option>
										<option value="Blister / Cutie">Blister / Cutie</option>
									@endif
									
									
									
								</select>
								
								<p class="tited">Atentie</p>
								<p class="sbinput atentie">{{$prod->atentie, 0,20}}</p>
								<select name="atentie" class="sbIn" id="" style="display:none">
									
									@if($prod->atentie == 'Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.')
										<option value="Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.">Cunostinte deosebite</option>
										<option value="Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.">Cunostinte banale</option>
									@elseif($prod->atentie == 'Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.')
										<option value="Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.">Cunostinte banale</option>
										<option value="Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.">Cunostinte deosebite</option>
									@else
										<option value="non">Alege atentie</option>
										<option value="Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.">Cunostinte deosebite</option>
										<option value="Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.">Cunostinte banale</option>
									@endif
								</select><br>	
								<p class="tited">Pozitie Raft</p>
								<p class="sbinput pozraft">{{$prod->pozitie_raft}}</p>
								<input type="text" name="pozraft" class="sbIn" placeholder="Pozitie Raft" style="display:none" value="{{$prod->pozitie_raft}}">
								
								

							</div>
								
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="submit" class="sbeditare" style="display:none" value="Salvare">
						</form>
						<div class="clonform" >
							
							<form action="/dashboard/cloneaza"  class="editp"method="get">
							<a href="" class="closeclon">Inchide Clonarea</a>
			
							<div>
								
	

	
								<p class="tited">Disponibilitate</p>
								<select name="disp"   id="">
									<option value="non">
										@if($disp[$e ]->in_stoc == 1)
											In Stoc
										@elseif($disp[$e ]->stoc_redus == 1)
											Stoc Redus
										@elseif($disp[$e ]->produs_indisponibil == 1)
											Produs Indisponibil
										@elseif($disp[$e ]->in_curand == 1)
											In curand
										@endif
									</option>
									<option value="stoc">In Stoc</option>
									<option value="redus">Stoc Redus</option>
									<option value="indis">Prod Indisponibil</option>
									<option value="curand">In Curand</option>
								</select>
								<input type="hidden" name="dispid"  value="{{$disp[$e ]->id}}">
								
								<p class="tited">Producator</p>
									<select name="prod"  id=""> 
										<option value="{{$prod->producator_id}}">{{$producator[$prod->producator_id -1]->nume}}</option>
										<option value="1">OEM</option>
										<option value="2">Apple</option>
										<option value="3">Samsung</option>
										<option value="4">Sony</option>
										<option value="5">LG</option>
										<option value="6">HTC</option>
										<option value="7">Nokia</option>
										<option value="8">Blackberry</option>
										<option value="9">Motorola</option>
										<option value="10">Huawei</option>
										<option value="11">ZTE</option>
										<option value="12">Sony Ericsson</option>
										<option value="13">Dell</option>
										<option value="14">Alcatel</option>
										<option value="15">Amazon</option>
										<option value="16">Lenovo</option>
										<option value="17">Google</option>
										<option value="18">Acer</option>
										<option value="19">Asus</option>
										<option value="20">Phillips</option>
										<option value="21">Siemens</option>
										<option value="22">Vodafone</option>
										<option value="23">Orange</option>
										<option value="24">Allview</option>
										<option value="25">Cosmote</option>
										<option value="26">Xiaomi</option>
									</select>

									<p class="tited">Brand</p>	
									<select name="brand"  id="">		
										<option value="{{$prod->brand_id}}">{{$brand[$prod->brand_id -1]->nume}}</option>
										<option value="1">Apple</option>
										<option value="2">Samsung</option>
										<option value="3">Sony</option>
										<option value="4">LG</option>
										<option value="5">HTC</option>
										<option value="6">Nokia</option>
										<option value="7">Blackberry</option>
										<option value="8">Motorola</option>
										<option value="9">Huawei</option>
										<option value="10">ZTE</option>
										<option value="11">Sony Ericsson</option>
										<option value="12">Dell</option>
										<option value="13">Alcatel</option>
										<option value="14">Amazon</option>
										<option value="15">Lenovo</option>
										<option value="16">Google</option>
										<option value="17">Acer</option>
										<option value="18">Asus</option>
										<option value="19">Phillips</option>
										<option value="20">Siemens</option>
										<option value="21">Vodafone</option>
										<option value="22">Orange</option>
										<option value="23">Allview</option>
										<option value="24">Cosmote</option>
										<option value="25">Xiaomi</option>
									</select>

								<p class="tited">Cod Produs / Muta Produsul Clonat</p>
								<input type="text" name="codprod"  placeholder="Cod Produs"  value="{{$prod->cod_produs}}">
								<p class="tited">Denumire Produs</p>
								<textarea name="denprod"  id="" placeholder="Denumire Produs"  cols="30" rows="10">{{$prod->denumire_produs}}</textarea>

								<p class="tited">Pret Afisat cu Tva</p>
								<input type="text" name="pret"  placeholder="Pret afisat cu TVA"  value="{{$prod->pret}}">
								<p class="tited">Pret fara Tva</p>
								<input type="text" name="pretorig"  placeholder="Pret fara Tva"  value="{{$prod->pret_original}}">
								<p class="tited">Pret cu Tva</p>
								<input type="text" name="prettva"  placeholder="Pret cu Tva"  value="{{$prod->pretTva}}">

								<input type='hidden' name='tvaval' value='{{$prod->tva}}'>

								<p class="tited">Alerta Stoc</p>
								<input type="text" name="alertastoc"  placeholder="Total stoc"  value="{{$prod->alerta_stoc}}">
								<p class="tited">Stoc</p>
								<input type="text" name="stoc"  placeholder="Stoc"  value="{{$prod->stoc}}">
								<p class="tited">Poza</p>
								<input type="text" name="poza"  placeholder="Poza"  value="{{$prod->poza}}">
								<hr>
								<p class="tited totalst">Total Stoc</p>
								<input type="text" name="totalstoc"  placeholder="Total stoc"  value="{{$prod->total_stoc}}">
									
								<br>
								
								<p class="tited">Discount</p>
								<input type="text" name="discount"  placeholder="Discount"  value="{{$prod->discount}}">
								<p class="tited">Promotii</p>
								<select name="promotii"   id="" >
									@if($prod->promotii == 1)
										<option value="1">Afirmativ</option>
										<option value="0">Negativ</option>
									@else
										<option value="0">Negativ</option>
										<option value="1">Afirmativ</option>
									@endif
								</select>
								<p class="tited">Noutati</p>
								<select name="noutati"   id="" >
									@if($prod->noutati == 1)
										<option value="1">Afirmativ</option>
										<option value="0">Negativ</option>
									@else
										<option value="0">Negativ</option>
										<option value="1">Afirmativ</option>
									@endif
								</select>
								<p class="tited">Lichidari Stoc</p>
								<select name="lichidstoc"   id="" >
									@if($prod->lichidari_stoc == 1)
										<option value="1">Afirmativ</option>
										<option value="0">Negativ</option>
									@else
										<option value="0">Negativ</option>
										<option value="1">Afirmativ</option>
									@endif
								</select>
								<p class="tited">Descriere Detalii</p>
								<textarea name="detalii"  id="" placeholder="Descriere Detalii"  cols="60" rows="10">{{$prod->detalii}}</textarea>	
								<p class="tited">Culoare</p>
								<textarea name="culoare"  id="" cols="60"  placeholder="Culoare"  rows="3">{{$prod->culoare}}</textarea>
								<p class="tited">Dimensiune</p>					
								<textarea name="dimensiune"  id="" cols="60"  placeholder="Dimensiune"  rows="3">{{$prod->dimensiune}}</textarea>
								<p class="tited">Tip Ambalaj</p>
								<select name="tip_ambalaj" id="">
									@if($prod->tip_ambalaj == 'Bulk / Folie Protectie')
										<option value="Bulk / Folie Protectie">Bulk / Folie Protectie</option>
										<option value="Blister / Cutie">Blister / Cutie</option>
									@elseif($prod->tip_ambalaj == 'Blister / Cutie')
										<option value="Blister / Cutie">Blister / Cutie</option>
										<option value="Bulk / Folie Protectie">Bulk / Folie Protectie</option>
									@else
										<option value="non">Alege Tipul de Ambalaj</option>
										<option value="Bulk / Folie Protectie">Bulk / Folie Protectie</option>
										<option value="Blister / Cutie">Blister / Cutie</option>
									@endif
									
									
									
								</select>
									<p class="tited">Atentie</p>	
									<select name="atentie"  id="" >
									
									@if($prod->atentie == 'Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.')
										<option value="Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.">Cunostinte deosebite</option>
										<option value="Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.">Cunostinte banale</option>
									@elseif($prod->atentie == 'Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.')
										<option value="Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.">Cunostinte banale</option>
										<option value="Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.">Cunostinte deosebite</option>
									@else
										<option value="non">Alege atentie</option>
										<option value="Atentie !!! Montarea / Inlocuirea acestei componente necesita indemanare si cunostinte deosebite.">Cunostinte deosebite</option>
										<option value="Nu necesita cunostinte deosebite pentru montarea / inlocuirea acestei componente.">Cunostinte banale</option>
									@endif
								</select><br>	
								<p class="tited">Pozitie Raft</p>
								<input type="text" name="pozraft"  placeholder="Pozitie Raft"  value="{{$prod->pozitie_raft}}">
								
								

							</div>

							<input type="hidden" name="id" value="{{$prod->id}}">

							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<input type="submit" class="sbeditare"  value="Cloneaza">
						</form>
							</div>
						<div class="clear"></div>
						@if(Auth::user()->admin == 1)
						<a href="#" class="sterge" id="{{$prod->id}}">Sterge Produs</a>											
						@endif
						@if(Auth::user()->angajat_produse == 0)
							@if($prod->ascuns == 0)
								<a href="#" class="ascunde" id="{{$prod->id}}">Ascunde Produs</a>
							@else
								<a href="#" class="arata" id="{{$prod->id}}">Arata Produs</a>	
							@endif
						
						@endif
					</div>
						
				</div>
				<hr>	

			@endforeach

		</div>
		
		{!!$produs->render()!!}

	@endif
@stop	
