@extends('admin.layout.dashboard')

@section('content')

@include('admin.layout.tree')


@if(isset($alerta))

<div class="tabelComenzi">
		
		@if($sp[1] != 0)
			<div class="row ssp" style="margin-left:80px;">
				<div class="statp cid"><h4><a href="{{$sp[0]}}/1/id/alerta">Id</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/1/data/alerta">Data</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/1/codprod/alerta">Cod Produs</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/1/numeprod/alerta">Denumire Produs</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/1/disp/alerta">Disponibilitate</a></h4></div>
				
			</div>
		@else
			<div class="row ssp" style="margin-left:80px;">
				<div class="statp cid"><h4><a href="{{$sp[0]}}/0/id/alerta">Id</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/0/data/alerta">Data</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/0/codprod/alerta">Cod Produs</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/0/numeprod/alerta">Denumire Produs</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/0/disp/alerta">Disponibilitate</a></h4></div>
				
			</div>
		@endif

			
	

		@foreach($alerta as $index=>$astoc)	

			<div>
				<div class="row" style="margin-left:80px;">
					<div class="statp cid"><h4>{{$astoc->id}}</h4></div>
					<div class="statp colmare"><h4>{{$astoc->created_at}}</h4></div>
					<div class="statp colmare"><h4>{{$astoc['produs']['cod_produs']}}</h4></div>
					<div class="statp colmare"><h4>{{$astoc['produs']['denumire_produs']}}</h4></div>
					@if($astoc['disponibilitate']['in_stoc'] == 1)	
						<div class="statp colmare"><h4>In Stoc</h4></div>
					@elseif($astoc['disponibilitate']['stoc_redus'] == 1)
						<div class="statp colmare"><h4>Stoc Redus</h4></div>
					@elseif($astoc['disponibilitate']['produs_indisponibil'] == 1)
						<div class="statp colmare"><h4>Produs Indisponibil</h4></div>	
					@else
						<div class="statp colmare"><h4>In Curand</h4></div>
					@endif
				</div>			
			</div>

		@endforeach
	
	</div>
	{!!$alerta->render()!!}

@endif

@stop
