@extends('admin.layout.dashboard')

@section('content')

@include('admin.layout.tree')



@if(isset($StProd))

	<div class="tabelComenzi">
		
		
			<div class="row ssp" style="margin-left:80px;">
				<div class="statp cid"><h4>Id</h4></div>
				<div class="statp colmare"><h4>Data</h4></div>
				<div class="statp colmare"><h4>Cod Produs</h4></div>
				<div class="statp colmare"><h4>Denumire Produs</h4></div>
				<div class="statp colmic"><h4>Pret</h4></div>
				<div class="statp colmic"><h4>Stoc</h4></div>
				<div class="statp colmic"><h4>Total Stoc</h4></div>
				<div class="statp colmic"><h4>Intrari</h4></div>
				<div class="statp colmic"><h4>Iesiri</h4></div>
				<div class="statp colmic"><h4>Retur</h4></div>
				<div class="statp colmic"><h4>Vanzari</h4></div>
			</div>
		

			@if($StProd->isEmpty() == true)
				<h4>Indisponibil</h4>
			@else		
			<div>
				<div class="row" style="margin-left:80px;">
					<div class="statp cid"><h4>{{$StProd[0]->id}}</h4></div>
					<div class="statp colmare"><h4>{{$StProd[0]->created_at}}</h4></div>
					<div class="statp colmare"><h4>{{$StProd[0]['produs']->cod_produs}}</h4></div>
					<div class="statp colmare"><h4>{{$StProd[0]['produs']->denumire_produs}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[0]['produs']->pret}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[0]->stoc_salvat}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[0]['produs']->total_stoc}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[0]->intrari}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[0]->iesiri}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[0]->retur}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[0]->vanzari}}</h4></div>
				</div>			
			</div>
			@endif
	
	</div>
	{!!$StProd->render()!!}
	
@endif




@stop
