<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<title>Dashboard</title>

	<link rel="stylesheet" href="/css/dashboard.css">	
	<link rel="stylesheet" href="/js/jquery-ui-1.11.4.custom/jquery-ui.css">

</head>

<body> 

	
		<div class="pop">
			<p>Esti sigur ca vrei sa stergi produsul?</p>
			<a href="" class="popda">da</a>
			<a href="" class="popnu">nu</a>	
		</div>
		<div class="popdisc">
			<p>Esti sigur ca vrei sa stergi discountul?</p>
			<a href="" class="popdadisc">da</a>
			<a href="" class="popnudisc">nu</a>	
		</div>
		<div class="popasc" >
			<p>Esti sigur ca vrei sa ascunzi produsul?</p>
			<a href="" class="popdaasc">da</a>
			<a href="" class="popnuasc">nu</a>	
		</div>
		<div class="poparata" >
			<p>Esti sigur ca vrei sa arati produsul?</p>
			<a href="" class="popdaarata">da</a>
			<a href="" class="popnuarata">nu</a>	
		</div>
		<div class="navbar">

		@if(Auth::user()->admin == 1)
			<a href="/dashboard/setaridashboard"><div class="menuslideadmin">

				<img src="/thumbs/setari.png" style="width:65px;height:65px;">

			</div></a>
		@elseif(Auth::user()->angajat_produse == 1)
			<div class="menuslide">

				<div class="lines"></div>

				<div class="lines"></div>

				<div class="lines"></div>

				<div class="lines"></div>

			</div>
		@endif
			

			<div class="sl">

				<div class="search">

					<form method="get" action="/dashboard/1/search/search" autocomplete='off'>
						<select name="tosearch">
							<option value="produse">Produse</option>
							@if(Auth::user()->angajat_produse == 0)
							<option value="membri">Membri</option>
							
							<option value="comenzi">Comenzi</option>
							@endif
						</select>	
						<input type="search" style="width:500px;" name="search">
						

						<input type="submit" value="Cauta">

					</form>		

				</div>

				<div class="logout">

					<a href="/logout">Iesire</a>					

				</div>

			</div>

		</div>

	



			<div class="sidebar">

				<ul><a href="/home" target="_blank"><li li class="baza"><h4>Site</h4></li></a></ul>

				@if(isset($filtre) && $filtre == 1) 
				<ul>
					<a href=><li class="baza filtreopen show" ><h4>Filtre</h4></li></a>

					<form method="GET" action="/dashboard/filtreadmin" id="filtreadmin" style="background-color:#098ead;display:none;">
						<input type="hidden" value="{{$_SERVER['REQUEST_URI']}}" name="servuri">
						<label class="remove">
							<input type="checkbox"  class="Rinput" name="stoc"> In Stoc
						</label><br>
						<label class="remove">
							<input type="checkbox"  class="Rinput" name="noutati"> Noutati
						</label><br>
						<label class="remove">
							<input type="checkbox"  class="Rinput" name="promotii"> Promotii
						</label><br>
						<label class="remove">
							<input type="checkbox"  class="Rinput" name="lichidari_stoc"> Lichidari Stoc
						</label><br>
						<label class="remove">
							<input type="checkbox"  class="Rinput" name="discounturi"> Discounturi
						</label><br>
						<label class="remove">
							<input type="checkbox"  class="Rinput" name="swap"> SWAP
						</label><br>
						<label class="remove">
							<input type="checkbox"  class="Rinput" name="poza"> Doar cu Poza
						</label><br>
						<label class="remove">Pret<br>
							<select name='pret'>
								<option value="">Selecteaza</option>	
								<option value="1">Crescator</option>
								<option value="0">Descrescator</option>
							</select>
						</label><br>		
						<p>
							<label for="amount">
								Raza Pret:	
							</label>	
							<input type="text"  id="amount" readonly style="border:0;color:#f6931f;font-weight:bold;" >
							<input type="hidden" name="rangemin" class="rangemin">
							<input type="hidden" name="rangemax" class="rangemax">
						</p><br>
						<div id="slider-range">
						</div><br>
						<label class="remove">Alfabetic <br>
							<select name='denumire_produs'>
								<option value="">Selecteaza</option>	
								<option value="1">A-Z</option>
								<option value="0">Z-A</option>
							</select>
						</label><br>
						@if(isset($brand))
							@foreach($brand as $br)
								<label class="">
									<input type="checkbox"  class="" name="brand{{$br->id}}"> {{$br->nume}}
								</label><br>		
							@endforeach	
						@endif
				</form>
				</ul>
				@endif

				<ul>

					@foreach($baze as $baza)

						<a href="/dashboard/{{$baza->id}}/1/baza/baza/prod"><li class="baza"><h4>{{$baza->nume}}</h4></li></a>

					@endforeach

				</ul>


				@if(Auth::user()->angajat_produse == 0)
				<ul>

					<a href="/dashboard/1/mem/membrii"><li class="baza"><h4>Membrii</h4></li></a>

					<a href="/dashboard/adaugaPage"><li class="baza"><h4>Adauga</h4></li></a>

				</ul>



				<ul>

					<a href="/dashboard/1/comenzi/comanda"><li class="baza"><h4>Comenzi</h4></li></a>

				</ul>

				<ul>

					<a href="/dashboard/1/statusstoc/status"><li class="baza"><h4>Status Stoc</h4></li></a>
					<a href="/dashboard/1/alertastoc/alerta"><li class="baza"><h4>Alerta Stoc</h4></li></a>
					
				</ul>	
				<ul>
					<a href="/dashboard/discounturi"><li class="baza"><h4>Discounturi</h4></li></a>
				</ul>
				
				@endif				

			</div>

			<div class="pagsus" style="float:left;">
				@if(isset($membrii))
					{!!$membrii->render()!!}
				@endif
				@if(isset($produs))
					{!!$produs->render()!!}
				@endif
				@if(isset($StProd))
					{!!$StProd->render()!!}
				@endif
			</div>

			<div class="content"> 

				@yield('content')

			</div>	

		



	<script src="/js/jquery.js"></script>

	<script src="/js/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
	<script>
		$(function(){
			$("#slider-range").slider({
				range:true,
				min:0,
				max:1500,
				values:[0,1000],
				slide:function(event,ui){
					$('#amount').val(ui.values[0]+" RON - " +ui.values[1]+" RON");		

					$('.rangemin').val(ui.values[0]);
					$('.rangemax').val(ui.values[1]);
				}	
			});	
			$("#amount").val($("#slider-range").slider("values",0) + " RON - " + $("#slider-range").slider("values",1)+ " RON");

			$('.rangemin').val($("#slider-range").slider("values",0));
			$('.rangemax').val($("#slider-range").slider("values",1));

		});	
	</script>

	<script src="/js/jquery.cookie.js"></script>

	<script src="/js/admin.js"></script>

</body>

</html>

