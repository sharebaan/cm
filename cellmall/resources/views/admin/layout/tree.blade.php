<div class="navtree">	
	@if(isset($subcatBaze))
		<ul class="tree">
			@foreach($subcatBaze as $subBaza)
				<a href="/dashboard/{{$subBaza->id}}/1/subbaza/subcat/prod"><li>{{$subBaza->nume}}</li></a>
			@endforeach
		</ul>
		
		<hr>		
	@endif

	@if(isset($subcatCopil))
		<ul class="tree">
			@foreach($subcatCopil as $copil)
				<a href="/dashboard/{{$copil->id}}/1/subcat2/copil/prod"><li>{{$copil->nume}}</li></a>
			@endforeach
		</ul>
		
		<hr>	
	@endif

	@if(isset($subcatCopil2))
		<ul class="tree">
		
			@foreach($subcatCopil2 as $copil)
				<a href="/dashboard/{{$copil->id}}/1/subcat3/copil2/prod"><li>{{$copil->nume}}</li></a>
			@endforeach
		</ul>
		
		<hr>	
	@endif

	@if(isset($subcatCopil3))
		<ul class="tree">

			@foreach($subcatCopil3 as $copil)
				<a href="/dashboard/{{$copil->id}}/1/subcat4/copil3/prod"><li>{{$copil->nume}}</li></a>
			@endforeach
		</ul>
		
		<hr>	
	@endif

	
</div>
