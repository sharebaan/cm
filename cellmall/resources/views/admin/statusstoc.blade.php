@extends('admin.layout.dashboard')

@section('content')

@include('admin.layout.tree')



@if(isset($StProd))

	<div class="tabelComenzi">
		
		@if($sp[1] != 0)
			<div class="row ssp" style="margin-left:80px;">
				<div class="statp cid"><h4><a href="{{$sp[0]}}/1/id/status">Id</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/1/data/status">Data</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/1/codprod/status">Cod Produs</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/1/numeprod/status">Denumire Produs</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/1/pret/status">Pret</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/1/stoc/status">Stoc</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/1/totalstoc/status">Total Stoc</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/1/intrari/status">Intrari</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/1/iesiri/status">Iesiri</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/1/retur/status">Retur</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/1/vanzari/status">Vanzari</a></h4></div>
			</div>
		@else
			<div class="row ssp" style="margin-left:80px;">
				<div class="statp cid"><h4><a href="{{$sp[0]}}/0/id/status">Id</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/0/data/status">Data</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/0/codprod/status">Cod Produs</a></h4></div>
				<div class="statp colmare"><h4><a href="{{$sp[0]}}/0/numeprod/status">Denumire Produs</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/0/pret/status">Pret</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/0/stoc/status">Stoc</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/0/totalstoc/status">Total Stoc</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/0/intrari/status">Intrari</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/0/iesiri/status">Iesiri</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/0/retur/status">Retur</a></h4></div>
				<div class="statp colmic"><h4><a href="{{$sp[0]}}/0/vanzari/status">Vanzari</a></h4></div>
			</div>
		@endif

			
	
		

		@for($i=0;$i<count($StProd);$i++)
			<div>
				<div class="row" style="margin-left:80px;">
					<div class="statp cid"><h4>{{$StProd[$i]->id}}</h4></div>
					<div class="statp colmare"><h4>{{$StProd[$i]['produs']->created_at}}</h4></div>
					<div class="statp colmare"><h4>{{$StProd[$i]['produs']->cod_produs}}</h4></div>
					<div class="statp colmare"><h4>{{$StProd[$i]['produs']->denumire_produs}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[$i]['produs']->pret}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[$i]->stoc_salvat}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[$i]['produs']->total_stoc}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[$i]->intrari}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[$i]->iesiri}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[$i]->retur}}</h4></div>
					<div class="statp colmic"><h4>{{$StProd[$i]->vanzari}}</h4></div>
				</div>			
			</div>
		@endfor
	
	</div>
	{!!$StProd->render()!!}	
@endif




@stop
