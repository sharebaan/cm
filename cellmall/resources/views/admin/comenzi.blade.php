@extends('admin.layout.dashboard')

@section('content')

@include('admin.layout.tree')
	
	@if(isset($comenzi))
		<div class="tabelComenzi">

			<div class="row" style="margin-left:80px;">

				@if($sp[1] != 0)	
					<div class="comenzi cid"><h4><a href="{{$sp[0]}}1/id/comanda">Id</a></h4></div>
					<div class="comenzi datacom"><h4><a href="{{$sp[0]}}1/data/comanda">Data</a></h4></div>
					<div class="comenzi comid"><h4><a href="{{$sp[0]}}1/comid/comanda">Comanda Id</a></h4></div>
					<div class="comenzi memid"><h4><a href="{{$sp[0]}}1/memid/comanda">Membru Id</a></h4></div>
					<div class="comenzi nrfac"><h4><a href="{{$sp[0]}}1/fac/comanda">Nr Factura</a></h4></div>
					<div class="comenzi tf"><h4><a href="{{$sp[0]}}1/total/comanda">Total</a></h4></div>
					<div class="comenzi stcom"><h4><a href="{{$sp[0]}}1/statcom/comanda">Status Comanda</a></h4></div>
					<div class="comenzi stpl"><h4><a href="{{$sp[0]}}1/starepl/comanda">Stare Plata</a></h4></div>
					<div class="comenzi stt"><h4><a href="{{$sp[0]}}1/staretrans/comanda">Stare Transport</a></h4></div>
					<div class="comenzi metpl"><h4><a href="{{$sp[0]}}1/metpl/comanda">Metoda Plata</a></h4></div>
					<div class="comenzi metlv"><h4><a href="{{$sp[0]}}1/metliv/comanda">Metoda Livrare</a></h4></div>
					<div class="comenzi datacom"><h4><a href="{{$sp[0]}}1/updated/comanda">Updated at</a></h4></div>
					
				@else
					<div class="comenzi cid"><h4><a href="{{$sp[0]}}0/id/comanda">Id</a></h4></div>
					<div class="comenzi datacom"><h4><a href="{{$sp[0]}}0/data/comanda">Data</a></h4></div>
					<div class="comenzi comid"><h4><a href="{{$sp[0]}}0/comid/comanda">Comanda Id</a></h4></div>
					<div class="comenzi memid"><h4><a href="{{$sp[0]}}0/memid/comanda">Membru Id</a></h4></div>
					<div class="comenzi nrfac"><h4><a href="{{$sp[0]}}0/fac/comanda">Nr Factura</a></h4></div>
					<div class="comenzi tf"><h4><a href="{{$sp[0]}}0/total/comanda">Total</a></h4></div>
					<div class="comenzi stcom"><h4><a href="{{$sp[0]}}0/statcom/comanda">Status Comanda</a></h4></div>
					<div class="comenzi stpl"><h4><a href="{{$sp[0]}}0/starepl/comanda">Stare Plata</a></h4></div>
					<div class="comenzi stt"><h4><a href="{{$sp[0]}}0/staretrans/comanda">Stare Transport</a></h4></div>
					<div class="comenzi metpl"><h4><a href="{{$sp[0]}}0/metpl/comanda">Metoda Plata</a></h4></div>
					<div class="comenzi metlv"><h4><a href="{{$sp[0]}}0/metliv/comanda">Metoda Livrare</a></h4></div>
					<div class="comenzi datacom"><h4><a href="{{$sp[0]}}0/updated/comanda">Updated at</a></h4></div>
				@endif
			</div>
			@foreach($comenzi as $index=>$comanda)
			<div>
				<div class="row">
					<div><a href="" id="" class="detalii">detalii</a></div>
					<div><a href="/dashboard/comandadepozit/{{$comanda->comanda_id}}/{{$comanda['membru']['id']}}" id="" class="print">print</a></div>
					<div class="comenzi cid"><h4>{{$comanda->id}}</h4></div>
					<div class="comenzi datacom"><h4>{{$comanda->created_at}}</h4></div>
					<div class="comenzi comid"><h4><a href="/dashboard/comprod/{{$comanda->comanda_id}}">{{$comanda->comanda_id}}</a></h4></div>
					<div class="comenzi memid"><h4><a href="/dashboard/profilmem/{{$comanda->membru_id}}">{{$comanda['membru']['email']}}</a></h4></div>
					<div class="comenzi nrfac"><h4>{{$comanda->nr_factura}}</h4></div>
					
					<div class="comenzi tf"><h4>{{$comanda->total}}</h4></div>

					<div class="comenzi stcom"><h4>{{$comanda->status_comanda}}</h4></div>
					<div class="comenzi stpl"><h4>{{$comanda->stare_plata}}</h4></div>
					<div class="comenzi stt"><h4>{{$comanda->stare_transport}}</h4></div>
					<div class="comenzi metpl"><h4>{{$comanda->metoda_plata}}</h4></div>
					<div class="comenzi metlv"><h4>{{$comanda->metoda_livrare}}</h4></div>	
					<div class="comenzi datacom"><h4>{{$comanda->updated_at}}</h4></div>
				</div>
			
				<div class="dropd" id="">
				<form action="/dashboard/comandaedit/{{$comanda->comanda_id}}" method="post">
					<a href="" class="cedit">Edit</a>
					<div>
					<h4>Status Comanda</h4>
						<p class="cinput">{{$comanda->status_comanda}}</p>
						
						<select name="statcom" class="cIn" style="display:none">
							@if($comanda->status_comanda == 'Neprocesata')
								<option value="{{$comanda->status_comanda}}">{{$comanda->status_comanda}}</option>
								<option value="In curs de procesare">In curs de procesare</option>
								<option value="Procesata">Procesata</option>
								<option value="In curs de livrare">In curs de livrare</option>
								<option value="Livrata">Livrata</option>
								<option value="Anulata">Anulata</option>
							@elseif($comanda->status_comanda == 'In curs de procesare')
								<option value="{{$comanda->status_comanda}}">{{$comanda->status_comanda}}</option>
								<option value="Procesata">Procesata</option>
								<option value="In curs de livrare">In curs de livrare</option>
								<option value="Livrata">Livrata</option>
								<option value="Anulata">Anulata</option>	
								<option value="Neprocesata">Neprocesata</option>
							@elseif($comanda->status_comanda == 'Procesata')
								<option value="{{$comanda->status_comanda}}">{{$comanda->status_comanda}}</option>
								<option value="In curs de livrare">In curs de livrare</option>
								<option value="Livrata">Livrata</option>
								<option value="Anulata">Anulata</option>	
								<option value="Neprocesata">Neprocesata</option>	
								<option value="In curs de procesare">In curs de procesare</option>
							@elseif($comanda->status_comanda == 'In curs de livrare')
								<option value="{{$comanda->status_comanda}}">{{$comanda->status_comanda}}</option>
								<option value="Livrata">Livrata</option>
								<option value="Anulata">Anulata</option>	
								<option value="Neprocesata">Neprocesata</option>	
								<option value="In curs de procesare">In curs de procesare</option>
								<option value="Procesata">Procesata</option>
							@elseif($comanda->status_comanda == 'Livrata')
								<option value="{{$comanda->status_comanda}}">{{$comanda->status_comanda}}</option>
								<option value="Anulata">Anulata</option>	
								<option value="Neprocesata">Neprocesata</option>	
								<option value="In curs de procesare">In curs de procesare</option>	
								<option value="Procesata">Procesata</option>
								<option value="In curs de livrare">In curs de livrare</option>
							@elseif($comanda->status_comanda == 'Anulata')
								<option value="{{$comanda->status_comanda}}">{{$comanda->status_comanda}}</option>
								<option value="Neprocesata">Neprocesata</option>	
								<option value="In curs de procesare">In curs de procesare</option>	
								<option value="Procesata">Procesata</option>
								<option value="In curs de livrare">In curs de livrare</option>
								<option value="Livrata">Livrata</option>	
							@endif
							
						</select>
					<h4>Stare Plata</h4>
						<p class="cinput">{{$comanda->stare_plata}}</p>
						
						<select name="stareplata" class="cIn" style="display:none">
							@if($comanda->stare_plata == 'Neachitata')
								<option value="{{$comanda->stare_plata}}">{{$comanda->stare_plata}}</option>
								<option value="Termen de Plata">Termen de Plata</option>
								<option value="Termen de Plata Depasit">Termen de Plata Depasit</option>
								<option value="Achitata">Achitata</option>
							@elseif($comanda->stare_plata == 'Termen de Plata')
								<option value="{{$comanda->stare_plata}}">{{$comanda->stare_plata}}</option>
								<option value="Termen de Plata Depasit">Termen de Plata Depasit</option>
								<option value="Achitata">Achitata</option>
								<option value="Neachitata">Neachitata</option>
							@elseif($comanda->stare_plata == 'Termen de Plata Deposit')
								<option value="{{$comanda->stare_plata}}">{{$comanda->stare_plata}}</option>
								<option value="Achitata">Achitata</option>
								<option value="Neachitata">Neachitata</option>
								<option value="Termen de Plata">Termen de Plata</option>
							@elseif($comanda->stare_plata == 'Achitata')
								<option value="{{$comanda->stare_plata}}">{{$comanda->stare_plata}}</option>
								<option value="Neachitata">Neachitata</option>
								<option value="Termen de Plata">Termen de Plata</option>
								<option value="Termen de Plata Deposit">Termen de Plata Deposit</option>
							@endif	
						</select>
					<h4>Stare Transport</h4>
						<p class="cinput">{{$comanda->stare_plata}}</p>
									
						<select name="sttrans" class="cIn" style="display:none">
							@if($comanda->stare_transport == 'Necolectata')
								<option value="{{$comanda->stare_transport}}">{{$comanda->stare_transport}}</option>
								<option value="Colectata">Colectata</option>
								<option value="Receptionata Curier">Receptionata Curier</option>
								<option value="Plecata din Centru">Plecata din centru</option>
								<option value="Spre Destinatar">Spre Destinatar</option>
								<option value="Livrata">Livrata</option>
							@elseif($comanda->stare_transport == 'Colectata')
								<option value="{{$comanda->stare_transport}}">{{$comanda->stare_transport}}</option>
								<option value="Receptionata Curier">Receptionata Curier</option>
								<option value="Plecata din Centru">Plecata din centru</option>
								<option value="Spre Destinatar">Spre Destinatar</option>
								<option value="Livrata">Livrata</option>
								<option value="Necolectata">Necolectata</option>
							@elseif($comanda->stare_transport == 'Receptionata Curier')
								<option value="{{$comanda->stare_transport}}">{{$comanda->stare_transport}}</option>
								<option value="Plecata din Centru">Plecata din centru</option>
								<option value="Spre Destinatar">Spre Destinatar</option>
								<option value="Livrata">Livrata</option>
								<option value="Necolectata">Necolectata</option>
								<option value="Colectata">Colectata</option>
							@elseif($comanda->stare_transport == 'Plecata din centru')
								<option value="{{$comanda->stare_transport}}">{{$comanda->stare_transport}}</option>
								<option value="Spre Destinatar">Spre Destinatar</option>
								<option value="Livrata">Livrata</option>
								<option value="Necolectata">Necolectata</option>
								<option value="Colectata">Colectata</option>
								<option value="Receptionata Curier">Receptionata Curier</option>
							@elseif($comanda->stare_transport == 'Spre Destinatar')
								<option value="{{$comanda->stare_transport}}">{{$comanda->stare_transport}}</option>
								<option value="Livrata">Livrata</option>
								<option value="Necolectata">Necolectata</option>
								<option value="Colectata">Colectata</option>
								<option value="Receptionata Curier">Receptionata Curier</option>
								<option value="Plecata din centru">Plecata din centru</option>
							@elseif($comanda->stare_transport == 'Livrata')
								<option value="{{$comanda->stare_transport}}">{{$comanda->stare_transport}}</option>
								<option value="Necolectata">Necolectata</option>
								<option value="Colectata">Colectata</option>
								<option value="Receptionata Curier">Receptionata Curier</option>
								<option value="Plecata din centru">Plecata din centru</option>
								<option value="Spre Destinatar">Spre Destinatar</option>
							@endif	
						</select>
					</div>	
						<input type="submit" class="ceditaree" style="display:none" value="Editare">		
						<input type="hidden" name="fackey" value="{{$comanda->comanda_id}}">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
				</form>
			</div>
			</div>
			@endforeach
			{!!$comenzi->render()!!}
		</div>
		
	@endif

@stop
