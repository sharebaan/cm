   <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/layout.css">
  <link rel="stylesheet" href="/slick/slick.css">
  <link rel="stylesheet" href="/slick/slick-theme.css">
  <title>Finalizare Comanda</title>
</head>
<body>

@include('partiale/header')
@if(Session::has('err'))

               <div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2" role="alert">

                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>

                    <span class="sr-only">Eroare:</span>

                    	{!!Session::get('err')!!}

               </div>
@endif
 <div class="content col-xs-12 col-sm-12 col-md-12 col-lg-12">
@if(isset($cart))
	
    <form action="/cumpara" method="post" id="cumpara">
		
    <table class="fincom-prod">
      <thead>
        <tr>
          <th class="c1">&nbsp;</th>
          <th class="c2">Denumire Produs</th>
          <th class="c3">Cod Produs</th>
          <th class="c4">Cantitate</th>
          <th class="c5">Pret/buc</th>
          <th class="c6">Subtotal</th>
          <th class="c7">&nbsp;</th>
        </tr>
      </thead>
      <tbody>
	@for($i=count($cart['nume'])-1;$i>=0;$i--)	
        <tr class="cartmare">
		@if($cart['poza'][$i] == '')
          <td data-label="Imagine produs" class="c1"><a href="/produs/{{$cart['id'][$i]}}"><img src="/products/no_image.png" alt=""></a></td>
		@else
			<td data-label="Imagine produs" class="c1"><a href="/produs/{{$cart['id'][$i]}}"><img src="{{$cart['poza'][$i]}}" alt=""></a></td>
		@endif
          <td data-label="Denumire Produs" class="c2"><a href="/produs/{{$cart['id'][$i]}}">{{$cart['nume'][$i]}}</a></td>
		  <input type="hidden" name="comprod{{$i}}" value="{{$cart['nume'][$i]}}">
          <td data-label="Cod" class="c3 c3cos" >{{$cart['cod'][$i]}}</td>
		  
          <td data-label="Cantitate" class="c4">
              <div class="comqty">
								<input type="hidden" name="comqty{{$i}}" value="{{$cart['qty'][$i]}}">
								<div class="form-group qtygroupfin">
									<div class="btn-group">
										@if($cart['qty'][$i] == 1)
										<button disabled='disabled' class="btn btn-default plusminfin minusfin" value="-" name="{{$cart['id'][$i]}}"  type="button">-</button>
										@else
										<button  class="btn btn-default plusminfin minusfin" value="-" name="{{$cart['id'][$i]}}"  type="button">-</button>
										@endif
									</div>
									<input type="text" class="form-control qtyfin" style="font-weight:normal;" disabled value="{{$cart['qty'][$i]}}">
									<div class="btn-group">
										<button class="btn btn-default plusminfin plusfin" value="+" name="{{$cart['id'][$i]}}" type="button">+</button>
								  </div>
								</div>
							</div>
          </td>
          <td data-label="Pret/buc" class="c5"><p>{{number_format($cart['total'][$i] / $cart['qty'][$i],2,'.',',')}}</p><p>&nbsp RON</p></td>
		  <input type="hidden" name="compretbuc{{$i}}" value="{{number_format($cart['total'][$i] / $cart['qty'][$i],2,'.',',')}}">
          <td data-label="Subtotal" class="c6 c6elem"><p>{{number_format($cart['total'][$i],2,'.',',')}}</p><p>&nbsp RON</p></td>
          <td data-label="Sterge produs" class="c7">
            <a href="" name="{{$cart['id'][$i]}}" class="stergemare"><img src="/thumbs/close.png" alt="Sterge Produs"></a>
          </td>
        </tr>
		
	@endfor
        
      </tbody>
    </table>
    <div class="fincom-price">
      <ul class="labels">
        <li>Total Cos (TVA inclus):</li>
        <li>Cost Transport:</li>
        <li>Total de Plata (TVA inclus):</li>
        <div class="clear"></div>
      </ul>
      <ul class="prices psus">
        <li><p>{{number_format($cart['totalp'],2,'.',',')}}</p><p>&nbspRON</p></li>
        <li><p value="0"></p><p>&nbspRON</p></li>
        <li><p>{{number_format($cart['totalp'],2,'.',',')}}</p><p>&nbspRON</p></li>
        <div class="clear"></div>
      </ul>
      <div class="clear"></div>
      <a href="/home" role="button" class="btn btn-warning inapoicumpsus ">Inapoi la Cumparaturi</a>
    </div>

    <div class="fincom col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="date-pers col-lg-6">
        <h4>Date Personale</h4>
        <b class="title-data datec">Nume: </b><b class="datec dnume">{{Auth::user()['nume']}}</b><br>
        
        <b class="title-data datec">Prenume: </b><b class="datec dprenume">{{Auth::user()['prenume']}}</b><br>
       
        <b class="title-data datec">CNP: </b><b class="datec dcnp">{{Auth::user()['CNP']}}</b><br>
      
        <b class="title-data datec">E-mail: </b><b class="datec dcnp">{{Auth::user()['email']}}</b><br>
       
        <b class="title-data datec">Telefon: </b><b class="datec dtelefon">{{Auth::user()['telefon']}}</b><br>
        
      </div>
      <div class="detalii-comp col-lg-6">
        <h4>Detalii Companie</h4>
        <b class="title-data datec">Nume Companie: </b><b class="datec dnumecomp">{{Auth::user()['nume_companie']}}</b><br>
       
        <b class="title-data datec">C.U.I: </b><b class="datec dcui">{{Auth::user()['CUI']}}</b><br>
       
        <b class="title-data datec">Nr.ord.reg.com/an: </b><b class="datec dnrreg">{{Auth::user()['nr_reg']}}</b><br>
       
        <b class="title-data datec">Banca: </b><b class="datec dbanca">{{Auth::user()['banca']}}</b><br>
       
        <b class="title-data datec">Cont IBAN: </b><b class="datec dcontib">{{Auth::user()['cont_IBAN']}}</b><br>
       
      </div>
      <div class="adresa-sed-soc col-lg-4">
        <h4>Adresa Sediu Social</h4>
        <b class="title-data datec">Adresa Sediu Social: </b><b class="datec dass">{{Auth::user()['adresa_sediu']}}</b><br>
        
        <b class="title-data datec">Localitate Sediu Social: </b><b class="datec dlss">{{Auth::user()['localitate_sediu']}}</b><br>
       
        <b class="title-data datec">Judet/Sector Sediu Social: </b><b class="datec djsss">{{Auth::user()['judet_sector_sediu']}}</b><br>
      
      </div>
      <div class="adresa-fac col-lg-4">
        <h4>Adresa Facturare</h4>
        <b class="title-data datec">Adresa Facturare: </b><b class="datec daf">{{Auth::user()['adresa_fac']}}</b><br>
     
        <b class="title-data datec">Localitate Facturare: </b><b class="datec dlf">{{Auth::user()['localitate_fac']}}</b><br>
      
        <b class="title-data datec">Judet/Sector Facturare: </b><b class="datec djsf">{{Auth::user()['judet_sector_fac']}}</b><br>
      
      </div>
      <div class="adresa-liv col-lg-4">
        <h4>Adresa Livrare</h4>
        <b class="title-data datec">Adresa Livrare: </b><b class="datec dal">{{Auth::user()['adresa_liv']}}</b><br>
     
        <b class="title-data datec">Localitate Livrare: </b><b class="datec dll">{{Auth::user()['localitate_liv']}}</b><br>
    
        <b class="title-data datec">Judet/Sector Livrare: </b><b class="datec djsl">{{Auth::user()['judet_sector_liv']}}</b><br>
   
      </div>
    </div>

    <div class="optiuniplata col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="metplata col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <h4>Alege metoda de plata</h4>

								<div class="radio col-sm-12">

									<label for="numerar">

										<input type="radio" class="radio mp" name="metplata" id="numerar" value="Numerar">

										Numerar</label>

										<div class="small col-sm-12">

											<small>Achitare comanda la sediul nostru (doar persoane juridice)</small>

										</div>

								</div>



								<div class="radio col-sm-12">

									<label for="numerarcn">

										<input type="radio" class="radio mp" name="metplata" id="numerarcn" value="Numerar (Contul Nostru)">

										Numerar in contul nostru</label>

										<div class="small col-sm-12">

											<small>Depunere numerar direct in contul:</small>

										</div>

								</div>

									<div class="form-group col-sm-8 contbanca1" >

										<select name="contbanca1" id="contbanca1" class="form-control">

											<option value="" selected="selected">Libra Internet Bank SA</option>

											<option value="">Banca Transilvania SA</option>

										</select>

									</div>



									<div class="radio col-sm-12">

										<label for="op">

											<input type="radio" class="radio mp"  name="metplata" id="op" value="Ordin Plata">

											Ordin de plata</label>

											<div class="small col-sm-12">

												<small>Plata in avans in contul:</small>

											</div>

									</div>

										<div class="form-group col-sm-8 contbanca2" >

											<select name="contbanca2" id="contbanca2" class="form-control">

												<option value="" selected="selected">Libra Internet Bank SA</option>

												<option value="">Banca Transilvania SA</option>

											</select>

										</div>

										<div class="radio col-sm-12">

											<label for="virb">

												<input type="radio" class="radio mp" name="metplata" id="virb" value="Virament Bancar">

												Virament Bancar</label>

												<div class="small col-sm-12">

													<small>Din contul dumneavoastra in contul:</small>

												</div>

										</div>

											<div class="form-group col-sm-8 contbanca3">

												<select name="contbanca3" id="contbanca3" class="form-control">

													<option value="" selected="selected">Libra Internet Bank SA</option>

													<option value="">Banca Transilvania SA</option>

												</select>

											</div>

											<div class="radio col-sm-12">

												<label for="ramburs">

													<input type="radio" class="radio mp" name="metplata" id="ramburs" value="Ramburs">

													Ramburs</label>

													<div class="small col-sm-12">

														<small>Plata la livrare</small>

													</div>

											</div>



      </div>


      <div class="metliv col-xs-12 col-sm-12 col-md-4 col-lg-4"style="display:none;">


								<h4>Alege metoda de livrare</h4>

								<div class="radio col-sm-12">

									<label for="lasediu">

									<input type="radio" class="radio" name="metliv" id="lasediu" value="La Sediu">

										Ridicare la sediul firmei</label>

										<div class="small col-sm-12">

											<small>0 RON</small>

										</div>

									</div>

								<div class="radio col-sm-12">

									<label for="curbuc">

									<input type="radio" class="radio" name="metliv" id="curbuc" value="Curier Bucuresti">

										Curier Bucuresti</label>

									<div class="small col-sm-12">

											<small>Transport gratuit (Livrare in aceeasi zi pentru comenzile efectuate pana ora 14:00)</small>

										</div>

									</div>

								<div class="radio col-sm-12">

									<label for="fancur">

									<input type="radio" class="radio" name="metliv" id="fancur" value="Fan Curier">

										Fan Curier</label>

										<div class="small col-sm-12">

											<small>17 RON pentru orice localitate din tara (Livrare 24h-48h)</small>

										</div>

									</div>

								<div class="radio col-sm-12">

									<label for="cargus">

									<input type="radio" class="radio" name="metliv" id="cargus" value="Cargus">

										Cargus</label>

										<div class="small col-sm-12">

											<small>17 RON pentru orice localitate din tara (Livrare 24h-48h)</small>

										</div>

									</div>



      </div>
      <div class="tipfac col-xs-12 col-sm-12 col-md-4 col-lg-4" style="display:none;">
        <h4>Alege tipul de facturare</h4>

								<div class="radio col-sm-12">

									<label for="pf">

									<input type="radio" class="radio" name="tipfac" id="pf" value="Persoana Fizica">

										Persoana Fizica</label>

								</div>

								<div class="radio col-sm-12">

									<label for="pj">

									<input type="radio" class="radio" name="tipfac" id="pj" value="Persoana Juridica">

										Persoana Juridica</label>

								</div>
								<br><br><br>
								<div class="radio col-sm-12">
									<a href="" class="resetop"> <img src="/thumbs/refresh.png" class="resetop" style="width:32px;" alt="Reseteaza optiunile">	Reseteaza optiunile</a>
								</div>

      </div>
	<div class="final col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="mentiune col-xs-12 col-sm-12 col-md-6 col-lg-6 ">
        <h4>Adauga mentiuni</h4>
          <textarea name="mentliv" placeholder="Scrie aici alte mentiuni legate de comanda ta (puncte de reper, intervale orare distincte, etc) ..." class="form-control" id="mentiuniarea" cols="30" rows="10"></textarea>
      </div>
      <div class="accept col-xs-12 col-sm-12 col-md-6 col-lg-6">

        <div class="fincom-price">
          <ul class="labels">
            <li>Total Cos (TVA inclus):</li>
            <li>Cost Transport:</li>
            <li>Total de Plata (TVA inclus):</li>
            <div class="clear"></div>
          </ul>
          <ul class="prices pjos">
        	<li><p>{{number_format($cart['totalp'],2)}}</p><p>&nbspRON</p></li>
        	<li><p value="0"></p><p>&nbspRON</p></li>
        	<li><p>{{number_format($cart['totalp'],2)}}</p><p>&nbspRON</p></li>
        	<div class="clear"></div>
      	  </ul>
          <div class="clear"></div>

        </div>

        <div class="row">
          <div class="tercon col-sm-12 form-group">
            <label for="checkterm" style="cursor:pointer;">Prin trimiterea comenzii se accepta <a href="/informatii/termenisiconditii">Termenii si Conditiile</a> CellMall.ro </label>
            <input type="checkbox"value='1'style="margin-left:5px;" name="acceptterm" id="checkterm" class="form-controll">
          </div>

          <div class="fincombut">
            <a href="/home" role="button" class="btn btn-warning inapoicump">Inapoi la Cumparaturi</a>
			
			@if(Auth::user()->demo == 1)
            	<button type="submit" disabled class="btn btn-primary comanda">Trimite Comanda</button>
			@else
            	<button type="submit" class="btn btn-primary comanda">Trimite Comanda</button>
			@endif
          </div>
        </div>
		<input type="hidden" name="totalt" class="totaltinput" value="0">
		<input type="hidden" name="totalp" class="totalpinput" value="{{$cart['totalp']}}">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
      </div>
    </div>
	</div>
  </form>
@endif
  </div>

@include('partiale/footer')
@include('partiale/footermob')


<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/jnumber.min.js"></script>
<script src="/slick/slick.min.js"></script>
<script src="/js/footercar.js"></script>
<script src="js/mobdd.js"></script>
<script src="js/metplata.js"></script>
<script src="js/fincomanda.js"></script>
<script src="/js/misc.js"></script>
</body>
</html>                                                                                                                                                                                                                                                                                                                                                        
