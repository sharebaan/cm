<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/welcome.css">
	<title>CellMall</title>
</head>

<body>
				<div class="cover-container col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<div class="inner cover">
					<div class="row center-block">
						<img src="/thumbs/logo.png" alt="..." class="center-block img-responsive">	
					</div>
						@if(Session::has('success'))
							<div class="alert alert-success register-suc col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Succes:</span>
								{{Session::get('success')}}
							</div>
						@endif
							
					<form class="form-horizontal" method="post" action="/resetpass" >
						@if(Session::has('err'))
						<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Eroare:</span>
								{{Session::get('err')}}
						</div>
						@endif
						
						<div class="form-group">
							<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
								<input type="password" class="form-control" name="parola" id="inputPassword3" placeholder="Introdu noua parola">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
								<input type="password" class="form-control" name="paroladinnou" id="inputPassword3" placeholder="Introdu noua parola din nou">
							</div>
						</div>
						@if(Session::has('incorect'))
							<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Eroare:</span>
								{{Session::get('incorect')}}
							</div>
						@endif
						@if($errors->any())
						<div class="alert alert-danger col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" role="alert" >
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only">Eroare:</span>
								@foreach($errors->all() as $err)
									{{$err}}	
								@endforeach
						</div>
						
						@endif
						<div class="form-group">
							<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
								<button type="submit"  class="btn btn-warning col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">Schimba</button>
							</div>
							<div class="col-sm-offset-2 col-sm-10">	
							
							</div>
						</div>
						<input type="hidden" name="id" value="{{$id}}">	
						<input type="hidden" name="_token" value="{{csrf_token()}}">	
					</form>
					<br>
					<div class="pozetech col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4" >
						<div class="col-xs-4" >
								<img src="/thumbs/android.png" class="img-responsive" style="display:block;margin:0 auto;" alt="...">
						</div>
						<div class="col-xs-4 " >
								<img src="/thumbs/apple.png" class="img-responsive" style="display:block;margin:0 auto;" alt="...">
						</div>
						<div class="col-xs-4 " >
								<img src="/thumbs/windows.png" class="img-responsive" style="display:block;margin:0 auto;" alt="...">
						</div>
					</div>
				</div>

				
				<div class="modal fade" id="termcon" role="dialog">
					<div class="modal-dialog">
					
						<div class="modal-content">
							<div class="alert alert-danger" role="alert" style="margin:0">
								<h2><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>  Atentie</h2>
								<h4>Acest site este dedicat exclusiv Persoanelor Juridice.</h4><br>
								<h4>Este necesar sa te loghezi sau sa creezi un cont nou pentru a avea acces la site.</h4>
								<small>Click oriunde pentru a continua.</small>
							</div>
							
							
						</div>
					</div>
				</div>
			</div>



<script src="/js/jquery.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
	
</body>
</html>
