<?php namespace App;







use Illuminate\Database\Eloquent\Model;







class StatusStoc extends Model{	







	protected $table = 'status_stoc';







	protected $fillable = ['prod_id','stoc_salvat','intrari','iesiri','retur','vanzari'];



	public function produs(){
		return $this->belongsTo('App\Produse','prod_id');
	}

	public static function stsprod($ord,$col){
			
		if($ord == 1){
			return static::leftJoin(
				'produse',
				'status_stoc.prod_id','=','produse.id'			
			)->orderBy($col,'asc');	
		}else{
			return static::leftJoin(
				'produse',
				'status_stoc.prod_id','=','produse.id'			
			)->orderBy($col,'desc');
		}
	}
}
