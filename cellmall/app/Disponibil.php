<?php namespace App;



use Illuminate\Database\Eloquent\Model;



class Disponibil extends Model{	



		protected $table = 'disponibilitate'; 
		
		
	protected $fillable = ['id','in_stoc','produs_id','stoc_redus','produs_indisponibil','in_curand'];


	public function produs(){
		return $this->hasOne('App\Produse','id');
	}

	
	public static function cuNoutati(){
		return static::leftJoin(
			'produse',
			'disponibilitate.produs_id','=','produse.id'
			)->where('noutati','=',1);
	}
	public static function catbaza(){
		return static::leftJoin(
			'produse',
			'disponibilitate.produs_id','=','produse.id'		
		)->where('cat_baza_id','=',1);
	}
	
}
