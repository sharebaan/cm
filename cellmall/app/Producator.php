<?php namespace App;



use Illuminate\Database\Eloquent\Model;



class Producator extends Model{	

	protected $table = 'producatori';

	protected $fillable = ['poza','nume'];

}