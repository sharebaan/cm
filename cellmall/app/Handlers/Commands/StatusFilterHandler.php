<?php namespace App\Handlers\Commands;

use App\Commands\StatusFilter;
use App\StatusStoc;
use Illuminate\Queue\InteractsWithQueue;

class StatusFilterHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle(StatusFilter $command){
		
		switch ($command->sta) {
				case 'statusstoc':
					$col = 'statusstoc';
					return $this->filterStatus($command,$col);
					break;
				
				case 'id':
					$col = 'id';
					return $this->filter($command,$col);
					break;

				case 'data':
					$col = 'created_at';
					return $this->filter($command,$col);
					break;
				case 'codprod':
					$col = 'cod_produs';
					return $this->prodfilter($command,$col);
					break;			
				case 'numeprod':
					$col = 'denumire_produs';
					return $this->prodfilter($command,$col);
					break;
		
				case 'pret':
					$col = 'pret';
					return $this->prodfilter($command,$col);
					break;
				case 'stoc':
					$col = 'stoc';
					return $this->prodfilter($command,$col);
					break;
	
				case 'totalstoc':
					$col = 'total_stoc';
					return $this->prodfilter($command,$col);
					break;	
				case 'intrari':
					$col = 'intrari';
					return $this->filter($command,$col);
					break;

				case 'iesiri':
					$col = 'iesiri';
					return $this->filter($command,$col);
					break;	
				case 'retur':
					$col = 'retur';
					return $this->filter($command,$col);
					break;
				case 'vanzari':
					$col = 'vanzari';
					return $this->filter($command,$col);
					break;	

			}	
	}
	public function filterStatus(StatusFilter $command,$col){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
		
		return [
			StatusStoc::orderBy('id','asc')->with('produs')->paginate(50),
			[substr($_SERVER['REQUEST_URI'],0,10),$segment[sizeof($segment)-3],$segment[sizeof($segment)-2]]
			//StatusStoc::with('produs')->get()
		];
	}

	public function filter(StatusFilter $command,$col){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
		
		if($command->ord == 1){
			return [
				StatusStoc::orderBy($col,'asc')->with('produs')->paginate(50),
				[substr($_SERVER['REQUEST_URI'],0,10),0,$segment[sizeof($segment)-2]]
				//StatusStoc::with('produs')->get()
			];
		}else{
			return [
				StatusStoc::orderBy($col,'desc')->with('produs')->paginate(50),
				[substr($_SERVER['REQUEST_URI'],0,10),1,$segment[sizeof($segment)-2]]
				//StatusStoc::with('produs')->get()
			];
		}
	}


	public function prodfilter(StatusFilter $command,$col){
			$segment = explode('/', $_SERVER['REQUEST_URI']);
			if($command->ord == 1){
				return [
					StatusStoc::paginate(50),		
					[substr($_SERVER['REQUEST_URI'],0,10),0,$segment[sizeof($segment)-2]],		
					StatusStoc::stsprod($command->ord,$col)->paginate(50)			
				];
			}else{
				return [
					StatusStoc::paginate(50),		
					[substr($_SERVER['REQUEST_URI'],0,10),1,$segment[sizeof($segment)-2]],		
					StatusStoc::stsprod($command->ord,$col)->paginate(50)			
				];	
			}
	}
}
