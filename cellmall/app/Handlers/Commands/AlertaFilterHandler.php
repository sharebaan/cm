<?php namespace App\Handlers\Commands;

use App\AlertaStoc;
use App\Commands\AlertaFilter;
use Illuminate\Queue\InteractsWithQueue;

class AlertaFilterHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle(AlertaFilter $command)
	{
		switch ($command->sta){
			case 'alertastoc':
				$col = 'alertastoc';
				return $this->filterAlerta($command,$col);
			break;	
			case 'id':
				$col = 'id';
				return $this->filter($command,$col);
			break;
			case 'data':
				$col = 'created_at';
				return $this->filter($command,$col);
			break;
			
		}
	}
	
	public function filterAlerta(AlertaFilter $command,$col){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
		
		return [
			AlertaStoc::orderBy('id','asc')->with('produs')->with('disponibilitate')->paginate(50),
			[substr($_SERVER['REQUEST_URI'],0,10),$segment[sizeof($segment)-3],$segment[sizeof($segment)-2]]
		];
	}

	public function filter(AlertaFilter $command,$col){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
		
		if($command->ord == 1){
			return [
				AlertaStoc::orderBy($col,'asc')->with('produs')->with('disponibilitate')->paginate(50),
				[substr($_SERVER['REQUEST_URI'],0,10),0,$segment[sizeof($segment)-2]]
			];
		}else{
			return [
				AlertaStoc::orderBy($col,'desc')->with('produs')->with('disponibilitate')->paginate(50),
				[substr($_SERVER['REQUEST_URI'],0,10),1,$segment[sizeof($segment)-2]]
			];
		}
	}
}
