<?php namespace App\Handlers\Commands;

use App\Commands\SearchFilter;
use App\Produse;
use App\Membrii;
use App\StatusFac;
use Illuminate\Queue\InteractsWithQueue;

class SearchFilterHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle(SearchFilter $command){

		switch($command->sta){
			case 'search':	
				return $this->search($command);
				break;
			case 'id':
				$col = 'id';
				return $this->filter($command,$col);
				break;

			case 'codprod':
				$col = 'cod_produs';
				return $this->filter($command,$col);
				break;	

			case 'numeprod':
				$col = 'denumire_produs';
				return $this->filter($command,$col);
				break;

			case 'pret_original':
				$col = 'pret_original';
				return $this->filter($command,$col);	
				break;
			case 'tva':
				$col = 'tva';
				return $this->filter($command,$col);	
				break;
			case 'pretTva':
				$col = 'pretTva';
				return $this->filter($command,$col);	
				break;
			case 'pret':
				$col = 'pret';
				return $this->filter($command,$col);	
				break;

			case 'stoc':
				$col = 'stoc';
				return $this->filter($command,$col);	
				break;	

			case 'totalstoc':
				$col = 'total_stoc';
				return $this->filter($command,$col);
				break;

			case 'intrari':
				$col = 'intrari';
				return $this->filter($command,$col);
				break;	

			case 'iesiri':
				$col = 'iesiri';
				return $this->filter($command,$col);
				break;
					
			case 'retur':
				$col = 'retur';
				return $this->filter($command,$col);
				break;
					
			case 'vanzari':
				$col = 'vanzari';
				return $this->filter($command,$col);
				break;	

			case 'pozitieraft':
				$col = 'pozitie_raft';
				return $this->filter($command,$col);
				break;	

			case 'promotii':
				$col = 'promotii';
				return $this->filter($command,$col);
				break;	

			case 'noutati':
				$col = 'noutati';
				return $this->filter($command,$col);
				break;	

			case 'alertastoc':
				$col = 'alerta_stoc';
				return $this->filter($command,$col);
				break;	

			case 'culoare':
				$col = 'culoare';
				return $this->filter($command,$col);
				break;

			case 'dimensiune':
				$col = 'dimensiune';
				return $this->filter($command,$col);
				break;

			case 'atentie':
				$col = 'atentie';
				return $this->filter($command,$col);
				break;	

			case 'detalii':
				$col = 'detalii';
				return $this->filter($command,$col);
				break;				

			case 'tipambalaj':
				$col = 'tip_ambalaj';
				return $this->filter($command,$col);
				break;	

			case 'lichidaristoc':
				$col = 'lichidari_stoc';
				return $this->filter($command,$col);
				break;

			case 'brand':
				$col = 'brand_id';
				return $this->filter($command,$col);
				break;	

			case 'producator':
				$col = 'producator_id';
				return $this->filter($command,$col);
				break;	

			case 'disponibil':
				
				return $this->disponibil($command);
				break;	
			case 'poza':
				$col = 'poza';
				return $this->filter($command,$col);
				break;
			case 'discount':
				$col = 'discount';
				return $this->filter($command,$col);
				break;
			case 'updated':
				$col='updated_at';
				return $this->filter($command,$col);
				break;
			case 'created':
				$col='created_at';
				return $this->filter($command,$col);
				break;	
			case 'email':
				$col='email';
				return $this->filter($command,$col);
				break;
			case 'telefon':
				$col='telefon';
				return $this->filter($command,$col);
				break;
			case 'nume':
				$col='nume';
				return $this->filter($command,$col);
				break;	
			case 'prenume':
				$col='prenume';
				return $this->filter($command,$col);
				break;
			case 'cnp':
				$col='CNP';
				return $this->filter($command,$col);
				break;	
			case 'adresafac':
				$col='adresa_fac';
				return $this->filter($command,$col);
				break;
			case 'localfac':
				$col='localitate_fac';
				return $this->filter($command,$col);
				break;
			case 'judetfac':
				$col='judet_sector_fac';
				return $this->filter($command,$col);
				break;	
			case 'adresaliv':
				$col='adresa_liv';
				return $this->filter($command,$col);
				break;
			case 'localliv':
				$col='localitate_liv';
				return $this->filter($command,$col);
				break;	
			case 'judetliv':
				$col='judet_sector_liv';
				return $this->filter($command,$col);
				break;
			case 'status':
				$col='status';
				return $this->filter($command,$col);
				break;
			case 'tipcont':
				$col='tipCont';
				return $this->filter($command,$col);
				break;	
			case 'recomandari':
				$col='recomandari';
				return $this->filter($command,$col);
				break;
			case 'created':
				$col='created_at';
				return $this->filter($command,$col);
				break;	
			case 'updated':
				$col='updated_at';
				return $this->filter($command,$col);
				break;	
		}	
	}

	public function preSearchMembri(SearchFilter $command){
			$membru = Membrii::search($command->input)->with('comenzi')->paginate(50)
					->appends([
						'tosearch'=>$command->tosearch,
						'search'=>$command->input
						]);
		if($membru->isEmpty() == false){
			if($membru[0]['comenzi']->isEmpty() == false){
				return $membru;	
			}else{
				return [['comenzi'=>StatusFac::search($command->input)->with('membru')->paginate(50)->appends([
						'tosearch'=>$command->tosearch,
						'search'=>$command->input
						])]];
			}	
		}else{
			return [['comenzi'=>StatusFac::search($command->input)->with('membru')->paginate(50)->appends([
						'tosearch'=>$command->tosearch,
						'search'=>$command->input
						])]];
		}
	}

	public function search(SearchFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
		
		if($command->tosearch == 'produse'){
			return[
					Produse::search($command->input)->with('poze')->paginate(20)->appends([
						'tosearch'=>$command->tosearch,
						'search'=>$command->input
						]),
				[substr($_SERVER['REQUEST_URI'],0,10),1,explode('?',$segment[sizeof($segment)-1])[1]]
			];
		}elseif($command->tosearch == 'membri'){
			return[
					Membrii::search($command->input)->paginate(20)->appends([
						'tosearch'=>$command->tosearch,
						'search'=>$command->input
						]),
				[substr($_SERVER['REQUEST_URI'],0,10),1,explode('?',$segment[sizeof($segment)-1])[1]]
			];
		}elseif($command->tosearch == 'comenzi'){
			
			return[
					$this->preSearchMembri($command),
					[substr($_SERVER['REQUEST_URI'],0,10),1,explode('?',$segment[sizeof($segment)-1])[1]],
					
			];
		}	
		
	}	
	public function filter(SearchFilter $command,$col){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
			
		if($command->ord ==1){
			if($command->tosearch == 'produse'){
				return [
						Produse::search($command->input)->with('poze')->orderBy($col,'asc')->paginate(20)->appends([
							'tosearch'=>$command->tosearch,
							'search'=>$command->input
						]),
					[substr($_SERVER['REQUEST_URI'],0,10),0,explode('?',$segment[sizeof($segment)-1])[1]]
				];
			}elseif($command->tosearch == 'membri'){
				return [
						Membrii::search($command->input)->orderBy($col,'asc')->paginate(20)->appends([
							'tosearch'=>$command->tosearch,
							'search'=>$command->input
						]),
					[substr($_SERVER['REQUEST_URI'],0,10),0,explode('?',$segment[sizeof($segment)-1])[1]]
				];
			}elseif($command->tosearch == 'comenzi'){
				return [
						StatusFac::search($command->input)->orderBy($col,'asc')->paginate(20)->appends([
							'tosearch'=>$command->tosearch,
							'search'=>$command->input
						]),
					[substr($_SERVER['REQUEST_URI'],0,10),0,explode('?',$segment[sizeof($segment)-1])[1]]
				];
			}
			
		}else{
			if($command->tosearch == 'produse'){
				return [
						Produse::search($command->input)->with('poze')->orderBy($col,'desc')->paginate(20)->appends([
							'tosearch'=>$command->tosearch,
							'search'=>$command->input
						]),
					[substr($_SERVER['REQUEST_URI'],0,10),1,explode('?',$segment[sizeof($segment)-1])[1]]
				];
			}elseif($command->tosearch == 'membri'){
				return [
						Membrii::search($command->input)->orderBy($col,'asc')->paginate(20)->appends([
							'tosearch'=>$command->tosearch,
							'search'=>$command->input
						]),
					[substr($_SERVER['REQUEST_URI'],0,10),0,explode('?',$segment[sizeof($segment)-1])[1]]
				];
			}elseif($command->tosearch == 'comenzi'){
				return [
						StatusFac::search($command->input)->orderBy($col,'asc')->paginate(20)->appends([
							'tosearch'=>$command->tosearch,
							'search'=>$command->input
						]),
					[substr($_SERVER['REQUEST_URI'],0,10),0,explode('?',$segment[sizeof($segment)-1])[1]]
				];
			}
			
		}
	}
	public function disponibil(SearchFilter $command,$col){
		dd($command);
	}
}
