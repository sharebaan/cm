<?php namespace App\Handlers\Commands;

use App\StatusFac;
use App\Commands\ComenziFilter;

use Illuminate\Queue\InteractsWithQueue;

class ComenziFilterHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle(ComenziFilter $command){
		
		switch ($command->sta) {
			case 'comenzi':
				return $this->filterComanda($command);
				break;

			case 'id':
				$col = 'id';
				return $this->filter($command,$col);
				break;

			case 'updated':
				$col = 'updated_at';
				return $this->filter($command,$col);
				break;
			case 'data':
				$col = 'created_at';
				return $this->filter($command,$col);
				break;

			case 'comid':
				$col = 'comanda_id';
				return $this->filter($command,$col);
				break;	

			case 'memid':
				$col = 'membru_id';
				return $this->filter($command,$col);
				break;
				
			case 'fac':
				$col = 'nr_factura';
				return $this->filter($command,$col);
				break;
				
			case 'total':
				$col = 'total';
				return $this->filter($command,$col);
				break;
				
			case 'statcom':
				$col = 'status_comanda';
				return $this->filter($command,$col);
				break;
				
			case 'starepl':
				$col = 'stare_plata';
				return $this->filter($command,$col);
				break;
				
			case 'staretrans':
				$col = 'stare_transport';
				return $this->filter($command,$col);
				break;
				
			case 'metpl':
				$col = 'metoda_plata';
				return $this->filter($command,$col);
				break;
				
			case 'metliv':
				$col = 'metoda_livrare';
				return $this->filter($command,$col);
				break;									
			default:
				# code...
				break;
		}
	}

	public function filterComanda(ComenziFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);

		return [
			StatusFac::orderBy('id','desc')->with('membru')->paginate(50),
			[substr($_SERVER['REQUEST_URI'],0,11),$segment[sizeof($segment)-3],$segment[sizeof($segment)-2]]	
		];
	}

	public function filter(ComenziFilter $command,$col){
		$segment = explode('/', $_SERVER['REQUEST_URI']);

		if($command->ord == 1){
			return [
						StatusFac::orderBy($col,'desc')->with('membru')->paginate(50),
						[substr($_SERVER['REQUEST_URI'],0,11),0,$segment[sizeof($segment)-2]]
					];
		}else{
			return [
						StatusFac::orderBy($col,'asc')->with('membru')->paginate(50),
						[substr($_SERVER['REQUEST_URI'],0,11),1,$segment[sizeof($segment)-2]]
					];
		}
	}	

}
