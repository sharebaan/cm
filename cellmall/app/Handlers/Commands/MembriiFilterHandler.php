<?php namespace App\Handlers\Commands;
use App\Commands\MembriiFilter;
use App\Membrii;

use Illuminate\Queue\InteractsWithQueue;

class MembriiFilterHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Handle the command.
	 *
	 * @param    $command
	 * @return void
	 */
	public function handle(MembriiFilter $command)
	{
		switch ($command->sta){
			case 'mem':
				return $this->filterMembrii($command);
				break;
			case 'id':
				$col = 'id';
				return $this->filter($command,$col);
				break;
			case 'status':
				$col = 'status';
				return $this->filter($command,$col);
				break;
			case 'tipcont':
				$col = 'tipCont';
				return $this->filter($command,$col);
				break;		
			case 'nume':
				$col = 'nume';
				return $this->filter($command,$col);
				break;
			case 'prenume':
				$col = 'prenume';
				return $this->filter($command,$col);
				break;
			case 'email':
				$col='email';
				return $this->filter($command,$col);
				break;
			case 'telefon':
				$col='telefon';
				return $this->filter($command,$col);
				break;
			case 'cnp':
				$col='CNP';
				return $this->filter($command,$col);	
				break;
			case 'adresafac':
				$col='adresa_fac';
				return $this->filter($command,$col);
				break;
			case 'localfac':
				$col='localitate_fac';
				return $this->filter($command,$col);
				break;
			case 'judetfac':
				$col='judet_sector_fac';
				return $this->filter($command,$col);
				break;	
			case 'adresaliv':
				$col='adresa_liv';
				return $this->filter($command,$col);
				break;
			case 'localliv':
				$col='localitate_liv';
				return $this->filter($command,$col);
				break;
			case 'judetliv':
				$col='judet_sector_liv';
				return $this->filter($command,$col);
				break;	
			case 'recomandari':
				$col='recomandari';
				return $this->filter($command,$col);
				break;
			case 'created':
				$col='created_at';
				return $this->filter($command,$col);
				break;	
			case 'updated':
				$col='updated_at';
				return $this->filter($command,$col);
				break;	
		}
	}

	public function filterMembrii(MembriiFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);		
		
		return [
			Membrii::orderBy('id','desc')->paginate(50),
			[substr($_SERVER['REQUEST_URI'],0,11),$segment[sizeof($segment)-3],$segment[sizeof($segment)-2]]	
		];
	}	

	public function filter(MembriiFilter $command,$col){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
		
		if($command->ord ==1){
			return [
				Membrii::orderBy($col,'desc')->paginate(50),
				[substr($_SERVER['REQUEST_URI'],0,11),0,$segment[sizeof($segment)-2]]
			];
		}else{
			return [
				Membrii::orderBy($col,'asc')->paginate(50),
				[substr($_SERVER['REQUEST_URI'],0,11),1,$segment[sizeof($segment)-2]]
			];
		}
	}

}
