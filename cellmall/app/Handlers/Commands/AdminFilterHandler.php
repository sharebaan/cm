<?php namespace App\Handlers\Commands;

use App\Poze;
use DB;
use App\Commands\AdminFilter;
use App\Produse;
use App\Disponibil;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use App\Subcat4Copil3;
use Illuminate\Queue\InteractsWithQueue;

class AdminFilterHandler {

	/**
	 * Create the command handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Handle the command.
	 *
	 * @param  AdminFilter  $command
	 * @return void
	 */

	public function handle(AdminFilter $command){
		//ADAUGI CASE NOU SI FUNCTIONEAZA

		switch ($command->sta) {
		
			case 'baza':
				return $this->filterBaza($command);
				break;	
			
			case 'subbaza':
				return $this->filterSubbaza($command);
				break;

			case 'subcat2':
				return $this->filterCopil($command);
				break;	
					
			case 'subcat3':
				return $this->filterCopil2($command);
				break;	

			case 'subcat4':
				return $this->filterCopil3($command);
				break;

			case 'id':
				$col = 'produse.id';
				return $this->filter($command,$col);
				break;

			case 'codprod':
				$col = 'produse.cod_produs';
				return $this->filter($command,$col);
				break;	

			case 'numeprod':
				$col = 'produse.denumire_produs';
				return $this->filter($command,$col);
				break;
			case 'pret_original':
				$col = 'produse.pret_original';
				return $this->filter($command,$col);	
				break;
			case 'tva':
				$col = 'produse.tva';
				return $this->filter($command,$col);	
				break;
			case 'pretTva':
				$col = 'produse.pretTva';
				return $this->filter($command,$col);	
				break;
			case 'pret':
				$col = 'produse.pret';
				return $this->filter($command,$col);	
				break;

			case 'stoc':
				$col = 'produse.stoc';
				return $this->filter($command,$col);	
				break;	

			case 'totalstoc':
				$col = 'produse.total_stoc';
				return $this->filter($command,$col);
				break;

			case 'intrari':
				$col = 'produse.intrari';
				return $this->filter($command,$col);
				break;	

			case 'iesiri':
				$col = 'produse.iesiri';
				return $this->filter($command,$col);
				break;
					
			case 'retur':
				$col = 'produse.retur';
				return $this->filter($command,$col);
				break;
					
			case 'vanzari':
				$col = 'produse.vanzari';
				return $this->filter($command,$col);
				break;	

			case 'pozitieraft':
				$col = 'produse.pozitie_raft';
				return $this->filter($command,$col);
				break;	

			case 'promotii':
				$col = 'produse.promotii';
				return $this->filter($command,$col);
				break;	

			case 'noutati':
				$col = 'produse.noutati';
				return $this->filter($command,$col);
				break;	

			case 'alertastoc':
				$col = 'produse.alerta_stoc';
				return $this->filter($command,$col);
				break;	

			case 'culoare':
				$col = 'produse.culoare';
				return $this->filter($command,$col);
				break;

			case 'dimensiune':
				$col = 'produse.dimensiune';
				return $this->filter($command,$col);
				break;

			case 'atentie':
				$col = 'produse.atentie';
				return $this->filter($command,$col);
				break;	

			case 'detalii':
				$col = 'produse.detalii';
				return $this->filter($command,$col);
				break;				

			case 'tipambalaj':
				$col = 'produse.tip_ambalaj';
				return $this->filter($command,$col);
				break;	

			case 'lichidaristoc':
				$col = 'produse.lichidari_stoc';
				return $this->filter($command,$col);
				break;

			case 'brand':
				$col = 'produse.brand_id';
				return $this->filter($command,$col);
				break;	

			case 'producator':
				$col = 'produse.producator_id';
				return $this->filter($command,$col);
				break;	

			case 'disponibil':
				
				return $this->disponibil($command);
				break;	
			case 'poza':
				$col = 'produse.poza';
				return $this->filter($command,$col);
				break;
			case 'discount':
				$col = 'produse.discount';
				return $this->filter($command,$col);
				break;	
			case 'swap':
				$col = 'produse.swap';
				return $this->filter($command,$col);
				break;
			case 'ascuns':
				$col = 'produse.ascuns';
				return $this->filter($command,$col);
				break;
			case 'updated':
				$col = 'produse.updated_at';
				return $this->filter($command,$col);
				break;
			case 'created':
				$col = 'produse.created_at';
				return $this->filter($command,$col);
				break;	
			default:
				# code...
				break;
		}
	}

	public function filterBaza(AdminFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);

		return  [
				Produse::where('cat_baza_id','=',$command->id)->with('poze')	
				->paginate(50),	
					[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
					Disponibil::all()
				];
	}

	public function filterSubbaza(AdminFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
		
		return [
				/*	DB::table('produse')
					->join('piv_sb1','produse.id','=','piv_sb1.prod_id')
					->where('piv_sb1.subcat_baza_1_id','=',$command->id)		
					//->join('poze','produse.id','=','poze.produs_id')
					->paginate(50),*/
				//	SubcatBaza1::with('produseSB1')->where('id','=',$command->id)->paginate(50),		
				Produse::whereHas('catSB1',function($q)use($command){
					$q->where('subcat_baza_1_id','=',$command->id);		
				})->with('poze')->paginate(50),
					[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
					Disponibil::all()
				];
	}

	public function filterCopil(AdminFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
		
		return [
				/*	DB::table('produse')
					->join('piv_s2c1','produse.id','=','piv_s2c1.prod_id')
					->where('piv_s2c1.subcat2_copil_1_id','=',$command->id)	
					->paginate(50),*/
					Produse::whereHas('catS2C1',function($q)use($command){
						$q->where('subcat2_copil_1_id','=',$command->id);	
					})->with('poze')->paginate(50),
					[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
					Disponibil::all()
				];
	}

	public function filterCopil2(AdminFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);

		return [
					/*DB::table('produse')
					->join('piv_s3c2','produse.id','=','piv_s3c2.prod_id')
					->where('piv_s3c2.subcat3_copil_2_id','=',$command->id)
					->paginate(50),*/
					Produse::whereHas('catS3C2',function($q)use($command){
						$q->where('subcat3_copil_2_id','=',$command->id);	
					})->with('poze')->paginate(50),
					[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
					Disponibil::all()
				];
	}

	public function filterCopil3(AdminFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);

		return [
					/*DB::table('produse')
					->join('piv_s4c3','produse.id','=','piv_s4c3.prod_id')
					->where('piv_s4c3.subcat4_copil_3_id','=',$command->id)
					->paginate(50),*/
					Produse::whereHas('catS4C3',function($q)use($command){
						$q->where('subcat4_copil_3_id','=',$command->id);	
					})->with('poze')->paginate(50),
					[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
					Disponibil::all()
				];		
	}

	public function filter(AdminFilter $command,$col){
		$segment = explode('/', $_SERVER['REQUEST_URI']);

		if($command->ord == 1){
			
			switch ($segment[sizeof($segment)-2]) {
	
				case 'baza':
						return  [
								Produse::where('cat_baza_id','=',$command->id)->with('poze')->orderBy($col,'desc')
								->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
							Disponibil::all()	
						];	
					break;
				case 'subcat':
						return [
							/*DB::table('produse')
							->join('piv_sb1','produse.id','=','piv_sb1.prod_id')
							->where('piv_sb1.subcat_baza_1_id','=',$command->id)->orderBy($col,'desc')
							->paginate(50),*/
							Produse::whereHas('catSB1',function($q)use($command,$col){
								$q->where('subcat_baza_1_id','=',$command->id);		
							})->with('poze')->orderBy($col,'desc')->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
							Disponibil::all()
						];
					break;

				case 'copil':
					
						return [
						/*	DB::table('produse')
							->join('piv_s2c1','produse.id','=','piv_s2c1.prod_id')
							->where('piv_s2c1.subcat2_copil_1_id','=',$command->id)->orderBy($col,'desc')	
							->paginate(50),*/
							Produse::whereHas('catS2C1',function($q)use($command){
								$q->where('subcat2_copil_1_id','=',$command->id);		
							})->with('poze')->orderBy($col,'desc')->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],					
							Disponibil::all()
						];
					break;	

				case 'copil2':
						return [
							/*DB::table('produse')
							->join('piv_s3c2','produse.id','=','piv_s3c2.prod_id')
							->where('piv_s3c2.subcat3_copil_2_id','=',$command->id)->orderBy($col,'desc')	
							->paginate(50),*/
							Produse::whereHas('catS3C2',function($q)use($command){
								$q->where('subcat3_copil_2_id','=',$command->id);		
							})->with('poze')->orderBy($col,'desc')->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
							Disponibil::all()
						];
					break;	

				case 'copil3':
						return [
							/*DB::table('produse')
							->join('piv_s4c3','produse.id','=','piv_s4c3.prod_id')
							->where('piv_s4c3.subcat4_copil_3_id','=',$command->id)->orderBy($col,'desc')
							->paginate(50),*/
							Produse::whereHas('catS4C3',function($q)use($command){
								$q->where('subcat4_copil_3_id','=',$command->id);		
							})->with('poze')->orderBy($col,'desc')->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],0,$segment[sizeof($segment)-2]],
							Disponibil::all()
						];
					break;

				default:
					# code...
					break;
			}
		}
		else{
				
			switch ($segment[sizeof($segment)-2]) {
				case 'baza':
						return  [
								Produse::where('cat_baza_id','=',$command->id)->orderBy($col,'asc')
								->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],1,$segment[sizeof($segment)-2]],
							Disponibil::all()
						];	
					break;

				case 'subcat':
						return [
							/*DB::table('produse')
							->join('piv_sb1','produse.id','=','piv_sb1.prod_id')
							->where('piv_sb1.subcat_baza_1_id','=',$command->id)->orderBy($col,'asc')
							->paginate(50),*/
							Produse::whereHas('catSB1',function($q)use($command){
								$q->where('subcat_baza_1_id','=',$command->id);		
							})->with('poze')->orderBy($col,'asc')->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],1,$segment[sizeof($segment)-2]],
							Disponibil::all()
						];
					break;	
				
				case 'copil':
						return [
							/*DB::table('produse')
							->join('piv_s2c1','produse.id','=','piv_s2c1.prod_id')
							->where('piv_s2c1.subcat2_copil_1_id','=',$command->id)->orderBy($col,'asc')
							->paginate(50),*/
							Produse::whereHas('catS2C1',function($q)use($command){
								$q->where('subcat2_copil_1_id','=',$command->id);		
							})->with('poze')->orderBy($col,'asc')->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],1,$segment[sizeof($segment)-2]],					
							Disponibil::all()
						];
					break;

				case 'copil2':
						return [
							/*DB::table('produse')
							->join('piv_s3c2','produse.id','=','piv_s3c2.prod_id')
							->where('piv_s3c2.subcat3_copil_2_id','=',$command->id)->orderBy($col,'asc')
							->paginate(50),*/
							Produse::whereHas('catS3C2',function($q)use($command){
								$q->where('subcat3_copil_2_id','=',$command->id);		
							})->with('poze')->orderBy($col,'asc')->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],1,$segment[sizeof($segment)-2]],
							Disponibil::all()
						];
					break;	

				case 'copil3':
						return [
							/*DB::table('produse')
							->join('piv_s4c3','produse.id','=','piv_s4c3.prod_id')
							->where('piv_s4c3.subcat4_copil_3_id','=',$command->id)->orderBy($col,'desc')
							->paginate(50),*/
							Produse::whereHas('catS4C3',function($q)use($command){
								$q->where('subcat4_copil_3_id','=',$command->id);		
							})->with('poze')->orderBy($col,'asc')->paginate(50),
							[substr($_SERVER['REQUEST_URI'],0,11).$segment[sizeof($segment)-5],1,$segment[sizeof($segment)-2]],
							Disponibil::all()
						];
					break;

				default:
					# code...
					break;
			}	
		}
		
	}

	public function disponibil(AdminFilter $command){
		$segment = explode('/', $_SERVER['REQUEST_URI']);
	dd($command);	
		if($command->ord == 1){
			return[
				
			];
		}else{
			return[

			];	
		}
	}

}
