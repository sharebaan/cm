<?php namespace App;



use Nicolaslopezj\Searchable\SearchableTrait;

use Illuminate\Database\Eloquent\Model;



class Produse extends Model{

	

	use SearchableTrait;



	protected $searchable = [

		'columns' => [

		//	'id' => 5,

			'denumire_produs' => 50,
	
			'cod_produs' => 20


		//	'detalii'=>5

		]

	];

	protected $table = 'produse';

	protected $guarded = ['id'];

	protected $fillable = [

							'cod_produs',

							'denumire_produs',

							'pret',
							'tva',
							'tvaVal',

							'pretTva',

							'nelistat',

							'pret_original',

							'poza',

							'discount',

							'stoc',

							'culoare',

							'tip_ambalaj',

							'dimensiune',
							'atentie',
							'detalii',
							'total_stoc',
							'intrari',
							'iesiri',
							'retur',
							'noutati',
							'promotii',
							'alerta_stoc',
							'vizitari',
							'cat_baza_id',
							'brand_id',
							"ascuns",
							'lichidari_stoc',
							'producator_id',
							'pozitie_raft',
							'swap'
							];



	public function disp(){
		return $this->hasOne('App\Disponibil','produs_id');
	}						

	

	public function membru(){
		return $this->belongsTo('App\Membrii');
	}

	public function producator(){

		return $this->hasOne('App\Producator','id');

	}

	public function carucior(){

		return $this->hasOne('App\Carucior','produs_id');

	}

	public function poze(){
		return $this->hasMany('App\Poze','produs_id');
	}

	public function branduri(){

		return $this->hasOne('App\Branduri','id');

	}						

	public function catSB1(){

		return $this->hasMany('App\PivSB1',

			'prod_id');

	}						
//de tinut minte	
	/*public function SB1(){
		return $this->belongsToMany('App\SubcatBaza1','piv_sb1','subcat_baza_1_id','prod_id');
	}*/
	public function dpc(){
		return $this->hasMany('App\DiscPerClient','prod_id');
	}

	public function catS2C1(){
		return $this->hasMany('App\PivS2C1',

			'prod_id');
	}

	public function catS3C2(){
		return $this->hasMany('App\PivS3C2',

			'prod_id');
	}

	public function catS4C3(){
		return $this->hasMany('App\PivS4C3',

			'prod_id');
	}

	
}
