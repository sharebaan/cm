<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PivSB1 extends Model{

	protected $table = 'piv_sb1';
	protected $fillable = ['id','subcat_baza_1_id','prod_id'];

	public function produs(){
		return $this->belongsTo('App\Produse',
			'prod_id');
	}
}
