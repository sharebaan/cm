<?php namespace App;



use Illuminate\Database\Eloquent\Model;



class Poze extends Model{

	

	protected $table = 'poze';

	protected $fillable = ['thumbnail','poza','produs_id'];

	public function produs(){
		return $this->belongsTo('App\Produse','id');
	}
}
