<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SubcatBaza1 extends Model{
	
	protected $table = 'subcat_baza_1';
	protected $fillable = ['nume'];

	public function produseSB1(){
		return $this->belongsToMany('App\Produse','piv_sb1',
			'subcat_baza_1_id','prod_id');
	}

	public function CatBaza(){
		return $this->belongsTo('App\CatBaza');
	}

	public function Subcat2Copil1(){
		return $this->hasMany('App\Subcat2Copil1','subcat_baza_1_id');
	}
}