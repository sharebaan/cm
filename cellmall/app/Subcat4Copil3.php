<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcat4Copil3 extends Model{
	
	protected $table = 'subcat4_copil_3';
	protected $fillable = ['nume'];

	public function produseS4C3(){
		return $this->belongsToMany('App\Produse',
			'piv_s4c3','subcat4_copil_3_id','prod_id');
	}

	public function Subcat2Copil1(){
		return $this->belongsTo('App\Subcat3Copil2');
	}

}