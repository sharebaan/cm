<?php namespace App;



use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;



class StatusFac extends Model{


	use SearchableTrait;

	protected $searchable=[
		'columns'=>[
			'comanda_id'=>50,
			'nr_factura'=>50,
			'comenzi.created_at'=>20,
			'membru_id'=>70,
			'status_comanda'=>30,
			'stare_plata'=>30,
			'stare_transport'=>30,
			'metoda_plata'=>30,
			'metoda_livrare'=>30
		],
		'joins'=>[
			'membrii'=>['comenzi.membru_id','membrii.id'],	
		]
	];

	protected $table = 'comenzi';

	protected $fillable = [

		

		'comanda_id',

		'membru_id',

		'total',

		'nr_factura',

		'status_comanda',

		'stare_plata',

		'stare_transport',

		'metoda_plata',

		'metoda_livrare',

		'total'

	];

	public function membru(){
		return $this->belongsTo('App\Membrii');
	}

	
}	
