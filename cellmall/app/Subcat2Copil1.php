<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcat2Copil1 extends Model{
	
	protected $table = 'subcat2_copil_1';
	protected $fillable = ['nume'];

	public function produseS2C1(){
		return $this->belongsToMany('App\Produse','piv_s2c1',
			'subcat2_copil_1_id','prod_id');
	}

	public function SubcatBaza1(){
		return $this->belongsTo('App\SubcatBaza1');
	}

	public function Subcat3Copil2(){
		return $this->hasMany('App\Subcat3Copil2','subcat2_copil_1_id');
	}
}