<?php namespace App;



use Illuminate\Database\Eloquent\Model;



class AlertaStoc extends Model{	



	protected $table = 'alerta_stoc';



	protected $fillable = ['produs_id','disponibilitate_id','stoc_ramas'];

	public function produs(){
		return $this->belongsTo('App\Produse','produs_id');
	}
	
	public function disponibilitate(){
		return $this->belongsTo('App\Disponibil','disponibilitate_id');
	}
}
