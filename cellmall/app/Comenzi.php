<?php namespace App;



use Illuminate\Database\Eloquent\Model;



class Comenzi extends Model{

	protected $table = 'produse_din_comenzi';

	
	protected $fillable = [
		
		'comandafac_key',

		'membru_id',

		'produs_id',

		'nume_produs',

		'cantitate',

		'total',
		'transport',
		'mod_cumparare',

		'metoda_plata',

		'metoda_livrare',

		'ment_livrare',

		'contbanca',
		'tip_document',
		'ment_livrare',
		'KAM_livrare'

	];



	public function membru(){

		return $this->belongsTo('App\Membrii','membru_id');

	}



	public function produs(){

		return $this->belongsTo('App\Produse','produs_id');

	}

	



}	
