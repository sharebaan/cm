<?php namespace App;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;

class Membrii extends Model{

	use SearchableTrait;

	protected $searchable =[
			'columns'=>[
				'nume'=>20,
				'prenume'=>20,
				'email'=>20,
				'telefon'=>20,
				'nume_companie'=>20,
				'adresa_sediu'=>20,
				'adresa_fac'=>20,
				'localitate_fac'=>20,
				'judet_sector_fac'=>20,
				'adresa_liv'=>20,
				'localitate_liv'=>20,
				'judet_sector_liv'=>20,
				'adresa_sediu'=>20,
				'localitate_sediu'=>20,
				'judet_sector_sediu'=>20,
				'status'=>20,
				'tipCont'=>20
			],
			'joins'=>[
				'comenzi'=>['comenzi.membru_id','membrii.id'],			
			]

		];	
	protected $table = 'membrii';
	protected $fillable = [
							'status',
							'tipCont',
							'demo',
							'nume',
							'prenume',
							'CNP',
							'email',
							'password',
							'telefon',
							'adresa_fac',
							'localitate_fac',
							'judet_sector_fac',
							'adresa_liv',
							'localitate_liv',
							'judet_sector_liv',
							'nume_companie',
							'CUI',
							'nr_reg',
							'adresa_sediu',
							'localitate_sediu',
							'judet_sector_sediu',
							'banca',
							'cont_IBAN',
							'mentiuni',
							'recomandari',	
							'temp_pass_token',
							'angajat_produse',
							'KAM_com',
							'KAM_liv'
							];

	public function carucior(){
		return $this->belongsTo('App\Carucior','membru_id');
	}						

	public function comenzi(){
		return $this->hasMany('App\StatusFac','membru_id');
	}

	
}
