<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Mesaje extends Model{
    protected $table = 'mesaje';
    protected $fillable = ['membru_id','subiect','text'];
} 