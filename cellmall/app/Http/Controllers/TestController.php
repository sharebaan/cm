<?php namespace App\Http\Controllers;

use App\Produse;
use App\PivSB1;
use App\PivS2C1;
use App\PivS3C2;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Filesystem\Filesystem as File;
use Illuminate\Http\Request;

class TestController extends Controller {	

	public function da($prod){
				if($prod == 'Apple'){
					return 1;
				}
				else if($prod == 'Samsung'){
					return 2;
				}
				else if($prod == 'Sony'){
					return 3;
				}
				else if($prod == 'LG'){
					return 4;
				}
				else if($prod == 'HTC'){
					return 5;
				}
				else if($prod == 'Nokia'){
					return 6;
				}
				else if($prod == 'BlackBerry' || $prod == 'Blackberry'){
					return 7;
				}
				else if($prod == 'Motorola'){
					return 8;
				}
				else if($prod == 'Huawei'){
					return 9;
				}
				else if($prod == 'ZTE'){
					return 10;
				}
				else if($prod == 'Sony Ericsson'){
					return 11;
				}
				else if($prod == 'Alcatel'){
					return 13;
				}
				else if($prod == 'Lenovo'){
					return 15;
				}
				else if($prod == 'Acer'){
					return 17;
				}
				else if($prod == 'Asus'){
					return 18;
				}
				else if($prod == 'Vodafone'){
					return 21;
				}
				else if($prod == 'Orange'){
					return 22;
				}
				else if($prod == 'Allview'){
					return 23;
				}
				else if($prod == 'Cosmote'){
					return 24;
				}
				else if($prod == 'Xiaomi'){
					return 25;
				}
				else{
					return null;
				}
			}

	public function test(){
		/*
		$f = new File();	
			
		foreach ($f->allFiles(public_path().'/products/') as $file){
			$filename = $file->getRelativePathName();
			Produse::where('cod_produs','=',substr($filename,0,9))->update(['poza'=>$filename]);			
		}
		*/
		//echo count(Produse::where('poza','!=','')->get());
	
		
/*		foreach (File::allFiles(public_path().'/products/') as $file){
		$filename = $file->getRelativePathName();

		echo $filename . '<br>';
		} 
*/		 
/*		foreach($this->products as $p){


			$prod = new Produse();
			$prod->denumire_produs = $p['title'];
			$prod->cod_produs = $p['cod'];
			$prod->stoc = $p['stoc'];
			$prod->pret = $p['price'];
			$prod->brand_id = $this->da($p['producator']);
			$prod->descriere = $p['text'];
			$prod->cat_baza_id = 1;
			$prod->total_stoc = $prod->pret * $prod->stoc;

			$prod->save();

			PivSB1::create([
				'subcat_baza_1_id' => 1,
				'prod_id' => $prod->id
				]);

			if($p['cat_id'] == 110){
				PivS2C1::create([
					'subcat2_copil_1_id' => 1,
					'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 4,
					'prod_id' => $prod->id 
					]);
			}
			else if($p['cat_id'] == 111){
				PivS2C1::create([
					'subcat2_copil_1_id' => 1,
					'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 6,
					'prod_id' => $prod->id 
					]);
			}
			else if($p['cat_id'] == 115){
				PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $prod->id
				]);	

				PivS3C2::create([
					'subcat3_copil_2_id' => 7,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 116 || $p['cat_id'] == 194){
				PivS3C2::create([
					'subcat3_copil_2_id' => 11,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 125 || $p['cat_id'] == 127){
				PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 02,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 128){
				PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 1,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 132){
				PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 5,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 136){
				PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 3,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 138){
				PivS2C1::create([
				'subcat2_copil_1_id' => 3,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 16,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 139){
				PivS2C1::create([
				'subcat2_copil_1_id' => 3,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 17,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 140){
				PivS2C1::create([
				'subcat2_copil_1_id' => 3,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 18,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 151){
				PivS2C1::create([
				'subcat2_copil_1_id' => 5,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 24,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 193){
				PivS2C1::create([
				'subcat2_copil_1_id' => 1,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 28,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 59){
				PivS2C1::create([
				'subcat2_copil_1_id' => 2,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 13,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 60){
				PivS2C1::create([
				'subcat2_copil_1_id' => 2,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 15,
					'prod_id' => $prod->id 
					]); 
			}
			else if($p['cat_id'] == 62){
				PivS2C1::create([
				'subcat2_copil_1_id' => 2,
				'prod_id' => $prod->id
				]);

				PivS3C2::create([
					'subcat3_copil_2_id' => 12,
					'prod_id' => $prod->id 
					]); 
			}
		}
*/


	}	
	public $products = array(

	);
}
