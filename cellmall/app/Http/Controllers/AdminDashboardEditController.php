<?php namespace App\Http\Controllers;


use App\Poze;
use App\PivSB1;
use App\PivS2C1;
use App\PivS3C2;
use App\PivS4C3;
use App\StatusFac;
use App\StatusStoc;
use App\Membrii as Membru;
use App\Produse as Produs;
use App\Disponibil as Disp;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Input;
use Illuminate\Support\Facades\Storage as Storage;
use Illuminate\Support\Facades\File as File;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\Mail as Mail;

class AdminDashboardEditController extends Controller {
	function __construct(Request $request){
		$this->request = $request;	
	}
	/**
	 * Update the specified resource in storage.
	 *	
	 * @param  int  $id
	 * @return Response
	 */
	public function updateProd(){			
		
		if(count(Input::file('poze')) <= 1){
			if(Input::file('poze')[0] != null){	
			//stabilit nume folder
				$foldername = explode(' ',Input::file('poze')[0]->getClientOriginalName())[0];

			//stabilit path pozeproduse
				$path = substr(storage_path(),0,27).'pozeproduse/';
			
				if(File::exists($path.$foldername) == false){
				
					//stabilit creare folder codprod
					File::makeDirectory($path.$foldername,777);

						Input::file('poze')[0]->move($path.$foldername.'/',Input::file('poze')[0]->getClientOriginalName());
						$poze = new Poze();
						$poze->poza = substr($path.$foldername.'/'.Input::file('poze')[0]->getClientOriginalName(),26,64);
						$poze->produs_id = $this->request->input('idp');
						$poze->save();
								
			
				}else{	
					//dd(File::get(Input::file('poze')[0]));
						//foreach(scandir($path.$foldername) as $d){
						//	if($d != Input::file('poze')[0]){
								//dd('neterminata/secundara');
									$obj= new \stdClass();
									$obj->poza = Input::file('poze')[0];
									dd(chown($path.$foldername,48));
									dd('up ok');
									File::put($path.$foldername.'/'.Input::file('poze')[0]->getClientOriginalName(),Input::file('poze')[0],false);
									//dd('break');
									die();
									$poze = new Poze();
									$poze->poza = substr($path.$foldername.'/'.Input::file('poze')[0]->getClientOriginalName(),26,64);
									$poze->produs_id = $this->request->input('idp');
									$poze->save();
								
						//	}	
						//}
				}
			}
			
		}
		elseif(count(Input::file('poze')) > 1){
				//	dd('multiple');
			if(Input::file('poze')[0] != null){	
			//stabilit nume folder
				$foldername = explode(' ',Input::file('poze')[0]->getClientOriginalName())[0];

				//stabilit path pozeproduse
				$path = substr(storage_path(),0,27).'pozeproduse/';
			
					if(File::exists($path.$foldername) == false){
				
					//stabilit creare folder codprod
						File::makeDirectory($path.$foldername,0777);

				
						foreach(Input::file('poze') as $p){
							$p->move($path.$foldername.'/',$p->getClientOriginalName());
							$poze = new Poze();
							$poze->poza = substr($path.$foldername.'/'.$p->getClientOriginalName(),26,64);
							$poze->produs_id = $this->request->input('idp');
							$poze->save();
						}
				
					}else{	
						foreach(scandir($path.$foldername) as $d){
							if($d != Input::file('poze')){
			//					dd('neterminata/secundara');
						
									foreach(Input::file('poze') as $p){
										$p->move($path.$foldername,$p->getClientOriginalName());
										//dd('break');
										$poze = new Poze();
										$poze->poza = substr($path.$foldername.'/'.$p->getClientOriginalName(),26,64);
										$poze->produs_id = $this->request->input('idp');
										$poze->save();
									}
							
								
								
							}	
						}
					}
			}

		}	
		


		//stabilit relatia poze tabel
		//$a = Produs::where('id','=',$id)->with('poze')->get();
		//dd($a);

		
		//================== VECHE/OUTDATED =============================
		/*$poza;
		if($this->request->poza == '' || $this->request->poza == '/products/'){
			$poza = '';
		}else{
			if(substr($this->request->poza,0,10) == '/products/'){
				$poza = $this->request->poza; 
			}else{
				$poza = '/products/'.$this->request->poza;	
			}
			
		}
		 ===============================================================*/

		$atentie;
		if($this->request->atentie == 'non'){
			$atentie = '';
		}else{
			$atentie = $this->request->atentie;
		}

		$tamb;
		if($this->request->tip_ambalaj == 'non'){
			$tamb = '';
		}else{
			$tamb = $this->request->tip_ambalaj;
		}		
		

		if($this->request->retur == null){$this->request->retur = 0;}
		if($this->request->iesiri == null){$this->request->iesiri = 0;}
		if($this->request->intrari == null){$this->request->intrari = 0;}


		$prod = Produs::find($this->request->input('idp'));
		
		$stoc = $prod->stoc+$this->request->retur-$this->request->iesiri+$this->request->intrari;

		if($this->request->stoc != $prod->stoc){
			$stoc = $this->request->stoc+$this->request->retur-$this->request->iesiri+$this->request->intrari;
		}

		$alerta_stoc = $prod->alerta_stoc;
		if($this->request->alertastoc != $prod->alerta_stoc){
			$alerta_stoc = $this->request->alertastoc;
		}


		if($this->request->disp != 'non'){
		
			if($this->request->disp == 'stoc'){
			Disp::find($this->request->dispid)->update([
					'in_stoc'=>1,
					'stoc_redus'=>0,
					'produs_indisponibil'=>0,
					'in_curand'=>0
				]);
			}
			elseif($this->request->disp == 'redus'){
				Disp::find($this->request->dispid)->update([
						'in_stoc'=>0,
						'stoc_redus'=>1,
						'produs_indisponibil'=>0,
						'in_curand'=>0
					]);
			}
			elseif($this->request->disp == 'indis'){
				Disp::find($this->request->dispid)->update([
						'in_stoc'=>0,
						'stoc_redus'=>0,
						'produs_indisponibil'=>1,
						'in_curand'=>0
					]);
			}
			elseif($this->request->disp == 'curand'){
				Disp::find($this->request->dispid)->update([
						'in_stoc'=>0,
						'stoc_redus'=>0,
						'produs_indisponibil'=>0,
						'in_curand'=>1
					]);
			}	
		}
		elseif($stoc == 0){
			Disp::find($this->request->dispid)->update([
						'in_stoc'=>0,
						'stoc_redus'=>0,
						'produs_indisponibil'=>1,
						'in_curand'=>0
					]);
		}
		elseif($stoc <= $alerta_stoc){
			Disp::find($this->request->dispid)->update([
						'in_stoc'=>0,
						'stoc_redus'=>1,
						'produs_indisponibil'=>0,
						'in_curand'=>0
					]);
		}
		elseif($stoc > $alerta_stoc){
			Disp::find($this->request->dispid)->update([
						'in_stoc'=>1,
						'stoc_redus'=>0,
						'produs_indisponibil'=>0,
						'in_curand'=>0
					]);
		}

		if($this->request->vanzari == null){$this->request->vanzari = 0;}

		if(!$this->request->intrari == null){
			$stoc = $prod->stoc + $this->request->intrari - $this->request->iesiri - $this->request->retur - $this->request->vanzari;
		
			$sStoc = new StatusStoc();
			$sStoc->prod_id = $this->request->input('idp');
			$sStoc->stoc_salvat = $stoc;
			$sStoc->intrari = $this->request->intrari;
			$sStoc->iesiri = $this->request->iesiri;
			$sStoc->retur =  $this->request->retur;
			$sStoc->vanzari = $this->request->vanzari;
			$sStoc->save();		
		}
		$produs = Produs::find($this->request->input('idp'));

		$pretBuc  = $this->request->pret;
		$pretTva = $this->request->pretTva; 
		$pretO = $this->request->pret_original;

		if($produs->pret_original != $this->request->pret_original){
			$pretTva = ($this->request->pret_original * $produs->tva / 100) +$this->request->pret_original;
			$pretBuc = $this->request->pret;
			$pretO = $this->request->pret_original;

			if($this->request->discount != 0){
				$pretBuc = $pretO;

				$discount = $this->request->discount / 100 * $pretBuc;	
				$pretBucpretBuc = $pretBuc - $discount;
			}
			elseif($this->request->discount == 0){
				$pretBuc = $pretTva;	
			}
		}elseif($this->request->pretTva != $produs->pretTva){	
			
			/*$reverseP = (100 - $produs->tva) / 100;	

			$pretTva = $this->request->pretTva;
				
			$pretO = round($this->request->pretTva * $reverseP,2);*/
			$pretO = $this->request->pretTva / 1.24;	
			$pretTva = $this->request->pretTva;
			//dd(round($pretO,2));

			if($this->request->discount != 0){
				$pretBuc = $pretO;

				$discount = $this->request->discount / 100 * $pretBuc;	
				$pretBucpretBuc = $pretBuc - $discount;
			}else{
				$pretBuc = $pretTva;
			}

		}elseif($produs->pret != $this->request->pret){
			$pretBuc = $this->request->pret;
			if($this->request->discount != 0 ){
			//	$revDisP = (100 - $this->request->discount) / 100;
			//	$pretTva = round($this->request->pret * $revDisP,2);
				$pretO = $this->request->pretTva / 1.24;	
				$pretTva = $this->request->pretTva;
			}else{
				$pretTva = $pretBuc;
			}
			$pretO = $this->request->pret / 1.24;	
			$pretTva = $this->request->pret;			
			/*$reverseP = (100 - $produs->tva) / 100;	

			$pretO = round($pretTva * $reverseP,2);*/	
			
		}

		if($produs->discount != $this->request->discount){
			$pretBuc  = $produs->pret;
			$pretO = $produs->pret_original;
			$pretTva = $produs->pretTva;
				
			if($this->request->discount > $produs->discount){
				$pretBuc = $pretTva - ($pretTva * $this->request->discount / 100);
			}elseif($this->request->discount < $produs->discount){

//					dd('nueste');
				$pretBuc = $pretTva - ($produs->pret * $this->request->discount / 100);
			}

		}

		 

		$totalStoc = $pretBuc * $stoc; 

		$prid = (int)$this->request->prod;
		$brid = (int)$this->request->brand;

		Produs::find($this->request->input('idp'))->update([
				'cod_produs' => $this->request->codprod,
				'denumire_produs' => $this->request->denprod,
				'pretTva'=>$pretTva,
				'pret' => $pretBuc,
				'pret_original'=>$pretO,
				'tvaVal'=>round($pretO*24/100,2),
				'brand_id' => $brid,
				'producator_id' => $prid,
				'stoc' => $stoc,
				'total_stoc' => $totalStoc,
				'alerta_stoc' => $alerta_stoc,
				'discount' => $this->request->discount,
				'swap' => $this->request->swap,
				'promotii' => $this->request->promotii,
				'noutati' => $this->request->noutati,
				'lichidari_stoc' => $this->request->lichidstoc,
				'culoare'=> $this->request->culoare,
				'dimensiune'=>$this->request->dimensiune,
				'tip_ambalaj'=>$tamb,
				'atentie'=>$atentie,
				'detalii'=>$this->request->detalii,
				'pozitie_raft' => $this->request->pozraft
			]);

		return redirect()->back();
	}

	public function updateMem($id){

		if(!$this->request->status == 0){
				Mail::send('emails.aprobat', ['subiect' => 'Ai fost aprobat.',
						'nume'=>$this->request->nume,
						'prenume'=>$this->request->prenume,
						'email'=>$this->request->email], function($message){	
				//$message->from('noreply@cellmall.ro','CellMall');
				$message->to($this->request->email)->subject('Contul tau a fost aprobat!');   
			});
		}

		Membru::find($id)->update([
				'status' => $this->request->status,
				'angajat_produse'=>$this->request->priv_prod,
				'tipCont' => $this->request->tipcont,
				'nume' => $this->request->nume,
				'prenume' => $this->request->prenume,			
				'demo'=>$this->request->demo,
				'CNP' => $this->request->cnp,
				'email' => $this->request->email,
				'telefon' => $this->request->telefon,
				'nume_companie' => $this->request->numecomp,
				'CUI' => $this->request->cui,
				'nr_reg' => $this->request->nrreg,
				'adresa_sediu' => $this->request->AdresaSediu,
				'localitate_sediu' => $this->request->LocalitateSediu,
				'judet_sector_sediu' => $this->request->JudetSectorSediu,
				'cont_IBAN' => $this->request->ContIBAN,
				'banca' => $this->request->Banca,
				'adresa_liv' => $this->request->Adresalivrare,
				'localitate_liv' => $this->request->Localitatelivrare,
				'judet_sector_liv' => $this->request->JudetSectorlivrare,
				'adresa_fac' => $this->request->Adresafactura,
				'localitate_fac' => $this->request->Localitatefactura,
				'judet_sector_fac' => $this->request->JudetSectorfactura,
				'KAM_com' => $this->request->KAMcomercial,
				'KAM_liv' => $this->request->KAMlivrare,
				'detalii_plata' => $this->request->Detaliiplata,
				'detalii_livrare'=> $this->request->Detaliilivrare,
				'detalii_transport' => $this->request->Detaliitransport,
				'mentiuni' => $this->request->mentiuni,
				'recomandari' => $this->request->recomand
			]);
		return redirect()->back();
	} 


	public function editComanda($fac){
		$com = StatusFac::where('comanda_id','=',$fac)->with('membru')->get();
		//dd($this->request->all());
		if($this->request->statcom == 'In curs de procesare'){
		//	dd($com[0]['membru']->nume);
			Mail::send('emails.confcomanda',['subiect'=>'Confirmare Comanda',
					'com'=>$com],function($message)use($com){	
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Confirmare Comanda');   
				$message->to($com[0]['membru']->email,$com[0]['membru']->nume.$com[0]['membru']->prenume)->subject('Confirmare Comanda');   
			});
		}
		$com[0]->status_comanda = $this->request->statcom;
		$com[0]->stare_plata = $this->request->stareplata;
		$com[0]->stare_transport = $this->request->sttrans;
		$com[0]->save();
		return redirect()->back();
	}

	public function getThumbs($id){
		if($this->request->ajax()){
			$poze = Poze::where('produs_id','=',$id)->get();
			return response()->json([
					'poze'=>$poze,
					'id'=>$id
				]);
		}		
	}

	public function setThumb($id,$name){
		if($this->request->ajax()){
			$poze = Poze::where('produs_id','=',$id)->get();		
			foreach($poze as $p){
				if(explode('/',$p->poza)[3] == $name){
					Produs::find($id)->update([
						'poza'=>$p->poza
					]);	
				}		
			}
		}
	}
}















