<?php namespace App\Http\Controllers;

use App\Membrii as Membrii;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as Validator;
use Illuminate\Support\Facades\Hash as Hash;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\Mail as Mail;

class WelcomeController extends Controller {


	public function __construct(Request $request)
	{
		$this->middleware('guest');
		$this->request = $request;
	}

	
	public function index(){
		return view('welcome');
	}

	public function register(){
		return view('register');
	}

	public function auth(){
		
		$rules = [
				'mail' => 'required',
				'parola' => 'required|min:6'
		];
		
		$validation = Validator::make($this->request->all(),$rules);
		
		if($validation->fails()){
			return redirect()->back()->withErrors($validation->errors());
		}
		else if(!$validation->fails()){
			$membru = Membrii::where('email','=',$this->request->input('mail'))->first();
			
			if($membru == null){
				return redirect()->back()->with('staterr','Contul acesta nu exista ,acceseaza formularul de inregistrare pentru a crea un cont nou.');
			}
			else if($membru['status'] == 1){
				if(Auth::attempt(array('email' => $this->request->input('mail'), 'password' => $this->request->input('parola')))){
					return redirect('/home');
				}else{
					return redirect()->back()->with('incorect','Combinatie gresita de E-mail/Parola.');
				}
						
			}
			else{
				return redirect()->back()->with('staterr','Momentan, contul tau nu este confirmat.');		
			}		
		}	
		else{
			return redirect()->back()->with('err','Cobinatie gresita de email/parola.');;	
		}
		
	}

	public function regPJ(){

		$rules = [
					'Nume_Companie'=>'required',
					'CUI'=>'required',
					'Nr_ord_reg_com/an'=>'required',
					'Adresa_Sediu_Social'=>'required',
					'Localitate_Sediu_Social'=>'required',
					'Judet/Sector_Sediu_Social'=>'required',
					'Banca'=>'required',
					'Cont_IBAN'=>'required',
					'Nume'=>'required',
					'Prenume'=>'required',
					'CNP'=>'required|min:13|max:13',
					'E-mail'=>'unique:membrii,email|required|email',
					'Parola'=>'required|min:6',
					'Verificare_Parola'=>'required|min:6|same:Parola',
					'Telefon'=>'required',
					'Adresa_Facturare'=>'required',
					'Localitate_Facturare'=>'required',
					'Judet/Sector_Facturare'=>'required',
					'Adresa_Livrare'=>'required',
					'Localitate_Livrare'=>'required',
					'Judet/Sector_Livrare'=>'required',
					'Recomandari'=>'required|max:100'
				];
		$validation = Validator::make($this->request->all(),$rules);
		
		if($validation->fails()){
			
			return redirect('/inregistrare')->withErrors($validation->errors())->withInput($this->request->all());
		}
		else{	
						
			Membrii::create([
					'nume' => $this->request->input('Nume'),
					'prenume' => $this->request->input('Prenume'),
					'CNP' => $this->request->input('CNP'),
					'email' => $this->request->input('E-mail'),
					'password' => Hash::make($this->request->input('Parola')),
					'telefon' => $this->request->input('Telefon'),
					'adresa_fac' => $this->request->input('Adresa_Facturare'),
					'localitate_fac' => $this->request->input('Localitate_Facturare'),
					'judet_sector_fac' => $this->request->input('Judet/Sector_Facturare'),
					'adresa_liv' => $this->request->input('Adresa_Livrare'),
					'localitate_liv' => $this->request->input('Localitate_Livrare'),
					'judet_sector_liv' => $this->request->input('Judet/Sector_Livrare'),
					'nume_companie' => $this->request->input('Nume_Companie'),
					'CUI' => $this->request->input('CUI'),
					'nr_reg' => $this->request->input('Nr_ord_reg_com/an'),
					'adresa_sediu' => $this->request->input('Adresa_Sediu_Social'),
					'localitate_sediu' => $this->request->input('Localitate_Sediu_Social'),
					'judet_sector_sediu' => $this->request->input('Judet/Sector_Sediu_Social'),
					'banca' => $this->request->input('Banca'),
					'cont_IBAN' => $this->request->input('Cont_IBAN'),
					'recomandari' => $this->request->input('Recomandari'),
					'mentiuni' => $this->request->input('mentiuni'),
					'tipCont' => 'PJ',
					'status' => 0
				]);
		
			Mail::send('emails.register', ['subiect' => 'Utilizator nou.'], function($message){	
				$message->to('comercial@cellmall.ro',Auth::user()['email'])->subject('Utilizator nou');   
			});
			Mail::send('emails.contact', ['subiect' => $this->request->input('subiect'),'dep'=>$this->request->input('dep'),'mesaj'=>$this->request->input('mesaj')], function($message)
			{	
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Utilizator nou');   
			});

			return redirect('/')->with('success','V-ati inregistrat ! Va rugam sa asteptati confirmarea.');
		}		
	}

	public function recPass(){
		return view('recpass');
	}

	public function recPassMail(Request $req){

		$rules = [
				'mail' => 'required'
		];
		
		$validation = Validator::make($this->request->all(),$rules);
		
		if($validation->fails()){
			return redirect()->back()->withErrors($validation->errors());
		}
		else if(!$validation->fails()){
			$membru = Membrii::where('email','=',$req->input('mail'))->get();
			if($membru->isEmpty() == true){
				return redirect()->back()->with('err','Utilizatorul cu acest e-mail nu exista, verifica daca ai introdus e-mail-ul corect');	
			}else{
				$mail = $req->input('mail');
				$serial_number = str_random(64); 
				Membrii::where('email','=',$req->input('mail'))->update([
					'temp_pass_token'=>$serial_number		
				]);
				Mail::send('emails.recparola', ['subiect' => 'Resetare Parola.',
						'membru'=>$membru,'link'=>$serial_number,'status'=>0], function($message) use($mail){	
					$message->from('cellmallmax@gmail.com','CellMall');
					$message->to($mail,Auth::user()['email'])->subject('Resetare Parola');   
				});
				Mail::send('emails.recparola', ['subiect' => 'Resetare Parola.',
						'membru'=>$membru,'link'=>$serial_number,'status'=>1], function($message)
				{	
					$message->from('cellmallmax@gmail.com','CellMall');
					$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Resetare Parola');   
				});

				return redirect()->back()->with('success','
						Cererea ta a fost trimisa cu succes. Vei primi un mail cu instructiunile de resetare a parolei.
						Nota: Mail-ul poate ajunge in sectiunea spam.');	
			}	
		}
	}

	public function resetParola($nume,$link){
		$membru = Membrii::where('nume','=',$nume)->get();
		if($membru[0]->temp_pass_token == ''){
			dd('err');
		}else{
			return view('resetparola')->with('id',$membru[0]->id);
		}
	}

	public function resetPass(Request $req){
		$rules = [
				'parola' => 'required|min:6',
				'paroladinnou'=>'required|min:6|same:parola'
		];
		//dd($req->all());	
		$validation = Validator::make($req->all(),$rules);

		if($validation->fails()){
				return redirect()->back()->withErrors($validation->errors());
		}
		else if(!$validation->fails()){		
				Membrii::find($req->input('id'))->update([
					'temp_pass_token'=>'',
					'password'=>Hash::make($req->input('parola'))
					]);
			return redirect('/')->with('success','Parola ta a fost schimbata cu success.');
		}
	}
}
