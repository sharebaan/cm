<?php namespace App\Http\Controllers;

use App\Poze;
use App\Carucior;
use App\Produse;
use App\Disponibil;
use App\Producator;
use App\Branduri;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use App\DiscPerClient;
use Illuminate\Support\Facades\Auth as Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AllCatController extends Controller {
	public $disp = [];	
	public $da = 20; 	
 	public $holder = [];
	public $cartHolder = [
		'qty' => array(),
		'nume' => array(),
		'total' => array(),
		'id' => array(),
		'cod'=>array()
	];

	private function getProds(){
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->holder, Carucior::find($c->id)->produse);	
		}
	}

	private function putCart(){
		foreach($this->holder as $h){
			foreach($h as $a){
				array_push($this->cartHolder['nume'], $a['denumire_produs']);
				array_push($this->cartHolder['id'], $a['id']);
				array_push($this->cartHolder['cod'], $a['cod_produs']);
			}
		}
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->cartHolder['qty'], $c->qty);
			array_push($this->cartHolder['total'], $c->total);
		}
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function dispItem($prod){
		foreach($prod as $p){
			array_push($this->disp,Disponibil::find($p->id));	
		}
	}

	public function telgsm(Request $req){
		$this->getProds();
		$this->putCart();		

		if(count($req->all()) > 0){
			$this->pagnr = $req->input('nrpag');
		}
		
		$prod = Produse::where('cat_baza_id','=',1)->where('ascuns','!=',1)->where('nelistat','!=',1)->orderBy('updated_at','desc')->orderBy('created_at','desc')->with('disp')->paginate(20);	
		$this->dispItem($prod);	
		$dpc = DiscPerClient::where('membru_id','=',Auth::user()->id)->get();


		return view('prod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',$prod)
		->with('disp',$this->disp)
		->with('dpc',$dpc);
	}
	
	public function componenteAcc(Request $req,$id){
		$this->getProds();
		$this->putCart();
	
		if(count($req->all()) > 0){
			$this->da = $req->input('nrpag');
		}
		$prod = \DB::table('produse')
		->join('piv_sb1','produse.id','=','piv_sb1.prod_id')
		->where('piv_sb1.subcat_baza_1_id','=',$id)->where('ascuns','!=',1)->where('nelistat','!=',1)->orderBy('produse.updated_at','desc')->orderBy('produse.created_at','desc')->paginate(20);	

		$this->dispItem($prod);

		$dpc = \DB::table('produse')
		->join('piv_sb1','produse.id','=','piv_sb1.prod_id')
		->join('disc_per_clients','produse.id','=','disc_per_clients.prod_id')
		->where('piv_sb1.subcat_baza_1_id','=',$id)->where('disc_per_clients.membru_id','=',Auth::user()->id)->where('ascuns','!=',1)->where('nelistat','!=',1)->orderBy('produse.updated_at','desc')->orderBy('produse.created_at','desc')->get();

		return view('prod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',$prod)
		->with('disp',$this->disp)
		->with('dpc',$dpc);	
	}

	public function titlu(Request $req,$id){
		$this->getProds();
		$this->putCart();

		if(count($req->all()) > 0){
			$this->c = $req->input('nrpag');
		}
		
		$prod = \DB::table('produse')
		->join('piv_s2c1','produse.id','=','piv_s2c1.prod_id')
		->where('piv_s2c1.subcat2_copil_1_id','=',$id)->where('ascuns','!=',1)->where('nelistat','!=',1)->orderBy('produse.updated_at','desc')->orderBy('produse.created_at','desc')->paginate(20);
		$this->dispItem($prod);
		$dpc = 	\DB::table('produse')
		->join('piv_s2c1','produse.id','=','piv_s2c1.prod_id')
		->join('disc_per_clients','produse.id','=','disc_per_clients.prod_id')
		->where('piv_s2c1.subcat2_copil_1_id','=',$id)->where('disc_per_clients.membru_id','=',Auth::user()->id)->where('ascuns','!=',1)->where('nelistat','!=',1)->orderBy('produse.updated_at','desc')->orderBy('produse.created_at','desc')->get();

		return view('prod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',$prod)
		->with('disp',$this->disp)	
		->with('dpc',$dpc);
	}		

	public function categorie(Request $req,$id){
		$this->getProds();
		$this->putCart();

		if(count($req->all()) > 0){
			$this->pagnr = $req->input('nrpag');
		}
		$prod = \DB::table('produse')
		->join('piv_s3c2','produse.id','=','piv_s3c2.prod_id')
		->where('piv_s3c2.subcat3_copil_2_id','=',$id)->where('ascuns','!=',1)->where('nelistat','!=',1)->orderBy('produse.updated_at','desc')->orderBy('produse.created_at','desc')->paginate(20);
		$this->dispItem($prod);

		$dpc = \DB::table('produse')
		->join('piv_s3c2','produse.id','=','piv_s3c2.prod_id')
		->join('disc_per_clients','produse.id','=','disc_per_clients.prod_id')
		->where('piv_s3c2.subcat3_copil_2_id','=',$id)->where('disc_per_clients.membru_id','=',Auth::user()->id)->where('ascuns','!=',1)->where('nelistat','!=',1)->orderBy('produse.updated_at','desc')->orderBy('produse.created_at','desc')->get();

		return view('prod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',$prod)
		->with('disp',$this->disp)
		->with('dpc',$dpc);	
	}

	public function veziprod($id){
		$this->getProds();
		$this->putCart();

		$prod = Produse::find($id);

		$br = Branduri::find($prod->brand_id);
		$poze = Poze::where('produs_id','=',$prod->id)->get(); 
		$producator = Producator::find($prod->producator_id);

		if($prod->ascuns == 1 || $prod->nelistat == 1){

			return view('produs')
			->with('cart',$this->cartHolder)
			->with('telgsm',SubcatBaza1::all())
			->with('componenteT',Subcat2Copil1::all())
			->with('componente',Subcat3Copil2::all())
			->with('null',$prod)
			->with('branduri',Branduri::all());
		}

		return view('produs')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('produs',$prod)
		->with('br',$br)
		->with('poze',$poze)
		->with('prod',$producator)
		->with('disp',Disponibil::where('produs_id','=',$id)->get());
	}

	public function veziprodnavid($sens,$id){
		$this->getProds();
		$this->putCart();

		$prod = Produse::find($id);

		if($sens == 'urmator'){
			if($prod == null){
				while($prod == null){
					$id ++;
					$prod = Produse::find($id);
				}
			}
		}
		elseif($sens == 'anterior'){
			if($prod == null){
				while($prod == null){
					$id --;
					$prod = Produse::find($id);
				}
			}
		}

		$br = Branduri::find($prod->brand_id);
		$poze = Poze::where('produs_id','=',$prod->id)->get(); 
		$producator = Producator::find($prod->producator_id);

		if($prod->ascuns == 1 || $prod->nelistat == 1){

			return view('produs')
			->with('cart',$this->cartHolder)
			->with('telgsm',SubcatBaza1::all())
			->with('componenteT',Subcat2Copil1::all())
			->with('componente',Subcat3Copil2::all())
			->with('null',$prod)
			->with('branduri',Branduri::all());
		}

		return view('produs')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('produs',$prod)
		->with('br',$br)
		->with('poze',$poze)
		->with('prod',$producator)
		->with('disp',Disponibil::where('produs_id','=',$id)->get());
	}

	public function veziprodnavcod($sens,$cod){
		$this->getProds();
		$this->putCart();
		
		$prod = Produse::where('cod_produs','=',$cod)->get();
			
		if($sens == 'urmator'){
			if($prod->isEmpty() == true){
				while($prod->isEmpty() == true){
					$cod ++;
					$prod = Produse::where('cod_produs','=',$cod)->get();
				}
			}
		}
		elseif($sens == 'anterior'){
			if($prod->isEmpty() == true){
				while($prod->isEmpty() == true){
					$cod --;
					$prod = Produse::where('cod_produs','=',$cod)->get();
				}
			}
		}

		$br = Branduri::find($prod[0]->brand_id);
		$poze = Poze::where('produs_id','=',$prod[0]->id)->get(); 
		$producator = Producator::find($prod[0]->producator_id);

		if($prod[0]->ascuns == 1 || $prod[0]->nelistat == 1){
			return view('produs')
			->with('cart',$this->cartHolder)
			->with('telgsm',SubcatBaza1::all())
			->with('componenteT',Subcat2Copil1::all())
			->with('componente',Subcat3Copil2::all())
			->with('null',$prod[0])
			->with('branduri',Branduri::all());
		}

		return view('produs')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('produs',$prod[0])
		->with('br',$br)
		->with('poze',$poze)
		->with('prod',$producator)
		->with('disp',Disponibil::where('produs_id','=',$prod[0]->id)->get());
	}




}
