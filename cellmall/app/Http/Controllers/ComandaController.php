<?php namespace App\Http\Controllers;

use App\Carucior;
use App\Produse;
use App\Comenzi;
use App\Disponibil;
use App\StatusStoc;
use App\AlertaStoc;
use App\StatusFac;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use App\DiscPerClient;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\Validator as Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail as Mail;
use App\Commands\CumpAlgoCommand;

class ComandaController extends Controller {
	public $stocErr = [];
	public $numeErr = [];

	public $holder = [];
	public $cartHolder = [
		'qty' => array(),
		'nume' => array(),
		'cod' => array(),
		'total' => array(),
		'id' => array(),
		'poza'=>array(),
		'totalp'=>''
	];

	private function getProds(){
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->holder, Carucior::find($c->id)->produse);	
		}
	}

	private function putCart(){
		foreach($this->holder as $h){
			foreach($h as $a){
				array_push($this->cartHolder['nume'], $a['denumire_produs']);
				array_push($this->cartHolder['id'], $a['id']);
				array_push($this->cartHolder['poza'], $a['poza']);
				array_push($this->cartHolder['cod'], $a['cod_produs']);
			}
		}
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->cartHolder['qty'], $c->qty);
			array_push($this->cartHolder['total'], $c->total);
		}
		
			$this->cartHolder['totalp'] = array_sum($this->cartHolder['total']);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$this->getProds();
		$this->putCart();

		return view('fincomanda')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all());
	}

	public function checkContbanca($p1,$p2,$p3){
		if(!empty($p1)){return $p1;}
		elseif(!empty($p2)){return $p2;}
		elseif(!empty($p3)){return $p3;}
		else{return 'Non bancar';}	
	}

	public function cumpara(Request $req){
		// UPDATE PE REPO PATTERN
			if($req->input('acceptterm')!=1){
				return redirect()->back()->with('err','Termenii si conditiile nu sunt acceptate');
			}

		if($req->input('metplata') === null || $req->input('metliv') === null || $req->input('tipfac') === null){
			return redirect()->back()->with('err','Te rog bifeaza toate optiunile pentru a finaliza o comanda.');
		}else{
			$count = (count($req->all())-2)/3;

			$key = rand(100000,999999);
			$key_fac = rand(100000,999999);
			
			for($i=(int)$count;$i>=0;$i--){			

				$prod = Produse::where('denumire_produs','=',$req->input('comprod'.$i))->get();
				
				foreach($prod as $p){
					
					if($req->input('comqty'.$i) == $p->stoc){
						$disp = Disponibil::where('produs_id','=',$p->id)->get();
						
						Disponibil::where('produs_id','=',$p->id)->update([
									'produs_id'=> $p->id,
									'in_stoc' => 0,
									'stoc_redus' => 0,
									'produs_indisponibil' => 1,
									'in_curand' => 0
								]);
						AlertaStoc::create([
									'produs_id' => $p->id,
									'disponibilitate_id' => $disp[0]->id,
									'stoc_ramas' => $p->stoc - $req->input('comqty'.$i)  
								]);
						
						Produse::find($p->id)->update([
								'stoc'=> $p->stoc - $req->input('comqty'.$i),
								'total_stoc' => ($p->stoc - $req->input('comqty'.$i))*$req->input('compretbuc'.$i)
							]);
					}									
					else if($req->input('comqty'.$i) < $p->stoc){
						$disp = Disponibil::where('produs_id','=',$p->id)->get();
						$curStoc = $p->stoc - $req->input('comqty'.$i);

						if($curStoc > $p->alerta_stoc){				
								
							Disponibil::where('produs_id','=',$p->id)->update([
									'produs_id'=> $p->id,
									'in_stoc' => 1,
									'stoc_redus' => 0,
									'produs_indisponibil' => 0,
									'in_curand' => 0
								]);

						}
						else if($curStoc <= $p->alerta_stoc){
								
							Disponibil::where('produs_id','=',$p->id)->update([
									'produs_id'=> $p->id,
									'in_stoc' => 0,
									'stoc_redus' => 1,
									'produs_indisponibil' => 0,
									'in_curand' => 0
								]);
							
							AlertaStoc::create([
									'produs_id' => $p->id,
									'disponibilitate_id' => $disp[0]->id,
									'stoc_ramas' => $p->stoc - $req->input('comqty'.$i)  
								]);		
						}

							Produse::find($p->id)->update([
								'stoc'=> $p->stoc - $req->input('comqty'.$i),
								'total_stoc' => ($p->stoc - $req->input('comqty'.$i))*$req->input('compretbuc'.$i)
							]);

					}
					else if($req->input('comqty'.$i) > $p->stoc){
						return redirect()->back()->with('err','Produsul <b>'.$p->denumire_produs.'</b> are stocul <b>'.$p->stoc.'</b>');
					}

					$DiscCheck = DiscPerClient::where('prod_id','=',$p->id)->where('membru_id','=',Auth::user()->id)->get();

					if($DiscCheck->isEmpty() == false){
						if($DiscCheck[0]->every_ord == 0){
							DiscPerClient::find($DiscCheck[0]->id)->delete();
						}
					}

					$comanda = new Comenzi();
					$comanda->membru_id = Auth::user()['id'];
					$comanda->comandafac_key = $key; 
					$comanda->produs_id = $p->id;
					$comanda->nume_produs = $p->denumire_produs;
					$comanda->cantitate = $req->input('comqty'.$i);
					$comanda->total = $req->input('totalp');
					$comanda->transport = $req->input('totalt');
					$comanda->metoda_livrare = $req->input('metliv');
					$comanda->contbanca = $this->checkContbanca($req->input('contbanca1'),$req->input('contbanca2'),$req->input('contbanca3'));
					$comanda->metoda_plata = $req->input('metplata');
					$comanda->ment_livrare = $req->input('mentliv');
					$comanda->mod_cumparare = $req->input('tipfac');
					$comanda->KAM_livrare = Auth::user()->KAM_liv;
					$comanda->save();
						
					$sStoc = new StatusStoc();
					$sStoc->prod_id = $p->id;
					$sStoc->vanzari = $req->input('comqty'.$i);
					$sStoc->save();



				}
			}

			StatusFac::create([
				'comanda_id' => $key,
				'membru_id'=> Auth::user()['id'],
				'nr_factura'=>$key_fac,
				'status_comanda' => 'Neprocesata',
				'stare_plata' => 'Neachitata',
				'stare_transport' => 'Necolectata',
				'total' => $req->input('totalp'),
				'metoda_plata' => $req->input('metplata'),
				'metoda_livrare' => $req->input('metliv'),
				]);

			Carucior::where('membru_id','=',Auth::user()['id'])->delete();

			Mail::send('emails.comanda', ['subiect' => 'Comanda noua.'], function($message){	
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Comanda noua.');   
			});
			Mail::send('emails.comanda', ['subiect' => 'Comanda noua.'], function($message){	
				$message->to('comenzi@cellmall.ro',Auth::user()['email'])->subject('Comanda noua.');   
			});

			return redirect('/home')->with('comsucc','Comanda ta a fost trimisa.');	
		}
	}

}

