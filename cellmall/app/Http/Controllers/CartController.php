<?php namespace App\Http\Controllers;

use App\Carucior;
use App\Produse;
use App\Membrii;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth as Auth;

use Illuminate\Http\Request;

class CartController extends Controller {

	public $holder = [];
	public $cartHolder = [
		'qty' => array(),
		'nume' => array(),
		'total' => array(),
		'id' => array(),
		'cod'=>array()
	];

	function __construct(Request $req){
		$this->req = $req;
	}

	private function getProds(){
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->holder, Carucior::find($c->id)->produse);	
		}
	}

	private function putCart(){
		foreach($this->holder as $h){
			foreach($h as $a){
				array_push($this->cartHolder['nume'], $a['denumire_produs']);
				array_push($this->cartHolder['id'], $a['id']);	
				array_push($this->cartHolder['cod'], $a['cod_produs']);
			}
		}
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->cartHolder['qty'], $c->qty);
			array_push($this->cartHolder['total'], $c->total);
		}
	}

	public function addCart($codprod,$qty){
		if($this->req->ajax()){
			$prod = Produse::find($codprod);
			$cart = Carucior::where('produs_id','=',$prod->id)->get();

			$errors = array_filter((array)$cart); 

			if($prod->dpc->isEmpty() == false){
				$pret = $prod->dpc[0]->pret_disc;
			}else{
				$pret = $prod->pret;
			}


			if(empty($errors)){
				$c = new Carucior();
				$c->produs_id = $prod->id;
				$c->membru_id = Auth::user()['id'];
				$c->qty = $qty;
				$c->total = $pret * $qty;
				$c->save();

				$this->getProds();
				$this->putCart();

				return response()->json([
					'produse'=>$this->cartHolder
					]);
			}
			else{	
				$cartQty = Carucior::find($cart[0]['id']);

				$curTotal = $pret * $qty;
				
				Carucior::where('produs_id','=',$prod->id)->update([
											'qty' => $cartQty->qty + $qty,
											'total' => $cartQty->total + $curTotal
										]);

				$this->getProds();
				$this->putCart();

				return response()->json([
					'produse'=>$this->cartHolder
					]);
			}

		}
	}

	public function decreaseCart($codprod,$qty){
		if($this->req->ajax()){
			$prod = Produse::find($codprod);
			$cart = Carucior::where('produs_id','=',$prod->id)->get();
		
			$cartQty = Carucior::find($cart[0]['id']);

			if($prod->dpc->isEmpty() == false){
				$pret = $prod->dpc[0]->pret_disc;
			}else{
				$pret = $prod->pret;
			}

			$curTotal = $pret * $qty;
				
			Carucior::where('produs_id','=',$prod->id)->update([
											'qty' => $cartQty->qty - $qty,
											'total' => $cartQty->total - $curTotal
										]);

			$this->getProds();
			$this->putCart();

			return response()->json([
					'produse'=>$this->cartHolder
				]);
		}
	}

	public function removeCart($id){
		if($this->req->ajax()){
			Carucior::where('produs_id','=',$id)->delete();
		}
	}

}
