<?php namespace App\Http\Controllers;

use App\PivSB1 as P1;
use App\PivS2C1 as P2;
use App\PivS3C2 as P3;
use App\PivS4C3 as P4;
use App\Produse;
use App\StatusStoc;
use App\Disponibil;
use App\Producator;
use App\CatBaza;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use App\Branduri;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AdminDashboardClonareController extends Controller {

	

	public function cloneaza(Request $req){
		$prodpiv = Produse::where('id','=',$req->input('id'))->with('catSB1')
							->with('catS2C1')
							->with('catS3C2')
							->with('catS4C3')->get();

	//	dd($prodpiv[0]['catS3C2'][0]->subcat3_copil_2_id);
	//dd('test');
		$produs = new Produse();
		$produs->tva = $req->input('tvaval');
		$produs->cod_produs = $req->input('codprod');
		$produs->denumire_produs = $req->input('denprod');
		$produs->detalii = $req->input('detalii');
		$produs->producator_id = $req->input('prod');
		$produs->pret = $req->input('pret');
		$produs->pret_original = $req->input('pretorig');
		$produs->pretTva = $req->input('prettva');
		$produs->culoare = $req->input('culoare');
		$produs->dimensiune = $req->input('dimensiune');
		$produs->discount = $req->input('discount');
		$produs->stoc = $req->input('stoc');
		$produs->total_stoc = $req->input('totalstoc');
		$produs->promotii = $req->input('promotii');
		$produs->noutati = $req->input('noutati');
		$produs->alerta_stoc = $req->input('alertastoc');
		$produs->lichidari_stoc = $req->input('lichidstoc');
		$produs->pozitie_raft = $req->input('pozraft');
		$produs->cat_baza_id = substr($req->codprod,0,1); 
		$produs->brand_id = $req->input('brand');
		$produs->poza = $req->input('poza');
		$produs->atentie = $req->input('atentie');
		$produs->save();

		$id = $produs->id;	

		if($req->input('stoc') == 0){
				Disponibil::create([
					'id'=>$id,
					'produs_id'=> $id,
					'in_stoc'=>0,
					'stoc_redus'=>0,
					'produs_indisponibil'=>0,
					'in_curand'=>1
				]);
		}
		elseif($req->input('stoc') <= $req->input('alertastoc')){
			Disponibil::create([
					'id'=>$id,
					'produs_id'=> $id,
					'in_stoc'=>0,
					'stoc_redus'=>1,
					'produs_indisponibil'=>0,
					'in_curand'=>0
				]);
		}else{
			Disponibil::create([
					'id'=>$id,
					'produs_id'=> $id,
					'in_stoc'=>1,
					'stoc_redus'=>0,
					'produs_indisponibil'=>0,
					'in_curand'=>0
				]);
		}

		P1::create([
				'id'=>$id,
				'subcat_baza_1_id' => $prodpiv[0]['catSB1'][0]->subcat_baza_1_id,
				'prod_id' => $id	
			]);
		if(!$prodpiv[0]['catS2C1'][0]->subcat2_copil_1_id == null){
			P2::create([
					'id'=>$id,	
					'subcat2_copil_1_id' => $prodpiv[0]['catS2C1'][0]->subcat2_copil_1_id,
					'prod_id' => $id
				]);
		}
		if(!$prodpiv[0]['catS3C2'][0]->subcat3_copil_2_id == null){
			P3::create([
					'id'=>$id,	
					'subcat3_copil_2_id' => $prodpiv[0]['catS3C2'][0]->subcat3_copil_2_id,
					'prod_id' => $id
				]);
		}	
		if($prodpiv[0]['catS4C3']->isEmpty() == false){
			P4::create([
				'id'=>$id,
				'subcat4_copil_3_id' => $prodpiv[0]['catS4C3'][0]->subcat4_copil_3_id,
				'prod_id' => $id
			]);
		}
		return redirect('dashboard/1/search/search?tosearch=produse&search='.$produs->cod_produs);
	}

/* MOMENTAN INUTIL
	public function clonacod(Request $req){
	//	dd($req->codprod,substr($req->codprod,1,2));	
		dd(substr($req->codprod,1,1));		
		$codpart = substr($req->codprod,1,2); 
		switch($codpart){
			case 12:
				$codpart =28; 
				return $codpart;
				break;
			case 13:
				$codpart =29; 
				return $codpart;
				break;	
			case 14:
				$codpart =12; 
				return $codpart;
				break;	
			case 15:
				$codpart =13; 
				return $codpart;
				break;
			case 16:
				$codpart =14; 
				return $codpart;
				break;
			case 17:
				$codpart =15; 
				return $codpart;
				break;	
			case 18:
				$codpart =16; 
				return $codpart;
				break;	
			case 19:
				$codpart =17; 
				return $codpart;
				break;	
			case 20:
				$codpart =18; 
				return $codpart;
				break;
			case 21:
				$codpart =19; 
				return $codpart;
				break;	
			case 22:
				$codpart =20; 
				return $codpart;
				break;	
			case 23:
				$codpart =21; 
				return $codpart;
				break;	
			case 24:
				$codpart =22; 
				return $codpart;
				break;
			case 25:
				$codpart =23; 
				return $codpart;
				break;	
			case 26:
				$codpart =24; 
				return $codpart;
				break;	
			case 27:
				$codpart =25; 
				return $codpart;
				break;	
			case 28:
				$codpart =26;
				return $codpart;
				break;	
			case 29:
				$codpart=27;
				return $codpart;
				break;
			default:
				return $codpart;
				break;
		}
	}
 */
}
