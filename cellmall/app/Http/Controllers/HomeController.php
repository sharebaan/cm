<?php namespace App\Http\Controllers;

use DB;
use App\Carucior;
use App\Produse;
use App\Branduri;
use App\Disponibil;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth as Auth;

use Illuminate\Http\Request;

class HomeController extends Controller {

	public $holder = [];
	public $cartHolder = [
		'qty' => array(),
		'nume' => array(),
		'total' => array(),
		'id' => array(),
		'cod'=>array()
	];

	private function getProds(){
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->holder, Carucior::find($c->id)->produse);	
		}
	}

	private function putCart(){
		foreach($this->holder as $h){
			foreach($h as $a){
				array_push($this->cartHolder['nume'], $a['denumire_produs']);
				array_push($this->cartHolder['id'], $a['id']);
				array_push($this->cartHolder['cod'], $a['cod_produs']);
			}
		}
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->cartHolder['qty'], $c->qty);
			array_push($this->cartHolder['total'], $c->total);
		}
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$this->getProds();
		$this->putCart();
		
		return view('home')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('carouselnou',Produse::where('noutati','=',1)->where('ascuns','!=',1)->orderBy('updated_at','desc')->orderBy('created_at','desc')->take(50)->get())
		->with('disp',Disponibil::cuNoutati()->get());
	}
	
	public function logout(){
		Auth::logout();
		return redirect('/');
	}
}
