<?php namespace App\Http\Controllers;

use App\StatusFac;
use App\Comenzi;
use App\CatBaza;
use App\Commands\ComenziFilter;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AdminDashboardComenziController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function viewC($ord,$sta){
		$command = new ComenziFilter($ord,$sta);
		$response = $this->dispatch($command);

		return view('admin/comenzi')
		->with('baze',CatBaza::all())
		->with('comenzi',$response[0])
		->with('sp',$response[1]);
	}

	

}
