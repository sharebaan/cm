<?php namespace App\Http\Controllers;

use App\PivSB1 as P1;
use App\PivS2C1 as P2;
use App\PivS3C2 as P3;
use App\PivS4C3 as P4;
use App\Produse;
use App\StatusStoc;
use App\Disponibil;
use App\Producator;
use App\CatBaza;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use App\Branduri;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AdminDashboardAddController extends Controller {

	function __construct(Request $req){
		$this->req = $req;
	}

	public function adauga(){
		return view('admin/dashboardAdd')
		->with('baze',CatBaza::all())
		->with('branduri',Branduri::all())
		->with('producator',Producator::all()); 
	}

	public function codPart($var){
		$zero = 0;
		$cp = (strlen($var) > 1 ? $var:$zero.$var);
		
		switch($cp){
			case 12:
				$cp =28; 
				return $cp;
				break;
			case 13:
				$cp =29; 
				return $cp;
				break;	
			case 14:
				$cp =12; 
				return $cp;
				break;	
			case 15:
				$cp =13; 
				return $cp;
				break;
			case 16:
				$cp =14; 
				return $cp;
				break;
			case 17:
				$cp =15; 
				return $cp;
				break;	
			case 18:
				$cp =16; 
				return $cp;
				break;	
			case 19:
				$cp =17; 
				return $cp;
				break;	
			case 20:
				$cp =18; 
				return $cp;
				break;
			case 21:
				$cp =19; 
				return $cp;
				break;	
			case 22:
				$cp =20; 
				return $cp;
				break;	
			case 23:
				$cp =21; 
				return $cp;
				break;	
			case 24:
				$cp =22; 
				return $cp;
				break;
			case 25:
				$cp =23; 
				return $cp;
				break;	
			case 26:
				$cp =24; 
				return $cp;
				break;	
			case 27:
				$cp =25; 
				return $cp;
				break;	
			case 28:
				$cp =26;
				return $cp;
				break;	
			case 29:
				$cp=27;
				return $cp;
				break;
			default:
				return $cp;
				break;
		}
	}

	public function codBrand($var){
		$zero = 0;
		$br = (strlen($var) > 1 ? $var:$zero.$var);
		if($br == 0){
			return null;
		}
		return $br;
	}

	public function adaugaProd(){
		
		$cod = $this->req->input('subcatbaza').
				$this->codPart($this->req->input('subcat2')).
				$this->codBrand($this->req->input('brand'));
			
		$poza;
		if($this->req->input('poza') == ''){
			$poza = '/products/no_image.png';
		}else{
			$poza = '/pozebranduricellmall/'.$this->req->input('poza');
		}


		
			
		$produs = new Produse();
		$produs->cod_produs = $cod.$this->req->input('cod');
		$produs->denumire_produs = $this->req->input('denumire_produs');
		$produs->detalii = $this->req->input('detalii');
		$produs->producator_id = $this->req->input('producator');
		$produs->culoare = $this->req->input('culoare');
		$produs->dimensiune = $this->req->input('dimensiune');
		$produs->tip_ambalaj = $this->req->input('tip_ambalaj');
		$produs->pret = $this->req->input('pret');
		$produs->discount = $this->req->input('discount');
		$produs->swap = $this->req->input('swap');
		$produs->stoc = $this->req->input('stoc');
		$produs->total_stoc = $this->req->input('stoc') * $this->req->input('pret');
		
		$produs->promotii = $this->req->input('promotii');
		$produs->noutati = $this->req->input('noutati');
		$produs->alerta_stoc = $this->req->input('alerta_stoc');
		$produs->lichidari_stoc = $this->req->input('lichidari_stoc');
		$produs->pozitie_raft = $this->req->input('pozitie_raft');
		$produs->cat_baza_id = $this->req->input('baza');
		$produs->brand_id = $this->req->input('brand');
		$produs->poza = $poza;
		$produs->atentie = $this->req->input('atentie');	
		$produs->save();

		$id = $produs->id;

		if($this->req->input('stoc') == 0){
				Disponibil::create([
					'id'=>$id,
					'produs_id'=> $id,
					'in_stoc'=>0,
					'stoc_redus'=>0,
					'produs_indisponibil'=>0,
					'in_curand'=>1
				]);
		}
		elseif($this->req->input('stoc') <= $this->req->input('alerta_stoc')){
			Disponibil::create([
					'id'=>$id,
					'produs_id'=> $id,
					'in_stoc'=>0,
					'stoc_redus'=>1,
					'produs_indisponibil'=>0,
					'in_curand'=>0
				]);
		}else{
			Disponibil::create([
					'id'=>$id,
					'produs_id'=> $id,
					'in_stoc'=>1,
					'stoc_redus'=>0,
					'produs_indisponibil'=>0,
					'in_curand'=>0
				]);
		}		

		

		P1::create([
				'id'=>$id,
				'subcat_baza_1_id' => $this->req->input('subcatbaza'),
				'prod_id' => $id	
			]);
		if(!$this->req->input('subcat1') == null){
			P2::create([
					'id'=>$id,	
					'subcat2_copil_1_id' => $this->req->input('subcat1'),
					'prod_id' => $id
				]);
		}
		if(!$this->req->input('subcat2') == null){
			P3::create([
					'id'=>$id,	
					'subcat3_copil_2_id' => $this->req->input('subcat2'),
					'prod_id' => $id
				]);
		}	
		if(!$this->req->input('subcat3') == null){
			P4::create([
				'id'=>$id,
				'subcat4_copil_3_id' => $this->req->input('subcat3'),
				'prod_id' => $id
			]);
		}

		return redirect('dashboard/1/search/search?tosearch=produse&search='.$produs->cod_produs);
	}

	public function subbaza($val){
		if($this->req->ajax()){
			
			return CatBaza::find($val)->SubcatBaza;
		}
	}

	public function subcat1($val){
		if($this->req->ajax()){
			return SubcatBaza1::find($val)->Subcat2Copil1;
		}	
	}

	public function subcat2($val){
		if($this->req->ajax()){
			return Subcat2Copil1::find($val)->Subcat3Copil2;
		}		
	}

	public function subcat3($val){
		if($this->req->ajax()){
			return Subcat3Copil2::find($val)->Subcat4Copil3;
		}
	}

}
