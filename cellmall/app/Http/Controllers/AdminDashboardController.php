<?php namespace App\Http\Controllers;

use App\Poze;
use DB;
use App\Disponibil;
use App\StatusFac;
use App\StatusStoc;
use App\Comenzi;
use App\Membrii;
use App\Branduri;
use App\Produse;
use App\CatBaza;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use App\Subcat4Copil3;
use App\PivS4C3;
use App\PivS3C2;
use App\PivS2C1;
use App\PivSB1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Commands\AdminFilter;
use App\Commands\StatusFilter;
use App\Commands\MembriiFilter;
use App\Commands\SearchFilter;
use App\Commands\AlertaFilter;

use Illuminate\Http\Request;

class AdminDashboardController extends Controller {
	public $disp = [];

	public function index(){
		return view('admin/dashboard')
			->with('baze',CatBaza::all())
			->with('brand',Branduri::all())
			->with('filtre',1);
	}

	public function membrii($ord,$sta){	
		$command = new MembriiFilter($ord,$sta);
		$response = $this->dispatch($command);

		return view('admin/membrii')
		->with('baze',CatBaza::all())
		->with('membrii',$response[0])
		->with('sp',$response[1])
		->with('brand',Branduri::all());
	}	

	public function subcatBaza($id,$ord,$sta){	
		$command = new AdminFilter($id,$ord,$sta);
		$response = $this->dispatch($command);	

			return view('admin/dashboard')
				->with('baze',CatBaza::all())
				->with('subcat',SubcatBaza1::all())
				->with('copil1',Subcat2Copil1::all())
				->with('copil2',Subcat3Copil2::all())
				->with('copil3',Subcat4Copil3::all())
				->with('subcatBaze',SubcatBaza1::where('cat_baza_id','=',$id)->get())
				->with('produs',$response[0])
				->with('brand',Produse::find(1)->branduri->get())
				->with('producator',Produse::find(1)->producator->get())
				->with('disp',$response[2])
				->with('sp',$response[1])->with('filtre',1);
			
	}
 
	public function subcatCopil($id,$ord,$sta){
		$command = new AdminFilter($id,$ord,$sta);
		$response = $this->dispatch($command);
		//dd($response[3][728]);
		//dd($response[0][0]['produseSB1']);
		//dd($response[0][728]->cod_produs);
		
		return view('admin/dashboard')
		->with('baze',CatBaza::all())
		->with('subcat',SubcatBaza1::all())
				->with('copil1',Subcat2Copil1::all())
				->with('copil2',Subcat3Copil2::all())
				->with('copil3',Subcat4Copil3::all())
		->with('subcatCopil',Subcat2Copil1::where('subcat_baza_1_id','=',$id)->get())
		->with('produs',$response[0])
		->with('brand',Produse::find(1)->branduri->get())
		->with('producator',Produse::find(1)->producator->get())
		->with('disp',$response[2])
		->with('sp',$response[1])->with('filtre',1);
	}

	public function subcatCopil2($id,$ord,$sta){
		$command = new AdminFilter($id,$ord,$sta);
		$response = $this->dispatch($command);
		//dd($response[0][1]->denumire_produs);

		return view('admin/dashboard')
		->with('baze',CatBaza::all())
		->with('subcat',SubcatBaza1::all())
				->with('copil1',Subcat2Copil1::all())
				->with('copil2',Subcat3Copil2::all())
				->with('copil3',Subcat4Copil3::all())
		->with('subcatCopil2',Subcat3Copil2::where('subcat2_copil_1_id','=',$id)->get())
		->with('produs',$response[0])
		->with('brand',Produse::find(1)->branduri->get())
		->with('producator',Produse::find(1)->producator->get())
		->with('disp',$response[2])
		->with('sp',$response[1])->with('filtre',1);
	}

	public function subcatCopil3($id,$ord,$sta){
		$command = new AdminFilter($id,$ord,$sta);
		$response = $this->dispatch($command);
		
		if($id == 3 || $id == 7){

			return view('admin/dashboard')
			->with('baze',CatBaza::all())
			->with('subcat',SubcatBaza1::all())
				->with('copil1',Subcat2Copil1::all())
				->with('copil2',Subcat3Copil2::all())
				->with('copil3',Subcat4Copil3::all())
			->with('subcatCopil3',Subcat4Copil3::where('subcat3_copil_2_id','=',$id)->get())
			->with('produs',$response[0])
			->with('brand',Produse::find(1)->branduri->get())
			->with('producator',Produse::find(1)->producator->get())
			->with('disp',$response[2])
			->with('sp',$response[1])->with('filtre',1);
		}
	
		return view('admin/dashboard')
				->with('baze',CatBaza::all())
				->with('subcat',SubcatBaza1::all())
				->with('copil1',Subcat2Copil1::all())
				->with('copil2',Subcat3Copil2::all())
				->with('copil3',Subcat4Copil3::all())
				->with('subcatCopil3',Subcat4Copil3::where('subcat3_copil_2_id','=',$id)->get())
				->with('produs',$response[0])
				->with('brand',Produse::find(1)->branduri->get())
				->with('producator',Produse::find(1)->producator->get())
				->with('disp',$response[2])
				->with('sp',$response[1])->with('filtre',1);

	}

	public function subcatCopil3prod($id,$ord,$sta){
		$command = new AdminFilter($id,$ord,$sta);
		$response = $this->dispatch($command);

		return view('admin/dashboard')
		->with('baze',CatBaza::all())
			->with('subcat',SubcatBaza1::all())
				->with('copil1',Subcat2Copil1::all())
				->with('copil2',Subcat3Copil2::all())
				->with('copil3',Subcat4Copil3::all())
		->with('subcatCopil3',Subcat4Copil3::where('subcat3_copil_2_id','=',$id)->get())
		->with('produs',$response[0])
		->with('brand',Produse::find(1)->branduri->get())
		->with('producator',Produse::find(1)->producator->get())
		->with('disp',$response[2])
		->with('sp',$response[1])->with('filtre',1);
	}
	
	public function dispItem($id){
		foreach($id as $p){
			array_push($this->disp,Disponibil::find($id));
		}
	}	

	public function search(Request $req,$ord,$sta){	
		if($req->input('tosearch')=='produse'){
			
			$command = new SearchFilter($ord,$sta,$req->input('search'),$req->input('tosearch'));
			$response = $this->dispatch($command);	
		
			foreach($response[0] as $p){
				array_push($this->disp,Disponibil::find($p->id));	
			}

			return view('admin/pageSearchProdus')
			->with('baze',CatBaza::all())
				->with('subcat',SubcatBaza1::all())
				->with('copil1',Subcat2Copil1::all())
				->with('copil2',Subcat3Copil2::all())
				->with('copil3',Subcat4Copil3::all())
			->with('produs',$response[0])
			->with('brand',Produse::find(1)->branduri->get())
			->with('producator',Produse::find(1)->producator->get())
			->with('disp',$this->disp)
			->with('poze',Poze::all())
			->with('sp',$response[1])->with('filtre',1);
		}elseif($req->input('tosearch')== 'membri'){	
			$command = new SearchFilter($ord,$sta,$req->input('search'),$req->input('tosearch'));
			$response = $this->dispatch($command);
			

			return view('admin/pageSearchMembri')
			->with('baze',CatBaza::all())
			->with('membrii',$response[0])	
			->with('sp',$response[1]);
		}else{
			$command = new SearchFilter($ord,$sta,$req->input('search'),$req->input('tosearch'));
			$response = $this->dispatch($command);

			//dd($response);
			return view('admin/pageSearchComenzi')
			->with('baze',CatBaza::all())
			->with('comenzisearch',$response[0])
			->with('sp',$response[1]);
		}	
		
	}
	
	public function seeProd($fac){
		return view('admin/produsecomenzi')
		->with('baze',CatBaza::all())
		->with('prod',Comenzi::where('comandafac_key','=',$fac)->paginate(50));
	}

	public function seeMem($memid){
		return view('admin/membru')
		->with('baze',CatBaza::all())
		->with('membru',Membrii::find($memid));
	}

	public function seeStatusStoc($id){
		return view('admin/statusstocProd')
		->with('baze',CatBaza::all())
		->with('StProd',StatusStoc::where('prod_id','=',$id)
		->with('produs')->paginate(50));
		//->with('prod',StatusStoc::with('produs')->paginate(50));
		
	}

	public function StatusStoc($ord,$sta){
		$command = new StatusFilter($ord,$sta);
		$response = $this->dispatch($command);
		
		return view('admin/statusstoc')
		->with('baze',CatBaza::all())
		->with('StProd',$response[0])
		//->with('prod',$response[2])
		->with('sp',$response[1]);
	
	}

	public function AlertaStoc($ord,$sta){
		$command = new AlertaFilter($ord,$sta);
		$response = $this->dispatch($command);
		//dd($response[0][0]->id);
		return view('admin/alertastoc')
		->with('baze',CatBaza::all())
		->with('alerta',$response[0])
		->with('sp',$response[1]);

	}

	public function stergeProd($id){
		
		$p3 = PivS4C3::where('prod_id','=',$id)->get();
		$p2 = PivS3C2::where('prod_id','=',$id)->get();
		$p1 = PivS2C1::where('prod_id','=',$id)->get();
		$psb = PivSB1::where('prod_id','=',$id)->get();
		$disp = Disponibil::where('produs_id','=',$id)->get();
		$com = Comenzi::where('produs_id','=',$id)->get();
		$sstoc = StatusStoc::where('prod_id','=',$id)->get();
		$prod = Produse::find($id);


		


		if($p3->isEmpty() != true){
			PivS4C3::where('prod_id','=',$id)->delete();		
		}
		if($p2->isEmpty() != true){
			PivS3C2::where('prod_id','=',$id)->delete();		
		}
		if($p1->isEmpty() != true){
			PivS2C1::where('prod_id','=',$id)->delete();
		}	
		if($psb->isEmpty() != true){
			PivSB1::where('prod_id','=',$id)->delete();
		}		
		if($disp->isEmpty() != true){
			Disponibil::where('produs_id','=',$id)->delete();
		}
		
		if($com->isEmpty() != true){
			Comenzi::where('produs_id','=',$id)->delete();
		}
			
		if($sstoc->isEmpty() != true){
			StatusStoc::where('prod_id','=',$id)->delete();
		}
		
		Produse::find($id)->delete();

		return redirect()->back();
	}

	public function mutaProd(Request $req){

		//dd($req->all());

		if(empty($req->input('catbaza')) || empty($req->input('subcat'))){
			return redirect()->back();		
		}	
		else{
			//dd('mutare');
			$catbaza = Produse::find($req->input('prodid'));

			$codPart = substr($catbaza->cod_produs,3,strlen($catbaza->cod_produs));

			//dd($codPart);	
			$newSubcat = $this->codPart($req->input('copil2'));
			if(strlen($codPart)== 5){$codPart = '0'.$codPart;}

			$catbaza->update([
					'cat_baza_id'=>$req->input('catbaza'),
					'cod_produs'=>$req->input('catbaza').$newSubcat.$codPart
					]);
			$subcat = PivSB1::find($req->input('prodid'));
			
			$subcat->update([
				'subcat_baza_1_id'=>$req->input('subcat')		
					]);

			if(!empty($req->input('copil1'))){
				$copil1 = PivS2C1::find($req->input('prodid'));	
				if($copil1 == null){
					$c1 = new PivS2C1();	
					$c1->id = $req->input('prodid');
					$c1->subcat2_copil_1_id = $req->input('copil1');
					$c1->prod_id = $req->input('prodid');
					$c1->save();
				}else{
					$copil1->update([
						'subcat2_copil_1_id'=>$req->input('copil1')
					]);	
				}
			}else{	
				$copil1 = PivS2C1::find($req->input('prodid'));	
				if($copil1 != null){
					$copil1->delete();
				}
			}
			
			if(!empty($req->input('copil2'))){
				$copil2 = PivS3C2::find($req->input('prodid'));
				if($copil2 == null){
					$c2 = new PivS3C2();	
					$c2->id = $req->input('prodid');
					$c2->subcat3_copil_2_id = $req->input('copil2');
					$c2->prod_id = $req->input('prodid');
					$c2->save();
				}else{
					$copil2->update([
						'subcat3_copil_2_id'=>$req->input('copil2')
					]);	
				}
			}else{	
				$copil2 = PivS3C2::find($req->input('prodid'));	
				if($copil2 != null){
					$copil2->delete();
				}
			}


			if(!empty($req->input('copil3'))){
				$copil3 = PivS4C3::find($req->input('prodid'));
				dd($copil3);
				if($copil3 == null){
					$c3 = new PivS4C3();	
					$c3->id = $req->input('copil3');
					$c3->subcat4_copil_3_id = $req->input('copil3');
					$c3->prod_id = $req->input('prodid');
					$c3->save();
				}else{
					$copil3->update([
						'subcat4_copil_3_id'=>$req->input('copil3')
					]);	
				}
			}else{	
				$copil3 = PivS4C3::find($req->input('prodid'));	
				if($copil3 != null){
					$copil3->delete();
				}
			}


		}

		return redirect()->back();	
	}

		public function codPart($var){
		$zero = 0;
		$cp = (strlen($var) > 1 ? $var:$zero.$var);
		
		switch($cp){
			case 12:
				$cp =28; 
				return $cp;
				break;
			case 13:
				$cp =29; 
				return $cp;
				break;	
			case 14:
				$cp =12; 
				return $cp;
				break;	
			case 15:
				$cp =13; 
				return $cp;
				break;
			case 16:
				$cp =14; 
				return $cp;
				break;
			case 17:
				$cp =15; 
				return $cp;
				break;	
			case 18:
				$cp =16; 
				return $cp;
				break;	
			case 19:
				$cp =17; 
				return $cp;
				break;	
			case 20:
				$cp =18; 
				return $cp;
				break;
			case 21:
				$cp =19; 
				return $cp;
				break;	
			case 22:
				$cp =20; 
				return $cp;
				break;	
			case 23:
				$cp =21; 
				return $cp;
				break;	
			case 24:
				$cp =22; 
				return $cp;
				break;
			case 25:
				$cp =23; 
				return $cp;
				break;	
			case 26:
				$cp =24; 
				return $cp;
				break;	
			case 27:
				$cp =25; 
				return $cp;
				break;	
			case 28:
				$cp =26;
				return $cp;
				break;	
			case 29:
				$cp=27;
				return $cp;
				break;
			default:
				return $cp;
				break;
		}
	}

	public function printcod($cod){
		$prod = Produse::where('cod_produs','=',$cod)->get();
		return view('admin.print')
		->with('prod',$prod);
	}	
	public function ascundeprod($id){	
		Produse::find($id)->update([
					'ascuns'=>1
				]);
		return redirect()->back();
	}	
	public function arataprod($id){
		Produse::find($id)->update([
					'ascuns'=>0
				]);
		return redirect()->back();
	}

	public function nelistprod(Request $req,$id){
		if($req->ajax()){
			Produse::find($id)->update([
					'nelistat'=>1
				]);
		}
		//return redirect()->back();
	}

	public function listprod(Request $req,$id){
		if($req->ajax()){
			Produse::find($id)->update([
					'nelistat'=>0
				]);
		}
		//return redirect()->back();
	}
	
	public function comDepPrint($comid,$memid){
		$membru = Membrii::find($memid);
		$statusCom = Comenzi::where('comandafac_key','=',$comid)->with('produs')->get();

		$dpc = [];

		foreach($statusCom as $com){
			array_push($dpc,Produse::find($com['produs']->id)->dpc);			
		}

		$comanda = StatusFac::where('comanda_id','=',$comid)->get();

		return view('admin.comdepprint')
		->with('membru',$membru)
		->with('statcom',$statusCom)
		->with('discP',$dpc)
		->with('comanda',$comanda);
	}

	public function veziindash($cod){
		//dd(Produse::search($cod)->with('poze')->paginate(20));	
		$prod = Produse::search($cod)->with('poze')->paginate(20);
		array_push($this->disp,Disponibil::find($prod[0]->id));

		return view('admin/pageSearchProdus')
			->with('baze',CatBaza::all())
			->with('produs',Produse::search($cod)->with('poze')->paginate(20))
			->with('brand',Produse::find(1)->branduri->get())
			->with('producator',Produse::find(1)->producator->get())
			->with('disp',$this->disp);
	}

	
}
