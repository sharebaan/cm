<?php namespace App\Http\Controllers;

use App\Membrii;
use App\Carucior;
use App\Produse;
use App\Comenzi;
use App\StatusFac;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use Illuminate\Support\Facades\Auth as Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail as Mail;

use Illuminate\Http\Request;

class ContulmeuController extends Controller {
	public $holder = [];
	public $cartHolder = [
		'qty' => array(),
		'nume' => array(),
		'total' => array(),
		'id' => array(),
		'poza'=>array(),
		'totalp'=>'',
		'cod'=>array()
	];

	private function getProds(){
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->holder, Carucior::find($c->id)->produse);	
		}
	}

	private function putCart(){
		foreach($this->holder as $h){
			foreach($h as $a){
				array_push($this->cartHolder['nume'], $a['denumire_produs']);
				array_push($this->cartHolder['id'], $a['id']);
				array_push($this->cartHolder['poza'], $a['poza']);
				array_push($this->cartHolder['cod'], $a['cod_produs']);
			}
		}
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->cartHolder['qty'], $c->qty);
			array_push($this->cartHolder['total'], $c->total);
		}
		
			$this->cartHolder['totalp'] = array_sum($this->cartHolder['total']);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		$this->getProds();
		$this->putCart();

		return view('contmeu')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all());
	}

	public function istoriccom(){
		$this->getProds();
		$this->putCart();
		
		return view('istoriccom')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('comenzi',StatusFac::where('membru_id','=',Auth::user()['id'])->orderBy('id','desc')->paginate(15));	
	}

	public function editcont(Request $req){
		Membrii::find($req->input('edid'))->update([
				'nume' => $req->input('ednume'),
				'prenume' => $req->input('edprenume'),
				'CNP' => $req->input('edcnp'),
				'telefon' => $req->input('edtelefon'),
				'adresa_fac' => $req->input('edaf'),
				'localitate_fac' => $req->input('edlf'),
				'judet_sector_fac' => $req->input('edjsf'),
				'adresa_liv' => $req->input('edal'),
				'localitate_liv' => $req->input('edll'),
				'judet_sector_liv' => $req->input('edjsl'),
				'nume_companie' => $req->input('ednumecomp'),
				'CUI' => $req->input('edcui'),
				'nr_reg' => $req->input('ednrreg'),
				'adresa_sediu' => $req->input('edass'),
				'localitate_sediu' => $req->input('edlss'),
				'judet_sector_sediu' => $req->input('edjsss'),
				'banca' => $req->input('edbanca'),
				'cont_IBAN' => $req->input('edcontib'),
			]);

		Mail::send('emails.mod', ['subiect' => 'Modificare Cont.','membru' => Membrii::find($req->input('edid'))], function($message){	
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email']);   
			});

		if($req->ajax()){
			return response()->json(['data' => Membrii::find($req->input('edid'))]);	
		}
	}

	public function veziprod($id){
		$this->getProds();
		$this->putCart();
		
		$com = Comenzi::where('comandafac_key','=',$id)->get();
		$com_id = [];
		$prod = [];
		$prodDisc = [];
		foreach($com as $p){
			array_push($com_id,$p->produs_id);	
		}
		foreach($com_id as $e){
			//dd(Produse::find($e)->dpc);
			$item = Produse::find($e);
			$disc = Produse::find($e)->dpc;
			array_push($prodDisc, $disc);
			array_push($prod, $item);
		}		
		
		return view('vcomprod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('pqty',$com)
		->with('cprod',$prod)
		->with('dprod',$prodDisc);
	}
}
