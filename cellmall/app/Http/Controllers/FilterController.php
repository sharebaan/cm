<?php namespace App\Http\Controllers;

use App\Carucior;
use App\Produse;
use App\Branduri;
use App\Disponibil;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;

use App\FilterData;
use App\Filter;

use App\Commands\SortFilter;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth as Auth;

use Illuminate\Http\Request;

class FilterController extends Controller {

	public $holder = [];
	public $cartHolder = [
		'qty' => array(),
		'nume' => array(),
		'total' => array(),
		'id' => array(),
		'cod'=>array()
	];

	private function getProds(){
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->holder, Carucior::find($c->id)->produse);	
		}
	}

	private function putCart(){
		foreach($this->holder as $h){
			foreach($h as $a){
				array_push($this->cartHolder['nume'], $a['denumire_produs']);
				array_push($this->cartHolder['id'], $a['id']);
				array_push($this->cartHolder['cod'], $a['cod_produs']);
			}
		}
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->cartHolder['qty'], $c->qty);
			array_push($this->cartHolder['total'], $c->total);
		}
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $req){
		return redirect()->back();
	}

	public $disp = [];


//============================================== SEARCH ==========================================

	public function search(Request $req){
		if($req->input('search') == null){
			return redirect()->back();
		}
		$this->getProds();
		$this->putCart();
		
		$prod = Produse::search($req->input('search'),null,false)->where('ascuns','!=',1)->where('nelistat','!=',1)->orderBy('relevance','desc')->paginate(20)->appends(['search'=>$req->input('search')]);
		foreach($prod as $p){
			array_push($this->disp,Disponibil::find($p->id));
		}
		return view('filterprod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',$prod)
		->with('disp',$this->disp);
	}
	
	//============================================ FILTRE ======================================

	public function filtre(Request $req){
		$this->getProds();
		$this->putCart();

		$command = new SortFilter($req->all());
		$response = $this->dispatch($command);

		//dd($response);
		//

		foreach($response[0] as $p){
			array_push($this->disp,Disponibil::find($p->id));
		}	

		return view('filterprod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',$response[0])
		->with('select',$response[1])
		->with('input',$response[2])
		->with('compare',$response[3])
		->with('brands',$response[4])
		->with('disp',$this->disp)
		->with('url',$response[5])
		->with('eQuery',$response[6]);


			
	}

}























