<?php namespace App\Http\Controllers;

use App\Carucior;
use App\Produse;
use App\Branduri;
use App\Disponibil;
use App\Mesaje;
use App\SubcatBaza1;
use App\Subcat2Copil1;
use App\Subcat3Copil2;
use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Support\Facades\Mail as Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class InfoController extends Controller {
	public $holder = [];
	public $disp = [];
	public $cartHolder = [
		'qty' => array(),
		'nume' => array(),
		'total' => array(),
		'id' => array(),
		'cod'=>array()
	];

	private function getProds(){
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->holder, Carucior::find($c->id)->produse);	
		}
	}

	private function putCart(){
		foreach($this->holder as $h){
			foreach($h as $a){
				array_push($this->cartHolder['nume'], $a['denumire_produs']);
				array_push($this->cartHolder['id'], $a['id']);
				array_push($this->cartHolder['cod'], $a['cod_produs']);
			}
		}
		foreach(Carucior::where('membru_id','=',Auth::user()['id'])->get() as $c){
			array_push($this->cartHolder['qty'], $c->qty);
			array_push($this->cartHolder['total'], $c->total);
		}
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function termeni(){
		$this->getProds();
		$this->putCart();

		return view('tercond')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all());
	}

	public function informatii(){
		$this->getProds();
		$this->putCart();

		return view('informatii')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all());
	}	

	public function contact(){
		$this->getProds();
		$this->putCart();

		return view('contact')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all());
	}
	
	public function dispItem($prod){
		foreach($prod as $p){
			
			array_push($this->disp,Disponibil::find($p->id));	
		}
	}

	public function noutati(Request $req){
		$this->getProds();
		$this->putCart();


		$prod = Produse::where('noutati','=',1)->where('ascuns','!=',1)->orderBy('updated_at','desc')->orderBy('created_at','desc')->paginate(20);
		//dd($prod);
		$this->dispItem($prod);
	

		return view('prod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',$prod)
		->with('disp',$this->disp);		
	}

	public function promotii(Request $req){
		$this->getProds();
		$this->putCart();

		$prod = Produse::where('promotii','=',1)->orderBy('updated_at','desc')->orderBy('created_at','desc')->paginate(20);
		$this->dispItem($prod);

		return view('prod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',$prod)
		->with('disp',$this->disp);		
	}

	public function licstoc(Request $req){
		$this->getProds();
		$this->putCart();

		/*if(count($req->all()) > 0){
			$pag = $req->input('nrpag');
		}else{
			$pag = 20;	
		}*/

		return view('prod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',Produse::where('lichidari_stoc','=',1)->orderBy('updated_at','desc')->orderBy('created_at','desc')->paginate(20))
		->with('disp',Disponibil::all());	
	}

	public function oferte(Request $req){
		$this->getProds();
		$this->putCart();

		/*if(count($req->all()) > 0){
			$pag = $req->input('nrpag');
		}else{
			$pag = 20;	
		}*/

		return view('prod')
		->with('cart',$this->cartHolder)
		->with('telgsm',SubcatBaza1::all())
		->with('componenteT',Subcat2Copil1::all())
		->with('componente',Subcat3Copil2::all())
		->with('branduri',Branduri::all())
		->with('prod',Produse::where('discount','>',0)->orderBy('updated_at','desc')->orderBy('created_at','desc')->paginate(20))
		->with('disp',Disponibil::all());	
	}

	public function mesaj(Request $req){
		if($req->input('dep') == 'DT'){
			Mail::send('emails.contact', ['subiect' => $req->input('subiect'),'dep'=>$req->input('dep'),'mesaj'=>$req->input('mesaj')], function($message)
			{	
				$message->to('tehnic@cellmall.ro',Auth::user()['email'])->subject('Dep Tehnic');   
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Dep Tehnic');   
			});
			/*Mail::send('emails.contact', ['subiect' => $req->input('subiect'),'dep'=>$req->input('dep'),'mesaj'=>$req->input('mesaj')], function($message)
			{	
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Dep Tehnic');   
			});*/
		}elseif($req->input('dep') == 'DC'){
			Mail::send('emails.contact', ['subiect' => $req->input('subiect'),'dep'=>$req->input('dep'),'mesaj'=>$req->input('mesaj')], function($message)
			{	
				$message->to('comercial@cellmall.ro',Auth::user()['email'])->subject('Dep Comercial');   
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Dep Comercial');   
			});
			/*Mail::send('emails.contact', ['subiect' => $req->input('subiect'),'dep'=>$req->input('dep'),'mesaj'=>$req->input('mesaj')], function($message)
			{	
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Dep Comercial');   
			});*/
		}else{
			Mail::send('emails.contact', ['subiect' => $req->input('subiect'),'dep'=>$req->input('dep'),'mesaj'=>$req->input('mesaj')], function($message)
			{	
				$message->to('admin@cellmall.ro',Auth::user()['email'])->subject('Dep Administrativ');   

				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Dep Administrativ');   
			});
			/*Mail::send('emails.contact', ['subiect' => $req->input('subiect'),'dep'=>$req->input('dep'),'mesaj'=>$req->input('mesaj')], function($message)
			{	
				$message->to('roxon_roxon@yahoo.com',Auth::user()['email'])->subject('Dep Administrativ');   
			});*/
		}
		
 
		Mesaje::create([
				'membru_id'=>Auth::user()['id'],
				'subiect'=>$req->input('subiect'),
				'text'=>$req->input('mesaj')
			]);

		return redirect()->back()->with('mesaj','Mesajul a fost trimis.');
	}	


}
