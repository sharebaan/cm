<?php namespace App\Http\Controllers;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash as Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session as Session;
use Illuminate\Http\Request;

class AdminAuthController extends Controller {

	public function index(){
		return view('admin.login');
	}

	public function login(Request $req){
		$admin = User::where('email','=',$req->input('nadmin'))->get();
		
		if($admin->isEmpty()){
			dd('errempt');
		}
		else{
			if($admin[0]->admin == 1){
				if(Auth::attempt(array('email'=> $req->input('nadmin'),'password'=> $req->input('padmin')))){
					return redirect('/dashboard');
				}else{
					dd('errauth');
				}
			}else{
				dd('erradmin');
			}				
		}
		


	}

}
