<?php
Route::get('/', 'WelcomeController@index');
Route::get('/inregistrare','WelcomeController@register');
Route::post('/autentificare','WelcomeController@auth');
Route::post('/crearecontpj','WelcomeController@regPJ');
Route::get('/recuperareparola','WelcomeController@recPass');
Route::post('/recpass','WelcomeController@recPassMail');
Route::get('/parolareset/{nume}/{link}','WelcomeController@resetParola');
Route::post('/resetpass','WelcomeController@resetPass');


//----- Auth filter -------------
	Route::get('/home',['middleware'=>'auth','uses'=>'HomeController@index']);

	Route::get('/home/telefoanegsm',['middleware'=>'auth','uses'=>'AllCatController@telgsm']);
	Route::get('/home/componenteaccesorii/{id}',['middleware'=>'auth','uses'=>'AllCatController@componenteAcc']);
	Route::get('/home/titlu/{id}',['middleware'=>'auth','uses'=>'AllCatController@titlu']);
	Route::get('/home/categorie/{id}',['middleware'=>'auth','uses'=>'AllCatController@categorie']);

	Route::get('/addcart/{codprod}/{qty}',['middleware'=>'auth','uses'=>'CartController@addCart']);	
	Route::get('/decreasecart/{codprod}/{qty}',['middleware'=>'auth','uses'=>'CartController@decreaseCart']);
	Route::get('/removecart/{id}',['middleware'=>'auth','uses'=>'CartController@removeCart']);
	Route::get('/fincomanda',['middleware'=>'auth','uses'=>'ComandaController@index']);
	Route::post('/cumpara',['middleware'=>'auth','uses'=>'ComandaController@cumpara']);
	Route::get('/filtru',['middleware'=>'auth','uses'=>'FilterController@index']);
	Route::get('/cauta',['middleware'=>'auth','uses'=>'FilterController@search']);
	Route::get('/informatii',['middleware'=>'auth','uses'=>'InfoController@informatii']);
	Route::get('/informatii/termenisiconditii',['middleware'=>'auth','uses'=>'InfoController@termeni']);
	Route::get('/informatii/contact',['middleware'=>'auth','uses'=>'InfoController@contact']);
	Route::post('/trimiteremesaj',['middleware'=>'auth','uses'=>'InfoController@mesaj']);
	Route::get('/contulmeu',['middleware'=>'auth','uses'=>'ContulmeuController@index']);
	Route::get('/istoriccom',['middleware'=>'auth','uses'=>'ContulmeuController@istoriccom']);
	Route::get('/logout',['middleware'=>'auth','uses'=>'HomeController@logout']);
	Route::post('/editcont',['middleware'=>'auth','uses'=>'ContulmeuController@editcont']);
	Route::get('/veziprod/{id}',['middleware'=>'auth','uses'=>'ContulmeuController@veziprod']);
	Route::get('/produsnoutati',['middleware'=>'auth','uses'=>'InfoController@noutati']);

	Route::get('/produspromotii',['middleware'=>'auth','uses'=>'InfoController@promotii']);
	Route::get('/lichidaristoc',['middleware'=>'auth','uses'=>'InfoController@licstoc']);
	Route::get('/ofertespeciale',['middleware'=>'auth','uses'=>'InfoController@oferte']);
	

	
	Route::get('/produs/{id}',['middleware'=>'auth','uses'=>'AllCatController@veziprod']);
	Route::get('/codprodus/{sens}/{cod}',['middleware'=>'admin.auth','uses'=>'AllCatController@veziprodnavcod']);
	Route::get('/produsid/{sens}/{id}',['middleware'=>'admin.auth','uses'=>'AllCatController@veziprodnavid']);



	Route::get('/filtre',['middleware'=>'auth','uses'=>'FilterController@filtre']);
    
//-----------------------------ADMIN----------------------------------------------------


Route::get('/dashboard',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@index']);

Route::group(['prefix'=>'dashboard'],function(){
	Route::get('{ord}/{sta}/search',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@search']);
	Route::get('/veziindashboard/{cod}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@veziindash']);
	Route::get('/chsubcat2/{val}',['middleware'=>'admin.auth','uses'=>'AdminDashboardAddController@subcat3']);
	Route::get('/chsubcat1/{val}',['middleware'=>'admin.auth','uses'=>'AdminDashboardAddController@subcat2']);
	Route::get('/chsubbaza/{val}',['middleware'=>'admin.auth','uses'=>'AdminDashboardAddController@subcat1']);
	Route::get('/chbaza/{val}',['middleware'=>'admin.auth','uses'=>'AdminDashboardAddController@subbaza']);
	Route::get('{ord}/{sta}/membrii',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@membrii']);
	Route::get('adaugaPage',['middleware'=>'admin.auth','uses'=>'AdminDashboardAddController@adauga']);
	Route::get('{ord}/{sta}/comanda',['middleware'=>'admin.auth','uses'=>'AdminDashboardComenziController@viewC']);

	Route::get('{id}/{ord}/{sta}/baza/prod',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@subcatBaza']);
	Route::get('{id}/{ord}/{sta}/subcat/prod',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@subcatCopil']);
	Route::get('{id}/{ord}/{sta}/copil/prod',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@subcatCopil2']);
	Route::get('{id}/{ord}/{sta}/copil2/prod',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@subcatCopil3']);
	Route::get('membrii/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@membruDetalii']);
	Route::get('{id}/{ord}/{sta}/copil3/prod',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@subcatCopil3prod']);
	//Route::get('{id}/{ord}/{sta}/copil4/prod',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@subcatCopil4prod']);
	
	Route::post('adaugaProd',['middleware'=>'admin.auth','uses'=>'AdminDashboardAddController@adaugaProd']);
	Route::post('editmem/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardEditController@updateMem']);
	Route::post('prodedit',['middleware'=>'admin.auth','uses'=>'AdminDashboardEditController@updateProd']);
	Route::post('comandaedit/{fac}',['middleware'=>'admin.auth','uses'=>'AdminDashboardEditController@editComanda']);
	Route::post('/discedit',['middleware'=>'admin.auth','uses'=>'AdminDashboardDicounturiController@discEdit']);

	Route::get('comprod/{fac}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@seeProd']);
	Route::get('profilmem/{memid}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@seeMem']);
	Route::get('vezistatusstoc/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@seeStatusStoc']);
	Route::get('{ord}/{sta}/status',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@StatusStoc']);
	Route::get('{ord}/{sta}/alerta',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@AlertaStoc']);
	Route::get('stergeprod/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@stergeProd']);	
	Route::get('stergedisc/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardDicounturiController@stergeDisc']);	
	Route::get('cloneaza',['middleware'=>'admin.auth','uses'=>'AdminDashboardClonareController@cloneaza']);

	Route::get('print/{cod}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@printcod']);
	Route::get('ascundeprod/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@ascundeprod']);	
	Route::get('arataprod/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@arataprod']);
	Route::get('listprod/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@listprod']);
	Route::get('nelistprod/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@nelistprod']);
	Route::get('comandadepozit/{comid}/{memid}',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@comDepPrint']);
	Route::get('/getthumbnail/{id}',['middleware'=>'admin.auth','uses'=>'AdminDashboardEditController@getThumbs']);
	Route::get('/setthumbnail/{id}/{name}',['middleware'=>'admin.auth','uses'=>'AdminDashboardEditController@setThumb']);

	Route::get('/discounturi',['middleware'=>'admin.auth','uses'=>'AdminDashboardDicounturiController@index']);
	Route::get('{ord}/{sta}/discounturicurente',['middleware'=>'admin.auth','uses'=>'AdminDashboardDicounturiController@indexCurent']);
	Route::get('/proddisc/{cod}',['middleware'=>'admin.auth','uses'=>'AdminDashboardDicounturiController@getProd']);
	Route::get('/memdisc/{mem}',['middleware'=>'admin.auth','uses'=>'AdminDashboardDicounturiController@getMem']);
	Route::get('/customdiscount',['middleware'=>'admin.auth','uses'=>'AdminDashboardDicounturiController@discount']);

	Route::get('/setaridashboard',['middleware'=>'admin.auth','uses'=>'AdminDashboardSetariController@index']);
	Route::post('/optiunetva',['middleware'=>'admin.auth','uses'=>'AdminDashboardSetariController@optiuniTVA']);
	Route::post('/mutaprod',['middleware'=>'admin.auth','uses'=>'AdminDashboardController@mutaProd']);

	Route::get('/filtreadmin',['middleware'=>'admin.auth','uses'=>'AdminFiltreController@index']);
});

//=================================== UTILITARE ==========================================
  Route::get('/tq',function(){
		  $da = App\Produse::whereHas('catS2C1',function($q){
			$q->where('subcat2_copil_1_id','=',2);		  
		  })->get();	

		  dd($da);
  });  
/*Route::get('/l',function(){
	$prod = App\Produse::where('nelistat','=',0)->get();

	foreach($prod as $p){
		
	}
});
/*Route::get('/plm',function(){
	$disp = App\Disponibil::all();
	$produse = App\Produse::all();
	foreach($produse as $d){
			if($d->nelistat == 1){
				$d->update([
						//'pret'=>0,
						//'pret_original'=>0,
						//'pretTva'=>0
						'tvaVal'=>0
				]);
			}
	}
	dd('gata');
});	

	/*Route::get('/nopoze',function(){
		$p = App\Produse::all();	
		foreach($p as $c){
			if($c->poza == ''){
				$c->update([
					'poza'=>'/products/no_image.png'			
				]);
			}
		}
		dd('gata');
	});
	/*Route::get('/delpoze',function(){
		$prod = App\Produse::all();

		foreach($prod as $p){
			$p->update([
				'discount'=>0	
			]);
		}		
		dd('gata');
	});
  /*Route::get('/mutare',function(){
	foreach(App\Produse::search('107')->get() as $p){
		
		if(substr($p->cod_produs,0,3) == '107'){
			$item = App\PivS4C3::where('prod_id','=',$p->id)->get();

			if($item->isEmpty() == true){
				if(substr($p->cod_produs,5,4) < 1000){
					$omie = new App\PivS4C3();
					$omie->id = $p->id;
					$omie->prod_id = $p->id;
					$omie->subcat4_copil_3_id = 5;
					$omie->save();
				}		
				elseif(substr($p->cod_produs,5,4) > 999 && substr($p->cod_produs,5,4) < 2000){
					$omie = new App\PivS4C3();
					$omie->id = $p->id;
					$omie->prod_id = $p->id;
					$omie->subcat4_copil_3_id = 6;
					$omie->save();
				}
			}
		}
		if(substr($p->cod_produs,0,3) != '102' 
		&& substr($p->cod_produs,0,3) != '101' 
		&& substr($p->cod_produs,0,3) != '111'
		&& substr($p->cod_produs,0,3) != '106'
		&& substr($p->cod_produs,0,3) != '105'){
			$item = App\PivS4C3::where('prod_id','=',$p->id)->get();

			if($item->isEmpty() == true){
				if(substr($p->cod_produs,5,4) < 1000){
					$omie = new App\PivS4C3();
					$omie->id = $p->id;
					$omie->prod_id = $p->id;
					$omie->subcat4_copil_3_id = 1;
					$omie->save();
				}		
				elseif(substr($p->cod_produs,5,4) > 999 && substr($p->cod_produs,5,4) < 2000){
					$omie = new App\PivS4C3();
					$omie->id = $p->id;
					$omie->prod_id = $p->id;
					$omie->subcat4_copil_3_id = 2;
					$omie->save();
				}
				elseif(substr($p->cod_produs,5,4) > 1999 && substr($p->cod_produs,5,4) < 3000){
					$omie = new App\PivS4C3();
					$omie->id = $p->id;
					$omie->prod_id = $p->id;
					$omie->subcat4_copil_3_id = 3;
					$omie->save();
				}
				elseif(substr($p->cod_produs,5,4) > 2999 && substr($p->cod_produs,5,4) < 4000){
					$omie = new App\PivS4C3();
					$omie->id = $p->id;
					$omie->prod_id = $p->id;
					$omie->subcat4_copil_3_id = 4;
					$omie->save();
				}
			}
		}
	}			  
	die();
  });
/*Route::get('/pret',function(){
	$p = App\Produse::all();			
	foreach($p as $i){
			App\Produse::find($i->id)->update([
						'pret_original'=>$i->pret
					]);
	}
	dd('gata');
});
/*Route::get('/test',function(){
	dd(App\SubcatBaza1::with('produseSB1')->get());		
});
/*Route::get('/testpoze',function(){
	$a = App\Produse::find(1);
	dd($a->poze());		
});
/*Route::get('/move',function(){
	$p = App\Produse::search('107091')->get();	
	dd(count($p));
	$id = [];
	foreach($p as $m){
		array_push($id,$m->id);	
	}
	dd($id);
	foreach($id as $p){
		$piv = new App\PivS4C3();
		$piv->subcat4_copil_3_id = 6;
		$piv->prod_id = $p;
		$piv->save();
	}
	dd('done');
});
/*Route::get('/time',function(){
	Config::set('app.timezone', 'Europe/Bucharest');
	dd(Carbon\Carbon::now());
});*/
/*Route::get('/da',function(){
	
	$prod = App\Produse::all();
	foreach($prod as $p){
			$disp = App\Disponibil::find($p->id);
			if($disp->produs_indisponibil == 1){
				
					
					$disp->produs_indisponibil = 0;
					$disp->in_curand = 1;
					$disp->save();
			}else{

					continue;
			}		
	}

dd('gata');
});
/*Route::get('/test',function(){
	$d= App\Produse::paginate(10)->setPath('custom/url');
	echo $d;
});
 */
/*
Route::get('/sterg',function(){
	$p = App\Disponibil::all();
	foreach($p as $r){
		$r->in_stoc = 1;
		$r->stoc_redus = 0;
		$r->produs_indisponibil = 0;
		$r->in_curand = 0;
		$r->save();	
	}
	echo 'gata';
});
/*
//Route::post('/crearecontpf','WelcomeController@regPF');

/*Route::get('/or',function(){
	$p = App\Produse::orderBy('denumire_produs','asc')->get();
	//dd($p);
	foreach($p as $r){
		echo $r->noutati . '<br>';
	}
});*/
/*Route::get('/da',function(){
	$poze = scandir('/home/cellmall/public_html/products/');
	$a = App\Produse::all();	
		
		
		foreach($poze as $p){
			for($x=1;$x<count($a);$x++){
				if(substr($p, 0,9) == $a[$x]->cod_produs){
					App\Produse::where('cod_produs','=',substr($p, 0,9))->update([
							'poza'=>'/products/'.$p	
						]);
				}else{
					continue;
				}
			}
		}
		echo 'gata';
		//echo count(App\Produse::where('poza','!=','')->get());
	
});
/*Route::get('/r',function(){
	dd();
	$temp = App\Temp::all();
	for($i=0;$i<count($temp);$i++){
		App\Produse::find($temp[$i]->id)->update([
				'denumire_produs' => $temp[$i]->denumire_produs,
				'detalii' => $temp[$i]->detalii
			]);
		//echo $temp[$i]->id;
	}
	echo 'done.';
});*/

/*Route::get('/display',function(){
	DB::table('produse')
					->join('piv_s3c2','produse.id','=','piv_s3c2.prod_id')
					->where('piv_s3c2.subcat3_copil_2_id','=',1)->update([
							'detalii' => '<p>Display -/ Accesorii -/ Componenta -/ Compatibila<br />
 											-/ Display -/ LCD -/ Ecran -/<br />
 											-/ Cu Geam -/ Touchscreen -/ Touch</p>'
						]);
	echo 'done.';				
});*/




