<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcat3Copil2 extends Model{
	
	protected $table = 'subcat3_copil_2';
	protected $fillable = ['nume'];

	public function produseS3C2(){
		return $this->belongsToMany('App\Produse',
			'piv_s3c2','subcat3_copil_2_id','prod_id');
	}

	public function Subcat2Copil1(){
		return $this->belongsTo('App\Subcat2Copil1');
	}

	public function Subcat4Copil3(){
		return $this->hasMany('App\Subcat4Copil3','subcat3_copil_2_id');
	}

}