<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PivS2C1 extends Model{

	protected $table = 'piv_s2c1';
	protected $fillable = ['id','subcat2_copil_1_id','prod_id'];


}
