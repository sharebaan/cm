<?php namespace App;



use Nicolaslopezj\Searchable\SearchableTrait;

use Illuminate\Database\Eloquent\Model;



class Temp extends Model{

	

	use SearchableTrait;



	protected $searchable = [

		'columns' => [

			'denumire_produs' => 1,
	
			'cod_produs' => 10,

			'culoare'=> 5,

			'detalii'=>5

		]

	];

	protected $table = 'temp';

	protected $guarded = ['id'];

	protected $fillable = [

							'cod_produs',

							'denumire_produs',

							'pret',

							'poza',

							'discount',

							'stoc',

							'culoare',

							'tip_ambalaj',

							'dimensiune',

							'atentie',

							'detalii',

							'total_stoc',

							'intrari',

							'iesiri',

							'retur',

							'noutati',

							'promotii',

							'alerta_stoc',

							'vizitari',

							'cat_baza_id',

							'brand_id',

							'lichidari_stoc'

							];



	public function disp(){

		return $this->belongsTo('App\Disponibil');

	}						



	public function producator(){

		return $this->hasOne('App\Producator','id');

	}



	public function carucior(){

		return $this->belongsTo('App\Carucior','produs_id');

	}



	public function branduri(){

		return $this->hasOne('App\Branduri','id');

	}						



	public function catSB1(){

		return $this->belongsTo('App\PivSB1',

			'prod_id');

	}						

}