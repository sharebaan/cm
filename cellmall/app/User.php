<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'membrii';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
							'nume',
							'prenume',
							'password',
							'email',
							'telefon',
							'cnp',
							'companie',
							'CUI',
							'nr_reg',
							'banca',
							'cont',
							'adresa_factura',
							'localitate_factura',
							'judet_sector_factura',
							'adresa_livrare',
							'localitate_livrare',
							'judet_sector_livrare',
							'recomandare'
							];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

}
