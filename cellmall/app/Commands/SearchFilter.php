<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class SearchFilter extends Command {

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public $ord,$sta,$input,$tosearch;

	public function __construct($ord,$sta,$input,$tosearch)
	{
		$this->ord = $ord;
		$this->sta = $sta;
		$this->input = $input;	
		$this->tosearch = $tosearch;
	}
}
