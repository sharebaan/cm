<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class AdminFilter extends Command  {

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */

	public $id,$ord,$sta;

	public function __construct($id,$ord,$sta)
	{
		$this->id = $id;
		$this->ord = $ord;
		$this->sta = $sta;
	}


}
