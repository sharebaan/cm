<?php namespace App\Commands;

use App\Commands\Command;

use Illuminate\Contracts\Bus\SelfHandling;

class MembriiFilter extends Command {

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public $ord,$sta;

	public function __construct($ord,$sta)
	{
		$this->ord = $ord;
		$this->sta = $sta;	
	}
}
