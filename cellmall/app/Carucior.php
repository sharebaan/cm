<?php namespace App;

use Illuminate\Support\Facades\Auth as Auth;
use Illuminate\Database\Eloquent\Model;

class Carucior extends Model{

	protected $table = 'carucior';
	protected $fillable = ['produs_id','membru_id'];

	public function produse(){
		return $this->hasMany('App\Produse','id','produs_id');
	}



	public function membrii(){
		return $this->hasMany('App\Membrii','membru_id');
	}
}