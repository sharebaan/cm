<?php namespace App;



use Illuminate\Database\Eloquent\Model;



class Descriere extends Model{	

	protected $table = 'descriere';

	protected $fillable = [
			'produs_id',
			'culoare',
			'dimensiune',
			'tip_ambalaj',
			'atentie',
			'detalii'
	];

	//relatii prod ...

}