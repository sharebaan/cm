<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CatBaza extends Model{
	
	protected $table = 'cat_baza';
	protected $fillable = ['nume'];

	public function SubcatBaza(){
		return $this->hasMany('App\SubcatBaza1');
	}
}