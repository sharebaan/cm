<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PivS3C2 extends Model{

	protected $table = 'piv_s3c2';
	protected $fillable = ['id','subcat3_copil_2_id','prod_id'];

}
