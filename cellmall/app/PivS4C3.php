<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class pivS4C3 extends Model{

	protected $table = 'piv_s4c3';
	protected $fillable = ['id','subcat4_copil_3_id','prod_id'];

}
