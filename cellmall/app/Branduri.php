<?php namespace App;



use Illuminate\Database\Eloquent\Model;



class Branduri extends Model{

	

	protected $table = 'branduri';

	protected $fillable = ['nume','poza'];



	public function produse(){

		return $this->belongsTo('App\Produse','brand_id');

	}

	public static function cuNoutati(){
		return static::leftJoin(
			'produse',
			'branduri.prod_id','=','produse.id'
			)->where('noutati','=',1);
	}
}