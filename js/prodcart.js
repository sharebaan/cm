$( document ).ready(function(){

	var produse = [];

	$('.caradd').click(function(e){
		e.preventDefault();
		var input = $(this).parent().parent().next().next().children().next().children().val();

//		console.log($(this).parent().parent().next().next().children().next());	

		$('.cgol').remove();
		$('.itemc').remove();
		$('.itemcnoajax').remove();
		$('.fincomanda').remove();
		$('.brnoajax').remove();
		$('.cartload').css('width','0');
		$('.modal-body h5').hide();
		$('#addcart').modal({
			backdrop: 'static',
    		keyboard: false
		});
		$('#addcart').modal('show');
		$('.progress .progress-bar').progressbar({
			transition_delay: 1000
		});

		$.get('/addcart/'+$(this).attr('name')+'/'+input,function(data){
		}).done(function(data){
			setTimeout(function(){
				$('.modal-body').append('<h5 style="text-align:center"><b>Produsul a fost adaugat in cosul tau.</b></h5>');
			},900);
			setTimeout(function(){
				$('.modal').modal('hide');
			},1500);
			this.produse = data.produse['nume'];
			for(i=data.produse['nume'].length - 1;i>=0;i--){
				$('.cart').append('<a class="itemca" href="/produs/'+data.produse['id'][i]+'"><div class="itemc"><div class="cqty ic"><b>'+data.produse['qty'][i]+'x</b></div><div class="cnume"><b>'+data.produse['nume'][i].substr(0,60)+'</b></div><div class="cpret ic"><b>'+data.produse['total'][i]+' RON</b></div><div class="cdelete ic"><a href="#" name="'+data.produse['id'][i]+'" class="cdelr"><img src="/thumbs/stergemic.png" alt="Sterge Produs"></a></div></div></a>');
			}
			$('.cart').append('<div class="fincomanda"><a href="/fincomanda">Finalizeaza Comanda</a></div>');
			$('.cdelr').click(function(e){
				e.preventDefault();
				$(this).parent().parent().remove();
				$(this).parent().parent().next().remove();
				$.get('/removecart/'+$(this).attr('name'),function(){});
			});
		});
	});

	$('.cdel').click(function(e){
		e.preventDefault();
		$(this).parent().parent().next().remove();
		$(this).parent().parent().remove();
		//DE STERS IN TIMP REAL DIN COS MARE
		$.get('/removecart/'+$(this).attr('name'),function(){});
	});
});
