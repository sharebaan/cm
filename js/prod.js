$( document ).ready(function(){

	$('.qty').val(1);
	
	
	$('.plusmin').click(function(){
		var button = $(this);
		var old = button.parent().parent().find('input').val();
		
		if(button.val() == "+"){
			var newVal = parseFloat(old) + 1;
		}
		else if(button.val() == "-"){
			if(old > 0){
				newVal = parseFloat(old) - 1;
			}else{
				newVal = 0;
			}
		}
		button.parent().parent().find('input').val(newVal);
	});

	
});