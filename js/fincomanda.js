$(document).ready(function(){
	
	$('.plusminfin').click(function(){
		var button = $(this);
		var old = button.parent().parent().find('input').val();
		
				
		if(button.val() == "+"){
			var newVal = parseFloat(old) + 1;
		}
		else if(button.val() == "-"){
			if(old > 0){
				newVal = parseFloat(old) - 1;
			}else{
				newVal = 0;
			}
		}
		
		if(newVal == 1){
			button.prop('disabled',true);
		}else if(newVal > 1){
			button.parent().prev().prev().children().prop('disabled',false);
		}

		button.parent().parent().find('input').val(newVal);
		var pretbuc = $(this).parent().parent().parent().parent().next().children().first();
		var pretbucVal = parseFloat($(this).parent().parent().parent().parent().next().children().first().text());
		var subtotal = $(this).parent().parent().parent().parent().next().next().children().first();
		var subtotalVal = parseFloat($(this).parent().parent().parent().parent()
				.next().next().children().first().text());
		var total = $(this).parent().parent().parent().parent().parent()
				.parent().parent().next().children().next().children().children().first();
		var costTransVal = parseFloat($(this).parent().parent().parent().parent().parent()
				.parent().parent().next().children()
				.next().children().next().children().first().attr('value'));	
		var totalPlata = $(this).parent().parent().parent().parent().parent()
				.parent().parent().next().children().next().children().next().next().children().first();
		var totalPlataJos = $(this).parent().parent().parent().parent().parent()
				.parent().parent().next().next()
				.next().children().next().next().next().children()
				.next().children().children().next().children().next()
				.next().children().first();
		var totalJos = $(this).parent().parent().parent().parent().parent()
				.parent().parent().next().next()
				.next().children().next().next().next().children()
				.next().children().children().next().children().children().first();
		
		var totalVal = parseFloat($(this).parent().parent().parent().parent().parent()
				.parent().parent().next().children().next().children().children().first().text());
		var qty = $(this).parent().parent().prev();
		var totalp = 0;		
		var totalb = [];
		var cart = [];
		var minicart =[];
		
		if(button.attr('class') == $('.plusfin').attr('class')){
			subtotal.html($.number(newVal*pretbucVal,2));	
			
			$.get('/addcart/'+button.attr('name')+'/'+1,function(data){
			}).done(function(data){
				$.each(data.produse['total'],function(){
					totalb.push(parseFloat(this));
					totalp += parseFloat(this);			
					});	
				$.each($('.c6elem'),function(){
						cart.push($(this).children().first());		
					});	
				$.each($('.item-cos'),function(){
						minicart.push($(this).children().children().next().next().children().first());
					});
				$.each(totalb.reverse(),function(i){
						cart[i].html($.number(this,2));	
						minicart[i].html($.number(this,2));	
					});
				total.html($.number(totalp,2));
				totalJos.html($.number(totalp,2));
				totalPlata.html($.number(Math.abs(totalp)+Math.abs(costTransVal),2));
				totalPlataJos.html($.number(Math.abs(totalp)+Math.abs(costTransVal),2));	
				$('.totalpinput').val($.number(Math.abs(totalp)+Math.abs(costTransVal),2));			
				//button.parent().parent().parent().parent().next().next().next().children().first().html($.number(totalp,2));
				qty.val(newVal);
			});	
		}else{
			subtotal.html($.number(subtotalVal-pretbucVal,2));

			$.get('/decreasecart/'+button.attr('name')+'/'+1,function(data){
			}).done(function(data){	
				$.each(data.produse['total'],function(){
					totalb.push(parseFloat(this));
					totalp += parseFloat(this);			
				});	
				$.each($('.c6elem'),function(){
						cart.push($(this).children().first());		
					});	
				$.each($('.item-cos'),function(){
						minicart.push($(this).children().children().next().next().children().first());
					});
				$.each(totalb.reverse(),function(i){
						cart[i].html($.number(this,2));	
						minicart[i].html($.number(this,2));	
					});
				total.html($.number(totalp,2));
				totalJos.html($.number(totalp,2));
				totalPlata.html($.number(Math.abs(totalp)+Math.abs(costTransVal),2));	
				totalPlataJos.html($.number(Math.abs(totalp)+Math.abs(costTransVal),2));
				$('.totalpinput').val($.number(Math.abs(totalp)+Math.abs(costTransVal),2));
				/*$('.c6').children().first().html($.number(totalp,2));
				qty.val(newVal);*/
				
				qty.val(newVal);
			});
			
		}
		
	});				

	$('.cossterge').click(function(e){
		e.preventDefault();
		cartmare = [];

		var pjos =  $('.pjos').children().children().first();
		var psus = $('.psus').children().children().first(); 
		var tpjos = $('.pjos').children().next().next().children().first();
		var tpsus = $('.psus').children().next().next().children().first();
		var valsters = $(this).prev().children().next().next().children().first().text();
		
		psus.html($.number(parseFloat(psus.text())-parseFloat(valsters),2));
		pjos.html($.number(parseFloat(pjos.text())-parseFloat(valsters),2));
		tpsus.html($.number(parseFloat(tpsus.text())-parseFloat(valsters),2));
		tpjos.html($.number(parseFloat(tpjos.text())-parseFloat(valsters),2));
		$('.totalpinput').val($.number($('.totalpinput').val()-parseFloat(valsters),2));

		codmic = $(this).prev().children().next().next().html();

		$.each($('.c3cos'),function(){
			cartmare.push($(this));
			});
	
		for(i=0;i<cartmare.length;i++){
			if(cartmare[i].text() == codmic){
				cartmare[i].parent().remove();
			}else{continue;}	
		}	
		$(this).parent().remove();		
				
		$.get('/removecart/'+$(this).attr('name'),function(){});
	});


	$('.stergemare').click(function(e){
		e.preventDefault();			
		cartmic = [];

		var pjos =  $('.pjos').children().children().first();
		var psus = $('.psus').children().children().first(); 
		var tpjos = $('.pjos').children().next().next().children().first();
		var tpsus = $('.psus').children().next().next().children().first();
		var valsters = $(this).parent().prev().children().first().text();
		
		psus.html($.number(parseFloat(psus.text())-parseFloat(valsters),2));
		pjos.html($.number(parseFloat(pjos.text())-parseFloat(valsters),2));
		tpsus.html($.number(parseFloat(tpsus.text())-parseFloat(valsters),2));
		tpjos.html($.number(parseFloat(tpjos.text())-parseFloat(valsters),2));
		$('.totalpinput').val($.number($('.totalpinput').val()-parseFloat(valsters),2));
		
		var codmare = $(this).parent().prev().prev().prev().prev().prev().text();

		$.each($('.codcosmic'),function(i){
			cartmic.push($(this));	
			});
		
		for(i=0;i<cartmic.length;i++){
			if(cartmic[i].text() == codmare){
				cartmic[i].parent().parent().remove();
			}else{continue;}
		}
		$(this).parent().parent().remove();

		$.get('/removecart/'+$(this).attr('name'),function(){});
	});
});
