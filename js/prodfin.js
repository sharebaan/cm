$( document ).ready(function(){
  
	$('.plusminfin').click(function(){
		var button = $(this);
		var old = button.parent().parent().find('input').val();
		
		if(button.val() == "+"){
			var newVal = parseFloat(old) + 1;
		}
		else if(button.val() == "-"){
			if(old > 0){
				newVal = parseFloat(old) - 1;
			}else{
				newVal = 0;
			}
		}
		button.parent().parent().find('input').val(newVal);
		//console.log($.number($(this).parent().parent().parent().next().children('.pretbuc').text(),2));
		$(this).parent().parent().parent().next().next().children('.ptotal').number(newVal * $(this).parent().parent().parent().next().children('.pretbuc').text(),2);
		
		$(this).parent().parent().prev().val(newVal);

		if(button.attr('class') == $('.plusfin').attr('class')){
			var total = $(this).parent().parent().parent().next().children('.pretbuc').html();
			//console.log(parseFloat($('.tp').html()));
			tot = Number($('.tp').html()) + Number(total);
			//console.log(Number(tot));
			
			$('.totalp').val(tot);
			$('.tp').html(Math.round(tot*100)/100);
			//console.log(total);
			//console.log(tot);
			//console.log($.number(tot,2));
			//console.log($.number($('.tp').text(tot),2));
			
		}
		else{
			var total = $(this).parent().parent().parent().next().children('.pretbuc').html();
			tot = Number($('.tp').html()) - Number(total);
			$('.totalp').val(tot);
			$('.tp').text(tot);	
		}
	});
	
	$('.stergemare').click(function(e){
		e.preventDefault();
		tot = Number($('.tp').html()) - Number($(this).parent().prev().children('.ptotal').text());
		$('.totalp').val($.number(tot));
		$('.tp').text($.number(tot));
		$(this).parent().parent().remove();		
		//DE STERS IN TIMP REAL DIN COS MIC
		$.get('/removecart/'+$(this).attr('name'),function(){});
	});

//=============================== METODE PLATA / OPTIUNI ====================================== 
	$('#tottva').text(Number($('#totp').text()) + Number($('#ctrans').text()) );
	$('#tottvasus').text(Number($('#totp').text()) + Number($('#ctrans').text()) );
	
	$('#fancur').click(function(){
		$('#ctrans').text('17');
		$('#tottva').text(Number($('#totp').text()) + Number($('#ctrans').text()) );
		$('#ctranssus').text('17');
		$('#tottvasus').text(Number($('#totp').text()) + Number($('#ctrans').text()) );
		$('#totalpinput').val(Number($('#totp').text()) + Number($('#ctrans').text()) );
	});

	$('#cargus').click(function(){
		$('#ctrans').text('17');
		$('#tottva').text(Number($('#totp').text()) + Number($('#ctrans').text()));
		$('#ctranssus').text('17');
		$('#tottvasus').text(Number($('#totp').text()) + Number($('#ctrans').text()));
		$('#totalpinput').val(Number($('#totp').text()) + Number($('#ctrans').text()) );
	});

	$('#curbuc').click(function(){
		$('#ctrans').text('0');
		$('#tottva').text(Number($('#totp').text()) + Number($('#ctrans').text()) );
		$('#ctranssus').text('0');
		$('#tottvasus').text(Number($('#totp').text()) + Number($('#ctrans').text()) );
		$('#totalpinput').val(Number($('#totp').text()) + Number($('#ctrans').text()) );
	});

	$('#lasediu').click(function(){
		$('#ctrans').text('0');
		$('#tottva').text(Number($('#totp').text()) + Number($('#ctrans').text()) );
		$('#ctranssus').text('0');
		$('#tottvasus').text(Number($('#totp').text()) + Number($('#ctrans').text()) );
		$('#totalpinput').val(Number($('#totp').text()) + Number($('#ctrans').text()) );
	});

	
/*	$('.comanda').click(function(){
		$.get('',function(){
		});
		$('#cumpara').submit();
	});*/			

	
/*	$('.comandasus').click(function(e){
		e.preventDefault();
		$('.comandas').hide();
		$('.datepers').slideDown();
		$('.adrese').slideDown();
		$('.optiuniplata').slideDown();
		$('.metliv').slideDown();
		$('.tipfac').slideDown();
		$('.mentotcump').slideDown();		
	});*/	

	
	
	$('.comandasus').click(function(){
		$.get('',function(){
			
		});
		$('#cumpara').submit();
	});
	$('#numerar').click(function(){
		$('.metliv').fadeIn(500);
		$('#lasediu').prop('disabled',false);
		$('#lasediu').parent().css('cursor','pointer');
		$('#lasediu').css('cursor','pointer');
		$('#lasediu').prop('checked',false);

		$('#curbuc').prop('disabled',true);
		$('#curbuc').parent().css('cursor','not-allowed');
		$('#curbuc').css('cursor','not-allowed');
		$('#curbuc').prop('checked',false);

		$('#fancur').prop('disabled',true);
		$('#fancur').parent().css('cursor','not-allowed');
		$('#fancur').css('cursor','not-allowed');
		$('#fancur').prop('checked',false);

		$('#cargus').prop('disabled',true);
		$('#cargus').parent().css('cursor','not-allowed');
		$('#cargus').css('cursor','not-allowed');
		$('#cargus').prop('checked',false);

		$('.contbanca1').hide();
		$('.contbanca2').hide();
		$('.contbanca3').hide();
		$('.contbanca1').children().children().val('');
		$('.contbanca1').children().children().next().val('');
		$('.contbanca2').children().children().val('');
		$('.contbanca2').children().children().next().val('');
		$('.contbanca3').children().children().val('');
		$('.contbanca3').children().children().next().val('');
	});
	$('#numerarcn').click(function(){
		$('.metliv').fadeIn(500);
		$('#lasediu').prop('disabled',false);
		$('#lasediu').parent().css('cursor','pointer');
		$('#lasediu').css('cursor','pointer');
		$('#lasediu').prop('checked',false);

		$('#curbuc').prop('disabled',false);
		$('#curbuc').parent().css('cursor','pointer');
		$('#curbuc').css('cursor','pointer');
		$('#curbuc').prop('checked',false);		

		$('#fancur').prop('disabled',false);
		$('#fancur').parent().css('cursor','pointer');
		$('#fancur').css('cursor','pointer');
		$('#fancur').prop('checked',false);

		$('#cargus').prop('disabled',false);
		$('#cargus').parent().css('cursor','pointer');
		$('#cargus').css('cursor','pointer');
		$('#cargus').prop('checked',false);

		$('.contbanca1').show();
		$('.contbanca1').children().children().val('libra');
		$('.contbanca1').children().children().next().val('btransilvania');
		$('.contbanca2').children().children().val('');
		$('.contbanca2').children().children().next().val('');
		$('.contbanca3').children().children().val('');
		$('.contbanca3').children().children().next().val('');
		$('.contbanca2').hide();
		$('.contbanca3').hide();
	});
	$('#op').click(function(){
		$('.metliv').fadeIn(500);
		$('#lasediu').prop('disabled',false);
		$('#lasediu').parent().css('cursor','pointer');
		$('#lasediu').css('cursor','pointer');
		$('#lasediu').prop('checked',false);

		$('#curbuc').prop('disabled',false);
		$('#curbuc').parent().css('cursor','pointer');
		$('#curbuc').css('cursor','pointer');
		$('#curbuc').prop('checked',false);		

		$('#fancur').prop('disabled',false);
		$('#fancur').parent().css('cursor','pointer');
		$('#fancur').css('cursor','pointer');
		$('#fancur').prop('checked',false);

		$('#cargus').prop('disabled',false);
		$('#cargus').parent().css('cursor','pointer');
		$('#cargus').css('cursor','pointer');
		$('#cargus').prop('checked',false);

		$('.contbanca2').show();
		$('.contbanca1').children().children().val('');
		$('.contbanca1').children().children().next().val('');
		$('.contbanca2').children().children().val('libra');
		$('.contbanca2').children().children().next().val('btransilvania');
		$('.contbanca3').children().children().val('');
		$('.contbanca3').children().children().next().val('');
		$('.contbanca1').hide();
		$('.contbanca3').hide();

		
	});
	$('#virb').click(function(){
		$('.metliv').fadeIn(500);
		$('#lasediu').prop('disabled',false);
		$('#lasediu').parent().css('cursor','pointer');
		$('#lasediu').css('cursor','pointer');
		$('#lasediu').prop('checked',false);

		$('#curbuc').prop('disabled',false);
		$('#curbuc').parent().css('cursor','pointer');
		$('#curbuc').css('cursor','pointer');
		$('#curbuc').prop('checked',false);		

		$('#fancur').prop('disabled',false);
		$('#fancur').parent().css('cursor','pointer');
		$('#fancur').css('cursor','pointer');
		$('#fancur').prop('checked',false);

		$('#cargus').prop('disabled',false);
		$('#cargus').parent().css('cursor','pointer');
		$('#cargus').css('cursor','pointer');
		$('#cargus').prop('checked',false);

		$('.contbanca3').show();
		$('.contbanca1').children().children().val('');
		$('.contbanca1').children().children().next().val('');
		$('.contbanca2').children().children().val('');
		$('.contbanca2').children().children().next().val('');
		$('.contbanca3').children().children().val('libra');
		$('.contbanca3').children().children().next().val('btransilvania');
		$('.contbanca2').hide();
		$('.contbanca1').hide();
	});
	$('#ramburs').click(function(){
		$('.metliv').fadeIn(500);
		$('#lasediu').prop('disabled',true);
		$('#lasediu').parent().css('cursor','not-allowed');
		$('#lasediu').css('cursor','not-allowed');
		$('#lasediu').prop('checked',false);

		$('#curbuc').prop('disabled',false);
		$('#curbuc').parent().css('cursor','pointer');
		$('#curbuc').css('cursor','pointer');
		$('#curbuc').prop('checked',false);

		$('#fancur').prop('disabled',false);
		$('#fancur').parent().css('cursor','pointer');
		$('#fancur').css('cursor','pointer');
		$('#fancur').prop('checked',false);

		$('#cargus').prop('disabled',false);
		$('#cargus').parent().css('cursor','pointer');
		$('#cargus').css('cursor','pointer');
		$('#cargus').prop('checked',false);

		$('.contbanca3').hide();
		$('.contbanca2').hide();
		$('.contbanca1').hide();
		$('.contbanca1').children().children().val('');
		$('.contbanca1').children().children().next().val('');
		$('.contbanca2').children().children().val('');
		$('.contbanca2').children().children().next().val('');
		$('.contbanca3').children().children().val('');
		$('.contbanca3').children().children().next().val('');
	});

	$('#lasediu').click(function(){
		$('.tipfac').fadeIn(500);
		$('#pf').prop('checked',false);
		$('#pj').prop('checked',false);
	});	
	$('#curbuc').click(function(){
		$('.tipfac').fadeIn(500);
		$('#pf').prop('checked',false);
		$('#pj').prop('checked',false);
	});
	$('#fancur').click(function(){
		$('.tipfac').fadeIn(500);
		$('#pf').prop('checked',false);
		$('#pj').prop('checked',false);
	});
	$('#cargus').click(function(){
		$('.tipfac').fadeIn(500);
		$('#pf').prop('checked',false);
		$('#pj').prop('checked',false);
	});

	$('.resetop').click(function(e){
		e.preventDefault();
		$('.tipfac').fadeOut(500);
		$('.metliv').fadeOut(500);
		$('.contbanca1').hide();
		$('.contbanca2').hide();
		$('.contbanca3').hide();
		$('#numerar').prop('checked',false);
		$('#numerarcn').prop('checked',false);
		$('#ramburs').prop('checked',false);
		$('#virb').prop('checked',false);
		$('#op').prop('checked',false);
		$('#curbuc').prop('checked',false);
		$('#lasediu').prop('checked',false);
		$('#fancur').prop('checked',false);
		$('#cargus').prop('checked',false);
		$('#pf').prop('checked',false);
		$('#pj').prop('checked',false);
	});
/*	$('.qtyfin').click(function(){
		pretbuc = $(this).parent().parent().next().children('.pretbuc');
		total = $(this).parent().parent().next().next().children('.ptotal');
		old = $(this).val();

		$(this).change(function(){

			if($(this).val() > old){
				currentAdd = $(this).val() - old;
				currAddTotal = Number(currentAdd) * Number(pretbuc.text());

				$('.tp').text(currAddTotal + Number($('.tp').text()) + String('.00'));
				currAddTotal = 0;
				console.log(currAddTotal);
			}
			else if($(this).val() < old){
				currSub = old - $(this).val();

				newpret = Number($('.tp').text()) - Number(currSub * Number(pretbuc.text()));
				$('.tp').text(newpret);	
			}
			return false;
		});

	});*/

	
});
