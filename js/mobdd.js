$(document).ready(function(){
  //=========== NAVBAR DROPDOWN =========================
  $('.mob-nav').click(function(){

      if($(this).hasClass('ddactiv')){
          $('.mob-pills').slideUp(200);
          $(this).css('margin-top','10px').removeClass('ddactiv');
      }else{
          $('.mob-pills').slideDown(200);
          $(this).css('margin-top','0').addClass('ddactiv');
      }
  });
  //============== NAVBAR END ==============================
  //============== NAVBAR NEST LVL  ========================
   $('.next-level').click(function(e){
     e.preventDefault(); 
     if($(this).next().next().hasClass('nxt-lvl-active')){
       $(this).children().removeClass("glyphicon glyphicon-chevron-up")
       .addClass("glyphicon glyphicon-chevron-down");
       $(this).next().next().slideUp(200);
       $(this).next().next().removeClass('nxt-lvl-active');
     }else{
       $(this).children().removeClass("glyphicon glyphicon-chevron-down")
       .addClass("glyphicon glyphicon-chevron-up");
       $(this).next().next().slideDown(200);
       $(this).next().next().addClass('nxt-lvl-active');
     }

   });
  //============== NAVBAR NEST END ========================
});
