$(document).ready(function(){


						$('.editcont').click(function(e){
							e.preventDefault();
							
							$('.modtitlu').text('Atentie');
							$('.modintreb').show();
							$('.butvalid').show();
							$('.progress').hide();
							$('.econt').css('width','0%');

							if(!$(this).hasClass('active')){
								$('.atentie').fadeIn(200);
								$(this).addClass('active');
								$(this).text('Anuleaza Editarea Contului');
								$('.datec').fadeOut(100);
								$('.edcont').fadeIn(100);
								$('.ednume').val($('.dnume').text());
								$('.edprenume').val($('.dprenume').text());
								$('.edcnp').val($('.dcnp').text());
								$('.edemail').val($('.demail').text());
								$('.edtelefon').val($('.dtelefon').text());
								$('.ednumecomp').val($('.dnumecomp').text());
								$('.edcui').val($('.dcui').text());
								$('.ednrreg').val($('.dnrreg').text());
								$('.edbanca').val($('.dbanca').text());
								$('.edcontib').val($('.dcontib').text());
								$('.edass').val($('.dass').text());
								$('.edlss').val($('.dlss').text());
								$('.edjsss').val($('.djsss').text());
								$('.edaf').val($('.daf').text());
								$('.edlf').val($('.dlf').text());
								$('.edjsf').val($('.djsf').text());
								$('.edal').val($('.dal').text());
								$('.edll').val($('.dll').text());
								$('.edjsl').val($('.djsl').text());
								$('.edsubmit').fadeIn(100);
							}else{
								$('.atentie').fadeOut(200);
								$(this).removeClass('active');
								$(this).text('Editeaza Contul');
								$('.datec').fadeIn(100);
								$('.edcont').fadeOut(100);
								$('.edsubmit').fadeOut(100);
							}
						});
						
						$('.edsubmit').click(function(e){
							e.preventDefault();
							$('#editcontatentie').modal({
								backdrop: 'static',
    							keyboard: false
							});
							$('#editcontatentie').modal('show');
							$('.noedit').click(function(){
								$('.ecomplete').hide();
								$('.atentie').fadeOut(200);
								$('.editcont').removeClass('active');
								$('.editcont').text('Editeaza Contul');
								$('.datec').fadeIn(100);
								$('.edcont').fadeOut(100);
								$('.edsubmit').fadeOut(100);
								$('#editcontatentie').modal('hide');
							});
							$('.okedit').click(function(){
								$('.modtitlu').text('Te rog asteapta');
								$('.modintreb').hide();
								$('.butvalid').hide();
								$('.progress').fadeIn(200);
								$('.progress .progress-bar').progressbar({
									transition_delay: 1000
								});
								setTimeout(function(){
									$('.ecomplete').show();
								},1500);
								$.post('/editcont',$('#editc').serialize()).done(function(data){
									$.each(data,function(i,v){
										$('.dnume').text(v.nume);
										$('.dprenume').text(v.prenume);
										$('.dcnp').text(v.CNP);
										$('.demail').text(v.email);
										$('.dtelefon').text(v.telefon);
										$('.dnumecomp').text(v.nume_companie);
										$('.dcui').text(v.CUI);
										$('.dnrreg').text(v.nr_reg);
										$('.dbanca').text(v.banca);
										$('.dcontib').text(v.cont_IBAN);
										$('.dass').text(v.adresa_sediu);
										$('.dlss').text(v.localitate_sediu);
										$('.djsss').text(v.judet_sector_sediu);
										$('.daf').text(v.adresa_fac);
										$('.dlf').text(v.localitate_liv);
										$('.djsf').text(v.judet_sector_fac);
										$('.dal').text(v.adresa_liv);
										$('.dll').text(v.localitate_liv);
										$('.djsl').text(v.judet_sector_liv);
									});
								});	
									
									setTimeout(function(){
										$('.atentie').fadeOut(200);
										$('.editcont').removeClass('active');
										$('.editcont').text('Editeaza Contul');
										$('.datec').fadeIn(100);
										$('.edcont').fadeOut(100);
										$('.edsubmit').fadeOut(100);
										$('.ecomplete').hide();
										
									},2000);	
									setTimeout(function(){
										$('#editcontatentie').modal('hide');
									},2000);
								
							});
							
						});
					});
